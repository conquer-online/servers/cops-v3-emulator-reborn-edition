﻿using System;
using System.IO;
using System.Text;

namespace DMapReader
{
    public class GameMap
    {
        private uint m_Count;
        private DMap[] m_AllDMaps;

        public uint Count { get { return this.m_Count; } }
        public DMap[] AllDMaps { get { return this.m_AllDMaps; } }

        public GameMap(string File)
        {
            FileStream FStream = new FileStream(File, FileMode.Open);
            BinaryReader BReader = new BinaryReader(FStream);

            m_Count = BReader.ReadUInt32();

            m_AllDMaps = new DMap[m_Count];
            for (uint i = 0; i < m_Count; i++) //Get All DMap
            {
                uint MapId = BReader.ReadUInt32();
                string Path = Encoding.ASCII.GetString(BReader.ReadBytes(BReader.ReadInt32()));
                BReader.ReadUInt32();

                m_AllDMaps[i] = new DMap((ushort)MapId, Path);
            }
            BReader.Close();
            FStream.Dispose();
        }
    }
}

