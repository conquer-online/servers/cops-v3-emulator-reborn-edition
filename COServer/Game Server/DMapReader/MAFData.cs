﻿using System;
using System.IO;
using System.Text;

namespace DMapReader
{
    public class MAFData
    {
        private bool[,] m_AllTiles;
        private ushort m_Width;
        private ushort m_Height;

        public ushort Width { get { return this.m_Width; } }
        public ushort Height { get { return this.m_Height; } }

        public MAFData(string File)
        {
            FileStream FStream = new FileStream(File, FileMode.Open);
            BinaryReader BReader = new BinaryReader(FStream);

            BReader.BaseStream.Seek(3, SeekOrigin.Begin);
            //Encoding.ASCII.GetString(BReader.ReadBytes(3)); //Header -> Char[4] (MAF)

            m_Width = BReader.ReadUInt16(); //Width -> UInt16
            m_Height = BReader.ReadUInt16(); //Height -> UInt16

            m_AllTiles = new bool[m_Width, m_Height];
            for (ushort x = 0; x < m_Width; x++) //Get map's access
            {
                for (ushort y = 0; y < m_Height; y++)
                    m_AllTiles[x, y] = BReader.ReadBoolean();
            }

            BReader.Close();
            FStream.Dispose();
        }

        public bool CanAcess(ushort MapX, ushort MapY)
        {
            if (MapX < m_Width && MapY < m_Height)
                return m_AllTiles[MapX, MapY];

            return false;
        }
    }
}

