﻿using System;

namespace DMapReader
{
    public class DMap
    {
        private ushort m_Id;
        private string m_Path;

        public ushort Id { get { return m_Id; } }
        public string Path { get { return m_Path; } }

        public DMap(ushort Id, string Path)
        {
            m_Id = Id;
            m_Path = Path;
        }
    }
}

