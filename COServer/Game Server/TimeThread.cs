﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Timers;
using System.IO;
using INI_CORE_DLL;

namespace COServer
{
    public class TimeChecker
    {
        private static Timer CheckTimer = null;
        private static DateTime Date = DateTime.Now;
        private static string Day = null;
        private static bool IsRunning = false;

        private static bool GuildWar = false;
        private static bool GuildBeast = false;
        private static bool PKTournament = false;
        private static bool DisCity = false;

        public TimeChecker()
        {
            Ini Reader = new Ini(System.Windows.Forms.Application.StartupPath + "\\Event.ini");
            GuildWar = Reader.ReadBoolean("GuildWar", "Active");
            GuildBeast = Reader.ReadBoolean("GuildBeast", "Active");
            PKTournament = Reader.ReadBoolean("PKTournament", "Active");
            DisCity = Reader.ReadBoolean("DisCity", "Active");
            Reader = null;

            CheckTimer = new Timer();
            CheckTimer.Interval = 10000;
            CheckTimer.Elapsed += new ElapsedEventHandler(Check);
        }

        public void Start()
        {
            if (!IsRunning)
            {
                CheckTimer.Start();
                IsRunning = true;
            }
        }

        public void Stop()
        {
            if (IsRunning)
            {
                CheckTimer.Stop();
                IsRunning = true;
            }
        }

        private void Check(object Sender, ElapsedEventArgs Args)
        {
            try
            {
                Date = DateTime.Now;
                Day = Date.DayOfWeek.ToString();

                //Loto
                if (Date.Hour == 0 && Date.Minute < 1)
                {
                    foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                        (DE.Value as Character).LotoCount = 0;

                    try
                    {
                        string[] Characters = Directory.GetFiles(System.Windows.Forms.Application.StartupPath + "\\Characters", "*.chr");
                        for (int i = 0; i < Characters.Length; i++)
                        {
                            byte[] Buffer = File.ReadAllBytes(Characters[i]);
                            MemoryStream MStream = new MemoryStream(Buffer, true);
                            BinaryWriter Writer = new BinaryWriter(MStream);

                            Writer.BaseStream.Seek(-1, SeekOrigin.End);
                            Writer.Write((byte)0x00);

                            Writer.Close();
                            MStream.Dispose();
                            File.WriteAllBytes(Characters[i], Buffer);
                            Buffer = null;
                        }
                    }
                    catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
                }

                #region Guild War
                if (GuildWar)
                {
                    if ((Day == "Saturday" && Date.Hour >= 14) || (Day == "Sunday" && Date.Hour < 20))
                    {
                        if (!World.GWOn)
                        {
                            World.GWOn = true;
                            World.JailOn = true;
                            CoServer.JailSystem.Start();
                            World.SendMsgToAll("La guerre de guilde est commencée!", "SYSTEM", 2011);
                        }
                        else
                        {
                            World.GWOn = true;
                            World.JailOn = true;
                        }
                    }
                    else
                    {
                        World.GWOn = false;
                        World.JailOn = false;
                        CoServer.JailSystem.Stop();
                    }
                }
                #endregion
                #region Guild Beast
                if (GuildBeast)
                {
                    if (Day != "Saturday" && Day != "Sunday" && Date.Hour == 21)
                    {
                        if (!World.GBOn && World.PoleHolder != null)
                        {
                            World.GBOn = true;
                            World.SendMsgToAll("La bête de guilde est apparu dans le chateau! (80, 75)", "SYSTEM", 2011);

                            uint UID = (uint)Program.Rand.Next(800000, 1000000);
                            while (World.AllMobs.ContainsKey(UID))
                                UID = (uint)Program.Rand.Next(800000, 1000000);
                            //1038 80 75
                            Monster Mob = new Monster(3120, UID, 1038, 80, 75);
                            World.AllMobs.Add(UID, Mob);
                        }
                    }
                    else
                    {
                        foreach (KeyValuePair<uint, Monster> DE in World.AllMobs)
                        {
                            Monster TheMob = (Monster)DE.Value;
                            if (TheMob.Type == 6)
                            {
                                TheMob.Dissappear();
                                break;
                            }
                        }
                        World.GBOn = false;
                    }
                }
                #endregion
                #region PKTournament
                if (PKTournament)
                {
                    //Pkt On/Off NPC 1
                    if ((Day == "Saturday" || Date.Day == 1) && Date.Hour == 20 && Date.Minute > 29)
                    {
                        if (!World.PktOn || Date.Minute < 35)
                        {
                            World.PktOn = true;
                            if (!NPCs.AllNPCs.ContainsKey(9997))
                            {
                                SingleNPC npcc = new SingleNPC(9997, 310, 2, 0, 451, 291, 1002, 0);
                                World.SpawnNPC(npcc);
                                NPCs.AllNPCs.Add(9997, npcc);
                            }
                            World.SendMsgToAll("Le Tournois de PK est commencée! Allez voir GénéralBrave â la ville Dragon(451, 291).", "SYSTEM", 2011);
                        }
                    }
                    else
                    {
                        World.PktOn = false;
                        if (NPCs.AllNPCs.ContainsKey(9997))
                        {
                            SingleNPC npcc = new SingleNPC(9997, 310, 2, 0, 451, 291, 1002, 0);
                            NPCs.AllNPCs.Remove(9997);
                            World.RemoveEntity(npcc);
                        }
                    }

                    //Pkt On/Off NPC 2
                    if ((Day == "Saturday" || Date.Day == 1) && Date.Hour == 21 && Date.Minute > 29 && Date.Minute < 35)
                    {
                        if (!World.PktOn || Date.Minute < 35)
                        {
                            World.PktOn = true;
                            if (!NPCs.AllNPCs.ContainsKey(9996))
                            {
                                SingleNPC npcc = new SingleNPC(9996, 310, 2, 0, 240, 154, 1081, 0);
                                World.SpawnNPC(npcc);
                                NPCs.AllNPCs.Add(9996, npcc);
                            }
                            World.SendMsgToAll("La deuxième carte du tournois est ouverte. Allez voir GénéralBrave(240, 154)", "SYSTEM", 2011);
                        }
                    }
                    else
                    {
                        World.PktOn = false;
                        if (NPCs.AllNPCs.ContainsKey(9996))
                        {
                            SingleNPC npcc = new SingleNPC(9996, 310, 2, 0, 240, 154, 1081, 0);
                            NPCs.AllNPCs.Remove(9996);
                            World.RemoveEntity(npcc);
                        }
                    }

                    //Pkt On/Off NPC 3
                    if ((Day == "Saturday" || Date.Day == 1) && Date.Hour == 22 && Date.Minute > 29 && Date.Minute < 35)
                    {
                        if (!World.PktOn || Date.Minute < 35)
                        {
                            World.PktOn = true;
                            if (!NPCs.AllNPCs.ContainsKey(9995))
                            {
                                SingleNPC npcc = new SingleNPC(9995, 310, 2, 0, 162, 134, 1090, 0);
                                World.SpawnNPC(npcc);
                                NPCs.AllNPCs.Add(9995, npcc);
                            }
                            World.SendMsgToAll("La troisième carte du tournois est ouverte. Allez voir GénéralBrave(162, 134)", "SYSTEM", 2011);
                        }
                    }
                    else
                    {
                        World.PktOn = false;
                        if (NPCs.AllNPCs.ContainsKey(9995))
                        {
                            SingleNPC npcc = new SingleNPC(9995, 310, 2, 0, 162, 134, 1090, 0);
                            NPCs.AllNPCs.Remove(9995);
                            World.RemoveEntity(npcc);
                        }
                    }

                    //Pkt On/Off Remove Player
                    if ((Day == "Saturday" || Date.Day == 1) && Date.Hour == 21 && Date.Minute > 34)
                    {
                        foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                        {
                            Character Chaar = (Character)DE.Value;
                            if (Chaar.LocMap == 1081)
                                Chaar.Teleport(1002, 430, 380);
                        }
                    }
                    //Pkt On/Off Remove Player
                    if ((Day == "Saturday" || Date.Day == 1) && Date.Hour == 22 && Date.Minute > 34)
                    {
                        foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                        {
                            Character Chaar = (Character)DE.Value;
                            if (Chaar.LocMap == 1090)
                                Chaar.Teleport(1002, 430, 380);
                        }
                    }

                    //Pkt Price 1
                    if ((Day == "Saturday" || Date.Day == 1) && Date.Hour == 21 && Date.Minute < 30)
                    {
                        if (!NPCs.AllNPCs.ContainsKey(9994))
                        {
                            SingleNPC npcc = new SingleNPC(9994, 320, 2, 0, 224, 154, 1081, 0);
                            World.SpawnNPC(npcc);
                            NPCs.AllNPCs.Add(9994, npcc);
                        }
                    }
                    else
                    {
                        if (NPCs.AllNPCs.ContainsKey(9994))
                        {
                            SingleNPC npcc = new SingleNPC(9994, 320, 2, 0, 224, 154, 1081, 0);
                            NPCs.AllNPCs.Remove(9994);
                            World.RemoveEntity(npcc);
                        }
                    }

                    //Pkt Price 2
                    if ((Day == "Saturday" || Date.Day == 1) && Date.Hour == 22 && Date.Minute < 30)
                    {
                        if (!NPCs.AllNPCs.ContainsKey(9993))
                        {
                            SingleNPC npcc = new SingleNPC(9993, 320, 2, 0, 167, 133, 1090, 0);
                            World.SpawnNPC(npcc);
                            NPCs.AllNPCs.Add(9993, npcc);
                        }
                    }
                    else
                    {
                        if (NPCs.AllNPCs.ContainsKey(9993))
                        {
                            SingleNPC npcc = new SingleNPC(9993, 320, 2, 0, 167, 133, 1090, 0);
                            NPCs.AllNPCs.Remove(9993);
                            World.RemoveEntity(npcc);
                        }
                    }

                    //Pkt Price 3
                    if ((Day == "Saturday" || Date.Day == 1) && Date.Hour == 22 && Date.Minute > 34)
                    {
                        if (!NPCs.AllNPCs.ContainsKey(9992))
                        {
                            SingleNPC npcc = new SingleNPC(9992, 320, 2, 0, 50, 37, 1091, 0);
                            World.SpawnNPC(npcc);
                            NPCs.AllNPCs.Add(9992, npcc);
                        }
                    }
                    else
                    {
                        if (NPCs.AllNPCs.ContainsKey(9992))
                        {
                            SingleNPC npcc = new SingleNPC(9992, 320, 2, 0, 50, 37, 1091, 0);
                            NPCs.AllNPCs.Remove(9992);
                            World.RemoveEntity(npcc);
                        }
                    }
                }
                #endregion
                #region Dis City
                if (DisCity)
                {
                    //Dis On/Off
                    if (((Day == "Monday" || Day == "Wednesday" || Day == "Friday") && Date.Hour == 20 && Date.Minute < 6) || ((Day == "Tuesday" || Day == "Thursday") && Date.Hour == 21 && Date.Minute < 6))
                    {
                        World.DisOn = true;
                        World.SendMsgToAll("Dis City est commencée! Allez voir TaoïsteDeMer à Ville Tigre (532,480).", "SYSTEM", 2011);
                    }
                    else
                        World.DisOn = false;

                    //Dis Map 4
                    World.DisMap1.Clear();
                    World.DisMap2.Clear();
                    World.DisMap3.Clear();
                    World.DisMap4.Clear();
                    foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                    {
                        Character Charr = (Character)DE.Value;

                        if (Charr.LocMap == 2024)
                            World.DisMap4.Add(Charr.Name, Charr.UID);

                        if (Charr.LocMap == 2023)
                            World.DisMap3.Add(Charr.Name, Charr.UID);

                        if (Charr.LocMap == 2022)
                            World.DisMap2.Add(Charr.Name, Charr.UID);

                        if (Charr.LocMap == 2021)
                            World.DisMap1.Add(Charr.Name, Charr.UID);
                    }
                    if (World.DisMap1.Count == 0 && World.DisMap2.Count == 0)
                    {
                        foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                        {
                            Character Chaar = (Character)DE.Value;
                            if (Chaar.LocMap == 2023)
                            {
                                Chaar.Teleport(2024, 150, 283);
                            }
                        }
                    }
                    if (World.DisMap1.Count == 0 && World.DisMap2.Count == 0 && World.DisMap3.Count == 0 && World.DisMap4.Count == 1)
                    {
                        World.DisBoss.Clear();
                        foreach (KeyValuePair<uint, Monster> DE in World.AllMobs)
                        {
                            Monster Mobz = (Monster)DE.Value;
                            if (Mobz.LocMap == 2024 && Mobz.Alive)
                                World.DisBoss.Add(Mobz.UID);
                        }
                        if (World.DisBoss.Count == 0)
                        {
                            uint UID = (uint)Program.Rand.Next(800000, 1000000);
                            while (World.AllMobs.ContainsKey(UID))
                                UID = (uint)Program.Rand.Next(800000, 1000000);

                            Monster Mob = new Monster(5059, UID, 2024, 149, 139);
                            World.AllMobs.Add(UID, Mob);
                        }
                    }
                }
                #endregion
            }
            catch (Exception Exc) { Program.WriteLine(Exc); }
        }
    }
}
