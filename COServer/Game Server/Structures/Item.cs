﻿using System;

namespace COServer
{
    public struct Item
    {
        public uint Id;
        //public string Name;
        public byte Job;
        public byte Prof;
        public byte Level;
        public byte Sex;
        public ushort Strength;
        public ushort Agility;
        public uint Value;
        public ushort MinAtk;
        public ushort MaxAtk;
        public ushort Defence;
        public ushort MagicDef;
        public ushort MagicAtk;
        public byte Dodge;
        public byte Dexterity;
        public uint CPsValue;
    }
}
