﻿using System;
using System.Collections.Generic;
using INI_CORE_DLL;
using CoS_Math;

namespace COServer
{
    public class Drop
    {
        public byte HeadId = 99;
        public byte NecklaceId = 99;
        public byte ArmorId = 99;
        public byte RingId = 99;
        public byte WeaponId = 99;
        public byte ShieldId = 99;
        public byte ShoesId = 99;

        public struct Rate
        {
            public static double Refined = 0.154;
            public static double Unique = 0.096;
            public static double Elite = 0.0192;
            public static double Super = 0.0048;
            public static double Craft = 0.0337;
            public static double Meteor = 0.072;
            public static double DragonBall = 0.00256;
            public static double Gem = 0.072;
        }

        public Dictionary<uint, double> Specials;

        public Drop Create(string File)
        {
            Ini Reader = new Ini(File);
            HeadId = Reader.ReadByte("DropId", "HeadId");
            NecklaceId = Reader.ReadByte("DropId", "NecklaceId");
            ArmorId = Reader.ReadByte("DropId", "ArmorId");
            RingId = Reader.ReadByte("DropId", "RingId");
            WeaponId = Reader.ReadByte("DropId", "WeaponId");
            ShieldId = Reader.ReadByte("DropId", "ShieldId");
            ShoesId = Reader.ReadByte("DropId", "ShoesId");

            Rate.Refined = Reader.ReadDouble("DropRate", "Refined");
            Rate.Unique = Reader.ReadDouble("DropRate", "Unique");
            Rate.Elite = Reader.ReadDouble("DropRate", "Elite");
            Rate.Super = Reader.ReadDouble("DropRate", "Super");
            Rate.Craft = Reader.ReadDouble("DropRate", "Craft");
            Rate.Meteor = Reader.ReadDouble("DropRate", "Meteor");
            Rate.DragonBall = Reader.ReadDouble("DropRate", "DragonBall");
            Rate.Gem = Reader.ReadDouble("DropRate", "Gem");

            int Count = Reader.ReadInt32("Special", "Count");
            Specials = new Dictionary<uint, double>(Count);
            for (int i = 0; i < Count; i++)
            {
                Specials.Add(
                    Reader.ReadUInt32("Special", "Id" + i.ToString()),
                    Reader.ReadDouble("Special", "Rate" + i.ToString()));
            }

            return this;
        }

        public uint GenerateEquip()
        {
            ushort ItemType = 0;
            byte Level = 0;
            byte Color = (byte)Program.Rand.Next(3, 10);

            int Type = Program.Rand.Next(0, 7);
            switch (Type)
            {
                case 0: //Head
                    ItemType = 110;
                    Level = HeadId;
                    break;
                case 1: //Necklace
                    ItemType = 120;
                    Level = NecklaceId;
                    break;
                case 2: //Armor
                    ItemType = 130;
                    Level = ArmorId;
                    break;
                case 3: //Weapon
                    ItemType = 400;
                    Level = WeaponId;
                    break;
                case 4: //Shield
                    ItemType = 900;
                    Level = ShieldId;
                    break;
                case 5: //Ring
                    ItemType = 150;
                    Level = RingId;
                    break;
                case 6: //Boots
                    ItemType = 160;
                    Level = ShoesId;
                    break;
            }

            if (Level == 99)
                return 0;

            if (MyMath.Success(50))
                Level -= (byte)Program.Rand.Next(0, Level / 2);

            //Head
            if (ItemType == 110)
            {
                int WType = Program.Rand.Next(1, 7);
                if (WType == 1)
                    ItemType += 1;
                else if (WType == 2)
                    ItemType += 2;
                else if (WType == 3)
                    ItemType += 3;
                else if (WType == 4)
                    ItemType += 4;
                else if (WType == 5)
                    ItemType += 7;
                else if (WType == 6)
                    ItemType += 8;
            }

            //Necklace
            if (ItemType == 120)
                ItemType += (byte)Program.Rand.Next(0, 2);

            //Armor
            if (ItemType == 130)
            {
                int WType = Program.Rand.Next(1, 10);
                if (WType == 1)
                    ItemType += 1;
                else if (WType == 2)
                    ItemType += 2;
                else if (WType == 3)
                    ItemType += 3;
                else if (WType == 4)
                    ItemType += 4;
                else if (WType == 5)
                    ItemType += 5;
                else if (WType == 6)
                    ItemType += 6;
                else if (WType == 7)
                    ItemType += 8;
                else if (WType == 8)
                    ItemType += 9;
            }

            //Weapon
            if (ItemType == 400)
            {
                int WType = Program.Rand.Next(1, 18);
                if (WType == 1)
                    ItemType = 410;
                else if (WType == 2)
                    ItemType = 420;
                else if (WType == 3)
                    ItemType = 421;
                else if (WType == 4)
                    ItemType = 430;
                else if (WType == 5)
                    ItemType = 440;
                else if (WType == 6)
                    ItemType = 450;
                else if (WType == 7)
                    ItemType = 460;
                else if (WType == 8)
                    ItemType = 480;
                else if (WType == 9)
                    ItemType = 481;
                else if (WType == 10)
                    ItemType = 490;
                else if (WType == 11)
                    ItemType = 500;
                else if (WType == 12)
                    ItemType = 510;
                else if (WType == 13)
                    ItemType = 530;
                else if (WType == 14)
                    ItemType = 540;
                else if (WType == 15)
                    ItemType = 560;
                else if (WType == 16)
                    ItemType = 561;
                else if (WType == 17)
                    ItemType = 580;
            }

            //Ring
            if (ItemType == 150)
                ItemType += (byte)Program.Rand.Next(0, 3);

            byte Quality = (byte)MyMath.Generate(3, 6);
            if (MyMath.Success(Rate.Refined * DataBase.Rate.Refined))
                Quality = 6;
            if (MyMath.Success(Rate.Unique * DataBase.Rate.Unique))
                Quality = 7;
            if (MyMath.Success(Rate.Elite * DataBase.Rate.Elite))
                Quality = 8;
            if (MyMath.Success(Rate.Super * DataBase.Rate.Super))
                Quality = 9;

            uint ItemId = (uint)((ItemType * 1000) + (Level * 10) + Quality);
            if (((int)(ItemType / 10) == 11 && ItemType != 117) || (int)(ItemType / 10) == 13 || (int)(ItemType / 10) == 90)
                ItemId += (uint)(Color * 100);

            if (DataBase.Items.ContainsKey(ItemId))
                return ItemId;

            return 0;
        }
    }
}
