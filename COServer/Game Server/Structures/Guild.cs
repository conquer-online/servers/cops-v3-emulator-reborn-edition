﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text;
using System.IO;
using INI_CORE_DLL;

namespace COServer
{
    public class GuildMember
    {
        private uint m_UniqId = 0;
        private string m_Name = "Unknow";
        public int Donation = 0;
        public byte Level = 0;

        public uint UniqId { get { return m_UniqId; } }
        public string Name { get { return m_Name; } }

        public GuildMember Create(Character Member)
        {
            m_UniqId = Member.UID;
            m_Name = Member.Name;
            Donation = 0;
            Level = Member.Level;
            return this;
        }

        public GuildMember Create(Character Member, int tDonation)
        {
            m_UniqId = Member.UID;
            m_Name = Member.Name;
            Donation = tDonation;
            Level = Member.Level;
            return this;
        }

        public GuildMember Create(uint UniqId, int tDonation)
        {
            string[] Characters = Directory.GetFiles(Application.StartupPath + "\\Characters", "*.chr");
            for (int i = 0; i < Characters.Length; i++)
            {
                try
                {
                    FileStream FStream = new FileStream(Characters[i], FileMode.Open);
                    BinaryReader Reader = new BinaryReader(FStream);

                    if (Reader.ReadUInt32() == UniqId)
                    {
                        m_UniqId = UniqId;
                        Donation = tDonation;
                        Reader.BaseStream.Seek(20, SeekOrigin.Begin);
                        m_Name = Encoding.ASCII.GetString(Reader.ReadBytes(16)).TrimEnd((char)0x00);
                        Reader.BaseStream.Seek(17, SeekOrigin.Current);
                        Level = Reader.ReadByte();
                    }
                    Reader.Close();
                    FStream.Dispose();
                }
                catch { continue; }
            }
            return this;
        }
    }

    public class Guild
    {
        public ushort UniqId;
        public string Name;
        public uint Fund;
        public string Bulletin;
        public bool HoldingPole;

        public GuildMember Leader;
        public Dictionary<uint, GuildMember> Deputies;
        public Dictionary<uint, GuildMember> Members;

        public List<ushort> Allies;
        public List<ushort> Enemies;

        public int PoleDamage = 0;

        public Guild(ushort tUID, string tName, GuildMember tLeader,  Dictionary<uint, GuildMember> tDeputies, Dictionary<uint, GuildMember> tMembers, List<ushort> tAllies, List<ushort> tEnemies, uint tFund, string tBulletin, byte tHolding)
        {
            UniqId = tUID;
            Name = tName;
            Fund = tFund;
            Bulletin = tBulletin;
            HoldingPole = tHolding != 0 ? true : false;

            Leader = tLeader;
            Deputies = tDeputies;
            Members = tMembers;

            Allies = tAllies;
            Enemies = tEnemies;
        }

        public void Disband(bool LowFund)
        {
            foreach (Character Player in World.AllChars.Values)
            {
                Player.MyClient.SendPacket(CoServer.MyPackets.SendGuild(UniqId, 19));
            }

            foreach (uint UniqId in Deputies.Keys)
            {
                if (World.AllChars.ContainsKey(UniqId))
                {
                    Character Deputy = World.AllChars[UniqId];

                    Deputy.MyGuild = null;
                    Deputy.GuildDonation = 0;
                    Deputy.GuildID = 0;
                    Deputy.GuildPosition = 0;
                }
                else
                    DataBase.NoGuild(UniqId);
            }

            foreach (uint UniqId in Members.Keys)
            {
                if (World.AllChars.ContainsKey(UniqId))
                {
                    Character Member = World.AllChars[UniqId];

                    Member.MyGuild = null;
                    Member.GuildDonation = 0;
                    Member.GuildID = 0;
                    Member.GuildPosition = 0;
                }
                else
                    DataBase.NoGuild(UniqId);
            }

            if (World.AllChars.ContainsKey(Leader.UniqId))
            {
                Character Player = World.AllChars[Leader.UniqId];

                Player.MyGuild = null;
                Player.GuildDonation = 0;
                Player.GuildID = 0;
                Player.GuildPosition = 0;
            }
            else
                DataBase.NoGuild(Leader.UniqId);

            foreach (Guild Guild in Guilds.AllGuilds.Values)
            {
                if (Guild.Allies.Contains(UniqId))
                    Guild.Allies.Remove(UniqId);
                if (Guild.Enemies.Contains(UniqId))
                    Guild.Enemies.Remove(UniqId);
            }

            Deputies.Clear();
            Members.Clear();
            Allies.Clear();
            Enemies.Clear();

            Guilds.AllGuilds.Remove(UniqId);
            DataBase.DisbandGuild(UniqId);

            if (LowFund)
                World.SendMsgToAll(Name + " a dégroupé car elle a manqué de fond!", "SYSTEM", 2000);
            else
                World.SendMsgToAll(Name + " a dégroupé.", "SYSTEM", 2000);
        }

        public void Refresh(uint UniqId, byte Level)
        {
            if (Members.ContainsKey(UniqId))
                Members[UniqId].Level = Level;

            if (Deputies.ContainsKey(UniqId))
                Deputies[UniqId].Level = Level;

            if (Leader.UniqId == UniqId)
                Leader.Level = Level; ;
        }

        public void Refresh(uint UniqId, int Donation)
        {
            if (Members.ContainsKey(UniqId))
            {
                Members[UniqId].Donation = Donation;
                DataBase.SaveGuild(this);
            }

            if (Deputies.ContainsKey(UniqId))
            {
                Deputies[UniqId].Donation = Donation;
                DataBase.SaveGuild(this);
            }

            if (Leader.UniqId == UniqId)
            {
                Leader.Donation = Donation;
                DataBase.SaveGuild(this);
            }
        }

        public GuildMember GetMember(uint UniqId)
        {
            if (Members.ContainsKey(UniqId))
                return Members[UniqId];

            if (Deputies.ContainsKey(UniqId))
                return Deputies[UniqId];

            if (Leader.UniqId == UniqId)
                return Leader;

            return null;
        }

        public byte GetPosition(uint UniqId)
        {
            if (Members.ContainsKey(UniqId))
                return 50;

            if (Deputies.ContainsKey(UniqId))
                return 90;

            if (Leader.UniqId == UniqId)
                return 100;

            return 0;
        }

        public void AddPlayer(Character Joiner)
        {
            if (!Members.ContainsKey(Joiner.UID))
            {
                GuildMessage(Joiner.Name + " a rejoin votre guilde.");
                Members.Add(Joiner.UID, new GuildMember().Create(Joiner));
                DataBase.SaveGuild(this);
            }
        }

        public void RemovePlayer(Character Quitter)
        {
            if (Members.ContainsKey(Quitter.UID))
            {
                GuildMessage(Quitter.Name + " a quitté la guilde.");
                Members.Remove(Quitter.UID);
                DataBase.SaveGuild(this);
            }

            if (Deputies.ContainsKey(Quitter.UID))
            {
                GuildMessage(Quitter.Name + " a quitté la guilde.");
                Deputies.Remove(Quitter.UID);
                DataBase.SaveGuild(this);
            }
        }

        public void KickPlayer(GuildMember Player)
        {
            if (Members.ContainsKey(Player.UniqId))
                Members.Remove(Player.UniqId);

            if (Deputies.ContainsKey(Player.UniqId))
                Deputies.Remove(Player.UniqId);

            if (World.AllChars.ContainsKey(Player.UniqId))
            {
                Character Member = World.AllChars[UniqId];
                Member.MyClient.SendPacket(CoServer.MyPackets.SendGuild(UniqId, 19));

                Member.MyGuild = null;
                Member.GuildDonation = 0;
                Member.GuildID = 0;
                Member.GuildPosition = 0;

                Member.Screen.SendScreen(true);
                Member.Screen.Clear();
                Member.Screen.SendScreen(false);
            }
            else
                DataBase.NoGuild(Player.UniqId);

            GuildMessage(Player.Name + " ne respecte pas les réglements et il a été renvoyé de la guilde.");
            DataBase.SaveGuild(this);
        }

        public void GuildMessage(Character Sender, string Message)
        {
            if (Leader.UniqId != Sender.UID)
                if (World.AllChars.ContainsKey(Leader.UniqId))
                    World.AllChars[Leader.UniqId].MyClient.SendPacket(CoServer.MyPackets.SendMsg(World.AllChars[Leader.UniqId].MyClient.MessageId, Sender.Name, "All", Message, 2004));

            foreach (uint UniqId in Deputies.Keys)
            {
                if (UniqId != Sender.UID)
                    if (World.AllChars.ContainsKey(UniqId))
                        World.AllChars[UniqId].MyClient.SendPacket(CoServer.MyPackets.SendMsg(World.AllChars[UniqId].MyClient.MessageId, Sender.Name, "All", Message, 2004));
            }

            foreach (uint UniqId in Members.Keys)
            {
                if (UniqId != Sender.UID)
                    if (World.AllChars.ContainsKey(UniqId))
                        World.AllChars[UniqId].MyClient.SendPacket(CoServer.MyPackets.SendMsg(World.AllChars[UniqId].MyClient.MessageId, Sender.Name, "All", Message, 2004));
            }
        }

        public void GuildMessage(string Message)
        {
            if (World.AllChars.ContainsKey(Leader.UniqId))
                World.AllChars[Leader.UniqId].MyClient.SendPacket(CoServer.MyPackets.SendMsg(World.AllChars[Leader.UniqId].MyClient.MessageId, "SYSTEM", "All", Message, 2004));

            foreach (uint UniqId in Deputies.Keys)
            {
                if (World.AllChars.ContainsKey(UniqId))
                    World.AllChars[UniqId].MyClient.SendPacket(CoServer.MyPackets.SendMsg(World.AllChars[UniqId].MyClient.MessageId, "SYSTEM", "All", Message, 2004));
            }

            foreach (uint UniqId in Members.Keys)
            {
                if (World.AllChars.ContainsKey(UniqId))
                    World.AllChars[UniqId].MyClient.SendPacket(CoServer.MyPackets.SendMsg(World.AllChars[UniqId].MyClient.MessageId, "SYSTEM", "All", Message, 2004));
            }
        }
    }
}
