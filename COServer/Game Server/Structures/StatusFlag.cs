﻿using System;
using System.Collections.Generic;

namespace COServer
{
    public class StatusFlag
    {
        internal class Flags
        {
            public const ulong Flashing = 0x01;
            public const ulong Poisoned = 0x02;
            public const ulong XPList = 0x10;
            public const ulong Frozen = 0x20; //??
            public const ulong TeamLeader = 0x40;
            public const ulong Accuracy = 0x80;
            public const ulong Shield = 0x100;
            public const ulong Stig = 0x200;
            public const ulong Dead = 0x400;
            public const ulong RedName = 0x4000;
            public const ulong BlackName = 0x8000;
            public const ulong SuperMan = 0x40000;
            public const ulong Restore = 0x200000;
            public const ulong Invisible = 0x400000;
            public const ulong Cyclone = 0x800000;
            public const ulong Burned = 0x1000000;
            public const ulong Flying = 0x8000000;
            public const ulong ThirdReborn = 0x20000000;
            public const ulong CastingPray = 0x40000000;
            public const ulong Praying = 0x80000000;
        }

        private ConquerObject Owner;
        private ulong dwFlag;

        public StatusFlag(ConquerObject Entity)
        {
            Owner = Entity;
        }

        private void RaiseFlag(ulong Flag)
        {
            dwFlag |= Flag;
            SendFlag();
        }

        private void RemoveFlag(ulong Flag)
        {
            dwFlag &= ~Flag;
            SendFlag();
        }

        private bool ContainsFlag(ulong Flag)
        {
            return (dwFlag & Flag) == Flag;
        }

        public ulong GetFlags()
        {
            return dwFlag;
        }

        public void RemoveFlags()
        {
            dwFlag = 0;
            SendFlag();
        }

        private void SendFlag()
        {
            if (Owner.IsPlayer())
                (Owner as Character).MyClient.SendPacket(CoServer.MyPackets.Vital(Owner.UID, 26, GetFlags()));

            foreach (Character Player in World.AllChars.Values)
            {
                if (Player.Screen.Contains(Owner.UID))
                    Player.MyClient.SendPacket(CoServer.MyPackets.Vital(Owner.UID, 26, GetFlags()));
            }
        }

        public bool Flashing
        {
            get { return ContainsFlag(Flags.Flashing); }
            set { if (value) RaiseFlag(Flags.Flashing); else RemoveFlag(Flags.Flashing); }
        }

        public bool Poisoned
        {
            get { return ContainsFlag(Flags.Poisoned); }
            set { if (value) RaiseFlag(Flags.Poisoned); else RemoveFlag(Flags.Poisoned); }
        }

        public bool XPList
        {
            get { return ContainsFlag(Flags.XPList); }
            set { if (value) RaiseFlag(Flags.XPList); else RemoveFlag(Flags.XPList); }
        }

        public bool Frozen
        {
            get { return ContainsFlag(Flags.Frozen); }
            set { if (value) RaiseFlag(Flags.Frozen); else RemoveFlag(Flags.Frozen); }
        }

        public bool TeamLeader
        {
            get { return ContainsFlag(Flags.TeamLeader); }
            set { if (value) RaiseFlag(Flags.TeamLeader); else RemoveFlag(Flags.TeamLeader); }
        }

        public bool Accuracy
        {
            get { return ContainsFlag(Flags.Accuracy); }
            set { if (value) RaiseFlag(Flags.Accuracy); else RemoveFlag(Flags.Accuracy); }
        }

        public bool Shielded
        {
            get { return ContainsFlag(Flags.Shield); }
            set { if (value) RaiseFlag(Flags.Shield); else RemoveFlag(Flags.Shield); }
        }

        public bool Stigged
        {
            get { return ContainsFlag(Flags.Stig); }
            set { if (value) RaiseFlag(Flags.Stig); else RemoveFlag(Flags.Stig); }
        }

        public bool Dead
        {
            get { return ContainsFlag(Flags.Dead); }
            set { if (value) RaiseFlag(Flags.Dead); else RemoveFlag(Flags.Dead); }
        }

        public bool RedName
        {
            get { return ContainsFlag(Flags.RedName); }
            set { if (value) RaiseFlag(Flags.RedName); else RemoveFlag(Flags.RedName); }
        }

        public bool BlackName
        {
            get { return ContainsFlag(Flags.BlackName); }
            set { if (value) RaiseFlag(Flags.BlackName); else RemoveFlag(Flags.BlackName); }
        }

        public bool SuperMan
        {
            get { return ContainsFlag(Flags.SuperMan); }
            set { if (value) RaiseFlag(Flags.SuperMan); else RemoveFlag(Flags.SuperMan); }
        }

        public bool Restoring
        {
            get { return ContainsFlag(Flags.Restore); }
            set { if (value) RaiseFlag(Flags.Restore); else RemoveFlag(Flags.Restore); }
        }

        public bool Invisible
        {
            get { return ContainsFlag(Flags.Invisible); }
            set { if (value) RaiseFlag(Flags.Invisible); else RemoveFlag(Flags.Invisible); }
        }

        public bool Cyclone
        {
            get { return ContainsFlag(Flags.Cyclone); }
            set { if (value) RaiseFlag(Flags.Cyclone); else RemoveFlag(Flags.Cyclone); }
        }

        public bool Burned
        {
            get { return ContainsFlag(Flags.Burned); }
            set { if (value) RaiseFlag(Flags.Burned); else RemoveFlag(Flags.Burned); }
        }

        public bool Flying
        {
            get { return ContainsFlag(Flags.Flying); }
            set { if (value) RaiseFlag(Flags.Flying); else RemoveFlag(Flags.Flying); }
        }

        public bool CastingPray
        {
            get { return ContainsFlag(Flags.CastingPray); }
            set { if (value) RaiseFlag(Flags.CastingPray); else RemoveFlag(Flags.CastingPray); }
        }

        public bool Praying
        {
            get { return ContainsFlag(Flags.Praying); }
            set { if (value) RaiseFlag(Flags.Praying); else RemoveFlag(Flags.Praying); }
        }

        public bool RebornFlag
        {
            get { return ContainsFlag(Flags.ThirdReborn); }
            set { if (value) RaiseFlag(Flags.ThirdReborn); else RemoveFlag(Flags.ThirdReborn); }
        }
    }
}
