﻿using System;

namespace COServer
{
    public struct Enchant
    {
        public byte Min;
        public byte Max;
    }
}
