﻿using System;

namespace COServer
{
    public struct Map
    {
        public ushort MapId;
        public string Name;
        public ushort DMap;
        public uint Flags;
        public byte Weather;
        public ushort PortalX;
        public ushort PortalY;
        public ushort RbMap;
        public uint Color;
    }
}
