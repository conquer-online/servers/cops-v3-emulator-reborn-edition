using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Timers;
using System.Collections;
using System.Threading;
using System.IO;
using COServer.Network;
using INI_CORE_DLL;
using DMapReader;
using CoS_Math;

namespace COServer
{
    public class Client
    {
        public CoEncryption Crypto;
        public HybridWinsockClient ListenSock;
        public string Account = "";
        public byte Authentication;
        public Character MyChar;
        public bool Online = true;
        public uint Status;
        public int CurrentNPC = 0;
        public bool There = false;
        public IPEndPoint IPE;
        bool UppAgree = false;

        private uint m_MessageId = 0;
        public uint MessageId { get { m_MessageId++; return m_MessageId; } }

        public int ChColor = 0;
        public byte GemId = 0;
        public byte WHOpen = 0;
        public uint GemHp = 700000;

        public HybridWinsockClient MySocket
        {
            get { return ListenSock; }
            set { ListenSock = value; }
        }

        public Client()
        {
            Crypto = new CoEncryption();
        }

        ~Client()
        {
            Crypto = null;
            ListenSock = null;
            MyChar = null;
            IPE = null;

            Account = null;
        }

        public void GetIPE()
        {
            IPE = (IPEndPoint)ListenSock.Connection.RemoteEndPoint;
        }

        public unsafe void GetPacket(byte[] Data)
        {
            try
            {
                Crypto.Decrypt(Data);

                ushort PacketId = (ushort)((Data[3] << 8) | Data[2]);
                int PacketType;

                Msg MsgPacket = null;

                switch (PacketId)
                {
                    case 1001:
                        {
                            MsgPacket = new CreateChar(Data, this);
                            MsgPacket = null;
                            break;
                        }
                    case 1004:
                        {
                            MsgPacket = new Chat(Data, this);
                            MsgPacket = null;
                            break;
                        }
                    case 1005:
                        {
                            MsgPacket = new Walk(Data, this);
                            MsgPacket = null;
                            break;
                        }
                    #region Packet 1009
                    case 1009:
                        {
                            PacketType = Data[0x0c];
                            switch (PacketType)
                            {
                                case 21: //Get Items of Shop 
                                    {
                                        uint ShopId = (uint)((Data[0x07] << 24) + (Data[0x06] << 16) + (Data[0x05] << 8) + Data[0x04]);
                                        if (World.AllShops.ContainsKey(ShopId))
                                        {
                                            Shop Shop = World.AllShops[ShopId];
                                            if (World.AllChars.ContainsKey(Shop.OwnerId))
                                            {
                                                Character Owner = World.AllChars[Shop.OwnerId];
                                                if (MyMath.CanSeeBig(MyChar.LocX, MyChar.LocY, Owner.LocX, Owner.LocY))
                                                    Shop.SendItems(MyChar);
                                            }
                                        }

                                        break;
                                    }
                                case 22: //Add Item to Shop (Money)
                                    {
                                        uint ItemUID = (uint)((Data[0x07] << 24) + (Data[0x06] << 16) + (Data[0x05] << 8) + Data[0x04]);
                                        uint Value = (uint)((Data[0x0b] << 24) + (Data[0x0a] << 16) + (Data[0x09] << 8) + Data[0x08]);

                                        if (MyChar.MyShop.AddItem(ItemUID, Value, true))
                                            SendPacket(Data);
                                        break;
                                    }
                                case 23: //Remove Item to Shop
                                    {
                                        uint ItemUID = (uint)((Data[0x07] << 24) + (Data[0x06] << 16) + (Data[0x05] << 8) + Data[0x04]);
                                        MyChar.MyShop.DelItem(ItemUID);
                                        break;
                                    }
                                case 24: //Buy Shop Item
                                    {
                                        uint ItemUID = (uint)((Data[0x07] << 24) + (Data[0x06] << 16) + (Data[0x05] << 8) + Data[0x04]);
                                        uint ShopId = (uint)((Data[0x0b] << 24) + (Data[0x0a] << 16) + (Data[0x09] << 8) + Data[0x08]);
                                        if (World.AllShops.ContainsKey(ShopId))
                                        {
                                            Shop Shop = World.AllShops[ShopId];
                                            if (World.AllChars.ContainsKey(Shop.OwnerId))
                                            {
                                                Character Owner = World.AllChars[Shop.OwnerId];
                                                if (MyMath.CanSeeBig(MyChar.LocX, MyChar.LocY, Owner.LocX, Owner.LocY))
                                                    Owner.MyShop.BuyItem(MyChar, ItemUID);
                                            }
                                        }

                                        break;
                                    }
                                case 29: //Add Item to Shop (CPs)
                                    {
                                        uint ItemUID = (uint)((Data[0x07] << 24) + (Data[0x06] << 16) + (Data[0x05] << 8) + Data[0x04]);
                                        uint Value = (uint)((Data[0x0b] << 24) + (Data[0x0a] << 16) + (Data[0x09] << 8) + Data[0x08]);

                                        if (MyChar.MyShop.AddItem(ItemUID, Value, false))
                                            SendPacket(Data);
                                        break;
                                    }
                                case 12: //Drop Gold
                                    {
                                        if (Status != 7)
                                        {
                                            uint MoneyDrops = (uint)((Data[7] << 24) + (Data[6] << 16) + (Data[5] << 8) + Data[4]);

                                            if (MyChar.Silvers >= MoneyDrops)
                                            {
                                                MyChar.Silvers -= MoneyDrops;

                                                string Item = "";
                                                if (MoneyDrops <= 10) //Silver
                                                    Item = "1090000-0-0-0-0-0";
                                                else if (MoneyDrops <= 100) //Sycee
                                                    Item = "1090010-0-0-0-0-0";
                                                else if (MoneyDrops <= 1000) //Gold
                                                    Item = "1090020-0-0-0-0-0";
                                                else if (MoneyDrops <= 2000) //GoldBullion
                                                    Item = "1091000-0-0-0-0-0";
                                                else if (MoneyDrops <= 5000) //GoldBar
                                                    Item = "1091010-0-0-0-0-0";
                                                else if (MoneyDrops > 5000) //GoldBars
                                                    Item = "1091020-0-0-0-0-0";
                                                else //Error
                                                    Item = "1090000-0-0-0-0-0";

                                                DroppedItem item = DroppedItems.DropItem(0, Item, (uint)MyChar.LocX, (uint)MyChar.LocY, (uint)MyChar.LocMap, MoneyDrops);
                                                World.ItemDrops(item);
                                            }
                                        }
                                        break;
                                    }
                                case 9:
                                    {
                                        uint NPCID = (uint)((Data[7] << 24) + (Data[6] << 16) + (Data[5] << 8) + (Data[4]));

                                        byte WH = 0;

                                        if (NPCID == 8)
                                            WH = 0;
                                        else if (NPCID == 10012)
                                            WH = 1;
                                        else if (NPCID == 10028)
                                            WH = 2;
                                        else if (NPCID == 10011)
                                            WH = 3;
                                        else if (NPCID == 10027)
                                            WH = 4;
                                        else if (NPCID == 44)
                                            WH = 5;
                                        else if (NPCID == 4101)
                                            WH = 6;

                                        SendPacket(CoServer.MyPackets.OpenWarehouse(NPCID, MyChar.WHSilvers));
                                        SendPacket(CoServer.MyPackets.WhItems(MyChar, WH, (ushort)NPCID));
                                        break;
                                    }
                                case 10://Deposit
                                    {
                                        uint Amount = (uint)((Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + (Data[8]));

                                        if (MyChar.Silvers >= Amount)
                                        {
                                            MyChar.Silvers -= Amount;
                                            MyChar.WHSilvers += Amount;
                                            SendPacket(CoServer.MyPackets.Vital(MyChar.UID, 10, MyChar.WHSilvers));
                                        }
                                        break;
                                    }
                                case 11://Withdraw
                                    {
                                        uint Amount = (uint)((Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + (Data[8]));

                                        if (MyChar.WHSilvers >= Amount)
                                        {
                                            MyChar.Silvers += Amount;
                                            MyChar.WHSilvers -= Amount;
                                            SendPacket(CoServer.MyPackets.Vital(MyChar.UID, 10, MyChar.WHSilvers));
                                        }

                                        break;
                                    }
                                case 20:
                                    {
                                        MyChar.Ready = false;
                                        uint UppedItemUID = (uint)((Data[7] << 24) + (Data[6] << 16) + (Data[5] << 8) + Data[4]);
                                        uint UppingItemUID = (uint)((Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + Data[8]);

                                        string UppedItem = "";
                                        string UppingItem = "";
                                        int Counter = 0;

                                        foreach (uint uid in MyChar.Inventory_UIDs)
                                        {
                                            if (uid == UppedItemUID)
                                                UppedItem = MyChar.Inventory[Counter];

                                            Counter++;
                                        }

                                        Counter = 0;

                                        foreach (uint uid in MyChar.Inventory_UIDs)
                                        {
                                            if (uid == UppingItemUID)
                                                UppingItem = MyChar.Inventory[Counter];

                                            Counter++;
                                        }

                                        string[] Splitter = UppedItem.Split('-');
                                        uint UppedItem2 = uint.Parse(Splitter[0]);
                                        string[] Splitter2 = UppingItem.Split('-');
                                        uint UppingItem2 = uint.Parse(Splitter2[0]);

                                        if (UppingItem2 == 1088001)
                                            if (Other.Upgradable(UppedItem2))
                                                if (Other.ItemInfo(UppedItem2).Level < 130)
                                                    if (ItemHandler.ItemType2(UppedItem2) == 11 && ItemHandler.WeaponType(UppedItem2) != 117 && Other.ItemInfo(UppedItem2).Level < 120 || ItemHandler.WeaponType(UppedItem2) == 117 && Other.ItemInfo(UppedItem2).Level < 112 || ItemHandler.ItemType2(UppedItem2) == 13 && Other.ItemInfo(UppedItem2).Level < 120 || ItemHandler.ItemType2(UppedItem2) == 15 && Other.ItemInfo(UppedItem2).Level < 127 || ItemHandler.ItemType2(UppedItem2) == 16 && Other.ItemInfo(UppedItem2).Level < 129 || ItemHandler.ItemType(UppedItem2) == 4 || ItemHandler.ItemType(UppedItem2) == 5 || ItemHandler.ItemType2(UppedItem2) == 12 || ItemHandler.WeaponType(UppedItem2) == 132 && Other.ItemInfo(UppedItem2).Level <= 12)
                                                    {
                                                        bool Success = false;
                                                        double RemoveChance = 0;

                                                        if (MyChar.LuckTime == 0)
                                                            RemoveChance = Other.ItemInfo(UppedItem2).Level / 3;
                                                        else
                                                            RemoveChance = Other.ItemInfo(UppedItem2).Level / 2.5;

                                                        if (ItemHandler.ItemQuality(UppedItem2) == 3 || ItemHandler.ItemQuality(UppedItem2) == 4 || ItemHandler.ItemQuality(UppedItem2) == 5)
                                                            if (Other.ChanceSuccess(90 - RemoveChance))
                                                                Success = true;

                                                        if (ItemHandler.ItemQuality(UppedItem2) == 6)
                                                            if (Other.ChanceSuccess(75 - RemoveChance))
                                                                Success = true;

                                                        if (ItemHandler.ItemQuality(UppedItem2) == 7)
                                                            if (Other.ChanceSuccess(60 - RemoveChance))
                                                                Success = true;

                                                        if (ItemHandler.ItemQuality(UppedItem2) == 8)
                                                            if (Other.ChanceSuccess(50 - RemoveChance))
                                                                Success = true;

                                                        if (ItemHandler.ItemQuality(UppedItem2) == 9)
                                                            if (Other.ChanceSuccess(45 - RemoveChance))
                                                                Success = true;

                                                        if (Success)
                                                        {
                                                            MyChar.RemoveItem((ulong)UppedItemUID);

                                                            UppedItem2 = Other.EquipNextLevel(UppedItem2);

                                                            if (Splitter[4] == "0")
                                                                if (Other.ChanceSuccess(0.5) || MyChar.LuckTime > 0 && Other.ChanceSuccess(0.75))
                                                                {
                                                                    Splitter[4] = "255";
                                                                    SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous avez cr�� le premier trou de votre �quipement!", 2005));
                                                                }

                                                            if (Splitter[5] == "0")
                                                                if (Splitter[4] != "0")
                                                                    if (Other.ChanceSuccess(0.25) || MyChar.LuckTime > 0 && Other.ChanceSuccess(0.5))
                                                                    {
                                                                        Splitter[5] = "255";
                                                                        SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous avez cr�� le deuxi�me trou de votre �quipement!", 2005));
                                                                    }

                                                            MyChar.AddItem(UppedItem2.ToString() + "-" + Splitter[1] + "-" + Splitter[2] + "-" + Splitter[3] + "-" + Splitter[4] + "-" + Splitter[5], 0, UppedItemUID);
                                                            SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Votre �quipement a �t� am�lior�!", 2005));
                                                        }
                                                        else
                                                            SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "L'am�lioration de votre �quipement a rat�!", 2005));

                                                        MyChar.RemoveItem((ulong)UppingItemUID);
                                                    }
                                        MyChar.Ready = true;
                                        break;
                                    }
                                case 19:
                                    {
                                        MyChar.Ready = false;
                                        uint UppedItemUID = (uint)((Data[7] << 24) + (Data[6] << 16) + (Data[5] << 8) + Data[4]);
                                        uint UppingItemUID = (uint)((Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + Data[8]);

                                        string UppedItem = "";
                                        string UppingItem = "";
                                        int Counter = 0;

                                        foreach (uint uid in MyChar.Inventory_UIDs)
                                        {
                                            if (uid == UppedItemUID)
                                                UppedItem = MyChar.Inventory[Counter];

                                            Counter++;
                                        }

                                        Counter = 0;

                                        foreach (uint uid in MyChar.Inventory_UIDs)
                                        {
                                            if (uid == UppingItemUID)
                                                UppingItem = MyChar.Inventory[Counter];

                                            Counter++;
                                        }

                                        string[] Splitter = UppedItem.Split('-');
                                        uint UppedItem2 = uint.Parse(Splitter[0]);
                                        string[] Splitter2 = UppingItem.Split('-');
                                        uint UppingItem2 = uint.Parse(Splitter2[0]);

                                        if (UppingItem2 == 1088000)
                                            if (Other.Upgradable(UppedItem2))
                                                if (ItemHandler.ItemQuality(UppedItem2) != 9)
                                                {
                                                    bool Success = false;
                                                    double RemoveChance = 0;

                                                    if (MyChar.LuckTime == 0)
                                                        RemoveChance = Other.ItemInfo(UppedItem2).Level / 3;
                                                    else
                                                        RemoveChance = Other.ItemInfo(UppedItem2).Level / 2.5;

                                                    if (ItemHandler.ItemQuality(UppedItem2) == 3 || ItemHandler.ItemQuality(UppedItem2) == 4 || ItemHandler.ItemQuality(UppedItem2) == 5)
                                                        if (Other.ChanceSuccess(64 - RemoveChance))
                                                            Success = true;

                                                    if (ItemHandler.ItemQuality(UppedItem2) == 6)
                                                        if (Other.ChanceSuccess(54 - RemoveChance))
                                                            Success = true;

                                                    if (ItemHandler.ItemQuality(UppedItem2) == 7)
                                                        if (Other.ChanceSuccess(48 - RemoveChance))
                                                            Success = true;

                                                    if (ItemHandler.ItemQuality(UppedItem2) == 8)
                                                        if (Other.ChanceSuccess(44 - RemoveChance))
                                                            Success = true;

                                                    if (Success)
                                                    {
                                                        if (ItemHandler.ItemQuality(UppedItem2) == 3 || ItemHandler.ItemQuality(UppedItem2) == 4)
                                                            UppedItem2 += 6 - (uint)ItemHandler.ItemQuality(UppedItem2);
                                                        else
                                                            UppedItem2 += 1;

                                                        MyChar.RemoveItem((ulong)UppedItemUID);

                                                        if (Splitter[4] == "0")
                                                            if (Other.ChanceSuccess(1) || MyChar.LuckTime > 0 && Other.ChanceSuccess(1.25))
                                                            {
                                                                Splitter[4] = "255";
                                                                SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous avez cr�� le premier trou de votre �quipement!", 2005));
                                                            }

                                                        if (Splitter[5] == "0")
                                                            if (Splitter[4] != "0")
                                                                if (Other.ChanceSuccess(0.5) || MyChar.LuckTime > 0 && Other.ChanceSuccess(0.75))
                                                                {
                                                                    Splitter[5] = "255";
                                                                    SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous avez cr�� le deuxi�me trou de votre �quipement!", 2005));
                                                                }

                                                        MyChar.AddItem(UppedItem2.ToString() + "-" + Splitter[1] + "-" + Splitter[2] + "-" + Splitter[3] + "-" + Splitter[4] + "-" + Splitter[5], 0, UppedItemUID);
                                                        SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Votre �quipement a �t� am�lior�!", 2005));
                                                    }
                                                    else
                                                        SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "L'am�lioration de votre �quipement a rat�!", 2005));

                                                    MyChar.RemoveItem((uint)UppingItemUID);
                                                }
                                        MyChar.Ready = true;
                                        break;
                                    }
                                case 27:
                                    {
                                        SendPacket(Data);
                                        break;
                                    }
                                case 28:
                                    {
                                        int TheItemUID = (Data[0x07] << 24) + (Data[0x06] << 16) + (Data[0x05] << 8) + Data[0x04];
                                        int GemUID = (Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + Data[8];
                                        string MainItem = MyChar.FindItem((uint)TheItemUID);
                                        string Minor1Item = MyChar.FindItem((uint)GemUID);

                                        if (MainItem == null || Minor1Item == null)
                                            return;

                                        uint MainItemId = 0;
                                        uint Minor1ItemId = 0;

                                        string[] Splitter;
                                        string[] MainItemE;
                                        MainItemE = MainItem.Split('-');
                                        MainItemId = uint.Parse(MainItemE[0]);
                                        Splitter = Minor1Item.Split('-');
                                        Minor1ItemId = uint.Parse(Splitter[0]);

                                        if (Minor1ItemId <= 700073 && Minor1ItemId >= 700001)
                                        {
                                            byte GemID = (byte)(Minor1ItemId - 700000);

                                            if (DataBase.Enchant.ContainsKey(GemID))
                                            {
                                                Enchant E = DataBase.Enchant[GemID];

                                                uint EnchantHP = (uint)Program.Rand.Next(E.Min, (E.Max + 1));
                                                if (EnchantHP >= uint.Parse(MainItemE[3]))
                                                {
                                                    MyChar.RemoveItem((uint)GemUID);
                                                    MyChar.RemoveItem((uint)TheItemUID);
                                                    MyChar.AddItem(MainItemE[0] + "-" + MainItemE[1] + "-" + MainItemE[2] + "-" + EnchantHP + "-" + MainItemE[4] + "-" + MainItemE[5], 0, (uint)TheItemUID);
                                                    SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous �tes chanceux, votre �quipement a �t� enchant� de " + EnchantHP + ".", 2000));
                                                    MyChar.Save();
                                                    return;
                                                }
                                                else
                                                {
                                                    MyChar.RemoveItem((uint)GemUID);
                                                    SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous �tes malchanceux, vous avez eut " + EnchantHP + " et il ne peut pas changer l'ancien.", 2000));
                                                    MyChar.Save();
                                                    return;
                                                }
                                            }
                                        }
                                        return;
                                    }
                                case 3:
                                    {
                                        if (Status != 7)
                                        {
                                            MyChar.Ready = false;
                                            uint ItemUID = (uint)((Data[7] << 24) + (Data[6] << 16) + (Data[5] << 8) + Data[4]);

                                            int Count = 0;

                                            foreach (uint uid in MyChar.Inventory_UIDs)
                                            {
                                                if (uid == ItemUID)
                                                {
                                                    string Item = MyChar.Inventory[Count];

                                                    ushort[] Coord = DroppedItems.GetCoord(MyChar);
                                                    if (Coord == null)
                                                        return;

                                                    ushort TheX = Coord[1];
                                                    ushort TheY = Coord[2];

                                                    DroppedItem e = DroppedItems.DropItem(0, Item, (uint)TheX, TheY, (uint)MyChar.LocMap, 0);
                                                    World.ItemDrops(e);

                                                    MyChar.RemoveItem(ItemUID);
                                                }
                                                Count++;
                                            }
                                            MyChar.Ready = true;
                                        }
                                        break;
                                    }
                                case 2:
                                    {
                                        MyChar.Ready = false;
                                        uint ItemUID = (uint)((Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + Data[8]);
                                        int Count = 0;
                                        foreach (uint uid in MyChar.Inventory_UIDs)
                                        {
                                            if (uid == ItemUID)
                                            {
                                                string Item = MyChar.Inventory[Count];
                                                string[] Splitter = Item.Split('-');
                                                uint ItemId = uint.Parse(Splitter[0]);

                                                if (DataBase.Items.ContainsKey(ItemId))
                                                {
                                                    MyChar.RemoveItem(ItemUID);
                                                    MyChar.Silvers += (DataBase.Items[ItemId].Value / 3);
                                                }
                                                break;
                                            }
                                            Count++;
                                        }
                                        MyChar.Ready = true;
                                        break;
                                    }
                                case 1:
                                    {
                                        MyChar.Ready = false;
                                        uint ItemID = (uint)((Data[0x0b] << 24) + (Data[0x0a] << 16) + (Data[0x09] << 8) + Data[0x08]);
                                        uint CPsVal = Data[18];
                                        uint Value = (uint)((Data[0x07] << 24) + (Data[0x06] << 16) + (Data[0x05] << 8) + Data[0x04]);
                                        byte Amount = Data[20];
                                        int Money = Data[5];
                                        if (Amount == 0)
                                            Amount = 1;

                                        string TehShop = System.IO.File.ReadAllText(System.Windows.Forms.Application.StartupPath + @"\Shop.dat");

                                        try
                                        {
                                            if (Other.CharExist(ItemID.ToString(), TehShop))
                                            {
                                                if (DataBase.Items.ContainsKey(ItemID))
                                                {
                                                    Value = DataBase.Items[ItemID].Value;
                                                    CPsVal = DataBase.Items[ItemID].CPsValue;
                                                }

                                                for (int i = 0; i < Amount; i++)
                                                {
                                                    if (MyChar.ItemsInInventory > 39)
                                                        return;
                                                    if (MyChar.Silvers >= Value && CPsVal == 0 || MyChar.CPs >= CPsVal && CPsVal != 0)
                                                    {
                                                        if (CPsVal == 0)
                                                            MyChar.Silvers -= Value;
                                                        if (CPsVal > 0)
                                                            MyChar.CPs -= CPsVal;

                                                        if (MyChar.Silvers < 0)
                                                            MyChar.Silvers = 0;
                                                        if (MyChar.CPs < 0)
                                                            MyChar.CPs = 0;

                                                        byte WithPlus = 0;
                                                        if (ItemID == 730001)
                                                            WithPlus = 1;
                                                        if (ItemID == 730002)
                                                            WithPlus = 2;
                                                        if (ItemID == 730003)
                                                            WithPlus = 3;
                                                        if (ItemID == 730004)
                                                            WithPlus = 4;
                                                        if (ItemID == 730005)
                                                            WithPlus = 5;
                                                        if (ItemID == 730006)
                                                            WithPlus = 6;
                                                        if (ItemID == 730007)
                                                            WithPlus = 7;
                                                        if (ItemID == 730008)
                                                            WithPlus = 8;

                                                        MyChar.AddItem(ItemID.ToString() + "-" + WithPlus + "-0-0-0-0", 0, (uint)Program.Rand.Next(10000000));
                                                    }
                                                    else
                                                    {
                                                        SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous n'avez pas " + Value + " argents ou " + CPsVal + " CPs.", 2005));
                                                        break;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Program.WriteLine("There is no such item in Shop.dat(" + ItemID + ")");
                                            }
                                        }
                                        catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
                                        MyChar.Ready = true;
                                        break;
                                    }
                                case 4:
                                    {
                                        MyChar.Ready = false;
                                        uint ItemUID = (uint)((Data[0x07] << 24) + (Data[0x06] << 16) + (Data[0x05] << 8) + Data[0x04]);
                                        byte RequestPos = Data[0x08];

                                        string TheItem = "";
                                        uint TheUID = 0;
                                        byte Count = 0;
                                        uint ItemID = 0;

                                        foreach (uint uid in MyChar.Inventory_UIDs)
                                        {
                                            if (ItemUID == uid)
                                            {
                                                TheUID = uid;
                                                TheItem = MyChar.Inventory[Count];
                                                string[] Splitter = TheItem.Split('-');
                                                ItemID = uint.Parse(Splitter[0]);
                                            }
                                            Count++;
                                        }

                                        if (MyChar.Flying)
                                            if ((RequestPos == 4 || RequestPos == 5) && ItemHandler.WeaponType(ItemID) != 500)
                                                return;

                                        if (ItemID == 1050000 || ItemID == 1050001 || ItemID == 1050002)
                                            RequestPos = 5;

                                        if (RequestPos != 0)
                                        {
                                            if (Other.CanEquip(ItemID, MyChar))
                                                if (ItemHandler.ItemType(ItemID) != 5 && RequestPos != 0)
                                                {
                                                    MyChar.RemoveItem(TheUID);
                                                    if (MyChar.Equips[RequestPos] == null || MyChar.Equips[RequestPos] == "0")
                                                        MyChar.AddItem(TheItem, RequestPos, TheUID);
                                                    else
                                                    {
                                                        if (RequestPos == 4 && MyChar.Equips[5] != null)
                                                        {
                                                            string Arrow = MyChar.Equips[5];
                                                            string[] Splitter = Arrow.Split('-');
                                                            if (Splitter[0] == "1050000" || Splitter[0] == "1050001" || Splitter[0] == "1050002")
                                                                MyChar.UnEquip(5);
                                                        }
                                                        MyChar.UnEquip(RequestPos);
                                                        MyChar.AddItem(TheItem, RequestPos, TheUID);
                                                    }
                                                    World.UpdateSpawn(MyChar);
                                                }
                                                else if (RequestPos != 0)
                                                    if (MyChar.ItemsInInventory < 40)
                                                    {
                                                        if (MyChar.Equips[4] == null && MyChar.Equips[5] == null)
                                                        {
                                                            MyChar.AddItem(TheItem, RequestPos, TheUID);
                                                            World.UpdateSpawn(MyChar);
                                                            MyChar.RemoveItem(TheUID);
                                                        }
                                                        else
                                                        {
                                                            MyChar.RemoveItem(TheUID);
                                                            if (MyChar.Equips[4] != null)
                                                                MyChar.UnEquip(4);
                                                            if (MyChar.Equips[5] != null)
                                                                MyChar.UnEquip(5);
                                                            MyChar.AddItem(TheItem, 4, TheUID);

                                                            World.UpdateSpawn(MyChar);
                                                        }
                                                    }

                                        }
                                        else
                                            MyChar.UseItem(TheUID, TheItem);
                                        MyChar.Ready = true;
                                        break;
                                    }
                                case 6:
                                    {
                                        MyChar.Ready = false;
                                        if (MyChar.ItemsInInventory > 39)
                                            return;

                                        ulong ItemUID = (ulong)((Data[7] << 24) + (Data[6] << 16) + (Data[5] << 8) + Data[4]);

                                        int count = 0;
                                        foreach (ulong uid in MyChar.Equips_UIDs)
                                        {
                                            if (uid == ItemUID)
                                            {
                                                MyChar.UnEquip((byte)count);
                                            }
                                            count++;
                                        }

                                        World.UpdateSpawn(MyChar);

                                        MyChar.Ready = true;
                                        break;
                                    }
                                default:
                                    Program.WriteLine("Client[1009]::Error -> Type[" + Data[0x0c] + "] not implemented!");
                                    break;
                            }
                            break;
                        }
                    #endregion
                    case 1010:
                        {
                            MsgPacket = new Action(Data, this);
                            MsgPacket = null;
                            break;
                        }
                    #region Packet 1015
                    case 1015://Send guild members
                        {
                            if (Data[0x08] == 11)
                            {
                                if (MyChar.MyGuild == null)
                                    return;

                                string OnlineMembers = "";
                                string OfflineMembers = "";
                                byte Count = (byte)(1 + MyChar.MyGuild.Deputies.Count + MyChar.MyGuild.Members.Count);

                                if (World.AllChars.ContainsKey(MyChar.MyGuild.Leader.UniqId))
                                    OnlineMembers += Convert.ToChar((MyChar.MyGuild.Leader.Name + (char)32 + MyChar.MyGuild.Leader.Level.ToString() + (char)32 + "1").Length) + (MyChar.MyGuild.Leader.Name + (char)32 + MyChar.MyGuild.Leader.Level.ToString() + (char)32 + "1");
                                else
                                    OfflineMembers += Convert.ToChar((MyChar.MyGuild.Leader.Name + (char)32 + MyChar.MyGuild.Leader.Level.ToString() + (char)32 + "0").Length) + (MyChar.MyGuild.Leader.Name + (char)32 + MyChar.MyGuild.Leader.Level.ToString() + (char)32 + "0");


                                foreach (GuildMember Deputy in MyChar.MyGuild.Deputies.Values)
                                {
                                    if (World.AllChars.ContainsKey(Deputy.UniqId))
                                        OnlineMembers += Convert.ToChar((Deputy.Name + (char)32 + Deputy.Level.ToString() + (char)32 + "1").Length) + (Deputy.Name + (char)32 + Deputy.Level.ToString() + (char)32 + "1");
                                    else
                                        OfflineMembers += Convert.ToChar((Deputy.Name + (char)32 + Deputy.Level.ToString() + (char)32 + "0").Length) + (Deputy.Name + (char)32 + Deputy.Level.ToString() + (char)32 + "0");
                                }

                                foreach (GuildMember Member in MyChar.MyGuild.Members.Values)
                                {
                                    if (World.AllChars.ContainsKey(Member.UniqId))
                                        OnlineMembers += Convert.ToChar((Member.Name + (char)32 + Member.Level.ToString() + (char)32 + "1").Length) + (Member.Name + (char)32 + Member.Level.ToString() + (char)32 + "1");
                                    else
                                        OfflineMembers += Convert.ToChar((Member.Name + (char)32 + Member.Level.ToString() + (char)32 + "0").Length) + (Member.Name + (char)32 + Member.Level.ToString() + (char)32 + "0");
                                }

                                SendPacket(Data);
                                SendPacket(CoServer.MyPackets.StringGuild(11, 11, OnlineMembers + OfflineMembers, Count));
                            }
                            else
                                Program.WriteLine("Client[1015]::Error -> Type[" + Data[0x08] + "] not implemented!");

                            break;
                        }
                    #endregion
                    case 1019:
                        {
                            MsgFriend Msg = new MsgFriend();
                            Msg.Process(Data, this);
                            Msg = null;
                            break;
                        }
                    #region Packet 1022
                    case 1022:
                        {
                            MyChar.Ready = false;
                            int AttackType = (Data[23] << 24) + (Data[22] << 16) + (Data[21] << 8) + (Data[20]);
                            if (AttackType == 21)
                            {
                                TimeSpan TS = DateTime.Now - MyChar.LastTargetting;
                                if (MyChar.AutoClick_Time == TS)
                                    MyChar.AutoClick_Count++;
                                MyChar.AutoClick_Time = TS;
                            }

                            if (DateTime.Now < MyChar.LastTargetting.AddMilliseconds(255))
                                return;

                            if (DateTime.Now > MyChar.LastTargetting.AddSeconds(5))
                                MyChar.AutoClick_Count = 0;

                            MyChar.LastTargetting = DateTime.Now;

                            if (AttackType == 8)//Marriage Proposal
                            {
                                fixed (byte* Ptr = Data)
                                {
                                    uint TargetUID = *(uint*)(Ptr + 12);
                                    Character Target = (Character)World.AllChars[TargetUID];
                                    *(uint*)(Ptr + 8) = MyChar.UID;
                                    {
                                        if (MyChar.Model == 2001 || MyChar.Model == 2002)
                                        {
                                            if (Target.Model == 1003 || Target.Model == 1004)
                                            {
                                                if (Target.Spouse == "Non")
                                                    Target.MyClient.SendPacket(Data);
                                                else
                                                {
                                                    SendPacket(CoServer.MyPackets.NPCSay(Target.Name + " est d�j� mari�."));
                                                    SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                                }
                                            }
                                            else
                                            {
                                                SendPacket(CoServer.MyPackets.NPCSay("Pas de mariage gay ici!"));
                                                SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                                SendPacket(CoServer.MyPackets.NPCFinish());
                                            }
                                        }
                                        if (MyChar.Model == 1003 || MyChar.Model == 1004)
                                        {
                                            if (Target.Model == 2001 || Target.Model == 2002)
                                            {
                                                if (Target.Spouse == "Non")
                                                    Target.MyClient.SendPacket(Data);
                                                else
                                                {
                                                    SendPacket(CoServer.MyPackets.NPCSay(Target.Name + " est d�j� mari�."));
                                                    SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                                }
                                            }
                                            else
                                            {
                                                SendPacket(CoServer.MyPackets.NPCSay("Pas de mariage gay ici!"));
                                                SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                                SendPacket(CoServer.MyPackets.NPCFinish());
                                            }
                                        }
                                    }
                                }
                            }
                            if (AttackType == 9)//Marriage Accept
                            {
                                fixed (byte* Ptr = Data)
                                {
                                    uint UID = *(uint*)(Ptr + 12);
                                    Character Spouse = (Character)World.AllChars[UID];
                                    {
                                        if (Spouse.Model == 2001 || Spouse.Model == 2002)
                                        {
                                            if (MyChar.Model == 1003 || MyChar.Model == 1004)
                                            {
                                                Spouse.Spouse = MyChar.Name;
                                                Spouse.Save();
                                                MyChar.Spouse = Spouse.Name;
                                                MyChar.Save();

                                                Spouse.Screen.SendPacket(CoServer.MyPackets.String(Spouse.UID, 6, MyChar.Name), true);
                                                MyChar.Screen.SendPacket(CoServer.MyPackets.String(MyChar.UID, 6, Spouse.Name), true);
                                                World.SendMsgToAll(Spouse.Name + " et " + MyChar.Name + " sont tomb�s dans le monde de cupidon!", "SYSTEM", 2011);
                                            }
                                            else
                                            {
                                                SendPacket(CoServer.MyPackets.NPCSay("Pas de mariage gay ici!"));
                                                SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                                SendPacket(CoServer.MyPackets.NPCFinish());
                                            }
                                        }
                                        if (Spouse.Model == 1003 || Spouse.Model == 1004)
                                        {
                                            if (MyChar.Model == 2001 || MyChar.Model == 2002)
                                            {
                                                Spouse.Spouse = MyChar.Name;
                                                Spouse.Save();
                                                MyChar.Spouse = Spouse.Name;
                                                MyChar.Save();

                                                Spouse.Screen.SendPacket(CoServer.MyPackets.String(Spouse.UID, 6, MyChar.Name), true);
                                                MyChar.Screen.SendPacket(CoServer.MyPackets.String(MyChar.UID, 6, Spouse.Name), true);
                                                World.SendMsgToAll(Spouse.Name + " et " + MyChar.Name + " sont tomb�s dans le monde de cupidon!", "SYSTEM", 2011);
                                            }
                                            else
                                            {
                                                SendPacket(CoServer.MyPackets.NPCSay("Pas de mariage gay ici!"));
                                                SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                                SendPacket(CoServer.MyPackets.NPCFinish());
                                            }
                                        }
                                    }
                                }
                            }
                            if (AttackType == 21)
                            {
                                MyChar.PTarget = null;
                                MyChar.TGTarget = null;
                                MyChar.MobTarget = null;
                                MyChar.Attacking = false;
                                ushort SkillId = Convert.ToUInt16(((long)Data[24] & 0xFF) | (((long)Data[25] & 0xFF) << 8));
                                SkillId ^= (ushort)0x915d;
                                SkillId ^= (ushort)MyChar.UID;
                                SkillId = (ushort)(SkillId << 0x3 | SkillId >> 0xd);
                                SkillId -= 0xeb42;

                                long x = (Data[16] & 0xFF) | ((Data[17] & 0xFF) << 8);
                                long y = (Data[18] & 0xFF) | ((Data[19] & 0xFF) << 8);

                                x = x ^ (uint)(MyChar.UID & 0xffff) ^ 0x2ed6;
                                x = ((x << 1) | ((x & 0x8000) >> 15)) & 0xffff;
                                x |= 0xffff0000;
                                x -= 0xffff22ee;

                                y = y ^ (uint)(MyChar.UID & 0xffff) ^ 0xb99b;
                                y = ((y << 5) | ((y & 0xF800) >> 11)) & 0xffff;
                                y |= 0xffff0000;
                                y -= 0xffff8922;

                                uint Target = ((uint)Data[12] & 0xFF) | (((uint)Data[13] & 0xFF) << 8) | (((uint)Data[14] & 0xFF) << 16) | (((uint)Data[15] & 0xFF) << 24);
                                Target = ((((Target & 0xffffe000) >> 13) | ((Target & 0x1fff) << 19)) ^ 0x5F2D2463 ^ MyChar.UID) - 0x746F4AE6;

                                if (SkillId != 1110 && SkillId != 1015 && SkillId != 1020 && SkillId != 1025)
                                {
                                    if (MyChar.LocMap == 1039 || SkillId == 1002 || SkillId == 1000 || SkillId == 1001)
                                    {
                                        MyChar.SkillLooping = SkillId;
                                        MyChar.SkillLoopingX = (ushort)x;
                                        MyChar.SkillLoopingY = (ushort)y;
                                        MyChar.SkillLoopingTarget = Target;
                                        MyChar.AtkType = 21;
                                        MyChar.Attack();
                                        MyChar.Attacking = true;
                                    }
                                    else
                                        MyChar.UseSkill(SkillId, (ushort)x, (ushort)y, Target);
                                }
                                else
                                    MyChar.UseSkill(SkillId, (ushort)x, (ushort)y, Target);
                            }
                            if (AttackType == 2 || AttackType == 25)
                            {
                                uint Target = Data[0x0f];
                                Target = (Target << 8) | Data[0x0e];
                                Target = (Target << 8) | Data[0x0d];
                                Target = (Target << 8) | Data[0x0c];
                                MyChar.TargetUID = Target;

                                if (Target < 800000 && Target >= 700000)
                                {
                                    SingleNPC ThisTGO = (SingleNPC)NPCs.AllNPCs[Target];
                                    MyChar.MobTarget = null;

                                    MyChar.AtkType = (byte)AttackType;
                                    MyChar.TGTarget = ThisTGO;

                                }
                                else if (Target >= 800000 && Target < 1000000)
                                {
                                    if (MyChar.Guard != null)
                                    {
                                        if (Target != MyChar.Guard.UID)
                                        {
                                            Monster TargetMob = (Monster)World.AllMobs[Target];
                                            MyChar.MobTarget = null;

                                            if (TargetMob != null)
                                            {
                                                MyChar.AtkType = (byte)AttackType;
                                                MyChar.MobTarget = TargetMob;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Monster TargetMob = (Monster)World.AllMobs[Target];
                                        MyChar.MobTarget = null;

                                        if (TargetMob != null)
                                        {
                                            MyChar.AtkType = (byte)AttackType;
                                            MyChar.MobTarget = TargetMob;
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                                    {
                                        Character ThisChar = (Character)DE.Value;
                                        if (ThisChar.UID == Target)
                                        {
                                            MyChar.PTarget = ThisChar;
                                            MyChar.AtkType = (byte)AttackType;
                                            break;
                                        }
                                    }
                                }
                                if (MyChar.AtkType == 2 && MyChar.PTarget != null && MyChar.PTarget.Flying)
                                    MyChar.PTarget = null;

                                MyChar.Attacking = true;
                                MyChar.Attack();
                            }
                            MyChar.Ready = true;
                            break;
                        }
                    #endregion
                    #region Packet 1023
                    case 1023:
                        {
                            byte Type = Data[4];
                            switch (Type)
                            {
                                case 0://Create
                                    {
                                        MyChar.TeamLeader = true;
                                        MyChar.MyTeamLeader = MyChar;
                                        MyChar.Flags.TeamLeader = true;
                                        SendPacket(CoServer.MyPackets.TeamPacket(MyChar.UID, 0));
                                        World.UpdateSpawn(MyChar);
                                        break;
                                    }
                                case 1://Join request
                                    {
                                        uint JoinWho = (uint)(Data[8] + (Data[9] << 8) + (Data[10] << 16) + (Data[11] << 24));
                                        Character Who = (Character)World.AllChars[JoinWho];
                                        if (Who.TeamLeader)
                                            if (!Who.JoinForbidden)
                                                if (Who.PlayersInTeam < 6)
                                                {
                                                    Who.MyClient.SendPacket(CoServer.MyPackets.TeamPacket(MyChar.UID, 1));
                                                    SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "[�quipe]La demande a �t� envoy�!", 2005));
                                                }
                                                else
                                                    SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "[�quipe]L'�quipe est pleine.", 2005));
                                            else
                                                SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "L'�quipe n'accepte pas d'autres membres.", 2005));
                                        else
                                            SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "[�quipe]La cible n'a pas cr�� d'�quipe.", 2005));

                                        break;
                                    }
                                case 2://Exit team
                                    MyChar.MyTeamLeader.TeamRemove(MyChar, false);
                                    break;
                                case 3://I accept invitation
                                    {
                                        uint WhoInvited = (uint)(Data[8] + (Data[9] << 8) + (Data[10] << 16) + (Data[11] << 24));
                                        Character Who = (Character)World.AllChars[WhoInvited];
                                        if (Who != null)
                                            Who.TeamAdd(MyChar);

                                        break;
                                    }
                                case 4://Invite request
                                    {
                                        uint InviteWho = (uint)(Data[8] + (Data[9] << 8) + (Data[10] << 16) + (Data[11] << 24));
                                        Character Invited = (Character)World.AllChars[InviteWho];
                                        if (!Invited.TeamLeader && Invited.MyTeamLeader == null && MyChar.TeamLeader && MyChar.PlayersInTeam < 6)
                                        {
                                            Invited.MyClient.SendPacket(CoServer.MyPackets.TeamPacket(MyChar.UID, 6));
                                            Invited.MyClient.SendPacket(CoServer.MyPackets.TeamPacket(MyChar.UID, 4));
                                        }

                                        break;
                                    }
                                case 5://I accept your join request
                                    {
                                        uint WhoJoins = (uint)(Data[8] + (Data[9] << 8) + (Data[10] << 16) + (Data[11] << 24));
                                        Character Joiner = (Character)World.AllChars[WhoJoins];

                                        MyChar.TeamAdd(Joiner);

                                        break;
                                    }
                                case 6://Dismiss
                                    MyChar.TeamDismiss();
                                    break;
                                case 7://Kick
                                    {
                                        uint KickWho = (uint)(Data[8] + (Data[9] << 8) + (Data[10] << 16) + (Data[11] << 24));
                                        Character Kicked = (Character)World.AllChars[KickWho];

                                        MyChar.TeamRemove(Kicked, true);
                                        break;
                                    }
                                case 8://Forbid joining
                                    MyChar.JoinForbidden = true;
                                    break;
                                case 9://UnForbid joining
                                    MyChar.JoinForbidden = false;
                                    break;
                            }
                            break;
                        }
                    #endregion
                    #region Packet 1024
                    case 1024:
                        {
                            ushort Points = (ushort)(Data[0x04] + Data[0x05] + Data[0x06] + Data[0x07]);

                            if (MyChar.StatP < Points)
                            {
                                MyChar.SendSysMsg("[Fraude]Vous essayez de distribuer plus de points que vous n'en avez.");
                                CoServer.WriteCheat(MyChar, "STR_ALLOT_CHEAT");
                                return;
                            }

                            MyChar.StatP -= Points;
                            MyChar.Str += Data[0x04];
                            MyChar.Agi += Data[0x05];
                            MyChar.Vit += Data[0x06];
                            MyChar.Spi += Data[0x07];
                            break;
                        }
                    #endregion
                    #region Packet 1027
                    case 1027:
                        {
                            MyChar.Ready = false;
                            uint MainItemUID = (uint)((Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + Data[8]);
                            uint GemUID = (uint)((Data[15] << 24) + (Data[14] << 16) + (Data[13] << 8) + Data[12]);
                            string RealItem = "";
                            string RealGem = "";

                            int Mode = Data[18];
                            int Slot = Data[16];
                            int Counter = 0;

                            if (Mode == 0)
                            {
                                if (GemUID != 0)
                                {
                                    foreach (ulong uid in MyChar.Inventory_UIDs)
                                    {
                                        if (GemUID == uid)
                                            RealGem = MyChar.Inventory[Counter];
                                        Counter++;
                                    }
                                }
                                Counter = 0;

                                if (MainItemUID != 0)
                                {
                                    foreach (ulong uid in MyChar.Inventory_UIDs)
                                    {
                                        if (MainItemUID == uid)
                                            RealItem = MyChar.Inventory[Counter];
                                        Counter++;
                                    }
                                }

                                if (RealItem != "")
                                    if (RealGem != "")
                                    {
                                        string[] ItemParts = RealItem.Split('-');
                                        string[] GemParts = RealGem.Split('-');
                                        if (Slot == 1)
                                            ItemParts[4] = (uint.Parse(GemParts[0]) - 700000).ToString();
                                        if (Slot == 2)
                                            ItemParts[5] = (uint.Parse(GemParts[0]) - 700000).ToString();
                                        MyChar.RemoveItem(MainItemUID);
                                        MyChar.RemoveItem(GemUID);
                                        MyChar.AddItem(ItemParts[0] + "-" + ItemParts[1] + "-" + ItemParts[2] + "-" + ItemParts[3] + "-" + ItemParts[4] + "-" + ItemParts[5], 0, MainItemUID);
                                    }
                            }
                            else
                            {
                                if (MainItemUID != 0)
                                {
                                    foreach (ulong uid in MyChar.Inventory_UIDs)
                                    {
                                        if (MainItemUID == uid)
                                            RealItem = MyChar.Inventory[Counter];
                                        Counter++;
                                    }
                                }
                                if (RealItem == null)
                                    return;
                                string[] ItemParts = RealItem.Split('-');
                                if (Slot == 1)
                                    if (ItemParts[4] != "0")
                                    {
                                        if (ItemParts[5] == "0" || ItemParts[5] == "255")
                                            ItemParts[4] = "255";
                                        else if (ItemParts[5] != "0")
                                        {
                                            ItemParts[4] = ItemParts[5];
                                            ItemParts[5] = "255";
                                        }
                                    }
                                if (Slot == 2)
                                    if (ItemParts[5] != "0")
                                        ItemParts[5] = "255";
                                MyChar.RemoveItem(MainItemUID);
                                MyChar.AddItem(ItemParts[0] + "-" + ItemParts[1] + "-" + ItemParts[2] + "-" + ItemParts[3] + "-" + ItemParts[4] + "-" + ItemParts[5], 0, MainItemUID);
                            }

                            if (Data.Length == 40)
                            {
                                MainItemUID = (uint)((Data[31] << 24) + (Data[30] << 16) + (Data[29] << 8) + Data[28]);
                                GemUID = (uint)((Data[35] << 24) + (Data[34] << 16) + (Data[33] << 8) + Data[32]);
                                RealItem = "";
                                RealGem = "";

                                Mode = Data[38];
                                Slot = Data[36];
                                Counter = 0;

                                if (Mode == 0)
                                {
                                    if (GemUID != 0)
                                    {
                                        foreach (ulong uid in MyChar.Inventory_UIDs)
                                        {
                                            if (GemUID == uid)
                                                RealGem = MyChar.Inventory[Counter];
                                            Counter++;
                                        }
                                    }
                                    Counter = 0;

                                    if (MainItemUID != 0)
                                    {
                                        foreach (ulong uid in MyChar.Inventory_UIDs)
                                        {
                                            if (MainItemUID == uid)
                                                RealItem = MyChar.Inventory[Counter];
                                            Counter++;
                                        }
                                    }

                                    if (RealItem != "")
                                        if (RealGem != "")
                                        {
                                            string[] ItemParts = RealItem.Split('-');
                                            string[] GemParts = RealGem.Split('-');
                                            if (Slot == 1)
                                                ItemParts[4] = (uint.Parse(GemParts[0]) - 700000).ToString();
                                            if (Slot == 2)
                                                ItemParts[5] = (uint.Parse(GemParts[0]) - 700000).ToString();
                                            MyChar.RemoveItem(MainItemUID);
                                            MyChar.RemoveItem(GemUID);
                                            MyChar.AddItem(ItemParts[0] + "-" + ItemParts[1] + "-" + ItemParts[2] + "-" + ItemParts[3] + "-" + ItemParts[4] + "-" + ItemParts[5], 0, MainItemUID);
                                        }
                                }
                                else
                                {
                                    string[] ItemParts = RealItem.Split('-');
                                    if (Slot == 1)
                                        ItemParts[4] = "0";
                                    if (Slot == 2)
                                        ItemParts[5] = "0";
                                    MyChar.RemoveItem(MainItemUID);
                                    MyChar.AddItem(ItemParts[0] + "-" + ItemParts[1] + "-" + ItemParts[2] + "-" + ItemParts[3] + "-" + ItemParts[4] + "-" + ItemParts[5], 0, MainItemUID);
                                }
                            }
                            MyChar.Ready = true;
                            break;
                        }
                    #endregion
                    #region Packet 1052
                    case 1052:
                        {
                            string Ip = IPE.Address.ToString();
                            ushort Port = (ushort)(IPE.Port - 1);

                            uint AccountId = (uint)((Data[0x07] << 24) + (Data[0x06] << 16) + (Data[0x05] << 8) + Data[0x04]);
                            uint Token = (uint)((Data[0x0b] << 24) + (Data[0x0a] << 16) + (Data[0x09] << 8) + Data[0x08]);

                            //Get the AccountID with the IP and the Port
                            string ThisAcc = DataBase.GetAccInfo(AccountId, Token);

                            //If the AccountID exist
                            if (ThisAcc != "_:_")
                            {
                                Program.WriteLine("Successful login for account " + ThisAcc + " with the Ip " + Ip + ":" + Port + "!");

                                //Determinate if the the Account exist on the serveur
                                byte Authenticated = DataBase.Authenticate(ThisAcc);

                                //Determinate the status of the Account
                                Status = DataBase.GetStatus(ThisAcc);
                                Account = ThisAcc;

                                Crypto.GenerateKeys(new byte[4] { Data[0x0b], Data[0x0a], Data[0x09], Data[0x08] }, new byte[4] { Data[0x07], Data[0x06], Data[0x05], Data[0x04] });
                                Crypto.EnableAlternateKeys();

                                if (Authenticated == 0)
                                    SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", "ALLUSERS", "NEW_ROLE", 0x835));
                                else if (Authenticated == 1)
                                {
                                    MyChar = new Character();
                                    MyChar.MyClient = this;

                                    DataBase.GetCharInfo(MyChar, Account);

                                    if (World.AllChars.ContainsKey(MyChar.UID))
                                        World.AllChars[MyChar.UID].MyClient.Drop();

                                    lock (World.AllChars) { World.AllChars.Add(MyChar.UID, MyChar); }

                                    SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", "ALLUSERS", "ANSWER_OK", 0x835));
                                    SendPacket(CoServer.MyPackets.CharacterInfo(MyChar));
                                }
                                else if (Authenticated == 3)
                                {
                                    SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", "ALLUSERS", "Vous �tes banni!", 2101));
                                    ListenSock.Disconnect();
                                }
                                else
                                    ListenSock.Disconnect();
                            }
                            else
                                ListenSock.Disconnect();

                            break;
                        }
                    #endregion
                    #region Packet 1056
                    case 1056:
                        {
                            uint UID = BitConverter.ToUInt32(Data, 4);
                            byte Type = Data[8];

                            switch (Type)
                            {
                                case 1://Request trade
                                    {
                                        Character Who = (Character)World.AllChars[UID];
                                        if (UID != MyChar.TradingWith && !Who.Trading)
                                        {
                                            Who.MyClient.SendPacket(CoServer.MyPackets.TradePacket(MyChar.UID, 1));
                                            MyChar.TradingWith = UID;
                                            Who.TradingWith = MyChar.UID;
                                            SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "[�change]Requ�te d'�change envoy�e.", 2005));
                                        }
                                        else
                                        {
                                            Who.MyTradeSide.Clear();
                                            Who.MyTradeSideCount = 0;
                                            Who.TradeOK = false;
                                            Who.Trading = true;

                                            MyChar.MyTradeSide.Clear();
                                            MyChar.MyTradeSideCount = 0;
                                            MyChar.TradeOK = false;
                                            MyChar.Trading = true;
                                            Who.MyClient.SendPacket(CoServer.MyPackets.TradePacket(MyChar.UID, 3));
                                            SendPacket(CoServer.MyPackets.TradePacket(Who.UID, 3));
                                        }
                                        break;
                                    }
                                case 2://Close trade
                                    {
                                        if (MyChar.Trading)
                                        {
                                            Character Who = (Character)World.AllChars[MyChar.TradingWith];
                                            if (Who != null)
                                            {
                                                Who.MyClient.SendPacket(CoServer.MyPackets.TradePacket(MyChar.TradingWith, 5));
                                                SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "�change rat�!", 2005));
                                                Who.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Who.MyClient.MessageId, "SYSTEM", Who.Name, "�change rat�!", 2005));
                                                Who.Trading = false;
                                                MyChar.Trading = false;
                                                Who.TradingWith = 0;
                                                MyChar.TradingWith = 0;
                                                foreach (uint iuid in MyChar.MyTradeSide)
                                                {
                                                    string Item = MyChar.FindItem(iuid);
                                                    string[] Splitter = Item.Split('-');
                                                    SendPacket(CoServer.MyPackets.AddItem(iuid, int.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 0, 100, 100));
                                                }
                                                foreach (uint iuid in Who.MyTradeSide)
                                                {
                                                    string Item = Who.FindItem(iuid);
                                                    string[] Splitter = Item.Split('-');
                                                    Who.MyClient.SendPacket(CoServer.MyPackets.AddItem(iuid, int.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 0, 100, 100));
                                                }
                                                MyChar.MyTradeSide = new ArrayList(20);
                                                Who.MyTradeSide = new ArrayList(20);
                                                MyChar.TradingCPs = 0;
                                                MyChar.TradingSilvers = 0;
                                                MyChar.TradeOK = false;
                                                Who.TradeOK = false;
                                                Who.MyTradeSideCount = 0;
                                                MyChar.MyTradeSideCount = 0;
                                            }
                                        }
                                        break;
                                    }
                                case 6://Add an item
                                    {
                                        if (Status != 7)
                                        {
                                            Character Who = (Character)World.AllChars[MyChar.TradingWith];
                                            if ((Who.ItemsInInventory + MyChar.MyTradeSideCount) < 40)
                                            {
                                                string Item = MyChar.FindItem(UID);
                                                Who.MyClient.SendPacket(CoServer.MyPackets.TradeItem(UID, Item));
                                                MyChar.MyTradeSide.Add(UID);
                                                MyChar.MyTradeSideCount++;
                                            }
                                            else
                                            {
                                                SendPacket(CoServer.MyPackets.TradePacket(UID, 11));
                                                SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "[�change]Votre partenaire ne peut pas avoir plus d'objets.", 2005));
                                            }
                                        }
                                        break;
                                    }
                                case 7://Specify money
                                    {
                                        if (Status != 7)
                                        {
                                            Character Who = (Character)World.AllChars[MyChar.TradingWith];
                                            if (MyChar.Silvers < UID)
                                            {
                                                MyChar.SendSysMsg("[Fraude]%s essaie d'�changer plus qu'il ne poss�de.".Replace("%s", MyChar.Name));
                                                Who.SendSysMsg("[Fraude]%s essaie d'�changer plus qu'il ne poss�de.".Replace("%s", MyChar.Name));
                                                CoServer.WriteCheat(MyChar, "STR_TRADE_CHEAT");
                                                return;
                                            }
                                            MyChar.TradingSilvers = UID;
                                            Who.MyClient.SendPacket(CoServer.MyPackets.TradePacket(UID, 8));
                                        }
                                        break;
                                    }
                                case 10://OK
                                    {
                                        Character Who = (Character)World.AllChars[MyChar.TradingWith];
                                        if (Who.TradeOK)
                                        {
                                            Who.MyClient.SendPacket(CoServer.MyPackets.TradePacket(MyChar.TradingWith, 5));
                                            SendPacket(CoServer.MyPackets.TradePacket(MyChar.UID, 5));
                                            foreach (uint itemuid in Who.MyTradeSide)
                                            {
                                                string item = Who.FindItem(itemuid);
                                                MyChar.AddItem(item, 0, itemuid);
                                                Who.RemoveItem(itemuid);
                                            }
                                            foreach (uint itemuid in MyChar.MyTradeSide)
                                            {
                                                string item = MyChar.FindItem(itemuid);
                                                Who.AddItem(item, 0, itemuid);
                                                MyChar.RemoveItem(itemuid);
                                            }
                                            if (MyChar.Silvers < MyChar.TradingSilvers || MyChar.CPs < MyChar.TradingCPs)
                                            {
                                                MyChar.SendSysMsg("[Fraude]%s essaie d'�changer plus qu'il ne poss�de.".Replace("%s", MyChar.Name));
                                                Who.SendSysMsg("[Fraude]%s essaie d'�changer plus qu'il ne poss�de.".Replace("%s", MyChar.Name));
                                                CoServer.WriteCheat(MyChar, "STR_TRADE_CHEAT");
                                                return;
                                            }

                                            if (Who.Silvers < Who.TradingSilvers || Who.CPs < Who.TradingCPs)
                                            {
                                                Who.SendSysMsg("[Fraude]%s essaie d'�changer plus qu'il ne poss�de.".Replace("%s", Who.Name));
                                                MyChar.SendSysMsg("[Fraude]%s essaie d'�changer plus qu'il ne poss�de.".Replace("%s", Who.Name));
                                                CoServer.WriteCheat(Who, "STR_TRADE_CHEAT");
                                                return;
                                            }

                                            MyChar.Silvers += Who.TradingSilvers;
                                            MyChar.CPs += Who.TradingCPs;

                                            MyChar.Silvers -= MyChar.TradingSilvers;
                                            MyChar.CPs -= MyChar.TradingCPs;

                                            Who.Silvers += MyChar.TradingSilvers;
                                            Who.CPs += MyChar.TradingCPs;

                                            Who.Silvers -= Who.TradingSilvers;
                                            Who.CPs -= Who.TradingCPs;

                                            MyChar.Save();
                                            Who.Save();

                                            SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "�change r�ussi.", 2005));
                                            Who.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Who.MyClient.MessageId, "SYSTEM", Who.Name, "�change r�ussi.", 2005));

                                            MyChar.Trading = false;
                                            MyChar.TradingWith = 0;
                                            MyChar.MyTradeSideCount = 0;
                                            MyChar.TradeOK = false;
                                            MyChar.MyTradeSide = new ArrayList(20);
                                            MyChar.TradingCPs = 0;
                                            MyChar.TradingSilvers = 0;

                                            Who.MyTradeSide = new ArrayList(20);
                                            Who.TradingCPs = 0;
                                            Who.TradingSilvers = 0;
                                            Who.TradeOK = false;
                                            Who.TradingWith = 0;
                                            Who.Trading = false;
                                            Who.MyTradeSideCount = 0;
                                        }
                                        else
                                        {
                                            MyChar.TradeOK = true;
                                            Who.MyClient.SendPacket(CoServer.MyPackets.TradePacket(0, 10));
                                        }
                                        break;
                                    }
                                case 13://Specify CPs
                                    {
                                        if (Status != 7)
                                        {
                                            Character Who = (Character)World.AllChars[MyChar.TradingWith];
                                            if (MyChar.CPs <= UID)
                                            {
                                                MyChar.SendSysMsg("[Fraude]%s essaie d'�changer plus qu'il ne poss�de.".Replace("%s", MyChar.Name));
                                                Who.SendSysMsg("[Fraude]%s essaie d'�changer plus qu'il ne poss�de.".Replace("%s", MyChar.Name));
                                                CoServer.WriteCheat(MyChar, "STR_TRADE_CHEAT");
                                                return;
                                            }
                                            MyChar.TradingCPs = UID;
                                            Who.MyClient.SendPacket(CoServer.MyPackets.TradePacket(UID, 12));
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Packet 1101
                    case 1101:
                        {
                            MyChar.Ready = false;
                            uint ItemUID = (uint)((Data[0x07] << 24) + (Data[0x06] << 16) + (Data[0x05] << 8) + Data[0x04]);

                            DroppedItem TheItem = null;
                            if (DroppedItems.AllDroppedItems.Contains(ItemUID))
                                TheItem = (DroppedItems.AllDroppedItems[ItemUID] as DroppedItem);

                            if (TheItem != null)
                            {
                                if (MyChar.ItemsInInventory > 39 && TheItem.Money == 0)
                                    return;

                                if (TheItem.Owner != 0)
                                    if (TheItem.Owner != MyChar.UID && !TheItem.Other)
                                    {
                                        SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Cet article n'est pas � vous, vous ne pouvez pas le ramasser pour l'instant!", 0x7d5));
                                        return;
                                    }

                                if (TheItem.Money == 0)
                                {
                                    MyChar.AddItem(TheItem.Item, 0, ItemUID);
                                    string ItemName = "";
                                    if (DataBase.ItemNames.ContainsKey(TheItem.ItemId))
                                        ItemName = DataBase.ItemNames[TheItem.ItemId];
                                    SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Obtenez l'article " + ItemName + ".", 0x7d5));
                                }
                                else
                                {
                                    if (TheItem.Money >= 1000)
                                        SendPacket(CoServer.MyPackets.GeneralData(MyChar, TheItem.Money, 121));
                                    MyChar.Silvers += TheItem.Money;
                                    SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Ramassez " + TheItem.Money + " argent.", 2005));
                                }

                                DroppedItems.AllDroppedItems.Remove(ItemUID);
                                World.ItemDissappears(TheItem);
                            }
                            MyChar.Ready = true;
                            break;
                        }
                    #endregion
                    #region Packet 1102
                    case 1102:
                        {
                            uint NPCID = (uint)((Data[7] << 24) + (Data[6] << 16) + (Data[5] << 8) + (Data[4]));
                            uint ItemUID = (uint)((Data[15] << 24) + (Data[14] << 16) + (Data[13] << 8) + (Data[12]));
                            byte Type = Data[8];
                            byte WHID = 0;

                            if (NPCID == 8)
                                WHID = 0;
                            else if (NPCID == 10012)
                                WHID = 1;
                            else if (NPCID == 10028)
                                WHID = 2;
                            else if (NPCID == 10011)
                                WHID = 3;
                            else if (NPCID == 10027)
                                WHID = 4;
                            else if (NPCID == 44)
                                WHID = 5;
                            else if (NPCID == 4101)
                                WHID = 6;


                            switch (Type)
                            {
                                case 0:
                                    SendPacket(CoServer.MyPackets.WhItems(MyChar, WHID, (ushort)NPCID));
                                    break;
                                case 1: //Throw an item into warehouse
                                    {
                                        if (WHID == 0 && MyChar.TCWHCount < 20 || WHID == 1 && MyChar.PCWHCount < 20 || WHID == 2 && MyChar.ACWHCount < 20 || WHID == 3 && MyChar.DCWHCount < 20 || WHID == 4 && MyChar.BIWHCount < 20 || WHID == 5 && MyChar.MAWHCount < 40 || WHID == 6 && MyChar.AZWHCount < 20)
                                        {
                                            string Item = MyChar.FindItem(ItemUID);
                                            MyChar.RemoveItem(ItemUID);
                                            MyChar.AddWHItem(Item, ItemUID, WHID);
                                            SendPacket(CoServer.MyPackets.WhItems(MyChar, WHID, (ushort)NPCID));
                                        }
                                        break;
                                    }
                                case 2: //Take an item from warehouse
                                    {
                                        if (MyChar.ItemsInInventory < 40)
                                        {
                                            string Item = MyChar.FindWHItem(ItemUID, WHID);
                                            MyChar.RemoveWHItem(ItemUID);
                                            SendPacket(CoServer.MyPackets.WhItems(MyChar, WHID, (ushort)NPCID));
                                            MyChar.AddItem(Item, 0, ItemUID);
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Packet 1107
                    case 1107:
                        {
                            byte Type = Data[4];
                            switch (Type)
                            {
                                case 1://Join guild request
                                    {
                                        if (MyChar.MyGuild == null)
                                        {
                                            uint UID = BitConverter.ToUInt32(Data, 8);

                                            Character JoinWho = (Character)World.AllChars[UID];
                                            if (JoinWho.MyGuild != null && JoinWho.GuildPosition == 100 || JoinWho.GuildPosition == 90)
                                                JoinWho.MyClient.SendPacket(CoServer.MyPackets.SendGuild(MyChar.UID, 1));
                                        }
                                        break;
                                    }
                                case 2://Accept join request
                                    {
                                        uint UID = BitConverter.ToUInt32(Data, 8);
                                        if (World.AllChars.ContainsKey(UID))
                                        {
                                            Character WhoJoins = (Character)World.AllChars[UID];
                                            if (WhoJoins.MyGuild == null)
                                            {
                                                WhoJoins.GuildID = MyChar.MyGuild.UniqId;
                                                WhoJoins.MyGuild = MyChar.MyGuild;
                                                WhoJoins.GuildPosition = 50;
                                                MyChar.MyGuild.AddPlayer(WhoJoins);
                                                World.UpdateSpawn(WhoJoins);

                                                WhoJoins.MyClient.SendPacket(CoServer.MyPackets.GuildName(WhoJoins.GuildID, WhoJoins.MyGuild.Name));
                                                WhoJoins.MyClient.SendPacket(CoServer.MyPackets.GuildInfo(WhoJoins.MyGuild, WhoJoins));
                                                World.SendAllGuild(WhoJoins);
                                            }
                                        }
                                        break;
                                    }
                                case 3://Leave the guild
                                    {
                                        if (MyChar.MyGuild != null && MyChar.GuildPosition != 100)
                                        {
                                            SendPacket(CoServer.MyPackets.SendGuild(MyChar.MyGuild.UniqId, 19));
                                            MyChar.MyGuild.RemovePlayer(MyChar);
                                            MyChar.GuildDonation = 0;
                                            MyChar.GuildID = 0;
                                            MyChar.GuildPosition = 0;
                                            MyChar.MyGuild = null;
                                            World.UpdateSpawn(MyChar);
                                            MyChar.Screen.SendScreen(true);
                                            MyChar.Screen.Clear();
                                            MyChar.Screen.SendScreen(false);
                                            World.SendAllGuild(MyChar);
                                        }
                                        break;
                                    }
                                case 11://Donate
                                    {
                                        if (MyChar.MyGuild != null)
                                        {
                                            uint Amount = BitConverter.ToUInt32(Data, 8);

                                            if (MyChar.Silvers < Amount)
                                            {
                                                MyChar.SendSysMsg("[Fraude]Vous essayez de donner plus d'argent que vous n'en avez.");
                                                CoServer.WriteCheat(MyChar, "STR_DONATE_CHEAT");
                                                return;
                                            }

                                            if (MyChar.Silvers >= Amount)
                                            {
                                                MyChar.MyGuild.Fund += Amount;
                                                MyChar.Silvers -= Amount;
                                                MyChar.GuildDonation += (int)Amount;
                                                MyChar.MyGuild.Refresh(MyChar.UID, (int)MyChar.GuildDonation);
                                                SendPacket(CoServer.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                                World.SendMsgToAll(MyChar.Name + " a donn� " + Amount.ToString() + " argents � " + MyChar.MyGuild.Name + ".", "SYSTEM", 2005);
                                            }
                                        }
                                        break;
                                    }
                                case 12://Guild status
                                    {
                                        if (MyChar.MyGuild != null)
                                        {
                                            SendPacket(CoServer.MyPackets.GuildName(MyChar.GuildID, MyChar.MyGuild.Name));
                                            SendPacket(CoServer.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                            SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, MyChar.MyGuild.Bulletin, 2111));
                                            SendPacket(CoServer.MyPackets.GeneralData(MyChar, 0, 97));
                                        }
                                        break;
                                    }
                                default:
                                    {
                                        Program.WriteLine("Client[1107]::Error -> Type[" + Data[0x04] + "] not implemented!");
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Packet 1112
                    case 1112://See guild member's status
                        {
                            string Name = "";
                            for (sbyte i = 9; i < 25; i++)
                                Name += (char)Data[i];
                            Name = Name.TrimEnd((char)0x00);

                            if (Name == MyChar.MyGuild.Leader.Name)
                                SendPacket(CoServer.MyPackets.MemberInfo(MyChar.MyGuild.Leader, 100));

                            foreach (GuildMember Deputy in MyChar.MyGuild.Deputies.Values)
                            {
                                if (Name == Deputy.Name)
                                {
                                    SendPacket(CoServer.MyPackets.MemberInfo(Deputy, 90));
                                    break;
                                }
                            }

                            foreach (GuildMember Member in MyChar.MyGuild.Members.Values)
                            {
                                if (Name == Member.Name)
                                {
                                    SendPacket(CoServer.MyPackets.MemberInfo(Member, 50));
                                    break;
                                }
                            }

                            break;
                        }
                    #endregion
                    #region Packet 2031
                    case 2031:
                        {
                            MyChar.Ready = false;
                            int NPCID = (Data[7] << 24) + (Data[6] << 16) + (Data[5] << 8) + Data[4];
                            SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous essayez de parler au NPC(ID: " + NPCID + ")", 2005));

                            if (ScriptHandler.AllScripts.ContainsKey((uint)NPCID))
                            {
                                MsgPacket = new Dialog(Data, this);
                                MsgPacket = null;
                                return;
                            }

                            int Control = (int)Data[10];
                            CurrentNPC = NPCID;
                            if (CurrentNPC == 699999)
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Bonjour, ce serveur utilise l'�mulateur du serveur COPS v3 - Reborn Edition. Cet �mulateur de Conqu�te 2.0 a �t� programm� par ~K!ra~ (CptSky) "));
                                SendPacket(CoServer.MyPackets.NPCSay("dans le cadre du projet COPS (Conquer Online Private Server). Le projet COPS a �t� fond� le 27 juin 2008 par ~K!ra~ et DarkShadow. Merci de continuer "));
                                SendPacket(CoServer.MyPackets.NPCSay("� d�montrer de l'int�r�t envers ce projet qui a �t� le premier projet d'�mulateur pour Conqu�te 2.0. En esp�rant que le propri�taire de ce serveur "));
                                SendPacket(CoServer.MyPackets.NPCSay("utilise les binaires de COPS v3 - Reborn Edition � leur plein potentiel. Bon jeu!"));
                                SendPacket(CoServer.MyPackets.NPCLink("Merci.", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(8));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 1061) //WH Pass
                            {
                                if (MyChar.WHPWcheck == 1)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Vous avez r�ussi � cr�er un mot de passe pour votre banque. Voulez-vous le changer?"));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je veux le changer.", 4));
                                    SendPacket(CoServer.MyPackets.NPCLink("Laissez moi r�fl�chir.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                else
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Tout le monde qui mette leurs tr�sors dans leurs banques est � leur risque."));
                                    SendPacket(CoServer.MyPackets.NPCSay(" Je vous offre la chance de mettre un mot de passe � votre banque."));
                                    SendPacket(CoServer.MyPackets.NPCSay(" Que voulez-vous faire?"));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je veux mettre un mot de passe.", 1));
                                    SendPacket(CoServer.MyPackets.NPCLink("Laissez moi r�fl�chir.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                            }
                            if (CurrentNPC == 300500) //First Reborn (300500)
                            {
                                if (MyChar.RBCount >= 1)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Vous avez d�j� reborn. Je ne peux plus vous aider."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                else
                                {
                                    if (MyChar.Level >= 120 || (MyChar.Level >= 110 && MyChar.Job == 135))
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("J'ai pass� toute ma vie � chercher la vie �ternelle, finalement j'ai d�couvert la renaissance."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je veux en savoir plus.", 1));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je fais que passer.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 119)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous pouvez rena�tre si vous �tes 120 ou plus haut niveau."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 390) //Love Stone
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Voulez-vous proposer votre amour?"));
                                SendPacket(CoServer.MyPackets.NPCSay("Rappelez-vous, ce mariage est s�rieux."));
                                SendPacket(CoServer.MyPackets.NPCLink("Oui, je veux proposer", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Je ne suis pas pr�s.", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 600055) //Divorce
                            {
                                if (MyChar.Spouse == "Non")
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Hey! Vous n'�tes pas mari�!"));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je sais.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                else
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Les �toiles dans le ciel repr�sentent la vrai amour..."));
                                    SendPacket(CoServer.MyPackets.NPCSay(" Malheureusement certaines �toiles ne brillent pas assez et nous devons les d�truire."));
                                    SendPacket(CoServer.MyPackets.NPCSay(" Le mariage repr�sente c'est �toile. Je peux vous divorcer avec votre femme si votre"));
                                    SendPacket(CoServer.MyPackets.NPCSay(" amour n'est plus assez grande."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je veux divorcer.", 1));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je suis heureux.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                            }
                            if (CurrentNPC == 5004) //MillionaireLee
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Avoir des m�t�ors et des Perles de Dragon est exitant. Mais votre inventaire est rapidement plein."));
                                SendPacket(CoServer.MyPackets.NPCLink("Oui, je vous comprends. Que me proposez-vous?", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Je suis pauvre je n'ai pas ce probl�me.", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 701700) //Pole
                            {
                                uint HealPole = 0;
                                foreach (KeyValuePair<uint, SingleNPC> DE in NPCs.AllNPCs)
                                {
                                    SingleNPC Npcc = (SingleNPC)DE.Value;
                                    if (Npcc.Sob == 2)
                                        HealPole = Npcc.MaxHP - Npcc.CurHP;
                                }
                                //SendPacket(CoServer.MyPackets.NPCSay("Voulez-vous redonner de la vie au Pole? Il va vous en couter " + HealPole + "$. L'argent sera retir� des fonds de votre guilde."));
                                SendPacket(CoServer.MyPackets.NPCSay("Voulez-vous redonner de la vie au Pole? Il va vous en couter 100'000 CPs."));
                                SendPacket(CoServer.MyPackets.NPCLink("Oui", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Non", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 701701 || CurrentNPC == 701702) //Gates
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Que voulez-vous faire?"));
                                SendPacket(CoServer.MyPackets.NPCLink("Ouvrir/Fermer la porte.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Entrer dans le ch�teau.", 2));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 300000)//npc to check the virtue points
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Bonjour, en quoi puis-je vous aider ?"));
                                SendPacket(CoServer.MyPackets.NPCLink("Combien ais-je de points de vertue ?", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Je voudrais un prix", 2));
                                SendPacket(CoServer.MyPackets.NPCLink("Je passais juste par l�...", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 3381) //Chirurgien
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Voulez-vous changer votre taille? C'est un grand cadeau que je vous fais."));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux changer ma taille.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Je ne veux pas changer.", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 350050) //Reallot
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Pour une db je peux vous remettre vos points comme si vous �tiez niveau 1 et vous pourrez distribuer les autres."));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux redistribuer mes points.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Je ne veux pas.", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 2065) //Gem Composer
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Je suis ici pour composer vos gemmes. 15 gemmes normals pour une raffin� et 20 gemmes raffin�s pour 1 superbe."));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux composer une gemme Ph�nix.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux composer une gemme Dragon.", 2));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux composer une gemme Fureur.", 3));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux composer une gemme Arc en Ciel.", 4));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux composer une gemme Ivoire.", 5));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux composer une gemme Violet.", 6));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux composer une gemme de Lune.", 7));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux composer une gemme Mythique.", 8));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 7050) //Artisant 120+
                            {
                                UppAgree = false;
                                SendPacket(CoServer.MyPackets.NPCSay("Je consacre toute ma vie � am�liorer les �quipements. Je souhaite que ma cause peut vous aidez � devenir c�l�bre. Que voulez-vous faire?"));
                                SendPacket(CoServer.MyPackets.NPCLink("Pouvez-vous m'en dire plus?", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Arme/Arc/Glaive.", 2));
                                SendPacket(CoServer.MyPackets.NPCLink("Collier/Sac.", 3));
                                SendPacket(CoServer.MyPackets.NPCLink("Bague/Bracelet.", 4));
                                SendPacket(CoServer.MyPackets.NPCLink("Botte.", 5));
                                SendPacket(CoServer.MyPackets.NPCLink("Armure/Robe.", 6));
                                SendPacket(CoServer.MyPackets.NPCLink("Casque/Boucle.", 7));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 10062) //Artisant 120-
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("M�me si l'Artisant du Vent peut faire un bon boulot, il ne r�ussi pas � tous les coups. Je ne veux pas voir le peuple perdre tout espoir. Donc j'ai d�cid� de les aider. J'ai besoin de plus d'objet mais je vous promet de r�ussir."));
                                SendPacket(CoServer.MyPackets.NPCLink("Am�liorer la qualit�.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Am�liorer le niveau.", 2));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC > 600500 && CurrentNPC < 600505) //GC
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Voulez-vous rentrer au si�ge central des guildes dans la ville Dragon?."));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux y aller.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Je ne veux pas encore rentrer.", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC > 600504 && CurrentNPC < 600509) //GC
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Voulez-vous aller � la transmission suivante de celle-ci?."));
                                SendPacket(CoServer.MyPackets.NPCLink("Oui, je veux y aller.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux acheter une transmission.", 2));
                                SendPacket(CoServer.MyPackets.NPCLink("Je ne veux pas encore rentrer.", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 10054) //Desert
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("C'est la route pour la ville du D�sert. Vous �tes fort mais cette endroit est vraiment dangereux."));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux quand m�me y aller!", 1));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 100000000) //Exit TG
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Voulez vous sortir du terrain d'entrainnement ?"));
                                SendPacket(CoServer.MyPackets.NPCLink("Oui.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Non, Je passais juste par l�.", 2));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 10003) //Guild Administrator
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Je m'occupe des guildes de la Ville Dragon. Lorsque vous avez des questions concernant les guildes, vous pouvez me le demander."));
                                SendPacket(CoServer.MyPackets.NPCLink("Cr�er une guilde", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("D�grouper la guilde", 2));
                                SendPacket(CoServer.MyPackets.NPCLink("Donation de guilde", 3));
                                SendPacket(CoServer.MyPackets.NPCLink("Chef abdique", 4));
                                SendPacket(CoServer.MyPackets.NPCLink("Commettre � sous-chef", 5));
                                SendPacket(CoServer.MyPackets.NPCLink("D�charger", 6));
                                SendPacket(CoServer.MyPackets.NPCLink("Consulter une guilde", 7));
                                SendPacket(CoServer.MyPackets.NPCLink("Autres options", 8));
                                SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC >= 925 && CurrentNPC <= 945 && CurrentNPC != 941) //Lotto Box
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Voulez-vous choisir cette bo�te?"));
                                SendPacket(CoServer.MyPackets.NPCLink("Oui.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Non.", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 923) //Lotto
                            {
                                if (MyChar.Level > 70 || MyChar.RBCount != 0)
                                {
                                    if (MyChar.LotoCount < 15)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Voulez vous tenter votre chance � la lotterie ? Il vous en co�tera 27 cps.	"));
                                        SendPacket(CoServer.MyPackets.NPCLink("Oui pourquoi pas.", 1));
                                        SendPacket(CoServer.MyPackets.NPCLink("Non c'est trop cher...", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous �tes d�j� entr� 30 fois aujourd'hui!"));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois...", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                else
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Vous devez �tre niveau 71 pour rentrer dans la lotterie!"));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je vois...", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                            }
                            if (CurrentNPC == 9997)
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Je suis le gardien du PKTour voulez-vous entrer?"));
                                SendPacket(CoServer.MyPackets.NPCLink("Oui, je veux rentrer", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Non, je suis faible", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 9996)
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Il est l'heure de passer � la 2e carte. Voulez-vous entrer?"));
                                SendPacket(CoServer.MyPackets.NPCLink("Oui, je veux rentrer", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Non, je suis faible", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 9995)
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Il est l'heure de passer � la 3e carte. Voulez-vous entrer?"));
                                SendPacket(CoServer.MyPackets.NPCLink("Oui, je veux rentrer", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Non, je suis faible", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC > 9991 && CurrentNPC < 9995)
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Si vous-�tes le gagnant du tournois vous pouvez r�clamer le prix. L'�tes-vous?"));
                                SendPacket(CoServer.MyPackets.NPCLink("Oui", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Non", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 3825) //UnknowMan
                            {
                                if (MyChar.Level >= 130)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("La Perle de Dragon est un objet rare qui peut am�liorer le niveau et la"));
                                    SendPacket(CoServer.MyPackets.NPCSay(" qualit� des �quipements, peut vous aider � am�liorer rapidement votre"));
                                    SendPacket(CoServer.MyPackets.NPCSay(" niveau."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je voudrais en savoir plus.", 1));
                                    SendPacket(CoServer.MyPackets.NPCLink("Excellent.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(6));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                else
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Vous devez �tre au moins niveau 130 pour pouvoir utiliser l'�nergie de la Perle de Dragon."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(6));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                            }
                            if (CurrentNPC == 42) //Jail
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Voulez-vous sortir de prison?"));
                                SendPacket(CoServer.MyPackets.NPCLink("Oui.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Non, pas maintenant.", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 140) //Bot Jail
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Voulez-vous sortir de prison? Si vous avez �t� envoy� ici plus de trois fois vous �tes prit. Il vous coutera 504 CPs pour sortir."));
                                SendPacket(CoServer.MyPackets.NPCLink("Oui, je veux sortir", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Non, je ne veux pas", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 600601) //SolarSaint
                            {
                                if (World.DisOn == true && MyChar.Level > 109)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Dis City est commenc� et vous �tes assez fort pour m'aider! �tes-vous pr�t � d�loger la vermine?"));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je veux y aller.", 1));
                                    SendPacket(CoServer.MyPackets.NPCLink("Au revoir...", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                else if (World.DisOn == true && MyChar.Level < 110)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Revenez lorsque vous aurez un niveau sup�rieur � 110..."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je vois", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                else
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Il n'est pas encore l'heure..."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je vois", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                            }
                            if (CurrentNPC == 600602) //SolarSaint
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Avez-vous les 5 Jades d'enfer? Si oui vous devez aller � la deuxi�me carte."));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux y aller.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Au revoir...", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 600603) //SolarSaint
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Vous-avez tu� " + MyChar.DisKO + " monstres. Voulez-vous entrer dans la troisi�me carte?"));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux y aller.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Au revoir...", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            #region WH Open
                            if (CurrentNPC == 8 || CurrentNPC == 44 || CurrentNPC == 4101 || CurrentNPC == 10011 || CurrentNPC == 10012 || CurrentNPC == 10027 || CurrentNPC == 10028)
                            {
                                if (WHOpen == 1)
                                    SendPacket(CoServer.MyPackets.GeneralData(MyChar, 4, 126));
                                else
                                {
                                    if (MyChar.WHPWcheck == 0)
                                        SendPacket(CoServer.MyPackets.GeneralData(MyChar, 4, 126));
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous avez cr�� un mot de passe pour votre banque. Veuillez entrer le mot de passe pour ouvrir votre banque."));
                                        SendPacket(CoServer.MyPackets.NPCLink2("Mot de passe.", 1));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je veux r�fl�chir.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            #endregion
                            if (CurrentNPC == 35016) //Wuxing
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Le winxfour peut composer et enchanter des items."));
                                SendPacket(CoServer.MyPackets.NPCLink("Composer(+1~+9)", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Enchanter", 2));
                                SendPacket(CoServer.MyPackets.NPCLink("Composer(+10~+12)", 21));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 35015) //Ether
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Je peux vous am�liorer le bless de vos objets. Il me faut 5 amulettes mythique pour le -1. Une amulette pour le -2. Deux amulettes pour le -3. Trois amulettes pour le -4. Quatre amulettes pour le -5. Cinq amulette pour le -6. Sept amulette pour le -7."));
                                SendPacket(CoServer.MyPackets.NPCLink("Allez � la carte mythique.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Casque/Boucle.", 7));
                                SendPacket(CoServer.MyPackets.NPCLink("Collier/Sac.", 3));
                                SendPacket(CoServer.MyPackets.NPCLink("Bague/Bracelet.", 4));
                                SendPacket(CoServer.MyPackets.NPCLink("Arme/Bouclier.", 101));
                                SendPacket(CoServer.MyPackets.NPCLink("Armure/Robe.", 6));
                                SendPacket(CoServer.MyPackets.NPCLink("Botte.", 5));
                                SendPacket(CoServer.MyPackets.NPCLink("Gourde/Robe.", 100));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 1550) //ForgeronLee
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Bonjour, je suis le ForgeronLee. Je peux socker votre premier trou dans un �quipement pour 3 ForeuseDiamant et le second trou pour 9 ForeuseDiamant."));
                                SendPacket(CoServer.MyPackets.NPCLink("1er sock", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("2nd sock", 2));
                                SendPacket(CoServer.MyPackets.NPCLink("Je ne fais que passer", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 10064) //Tinter
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Voulez-vous changer la couleur de votre armure, casque ou bouclier?"));
                                SendPacket(CoServer.MyPackets.NPCLink("Armure", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Casque", 2));
                                SendPacket(CoServer.MyPackets.NPCLink("Bouclier", 3));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 10063) //Server
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Notre teinturier est le meilleur en ville. Si vous voulez teindre votre �quipement vous devez entrer. Vous avez un grand choix de couleur. Une m�t�ore est charg�e pour teindre votre �quipement."));
                                SendPacket(CoServer.MyPackets.NPCLink("Oui, voici une m�t�ore.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Pouvez-vous teindre mon armure en noir?", 2));
                                SendPacket(CoServer.MyPackets.NPCLink("Je ne fais que passer.", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 10000) //FireTaoist
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Bonjour, je suis le ma�tre des taoistes de feu. Que puis-je faire pour vous?"));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux avancer.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux apprendre de nouveaux pouvoirs.", 100));
                                SendPacket(CoServer.MyPackets.NPCLink("Je ne fais que passer.", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 30) //WaterTaoist
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Bonjour, je suis le ma�tre des taoistes d'eau. Que puis-je faire pour vous?"));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux avancer.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux apprendre de nouveaux pouvoirs.", 100));
                                SendPacket(CoServer.MyPackets.NPCLink("Je ne fais que passer.", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 10022) //Trojan
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Bonjour, je suis le ma�tre des braves. Que puis-je faire pour vous?"));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux avancer.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux apprendre de nouveaux pouvoirs.", 222));
                                SendPacket(CoServer.MyPackets.NPCLink("Je ne fais que passer.", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 10001) //Warrior
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Bonjour, je suis le ma�tre des guerriers. Que puis-je faire pour vous?"));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux avancer.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux apprendre de nouveaux pouvoirs.", 150));
                                SendPacket(CoServer.MyPackets.NPCLink("Je ne fais que passer.", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 400) //Archer
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Bonjour, je suis le ma�tre des archers. Que puis-je faire pour vous?"));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux avancer.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Je veux apprendre de nouveaux pouvoirs.", 20));
                                SendPacket(CoServer.MyPackets.NPCLink("Je ne fais que passer.", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 41) //Ou Artisant
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("C'est vraiment difficile d'avoir des trous dans les armes. J'ai de grandes capacit�s. Que puis-je faire pour vous?"));
                                SendPacket(CoServer.MyPackets.NPCLink("Pouvez-vous cr�er un trou dans mon arme?", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Je ne fais que passer.", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 100000000) //Boxer
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Si vous �tes niveau 20 ou plus, je peux vous faire entrer sur le terrain d'entrainement. Voulez vous y aller pour 1.000 d'argent ? Je peux aussi vous envoyez au OfflineTG"));
                                SendPacket(CoServer.MyPackets.NPCLink("Oui s'il vous pla�t.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Non merci.", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 45) //Market Controller
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Voulez-vous sortir du march�? Je peux vous t�l�porter gratuitement."));
                                SendPacket(CoServer.MyPackets.NPCLink("Oui, Merci.", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Non, je reste ici.", 2));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 10002) //Barber
                            {
                                SendPacket(CoServer.MyPackets.NPCSay("Maintenant je peux offrir trois types de coupes : Style nouveau, style nostalgique et style sp�cial. Cela co�te 500 argent."));
                                SendPacket(CoServer.MyPackets.NPCLink("Nouveau style", 1));
                                SendPacket(CoServer.MyPackets.NPCLink("Style nostalgique", 2));
                                SendPacket(CoServer.MyPackets.NPCLink("Style sp�ciale", 3));
                                SendPacket(CoServer.MyPackets.NPCLink("Je garde ma coupe ! Escroc ! Voleur ! xD", 255));
                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                SendPacket(CoServer.MyPackets.NPCFinish());
                            }
                            MyChar.Ready = true;
                            break;
                        }
                    #endregion
                    #region Packet 2032
                    case 2032:
                        {
                            MyChar.Ready = false;

                            if (ScriptHandler.AllScripts.ContainsKey((uint)CurrentNPC))
                            {
                                MsgPacket = new Dialog(Data, this);
                                MsgPacket = null;
                                return;
                            }

                            int Control = (int)Data[10];

                            #region Set WH Pass
                            if (CurrentNPC == 1061)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Le mot de passe peut avoir 9 caract�res, le premier ne doit pas �tre un 0."));
                                    SendPacket(CoServer.MyPackets.NPCSay(" Entrez des chiffres. Si vous faites une faute la banque vas �tre bloqu�."));
                                    SendPacket(CoServer.MyPackets.NPCSay(" Veuillez �tre sure avant d'entrer le mot de passe."));
                                    SendPacket(CoServer.MyPackets.NPCLink2("Mot de passe.", 2));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je dois r�fl�chir.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    string WHPW = "";
                                    bool ValidPW = true;
                                    for (int i = 14; i < 14 + Data[13]; i++)
                                        WHPW += Convert.ToChar(Data[i]);
                                    if (WHPW.IndexOfAny(new char[27] { ' ', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' }) > -1)
                                        ValidPW = false;

                                    try
                                    {
                                        if (ValidPW)
                                        {
                                            MyChar.WHPW = WHPW;
                                            MyChar.WHPWcheck = 1;
                                            SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Le mot de passe est " + MyChar.WHPW, 2005));
                                            MyChar.Save();
                                        }
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Ce nom n'est pas valide!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("R�essayer.", 1));
                                            SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
                                }
                                if (Control == 4)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Veuillez entrer votre ancien mot de passe."));
                                    SendPacket(CoServer.MyPackets.NPCLink2("Mot de passe", 5));
                                    SendPacket(CoServer.MyPackets.NPCLink("Laissez moi r�fl�chir", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 5)
                                {
                                    string WHPW2 = "";
                                    bool ValidPW2 = false;
                                    for (int i = 14; i < 14 + Data[13]; i++)
                                        WHPW2 += Convert.ToChar(Data[i]);
                                    if (WHPW2 == MyChar.WHPW)
                                        ValidPW2 = true;

                                    try
                                    {
                                        if (ValidPW2)
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous voulez changer le mot de passe ou le supprimer ?"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Changer", 6));
                                            SendPacket(CoServer.MyPackets.NPCLink("Supprimer", 7));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Mauvais mot de passe, r�essayez."));
                                            SendPacket(CoServer.MyPackets.NPCLink("D'accord", 4));
                                            SendPacket(CoServer.MyPackets.NPCLink("Je vois", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
                                }
                                if (Control == 6)
                                {
                                    WHOpen = 0;
                                    SendPacket(CoServer.MyPackets.NPCSay("Entrez votre futur mot de passe de magasinier."));
                                    SendPacket(CoServer.MyPackets.NPCSay(" Entrez seulement des chiffrez !"));
                                    SendPacket(CoServer.MyPackets.NPCSay(" Retenez bien votre mot de passe."));
                                    SendPacket(CoServer.MyPackets.NPCLink2("Cr�er un mot de passe.", 2));
                                    SendPacket(CoServer.MyPackets.NPCLink("Laissez moi r�fl�chir.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 7)
                                {
                                    MyChar.WHPW = "";
                                    MyChar.WHPWcheck = 0;
                                    WHOpen = 0;
                                    SendPacket(CoServer.MyPackets.NPCSay("Mot de passe supprim� avec succ�s !"));
                                    SendPacket(CoServer.MyPackets.NPCLink("Merci", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                            }
                            if (CurrentNPC == 8)
                            {
                                if (Control == 1)
                                {
                                    string WHPW1 = "";
                                    bool ValidPW1 = false;
                                    for (int i = 14; i < 14 + Data[13]; i++)
                                        WHPW1 += Convert.ToChar(Data[i]);

                                    if (WHPW1 == MyChar.WHPW)
                                        ValidPW1 = true;
                                    try
                                    {
                                        if (ValidPW1)
                                        {
                                            SendPacket(CoServer.MyPackets.GeneralData(MyChar, 4, 126));
                                            WHOpen = 1;
                                        }
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Mauvais mot de passe. R�essayer."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 0));
                                            SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
                                }
                            }
                            #endregion
                            #region WH Open
                            if (CurrentNPC == 8 || CurrentNPC == 44 || CurrentNPC == 4101 || CurrentNPC == 10011 || CurrentNPC == 10012 || CurrentNPC == 10027 || CurrentNPC == 10028)
                            {
                                if (Control == 1)
                                {
                                    string WHPW1 = "";
                                    bool ValidPW1 = false;
                                    for (int i = 14; i < 14 + Data[13]; i++)
                                        WHPW1 += Convert.ToChar(Data[i]);

                                    if (WHPW1 == MyChar.WHPW)
                                        ValidPW1 = true;

                                    try
                                    {
                                        if (ValidPW1)
                                        {
                                            SendPacket(CoServer.MyPackets.GeneralData(MyChar, 4, 126));
                                            WHOpen = 1;
                                        }
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Mauvais mot de passe. R�essayer."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 0));
                                            SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
                                }
                            }
                            #endregion
                            #region FirstReborn
                            if (CurrentNPC == 300500)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Si vous voulez rena�tre vous devez atteindre un certain niveau, aussi vous devez avoir le plus haut"));
                                    SendPacket(CoServer.MyPackets.NPCSay(" titre de votre classe et avoir une CelestialStone. Apr�s la renaissance, vous pouvez distribuer"));
                                    SendPacket(CoServer.MyPackets.NPCSay(" votre attribut plus librement. Et vous pouvez apprendre plusieurs comp�tences puissantes."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Qu'es ce qu'une CelestialStone?", 8));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je veux rena�tre.", 7));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 8)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("La CelestialStone est compos� des 7 gemmes du monde. Il me la faut pour que je pr�pare la renaissance."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 7)
                                {
                                    if (MyChar.InventoryContains(721259, 1) && (MyChar.Job == 15 || MyChar.Job == 25 || MyChar.Job == 45 || MyChar.Job == 125 || MyChar.Job == 135 || MyChar.Job == 145 || MyChar.Job == 155))
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("En quel classe voulez-vous rena�tre?"));
                                        SendPacket(CoServer.MyPackets.NPCLink("Brave", 2));
                                        SendPacket(CoServer.MyPackets.NPCLink("Guerrier", 3));
                                        SendPacket(CoServer.MyPackets.NPCLink("Archer", 4));
                                        SendPacket(CoServer.MyPackets.NPCLink("Tao�ste de feu", 5));
                                        SendPacket(CoServer.MyPackets.NPCLink("Tao�ste d'eau", 6));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je dois r�fl�chir.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous ne pouvez pas rena�tre si vous n'avez pas la CelestialStone ou vous n'avez pas fait toutes vos promotions."));
                                        SendPacket(CoServer.MyPackets.NPCLink("je vois.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    MyChar.RemoveItem(MyChar.ItemNext(721259));
                                    MyChar.ReBorn(11);
                                }
                                if (Control == 3)
                                {
                                    MyChar.RemoveItem(MyChar.ItemNext(721259));
                                    MyChar.ReBorn(21);
                                }
                                if (Control == 4)
                                {
                                    MyChar.RemoveItem(MyChar.ItemNext(721259));
                                    MyChar.ReBorn(41);
                                }
                                if (Control == 5)
                                {
                                    MyChar.RemoveItem(MyChar.ItemNext(721259));
                                    MyChar.ReBorn(142);
                                }
                                if (Control == 6)
                                {
                                    MyChar.RemoveItem(MyChar.ItemNext(721259));
                                    MyChar.ReBorn(132);
                                }
                            }
                            #endregion
                            #region SecondReborn
                            if (CurrentNPC == 128)
                            {
                                if (MyChar.RBCount == 1)
                                {
                                    if (MyChar.Level >= 130)
                                    {
                                        if (MyChar.Job == 15 || MyChar.Job == 25 || MyChar.Job == 45 || MyChar.Job == 135 || MyChar.Job == 145)
                                        {
                                            if (Control == 11) //2Rb Troj
                                            {
                                                MyChar.RemoveItem(MyChar.ItemNext(723701));
                                                MyChar.ReBorn(11);
                                            }
                                            if (Control == 21) //2Rb War
                                            {
                                                MyChar.RemoveItem(MyChar.ItemNext(723701));
                                                MyChar.ReBorn(21);
                                            }
                                            if (Control == 41) //2Rb Archer
                                            {
                                                MyChar.RemoveItem(MyChar.ItemNext(723701));
                                                MyChar.ReBorn(41);
                                            }
                                            if (Control == 132) //2Rb Wat
                                            {
                                                MyChar.RemoveItem(MyChar.ItemNext(723701));
                                                MyChar.ReBorn(132);
                                            }
                                            if (Control == 142) //2Rb Fire
                                            {
                                                MyChar.RemoveItem(MyChar.ItemNext(723701));
                                                MyChar.ReBorn(142);
                                            }
                                        }
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous devez rentrer en fonction avant de rena�tre pour la deuxi�me fois!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(6));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous devez �tre niveau 130 minimum!"));
                                        SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(6));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                else
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas accompli la premi�re renaissance. Je ne peux pas vous aider!"));
                                    SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(6));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                            }
                            #endregion
                            if (CurrentNPC == 1000010) //AutoClick
                            {
                                MyChar.AutoClick_Check = false;
                            }

                            if (CurrentNPC == 390) //LoveStone
                            {
                                if (Control == 1)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Cliquez sur votre amour pour lui proposer."));
                                    SendPacket(CoServer.MyPackets.NPCLink("OK", 2));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.Spouse == "Non")
                                        MyChar.MyClient.SendPacket(CoServer.MyPackets.GeneralData(MyChar, 1067, 116));
                                }
                            }
                            if (CurrentNPC == 600055)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Pour le moment j'ai beaucoup de LarmeDeM�t�ore. Mais il me faut des M�t�ores. "));
                                    SendPacket(CoServer.MyPackets.NPCSay("Si vous me donnez une m�t�orite, je peux vous divorcer de votre femme."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Voici une m�t�orite", 2));
                                    SendPacket(CoServer.MyPackets.NPCLink("J'aime ma femme", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.InventoryContains(1088001, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(1088001));
                                        SendPacket(CoServer.MyPackets.NPCSay("C'est bien vous avez une m�t�orite. Mais �tes-vous certain(e) de vouloir divorcer?"));
                                        SendPacket(CoServer.MyPackets.NPCLink("Oui.", 3));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je ne suis pas certain...", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas de m�t�orite... Revenez lorsque vous en aurez."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 3)
                                {
                                    World.SendMsgToAll(MyChar.Name + " et " + MyChar.Spouse + " ont divorc�... Bonne chance lors de votre prochain mariage!", "SYSTEM", 2011);
                                    MyChar.RemoveItem(MyChar.ItemNext(1088001));

                                    bool IsOnline = false;
                                    foreach (Character Player in World.AllChars.Values)
                                    {
                                        if (Player.Name == MyChar.Spouse)
                                        {
                                            IsOnline = true;
                                            Player.Spouse = "Non";
                                            Player.Save();

                                            Player.Screen.SendPacket(CoServer.MyPackets.String(Player.UID, 6, Player.Spouse), true);
                                            break;
                                        }
                                    }
                                    if (!IsOnline)
                                        DataBase.NoSpouse(MyChar.Spouse);

                                    MyChar.Spouse = "Non";
                                    MyChar.Save();

                                    MyChar.Screen.SendPacket(CoServer.MyPackets.String(MyChar.UID, 6, MyChar.Spouse), true);
                                }
                            }
                            if (CurrentNPC == 701700)
                            {
                                if (Control == 1 && MyChar.MyGuild != null && MyChar.MyGuild == World.PoleHolder && MyChar.GuildPosition == 100)
                                {
                                    //uint HealPole = 0;
                                    foreach (KeyValuePair<uint, SingleNPC> DE in NPCs.AllNPCs)
                                    {
                                        SingleNPC Npcc = (SingleNPC)DE.Value;
                                        if (Npcc.Sob == 2)
                                        {
                                            /*HealPole = Npcc.MaxHP - Npcc.CurHP;
                                            if (MyChar.MyGuild.Fund > HealPole)
                                            {
                                                MyChar.MyGuild.Fund -= HealPole;
                                                World.RemoveEntity(Npcc);
                                                Npcc.CurHP = Npcc.MaxHP;
                                                World.SpawnNPC(Npcc);
                                                MyChar.MyGuild.Refresh(MyChar);
                                                SendPacket(CoServer.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                                World.SendMsgToAll(MyChar.Name + " des " + MyChar.MyGuild.Name + " a r�par� le Pole avec l'argent de ses capitaux!", "SYSTEM", 2011);
                                            }*/
                                            if (MyChar.CPs >= 100000)
                                            {
                                                MyChar.CPs -= 100000;
                                                World.RemoveEntity(Npcc);
                                                Npcc.CurHP = Npcc.MaxHP;
                                                World.SpawnNPC(Npcc);
                                                World.SendMsgToAll(MyChar.Name + " des " + MyChar.MyGuild.Name + " a r�par� le Pole avec 100'000 CPs!", "SYSTEM", 2011);
                                            }
                                        }
                                    }
                                }
                            }
                            if (CurrentNPC == 701701 || CurrentNPC == 701702)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.MyGuild != null && MyChar.MyGuild == World.PoleHolder && (MyChar.GuildPosition == 100 || MyChar.GuildPosition == 90))
                                    {
                                        SingleNPC Gate = (SingleNPC)NPCs.AllNPCs[(uint)CurrentNPC];
                                        if (Gate.Type == 240 && World.LGateDead == false)
                                            Gate.Type += 10;
                                        else if (Gate.Type == 270 && World.RGateDead == false)
                                            Gate.Type += 10;
                                        else if (Gate.Type == 250 && World.LGateDead == false)
                                            Gate.Type -= 10;
                                        else if (Gate.Type == 280 && World.RGateDead == false)
                                            Gate.Type -= 10;
                                        else if (MyChar.GuildPosition == 100 && MyChar.MyGuild.Fund >= 300000 && World.LGateDead == true || World.RGateDead == true)
                                        {
                                            if (Gate.Type == 240)
                                            {
                                                Gate.Type += 10;
                                                World.LGateDead = false;
                                            }
                                            else if (Gate.Type == 270)
                                            {
                                                Gate.Type += 10;
                                                World.RGateDead = false;
                                            }
                                            else if (Gate.Type == 250)
                                            {
                                                Gate.Type -= 10;
                                                World.LGateDead = false;
                                            }
                                            else if (Gate.Type == 280)
                                            {
                                                Gate.Type -= 10;
                                                World.RGateDead = false;
                                            }

                                            Gate.CurHP = Gate.MaxHP;
                                            MyChar.MyGuild.Fund -= 300000;
                                            MyChar.MyClient.SendPacket(CoServer.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                        }
                                        World.RemoveEntity(Gate);
                                        World.SpawnNPC(Gate);
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Juste la guilde poss�dant le Pole peut contr�ler les portes."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.MyGuild != null && MyChar.MyGuild == World.PoleHolder)
                                    {
                                        if (CurrentNPC == 701701)
                                            MyChar.Teleport(1038, 160, 190);
                                        if (CurrentNPC == 701702)
                                            MyChar.Teleport(1038, 200, 175);
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Juste la guilde poss�dant le Pole peut contr�ler les portes."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 5004)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Je peux empacter vos m�t�ore et vos Perle de Dragon pour vous. Donnez moi 10 m�t�ores ou 10 Perles de Dragon et je vais vous faire un rouleau de m�t�ores ou un rouleau de Perles de Dragon."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Empactez mes m�t�ores", 2));
                                    SendPacket(CoServer.MyPackets.NPCLink("Empactez mes Perles de Dragon", 3));
                                    SendPacket(CoServer.MyPackets.NPCLink("Empactez mes rouleaux de m�t�ores", 6));
                                    SendPacket(CoServer.MyPackets.NPCLink("Empactez mes rouleaux de Perles de Dragon", 7));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je veux 4 rouleaux de m�t�ores pour ma Perle de Dragon.", 4));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je veux une Perle de Dragon pour mes 6 rouleaux de m�t�ores.", 5));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.InventoryContains(1088001, 10))
                                    {
                                        for (byte i = 0; i < 10; i++)
                                            MyChar.RemoveItem(MyChar.ItemNext(1088001));
                                        MyChar.AddItem("720027-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                    }
                                }
                                if (Control == 3)
                                {
                                    if (MyChar.InventoryContains(1088000, 10))
                                    {
                                        for (byte i = 0; i < 10; i++)
                                            MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                        MyChar.AddItem("720028-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                    }
                                }
                                if (Control == 4)
                                {
                                    if (MyChar.InventoryContains(1088000, 1) && MyChar.ItemsInInventory < 37)
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                        MyChar.AddItem("720027-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                        MyChar.AddItem("720027-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                        MyChar.AddItem("720027-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                        MyChar.AddItem("720027-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                    }
                                }
                                if (Control == 5)
                                {
                                    if (MyChar.InventoryContains(720027, 6))
                                    {
                                        for (byte i = 0; i < 6; i++)
                                            MyChar.RemoveItem(MyChar.ItemNext(720027));
                                        MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                    }
                                }
                                if (Control == 6)
                                {
                                    if (MyChar.InventoryContains(720027, 10))
                                    {
                                        for (byte i = 0; i < 10; i++)
                                            MyChar.RemoveItem(MyChar.ItemNext(720027));
                                        MyChar.AddItem("722059-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                    }
                                }
                                if (Control == 7)
                                {
                                    if (MyChar.InventoryContains(720028, 10))
                                    {
                                        for (byte i = 0; i < 10; i++)
                                            MyChar.RemoveItem(MyChar.ItemNext(720028));
                                        MyChar.AddItem("722060-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                    }
                                }
                            }
                            if (CurrentNPC == 2065)
                            {
                                if (Control <= 8 && Control >= 1)
                                {
                                    GemId = (byte)Control;
                                    SendPacket(CoServer.MyPackets.NPCSay("Quelle qualit�e de gemme voulez vous ?"));
                                    SendPacket(CoServer.MyPackets.NPCLink("Une gemme raffin�e", 9));
                                    SendPacket(CoServer.MyPackets.NPCLink("Une gemme super", 10));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 9)
                                {
                                    uint TheGem = (uint)(700001 + (GemId - 1) * 10);

                                    if (MyChar.InventoryContains(TheGem, 15))
                                    {
                                        for (int i = 0; i < 15; i++)
                                            MyChar.RemoveItem(MyChar.ItemNext(TheGem));
                                        MyChar.AddItem((TheGem + 1).ToString() + "-0-0-0-0-0", 0, (uint)Program.Rand.Next(364573656));
                                    }
                                }
                                if (Control == 10)
                                {
                                    uint TheGem = (uint)(700002 + (GemId - 1) * 10);

                                    if (MyChar.InventoryContains(TheGem, 20))
                                    {
                                        for (int i = 0; i < 20; i++)
                                            MyChar.RemoveItem(MyChar.ItemNext(TheGem));
                                        MyChar.AddItem((TheGem + 1).ToString() + "-0-0-0-0-0", 0, (uint)Program.Rand.Next(364573656));
                                    }
                                }
                            }
                            if (CurrentNPC == 7050)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Il me faut 1 Perle de Dragon si l'objet est inf�rieur au niveau 130. Si vous voulez monter un objet � un niveau sup�rieur, il me faudra de la Citrine ou un Oeil De Tigre selon le niveau."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control <= 7 && Control >= 2)
                                {
                                    if (!UppAgree)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Il me faut une PerleDeDragon si votre �quipement est inf�rieur au niveau 130."));
                                        SendPacket(CoServer.MyPackets.NPCSay(" Sinon, il me faut une pierre de Citrine pour un �quipement de niveau inf�rieur � 135 et un Oeil De Tigre pour un �quipement de niveau inf�rieur � 140."));
                                        SendPacket(CoServer.MyPackets.NPCSay(" Pour un objet de niveau 140, il me faudra 3 pierres de Citrine et 3 Oeil De Tigre."));
                                        SendPacket(CoServer.MyPackets.NPCLink("J'ai ce qu'il faut.", (byte)Control));
                                        SendPacket(CoServer.MyPackets.NPCLink("Pas maintenant.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                        UppAgree = true;
                                    }
                                    else
                                    {
                                        byte Pos = 0;

                                        if (Control == 2)
                                            Pos = 4;
                                        if (Control == 3)
                                            Pos = 2;
                                        if (Control == 4)
                                            Pos = 6;
                                        if (Control == 5)
                                            Pos = 8;
                                        if (Control == 6)
                                            Pos = 3;
                                        if (Control == 7)
                                            Pos = 1;

                                        string[] Splitter = MyChar.Equips[Pos].Split('-');
                                        uint ItemId = uint.Parse(Splitter[0]);

                                        if (!Other.Upgradable(ItemId))
                                            return;
                                        if (!Other.EquipMaxedLvl(ItemId))
                                        {
                                            if (MyChar.Level >= Other.ItemInfo(Other.EquipNextLevel(ItemId)).Level)
                                            {
                                                if (Other.ItemInfo(ItemId).Level >= 120 && Other.ItemInfo(Other.EquipNextLevel(ItemId)).Level <= 130)
                                                {
                                                    if (MyChar.InventoryContains(1088000, 1))
                                                    {
                                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));

                                                        ItemId = Other.EquipNextLevel(ItemId);

                                                        MyChar.GetEquipStats(Pos, true);
                                                        MyChar.Equips[Pos] = ItemId.ToString() + "-" + Splitter[1] + "-" + Splitter[2] + "-" + Splitter[3] + "-" + Splitter[4] + "-" + Splitter[5];
                                                        MyChar.GetEquipStats(Pos, false);

                                                        SendPacket(CoServer.MyPackets.AddItem((long)MyChar.Equips_UIDs[Pos], (int)ItemId, byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), Pos, 100, 100));

                                                        SendPacket(CoServer.MyPackets.NPCSay("Votre objet a �t� am�lior�!"));
                                                        SendPacket(CoServer.MyPackets.NPCLink("Merci beaucoup!", 255));
                                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                                    }
                                                    else
                                                    {
                                                        SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas de Perle de Dragon"));
                                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                                    }
                                                }
                                                else if (Other.ItemInfo(ItemId).Level >= 120 && Other.ItemInfo(Other.EquipNextLevel(ItemId)).Level >= 131 && Other.ItemInfo(Other.EquipNextLevel(ItemId)).Level < 135)
                                                {
                                                    if (MyChar.InventoryContains(1088003, 1))
                                                    {
                                                        MyChar.RemoveItem(MyChar.ItemNext(1088003));

                                                        ItemId = Other.EquipNextLevel(ItemId);

                                                        MyChar.GetEquipStats(Pos, true);
                                                        MyChar.Equips[Pos] = ItemId.ToString() + "-" + Splitter[1] + "-" + Splitter[2] + "-" + Splitter[3] + "-" + Splitter[4] + "-" + Splitter[5];
                                                        MyChar.GetEquipStats(Pos, false);

                                                        SendPacket(CoServer.MyPackets.AddItem((long)MyChar.Equips_UIDs[Pos], (int)ItemId, byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), Pos, 100, 100));

                                                        SendPacket(CoServer.MyPackets.NPCSay("Votre objet a �t� am�lior�!"));
                                                        SendPacket(CoServer.MyPackets.NPCLink("Merci beaucoup!", 255));
                                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                                    }
                                                    else
                                                    {
                                                        SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas de pierre de Citrine!"));
                                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                                    }
                                                }
                                                else if (Other.ItemInfo(ItemId).Level >= 120 && Other.ItemInfo(Other.EquipNextLevel(ItemId)).Level >= 135 && Other.ItemInfo(Other.EquipNextLevel(ItemId)).Level < 140)
                                                {
                                                    if (MyChar.InventoryContains(1088004, 1))
                                                    {
                                                        MyChar.RemoveItem(MyChar.ItemNext(1088004));

                                                        ItemId = Other.EquipNextLevel(ItemId);

                                                        MyChar.GetEquipStats(Pos, true);
                                                        MyChar.Equips[Pos] = ItemId.ToString() + "-" + Splitter[1] + "-" + Splitter[2] + "-" + Splitter[3] + "-" + Splitter[4] + "-" + Splitter[5];
                                                        MyChar.GetEquipStats(Pos, false);

                                                        SendPacket(CoServer.MyPackets.AddItem((long)MyChar.Equips_UIDs[Pos], (int)ItemId, byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), Pos, 100, 100));

                                                        SendPacket(CoServer.MyPackets.NPCSay("Votre objet a �t� am�lior�!"));
                                                        SendPacket(CoServer.MyPackets.NPCLink("Merci beaucoup!", 255));
                                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                                    }
                                                    else
                                                    {
                                                        SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas d'Oeil De Tigre!"));
                                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                                    }
                                                }
                                                else if (Other.ItemInfo(ItemId).Level >= 120 && Other.ItemInfo(Other.EquipNextLevel(ItemId)).Level == 140)
                                                {
                                                    if (MyChar.InventoryContains(1088003, 3) && MyChar.InventoryContains(1088004, 3))
                                                    {
                                                        MyChar.RemoveItem(MyChar.ItemNext(1088003));
                                                        MyChar.RemoveItem(MyChar.ItemNext(1088003));
                                                        MyChar.RemoveItem(MyChar.ItemNext(1088003));
                                                        MyChar.RemoveItem(MyChar.ItemNext(1088004));
                                                        MyChar.RemoveItem(MyChar.ItemNext(1088004));
                                                        MyChar.RemoveItem(MyChar.ItemNext(1088004));

                                                        ItemId = Other.EquipNextLevel(ItemId);

                                                        MyChar.GetEquipStats(Pos, true);
                                                        MyChar.Equips[Pos] = ItemId.ToString() + "-" + Splitter[1] + "-" + Splitter[2] + "-" + Splitter[3] + "-" + Splitter[4] + "-" + Splitter[5];
                                                        MyChar.GetEquipStats(Pos, false);

                                                        SendPacket(CoServer.MyPackets.AddItem((long)MyChar.Equips_UIDs[Pos], (int)ItemId, byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), Pos, 100, 100));

                                                        SendPacket(CoServer.MyPackets.NPCSay("Votre objet a �t� am�lior�!"));
                                                        SendPacket(CoServer.MyPackets.NPCLink("Merci beaucoup!", 255));
                                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                                    }
                                                    else
                                                    {
                                                        SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas trois pierres de Citrine et trois Oeil De Tigre!"));
                                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                                    }
                                                }
                                                else
                                                {
                                                    SendPacket(CoServer.MyPackets.NPCSay("Votre �quipement est en dessous de 120, ne venez pas ici gaspiller une Perle de Dragon."));
                                                    SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                                }
                                            }
                                            else
                                            {
                                                SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                                SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                                SendPacket(CoServer.MyPackets.NPCFinish());
                                            }
                                        }
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Votre �quipement est d�j� a sont plus grand niveau."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                }
                            }
                            if (CurrentNPC == 10062)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Ah, excellent. Augmenter la qualit�e d'un objet am�liore ses comp�tences. Que voulez vous upgrader ?"));
                                    SendPacket(CoServer.MyPackets.NPCLink("Monter le coronet, casque, boucles.", 3));
                                    SendPacket(CoServer.MyPackets.NPCLink("Monter le collier de qualit�.", 4));
                                    SendPacket(CoServer.MyPackets.NPCLink("Monter l'armure de qualit�.", 5));
                                    SendPacket(CoServer.MyPackets.NPCLink("Monter la qualit� de l'arme.", 6));
                                    SendPacket(CoServer.MyPackets.NPCLink("Monter la qualit� de la bague.", 7));
                                    SendPacket(CoServer.MyPackets.NPCLink("Monter la qualit� des bottes.", 8));
                                    SendPacket(CoServer.MyPackets.NPCLink("Monter la qualit� du bouclier.", 9));
                                    SendPacket(CoServer.MyPackets.NPCLink("Au revoir.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                    UppAgree = false;
                                }
                                if (Control == 2)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Je vois, monter le niveau d'un objet pour le rendre plus puissant. C'est tr�s dure de faire cela. Que monter de niveau ?"));
                                    SendPacket(CoServer.MyPackets.NPCLink("Casque, boucle, coronet", 10));
                                    SendPacket(CoServer.MyPackets.NPCLink("Monter le niveau du collier.", 11));
                                    SendPacket(CoServer.MyPackets.NPCLink("Monter le niveau de l'armure.", 12));
                                    SendPacket(CoServer.MyPackets.NPCLink("Monter le niveau de l'arme.", 13));
                                    SendPacket(CoServer.MyPackets.NPCLink("Monter le niveau de la bague, anneau.", 14));
                                    SendPacket(CoServer.MyPackets.NPCLink("Monter le niveau des bottes.", 15));
                                    SendPacket(CoServer.MyPackets.NPCLink("Monter le niveau du bouclier.", 16));
                                    SendPacket(CoServer.MyPackets.NPCLink("Au revoir.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                    UppAgree = false;
                                }
                                if (Control <= 9 && Control >= 3)
                                {
                                    string TheEquip = "";

                                    if (Control == 3)
                                        TheEquip = MyChar.Equips[1];
                                    if (Control == 4)
                                        TheEquip = MyChar.Equips[2];
                                    if (Control == 5)
                                        TheEquip = MyChar.Equips[3];
                                    if (Control == 6)
                                        TheEquip = MyChar.Equips[4];
                                    if (Control == 7)
                                        TheEquip = MyChar.Equips[6];
                                    if (Control == 8)
                                        TheEquip = MyChar.Equips[8];
                                    if (Control == 9)
                                        TheEquip = MyChar.Equips[5];

                                    byte Pos = 0;

                                    if (Control == 3)
                                        Pos = 1;
                                    if (Control == 4)
                                        Pos = 2;
                                    if (Control == 5)
                                        Pos = 3;
                                    if (Control == 6)
                                        Pos = 4;
                                    if (Control == 7)
                                        Pos = 6;
                                    if (Control == 8)
                                        Pos = 8;
                                    if (Control == 9)
                                        Pos = 5;


                                    string[] Splitter = TheEquip.Split('-');
                                    uint ItemId = uint.Parse(Splitter[0]);

                                    if (!Other.Upgradable(ItemId) || ItemHandler.ItemQuality(ItemId) == 9)
                                        return;

                                    if (ItemId == 410301 || ItemId == 410302 || ItemId == 500301 || ItemId == 421301)
                                        return;

                                    byte RequiredDBs = 0;
                                    RequiredDBs = (byte)(Other.ItemInfo(ItemId).Level / 20);
                                    if (RequiredDBs == 0)
                                        RequiredDBs = 1;

                                    if (ItemHandler.ItemQuality(ItemId) == 6)
                                        RequiredDBs += 2;
                                    if (ItemHandler.ItemQuality(ItemId) == 7)
                                        RequiredDBs += 3;
                                    if (ItemHandler.ItemQuality(ItemId) == 8)
                                        RequiredDBs += 4;

                                    if (!UppAgree)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous avez besoin de " + RequiredDBs + " Perles de Dragon pour l'am�liorer. Voulez-vous l'am�liorer?"));
                                        SendPacket(CoServer.MyPackets.NPCLink("Oui", (byte)Control));
                                        SendPacket(CoServer.MyPackets.NPCLink("Non, j'ai chang� d'avis.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        if (MyChar.InventoryContains(1088000, RequiredDBs))
                                        {
                                            for (int i = 0; i < RequiredDBs; i++)
                                            {
                                                MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                            }

                                            if (ItemHandler.ItemQuality(ItemId) < 6)
                                                ItemId = ItemHandler.ChangeQuality(ItemId, 6);
                                            else
                                                ItemId++;

                                            MyChar.GetEquipStats(Pos, true);
                                            MyChar.Equips[Pos] = ItemId.ToString() + "-" + Splitter[1] + "-" + Splitter[2] + "-" + Splitter[3] + "-" + Splitter[4] + "-" + Splitter[5];
                                            MyChar.GetEquipStats(Pos, false);

                                            SendPacket(CoServer.MyPackets.AddItem((long)MyChar.Equips_UIDs[Pos], (int)ItemId, byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), Pos, 100, 100));
                                        }
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas assez de Perles de Dragon."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }

                                    UppAgree = true;
                                }
                                if (Control <= 16 && Control >= 10)
                                {
                                    string TheEquip = "";

                                    if (Control == 10)
                                        TheEquip = MyChar.Equips[1];
                                    if (Control == 11)
                                        TheEquip = MyChar.Equips[2];
                                    if (Control == 12)
                                        TheEquip = MyChar.Equips[3];
                                    if (Control == 13)
                                        TheEquip = MyChar.Equips[4];
                                    if (Control == 14)
                                        TheEquip = MyChar.Equips[6];
                                    if (Control == 15)
                                        TheEquip = MyChar.Equips[8];
                                    if (Control == 16)
                                        TheEquip = MyChar.Equips[5];

                                    byte Pos = 0;

                                    if (Control == 10)
                                        Pos = 1;
                                    if (Control == 11)
                                        Pos = 2;
                                    if (Control == 12)
                                        Pos = 3;
                                    if (Control == 13)
                                        Pos = 4;
                                    if (Control == 14)
                                        Pos = 6;
                                    if (Control == 15)
                                        Pos = 8;
                                    if (Control == 16)
                                        Pos = 5;

                                    string[] Splitter = TheEquip.Split('-');
                                    uint ItemId = uint.Parse(Splitter[0]);

                                    if (!Other.Upgradable(ItemId))
                                        return;

                                    if (ItemId == 410301 || ItemId == 410302 || ItemId == 500301 || ItemId == 421301)
                                        return;

                                    byte RequiredMets = 0;
                                    if ((Other.ItemInfo(ItemId).Level < 120 && ItemHandler.ItemType2(ItemId) != 90) || (Other.ItemInfo(ItemId).Level < 110))
                                    {
                                        RequiredMets = (byte)(Other.ItemInfo(ItemId).Level / 10);
                                        if (RequiredMets == 0)
                                            RequiredMets = 1;
                                    }
                                    if (RequiredMets != 0)
                                    {
                                        if (ItemHandler.ItemQuality(ItemId) < 7)
                                            RequiredMets = 2;
                                        if (ItemHandler.ItemQuality(ItemId) == 7)
                                            RequiredMets = (byte)(2 + RequiredMets / 5);
                                        if (ItemHandler.ItemQuality(ItemId) == 8)
                                            RequiredMets = (byte)(RequiredMets * 2.6);
                                        if (ItemHandler.ItemQuality(ItemId) == 9)
                                            RequiredMets = (byte)(RequiredMets * 3.1);
                                    }

                                    if (RequiredMets != 0)
                                    {
                                        if (!UppAgree)
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Il vous faut " + RequiredMets + " m�t�orites pour que vous am�liorez l'objet. Voulez-vous quand m�me?"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Oui", (byte)Control));
                                            SendPacket(CoServer.MyPackets.NPCLink("Non, j'ai chang� d'avis.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            if (MyChar.InventoryContains(1088001, RequiredMets) && MyChar.Level >= Other.ItemInfo(Other.EquipNextLevel(ItemId)).Level)
                                            {
                                                ItemId = Other.EquipNextLevel(ItemId);

                                                for (int i = 0; i < RequiredMets; i++)
                                                {
                                                    MyChar.RemoveItem(MyChar.ItemNext(1088001));
                                                }

                                                MyChar.GetEquipStats(Pos, true);
                                                MyChar.Equips[Pos] = ItemId.ToString() + "-" + Splitter[1] + "-" + Splitter[2] + "-" + Splitter[3] + "-" + Splitter[4] + "-" + Splitter[5];
                                                MyChar.GetEquipStats(Pos, false);

                                                SendPacket(CoServer.MyPackets.AddItem((long)MyChar.Equips_UIDs[Pos], (int)ItemId, byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), Pos, 100, 100));

                                                SendPacket(CoServer.MyPackets.NPCSay("Votre objet a �t� am�lior� avec succ�s."));
                                                SendPacket(CoServer.MyPackets.NPCLink("Merci beaucoup!", 255));
                                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                                SendPacket(CoServer.MyPackets.NPCFinish());
                                            }
                                            else
                                            {
                                                SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas assez de m�t�ores ou vous n'avez pas le niveau requis pour le prochain niveau de l'objet."));
                                                SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                                SendPacket(CoServer.MyPackets.NPCFinish());
                                            }

                                            UppAgree = false;
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Je ne peux plus am�liorer votre objet. Il est � son plus haut niveau."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    UppAgree = true;
                                }
                            }

                            if (CurrentNPC > 600500 && CurrentNPC < 600505)
                                if (Control == 1)
                                {
                                    if (MyChar.Silvers >= 500)
                                    {
                                        MyChar.Teleport(1038, 350, 339);
                                        MyChar.Silvers -= 500;
                                    }
                                }
                            if (CurrentNPC == 600505)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.Silvers >= 1000)
                                        if (DataBase.GC1Map != 0)
                                        {
                                            MyChar.Teleport(DataBase.GC1Map, (ushort)(DataBase.GC1X - 2), DataBase.GC1Y);
                                            MyChar.Silvers -= 1000;
                                        }
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.GuildPosition == 100 && MyChar.MyGuild.Fund >= 50000)
                                    {
                                        MyChar.MyGuild.Fund -= 50000;
                                        MyChar.MyClient.SendPacket(CoServer.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                        MyChar.AddItem("720021-0-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                                    }
                                }
                            }
                            if (CurrentNPC == 600506)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.Silvers >= 1000)
                                        if (DataBase.GC2Map != 0)
                                        {
                                            MyChar.Teleport(DataBase.GC2Map, (ushort)(DataBase.GC2X - 2), DataBase.GC2Y);
                                            MyChar.Silvers -= 1000;
                                        }
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.GuildPosition == 100 && MyChar.MyGuild.Fund >= 50000)
                                    {
                                        MyChar.MyGuild.Fund -= 50000;
                                        MyChar.MyClient.SendPacket(CoServer.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                        MyChar.AddItem("720022-0-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                                    }
                                }
                            }
                            if (CurrentNPC == 600507)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.Silvers >= 1000)
                                        if (DataBase.GC3Map != 0)
                                        {
                                            MyChar.Teleport(DataBase.GC3Map, (ushort)(DataBase.GC3X - 2), DataBase.GC3Y);
                                            MyChar.Silvers -= 1000;
                                        }
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.GuildPosition == 100 && MyChar.MyGuild.Fund >= 50000)
                                    {
                                        MyChar.MyGuild.Fund -= 50000;
                                        MyChar.MyClient.SendPacket(CoServer.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                        MyChar.AddItem("720023-0-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                                    }
                                }
                            }
                            if (CurrentNPC == 600508)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.Silvers >= 1000)
                                        if (DataBase.GC4Map != 0)
                                        {
                                            MyChar.Teleport(DataBase.GC4Map, (ushort)(DataBase.GC4X - 2), DataBase.GC4Y);
                                            MyChar.Silvers -= 1000;
                                        }
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.GuildPosition == 100 && MyChar.MyGuild.Fund >= 50000)
                                    {
                                        MyChar.MyGuild.Fund -= 50000;
                                        MyChar.MyClient.SendPacket(CoServer.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                        MyChar.AddItem("720024-0-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                                    }
                                }
                            }
                            if (CurrentNPC == 10054)
                                if (Control == 1)
                                    MyChar.Teleport(1000, 971, 666);

                            if (CurrentNPC == 10003)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.MyGuild == null && MyChar.GuildPosition == 0)
                                    {
                                        if (MyChar.Silvers >= 1000000)
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Comment voulez-vous nommer votre guilde?"));
                                            SendPacket(CoServer.MyPackets.NPCLink2("Elle s'appelle", 200));
                                            SendPacket(CoServer.MyPackets.NPCLink("Je veux r�fl�chir.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas assez d'argent, la cr�ation d'une guilde co�te 1 million d'argent."));
                                            SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous devez quitter votre guilde avant d'en cr�er une."));
                                        SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("R�fl�chissez, si vous d�groupez votre guilde, il sera impossible de la reconstituer."));
                                    SendPacket(CoServer.MyPackets.NPCLink("D�grouper", 201));
                                    SendPacket(CoServer.MyPackets.NPCLink("Ne pas d�grouper", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 3)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Une guilde forte a besoin d'un fond suffisant. Combien d'argent voulez-vous donnez?"));
                                    SendPacket(CoServer.MyPackets.NPCLink2("Je veux donner", 202));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je dois r�fl�chir.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 4)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Le chef est le responsable de la guilde. Vous devez r�fl�chir et choisir une personne convenable."));
                                    SendPacket(CoServer.MyPackets.NPCLink2("J'abdique le pouvoir �..", 203));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je dois r�fl�chir.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 5)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("La guilde a besoin non seulement d'un chef mais aussi d'un sous-chef. Qui voulez-vous commettre sous-chef?"));
                                    SendPacket(CoServer.MyPackets.NPCLink2("Je commet..", 204));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je dois r�fl�chir.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 6)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Pour le d�veloppement de la guilde, vous devez donner les fonctions aux talents!"));
                                    SendPacket(CoServer.MyPackets.NPCLink2("D�mettre la fonction de", 205));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je dois r�fl�chir.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 7)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("De quelle guilde voulez-vous savoir les renseignements?"));
                                    SendPacket(CoServer.MyPackets.NPCLink2("Le nom de guilde", 206));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je dois r�fl�chir.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 8)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Je m'occupe des guildes de la Ville Dragon. Lorsque vous avez des questions concernant les guildes, vous pouvez me le demander."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Noter des ennemis", 9));
                                    SendPacket(CoServer.MyPackets.NPCLink("R�concilier avec une guilde", 10));
                                    SendPacket(CoServer.MyPackets.NPCLink("Coalition de guilde", 11));
                                    SendPacket(CoServer.MyPackets.NPCLink("Annuler la coalition", 12));
                                    SendPacket(CoServer.MyPackets.NPCLink("Liste de guilde", 13));
                                    SendPacket(CoServer.MyPackets.NPCLink("Liste des membres en ligne", 14));
                                    SendPacket(CoServer.MyPackets.NPCLink("Exclure membre", 15));
                                    SendPacket(CoServer.MyPackets.NPCLink("Autres options", 16));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 9)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Quelle guilde est votre ennemi?"));
                                    SendPacket(CoServer.MyPackets.NPCLink2("Le nom de guilde", 207));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je dois r�fl�chir.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 10)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Vous voulez r�concilier avec quelle guilde?"));
                                    SendPacket(CoServer.MyPackets.NPCLink2("Le nom de ligue antagoniste", 208));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je dois r�fl�chir.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 11)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("La coalition aide votre guilde � d�velopper. Apr�s que les deux chefs de guilde sont dans une �quipe, ils peuvent se coaliser."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Ok", 209));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je dois r�fl�chir.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 12)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Vous voulez annuler la coalition avec quelle guilde?"));
                                    SendPacket(CoServer.MyPackets.NPCLink2("Le nom de guilde alli�e", 210));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je dois r�fl�chir.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 13)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Not implemented yet."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Ok", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 14)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Not implemented yet."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Ok", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 15)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Quel membre voulez-vous exclure?"));
                                    SendPacket(CoServer.MyPackets.NPCLink2("Nom de membre", 211));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je veux r�fl�chir un peu", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 16)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Je m'occupe des guildes de la Ville Dragon. Lorsque vous avez des questions concernant les guildes, vous pouvez me le demander."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Cr�er une filiade", 17));
                                    SendPacket(CoServer.MyPackets.NPCLink("D�signer un directeur de filiade", 18));
                                    SendPacket(CoServer.MyPackets.NPCLink("Fusioner les fonds", 19));
                                    SendPacket(CoServer.MyPackets.NPCLink("Autres options", 20));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 17)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Not implemented yet."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Ok", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 18)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Not implemented yet."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Ok", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 19)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Not implemented yet."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Ok", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 20)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Je m'occupe des guildes de la Ville Dragon. Lorsque vous avez des questions concernant les guildes, vous pouvez me le demander."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Cr�er une guilde", 1));
                                    SendPacket(CoServer.MyPackets.NPCLink("D�grouper la guilde", 2));
                                    SendPacket(CoServer.MyPackets.NPCLink("Donation de guilde", 3));
                                    SendPacket(CoServer.MyPackets.NPCLink("Chef abdique", 4));
                                    SendPacket(CoServer.MyPackets.NPCLink("Commettre � sous-chef", 5));
                                    SendPacket(CoServer.MyPackets.NPCLink("D�charger", 6));
                                    SendPacket(CoServer.MyPackets.NPCLink("Consulter une guilde", 7));
                                    SendPacket(CoServer.MyPackets.NPCLink("Autres options", 8));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 200)
                                {
                                    if (MyChar.Level >= 90)
                                    {
                                        string GuildName = "";
                                        for (int i = 14; i < 14 + Data[13]; i++)
                                        {
                                            GuildName += Convert.ToChar(Data[i]);
                                        }

                                        foreach (Guild TheGuild in Guilds.AllGuilds.Values)
                                        {
                                            if (TheGuild.Name == GuildName)
                                            {
                                                SendPacket(CoServer.MyPackets.NPCSay("D�soler... Ce nom de guilde existe d�j�!"));
                                                SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                                SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                                SendPacket(CoServer.MyPackets.NPCFinish());
                                                return;
                                            }
                                        }

                                        MyChar.Silvers -= 1000000;

                                        ushort GuildID = (ushort)Program.Rand.Next(1, ushort.MaxValue);
                                        while (Guilds.AllGuilds.ContainsKey(GuildID))
                                            GuildID = (ushort)Program.Rand.Next(1, ushort.MaxValue);

                                        Guilds.NewGuild(GuildID, GuildName, MyChar);
                                        MyChar.GuildID = GuildID;
                                        MyChar.GuildPosition = 100;
                                        MyChar.GuildDonation = 500000;
                                        MyChar.MyGuild = (Guild)Guilds.AllGuilds[GuildID];

                                        SendPacket(CoServer.MyPackets.GuildName(MyChar.GuildID, MyChar.MyGuild.Name));
                                        SendPacket(CoServer.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                        SendPacket(CoServer.MyPackets.GuildName(MyChar.GuildID, MyChar.MyGuild.Name));

                                        MyChar.Screen.SendScreen(true);
                                        MyChar.Screen.Clear();
                                        MyChar.Screen.SendScreen(false);
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous devez �tre niveau 90 pour cr�er une guilde."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 201)
                                {
                                    if (MyChar.GuildPosition == 100)
                                        MyChar.MyGuild.Disband(false);
                                }
                                if (Control == 202)
                                {
                                    if (MyChar.MyGuild != null)
                                    {
                                        string Donate = "";
                                        for (int i = 14; i < 14 + Data[13]; i++)
                                        {
                                            Donate += Convert.ToChar(Data[i]);
                                        }
                                        uint Amount = uint.Parse(Donate);
                                        if (MyChar.Silvers >= Amount)
                                        {
                                            MyChar.MyGuild.Fund += Amount;
                                            MyChar.Silvers -= Amount;
                                            MyChar.GuildDonation += (int)Amount;
                                            MyChar.MyGuild.Refresh(MyChar.UID, (int)MyChar.GuildDonation);
                                            SendPacket(CoServer.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                            World.SendMsgToAll(MyChar.Name + " a donn� " + Amount.ToString() + " argents � " + MyChar.MyGuild.Name + ".", "SYSTEM", 2005);
                                        }
                                    }
                                }
                                if (Control == 203)
                                {
                                    if (MyChar.GuildPosition == 100)
                                    {
                                        string Name = "";
                                        for (int i = 14; i < 14 + Data[13]; i++)
                                        {
                                            Name += Convert.ToChar(Data[i]);
                                        }

                                        uint CharID = 0;

                                        foreach (GuildMember Deputy in MyChar.MyGuild.Deputies.Values)
                                        {
                                            if (Deputy.Name == Name)
                                                CharID = Deputy.UniqId;
                                        }

                                        foreach (GuildMember Member in MyChar.MyGuild.Members.Values)
                                        {
                                            if (Member.Name == Name)
                                                CharID = Member.UniqId;
                                        }

                                        if (World.AllChars.ContainsKey(CharID))
                                        {
                                            if (MyChar.MyGuild.Members.ContainsKey(CharID))
                                                MyChar.MyGuild.Members.Remove(CharID);
                                            if (MyChar.MyGuild.Deputies.ContainsKey(CharID))
                                                MyChar.MyGuild.Deputies.Remove(CharID);
                                            Character Char = (Character)World.AllChars[CharID];
                                            Char.GuildPosition = 100;

                                            MyChar.MyGuild.Leader = new GuildMember().Create(Char);

                                            MyChar.MyGuild.Members.Add(MyChar.UID, new GuildMember().Create(MyChar));
                                            MyChar.GuildPosition = 50;

                                            SendPacket(CoServer.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                            Char.MyClient.SendPacket(CoServer.MyPackets.GuildInfo(Char.MyGuild, Char));
                                        }
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Le joueur doit �tre en-ligne et dans votre guilde."));
                                            SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Seul le chef de guilde peut commettre un chef."));
                                        SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 204)
                                {
                                    if (MyChar.GuildPosition == 100)
                                    {
                                        if (MyChar.MyGuild.Deputies.Count <= 5)
                                        {
                                            string Name = "";
                                            for (int i = 14; i < 14 + Data[13]; i++)
                                            {
                                                Name += Convert.ToChar(Data[i]);
                                            }

                                            uint CharID = 0;

                                            foreach (GuildMember Member in MyChar.MyGuild.Members.Values)
                                            {
                                                if (Member.Name == Name)
                                                    CharID = Member.UniqId;
                                            }

                                            if (World.AllChars.ContainsKey(CharID))
                                            {
                                                if (MyChar.MyGuild.Members.ContainsKey(CharID))
                                                    MyChar.MyGuild.Members.Remove(CharID);
                                                Character Char = (Character)World.AllChars[CharID];
                                                Char.GuildPosition = 90;

                                                MyChar.MyGuild.Deputies.Add(CharID, new GuildMember().Create(Char));
                                                Char.MyClient.SendPacket(CoServer.MyPackets.GuildInfo(Char.MyGuild, Char));
                                            }
                                            else
                                            {
                                                SendPacket(CoServer.MyPackets.NPCSay("Le joueur que vous voulez mettre sous-chef doit �tre en-ligne et dans votre guilde."));
                                                SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                                SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                                SendPacket(CoServer.MyPackets.NPCFinish());
                                            }
                                        }
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Une guilde peut avoir que 6 sous-chefs."));
                                            SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Seul le chef de guilde peut commettre un sous-chef."));
                                        SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 205)
                                {
                                    if (MyChar.GuildPosition == 100)
                                    {
                                        string Name = "";
                                        for (int i = 14; i < 14 + Data[13]; i++)
                                        {
                                            Name += Convert.ToChar(Data[i]);
                                        }

                                        uint CharID = 0;

                                        foreach (GuildMember Deputy in MyChar.MyGuild.Deputies.Values)
                                        {
                                            if (Deputy.Name == Name)
                                                CharID = Deputy.UniqId;
                                        }

                                        if (World.AllChars.ContainsKey(CharID))
                                        {
                                            if (MyChar.MyGuild.Deputies.ContainsKey(CharID))
                                                MyChar.MyGuild.Deputies.Remove(CharID);
                                            Character Char = (Character)World.AllChars[CharID];
                                            Char.GuildPosition = 50;

                                            MyChar.MyGuild.Members.Add(CharID, new GuildMember().Create(Char));
                                            Char.MyClient.SendPacket(CoServer.MyPackets.GuildInfo(Char.MyGuild, Char));
                                        }
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Le joueur doit �tre en-ligne et dans votre guilde."));
                                            SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Seul le chef de guilde peut enlever un sous-chef."));
                                        SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 206)
                                {
                                    bool Exist = false;
                                    string Name = "";
                                    for (int i = 14; i < 14 + Data[13]; i++)
                                    {
                                        Name += Convert.ToChar(Data[i]);
                                    }
                                    foreach (Guild TheGuild in Guilds.AllGuilds.Values)
                                    {
                                        if (TheGuild.Name == Name)
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Le nom de guilde: " + TheGuild.Name));
                                            SendPacket(CoServer.MyPackets.NPCSay("\nLe chef de guilde: " + TheGuild.Leader.Name));
                                            SendPacket(CoServer.MyPackets.NPCSay("\nNombre de membres: " + TheGuild.Members.Count.ToString()));
                                            SendPacket(CoServer.MyPackets.NPCSay("\nLe fond de guilde: " + TheGuild.Fund.ToString()));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                            Exist = true;
                                        }
                                    }
                                    if (!Exist)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Cette guilde n'existe pas!"));
                                        SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 207)
                                {
                                    if (MyChar.GuildPosition == 100)
                                    {
                                        if (MyChar.MyGuild.Allies.Count < 5)
                                        {
                                            bool Exist = false;
                                            string Name = "";
                                            for (int i = 14; i < 14 + Data[13]; i++)
                                            {
                                                Name += Convert.ToChar(Data[i]);
                                            }
                                            foreach (Guild TheGuild in Guilds.AllGuilds.Values)
                                            {
                                                if (TheGuild != MyChar.MyGuild)
                                                    if (TheGuild.Name == Name)
                                                        if (!MyChar.MyGuild.Enemies.Contains(TheGuild.UniqId))
                                                        {
                                                            MyChar.MyGuild.Enemies.Add(TheGuild.UniqId);
                                                            Exist = true;
                                                        }
                                            }
                                            if (!Exist)
                                            {
                                                SendPacket(CoServer.MyPackets.NPCSay("Cette guilde n'existe pas!"));
                                                SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                                SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                                SendPacket(CoServer.MyPackets.NPCFinish());
                                            }
                                            else
                                                foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                                                {
                                                    Character TheChar = (Character)DE.Value;
                                                    if (TheChar.GuildID == MyChar.GuildID)
                                                        World.SendAllGuild(TheChar);
                                                }
                                        }
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous ne pouvez pas avoir autant d'ennemies!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Seul le chef de guilde peut mettre une guilde ennemie."));
                                        SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 208)
                                {
                                    if (MyChar.GuildPosition == 100)
                                    {
                                        bool Exist = false;
                                        string Name = "";
                                        for (int i = 14; i < 14 + Data[13]; i++)
                                        {
                                            Name += Convert.ToChar(Data[i]);
                                        }
                                        foreach (Guild TheGuild in Guilds.AllGuilds.Values)
                                        {
                                            if (TheGuild != MyChar.MyGuild)
                                                if (TheGuild.Name == Name)
                                                    if (MyChar.MyGuild.Enemies.Contains(TheGuild.UniqId))
                                                    {
                                                        MyChar.MyGuild.Enemies.Remove(TheGuild.UniqId);
                                                        Exist = true;
                                                    }
                                        }
                                        if (!Exist)
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Cette guilde n'existe pas!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                            foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                                            {
                                                Character TheChar = (Character)DE.Value;
                                                if (TheChar.GuildID == MyChar.GuildID)
                                                    World.SendAllGuild(TheChar);
                                            }
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Seul le chef de guilde peut enlever une guilde ennemie."));
                                        SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 209)
                                {
                                    ushort AllyId = 0;
                                    if (MyChar.GuildPosition == 100)
                                    {
                                        if (MyChar.MyGuild.Allies.Count < 5)
                                        {
                                            if (MyChar.TeamLeader && MyChar.Team.Count == 1)
                                            {
                                                foreach (Character Member in MyChar.Team)
                                                {
                                                    if (Member.MyGuild != null)
                                                        if (Member.MyGuild != MyChar.MyGuild)
                                                            if (Member.GuildPosition == 100)
                                                                if (Member.MyGuild.Allies.Count < 5 && MyChar.MyGuild.Allies.Count < 5)
                                                                    if (!Member.MyGuild.Allies.Contains(MyChar.GuildID) && !MyChar.MyGuild.Allies.Contains(Member.GuildID))
                                                                    {
                                                                        AllyId = Member.GuildID;
                                                                        Member.MyGuild.Allies.Add(MyChar.GuildID);
                                                                        MyChar.MyGuild.Allies.Add(Member.GuildID);
                                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (MyChar.Team.Count == 1)
                                                    if (MyChar.MyTeamLeader.MyGuild != null)
                                                        if (MyChar.MyTeamLeader.MyGuild != MyChar.MyGuild)
                                                            if (MyChar.MyTeamLeader.GuildPosition == 100)
                                                                if (MyChar.MyTeamLeader.MyGuild.Allies.Count < 5 && MyChar.MyGuild.Allies.Count < 5)
                                                                    if (!MyChar.MyTeamLeader.MyGuild.Allies.Contains(MyChar.GuildID) && !MyChar.MyGuild.Allies.Contains(MyChar.MyTeamLeader.GuildID))
                                                                    {
                                                                        AllyId = MyChar.MyTeamLeader.GuildID;
                                                                        MyChar.MyTeamLeader.MyGuild.Allies.Add(MyChar.GuildID);
                                                                        MyChar.MyGuild.Allies.Add(MyChar.MyTeamLeader.GuildID);
                                                                    }
                                            }

                                            foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                                            {
                                                Character TheChar = (Character)DE.Value;
                                                if (TheChar.GuildID == MyChar.GuildID)
                                                    World.SendAllGuild(TheChar);
                                                if (TheChar.GuildID == AllyId)
                                                    World.SendAllGuild(TheChar);
                                            }
                                        }
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous ne pouvez pas avoir autant d'alli�s!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Seul le chef de guilde peut mettre une guilde alli�."));
                                        SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 210)
                                {
                                    if (MyChar.GuildPosition == 100)
                                    {
                                        bool Exist = false;
                                        string Name = "";
                                        for (int i = 14; i < 14 + Data[13]; i++)
                                        {
                                            Name += Convert.ToChar(Data[i]);
                                        }
                                        foreach (Guild TheGuild in Guilds.AllGuilds.Values)
                                        {
                                            if (TheGuild != MyChar.MyGuild)
                                                if (TheGuild.Name == Name)
                                                    if (MyChar.MyGuild.Allies.Contains(TheGuild.UniqId))
                                                    {
                                                        MyChar.MyGuild.Allies.Remove(TheGuild.UniqId);
                                                        Exist = true;
                                                    }
                                        }
                                        if (!Exist)
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Cette guilde n'existe pas!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                            foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                                            {
                                                Character TheChar = (Character)DE.Value;
                                                if (TheChar.GuildID == MyChar.GuildID)
                                                    World.SendAllGuild(TheChar);
                                            }
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Seul le chef de guilde peut enlever une guilde alli�."));
                                        SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 211)
                                {
                                    if (MyChar.GuildPosition == 100)
                                    {
                                        bool Exist = false;
                                        string Name = "";
                                        for (int i = 14; i < 14 + Data[13]; i++)
                                        {
                                            Name += Convert.ToChar(Data[i]);
                                        }
                                        foreach (GuildMember Member in MyChar.MyGuild.Members.Values)
                                        {
                                            if (Member.Name == Name)
                                            {
                                                Exist = true;
                                                MyChar.MyGuild.KickPlayer(Member);
                                                break;
                                            }
                                        }
                                        foreach (GuildMember Deputy in MyChar.MyGuild.Deputies.Values)
                                        {
                                            if (Deputy.Name == Name)
                                            {
                                                Exist = true;
                                                MyChar.MyGuild.KickPlayer(Deputy);
                                                break;
                                            }
                                        }
                                        if (!Exist)
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Ce membre n'existe pas!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Seul le chef de guilde peut exclure un membre."));
                                        SendPacket(CoServer.MyPackets.NPCLink("D'accord", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(7));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC >= 925 && CurrentNPC <= 945 && CurrentNPC != 941)
                            {
                                if (MyChar.ItemsInInventory < 40)
                                {
                                    if (Control == 1)
                                    {
                                        MyChar.LotoCount++;
                                        if (MyChar.PrevMap == 800)
                                            MyChar.Teleport(800, 290, 236);
                                        else
                                            MyChar.Teleport(1036, 219, 192);

                                        if (Other.ChanceSuccess(15))
                                        {
                                            byte Quality = 8;
                                            byte Plus = 0;
                                            byte Soc1 = 0;
                                            byte Soc2 = 0;

                                            if (Other.ChanceSuccess(3))
                                                Plus = 8;

                                            if (Plus == 0)
                                                if (Other.ChanceSuccess(10))
                                                {
                                                    Soc1 = 255;
                                                    if (Other.ChanceSuccess(7.5))
                                                        Soc2 = 255;
                                                }

                                            if (Plus == 0 && Soc1 == 0 && Soc2 == 0)
                                                Quality = 9;

                                            uint EquipID = Other.GenerateEquip((byte)Program.Rand.Next(10, 21), Quality);
                                            while (EquipID == 0)
                                                EquipID = Other.GenerateEquip((byte)Program.Rand.Next(10, 21), Quality);

                                            string ItemName = "";
                                            if (DataBase.ItemNames.ContainsKey(EquipID))
                                                ItemName = DataBase.ItemNames[EquipID];

                                            if (Quality == 9)
                                                ItemName += "Super";
                                            else
                                            {
                                                if (Plus == 8)
                                                    ItemName += "Elite(+8)";
                                                if (Soc1 == 255 && Soc2 == 0)
                                                    ItemName = "1-Trou " + ItemName;
                                                if (Soc1 == 255 && Soc2 == 255)
                                                    ItemName = "2-Trou " + ItemName;
                                            }

                                            MyChar.AddItem(EquipID.ToString() + "-" + Plus.ToString() + "-0-0-" + Soc1.ToString() + "-" + Soc2.ToString(), 0, (uint)Program.Rand.Next(263573635));
                                            World.SendMsgToAll(MyChar.Name + " a gagn� " + ItemName + " chez DameBonheur dans le march�!", "SYSTEM", 0x7d0);
                                        }
                                        else if (Other.ChanceSuccess(20))
                                        {
                                            uint ItemId = Other.GenerateGem();

                                            string ItemName = "";
                                            if (DataBase.ItemNames.ContainsKey(ItemId))
                                                ItemName = DataBase.ItemNames[ItemId];

                                            if (ItemHandler.ItemQuality(ItemId) == 3)
                                                ItemName += " Super";
                                            else
                                                ItemName += "Raffin�e";

                                            MyChar.AddItem(ItemId.ToString() + "-0-0-0-0-0", 0, (uint)Program.Rand.Next(263573635));
                                            World.SendMsgToAll(MyChar.Name + " a gagn� " + ItemName + " chez DameBonheur dans le march�!", "SYSTEM", 0x7d0);
                                        }
                                        else if (Other.ChanceSuccess(15))
                                        {
                                            uint ItemId = Other.GenerateGarment();

                                            string ItemName = "";
                                            if (DataBase.ItemNames.ContainsKey(ItemId))
                                                ItemName = DataBase.ItemNames[ItemId];

                                            MyChar.AddItem(ItemId.ToString() + "-0-0-0-0-0", 0, (uint)Program.Rand.Next(263573635));
                                            World.SendMsgToAll(MyChar.Name + " a gagn� " + ItemName + " chez DameBonheur dans le march�!", "SYSTEM", 0x7d0);
                                        }
                                        else if (Other.ChanceSuccess(25))
                                        {
                                            uint ItemId = Other.GenerateCrap();

                                            string ItemName = "";
                                            if (DataBase.ItemNames.ContainsKey(ItemId))
                                                ItemName = DataBase.ItemNames[ItemId];

                                            if (ItemHandler.ItemType2(ItemId) == 73)
                                                MyChar.AddItem(ItemId.ToString() + "-" + (ItemId - 730000) + "-0-0-0-0", 0, (uint)Program.Rand.Next(263573635));
                                            else
                                                MyChar.AddItem(ItemId.ToString() + "-0-0-0-0-0", 0, (uint)Program.Rand.Next(263573635));
                                            World.SendMsgToAll(MyChar.Name + " a gagn� " + ItemName + " chez DameBonheur dans le march�!", "SYSTEM", 0x7d0);
                                        }
                                        else
                                        {
                                            uint ItemId = Other.GenerateSpecial();
                                            string Plus = "0";

                                            if (ItemId == 730001)
                                                Plus = "1";
                                            MyChar.AddItem(ItemId.ToString() + "-" + Plus + "-0-0-0-0", 0, (uint)Program.Rand.Next(263573635));
                                        }
                                    }
                                }
                                else
                                    SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Votre sac est complet!", 2005));
                            }
                            if (CurrentNPC == 42)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.PKPoints >= 100)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous ne pouvez pas sortir dehors. Vous avez " + MyChar.PKPoints + "pk points. Pour sortir vous devez payer 108 CPs si vous avez 100 � 300 pk points et si vous en avez 301+ vous devez payer 504 CPs."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je veux payer.", 3));
                                        SendPacket(CoServer.MyPackets.NPCLink("Oh,ok.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous pouvez sortir de prison quand vous voulez. Voulez-vous sortir maintenant?"));
                                        SendPacket(CoServer.MyPackets.NPCLink("Oui.", 2));
                                        SendPacket(CoServer.MyPackets.NPCLink("Non.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    MyChar.Teleport(1002, 517, 351);
                                }
                                if (Control == 3)
                                {
                                    if (MyChar.PKPoints >= 100 && MyChar.PKPoints <= 300 && MyChar.CPs >= 108)
                                    {
                                        MyChar.CPs -= 108;
                                        MyChar.Teleport(1002, 517, 351);
                                        World.SendMsgToAll(MyChar.Name + " a pay� pour ses crimes et a �t� lib�r�!", "SYSTEM", 2011);
                                    }
                                    if (MyChar.PKPoints >= 301 && MyChar.CPs >= 504)
                                    {
                                        MyChar.CPs -= 504;
                                        MyChar.Teleport(1002, 517, 351);
                                        World.SendMsgToAll(MyChar.Name + " a pay� pour ses crimes et a �t� lib�r�!", "SYSTEM", 2011);
                                    }
                                }
                            }
                            if (CurrentNPC == 140)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.BanNB <= 3)
                                    {
                                        if (MyChar.CPs >= 504)
                                        {
                                            MyChar.CPs -= 504;
                                            MyChar.Teleport(1002, 430, 378);
                                            World.SendMsgToAll(MyChar.Name + " a pay� pour ses crimes et a �t� lib�r�!", "SYSTEM", 2011);
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous ne pouvez pas sortir de prison car vous avez �t� banni plus de 3 fois."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 600601)
                            {
                                if (Control == 1)
                                {
                                    int Teleport = 0;

                                    while (MyChar.InventoryContains(723085, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(723085));
                                    }

                                    if (MyChar.Level < 100)
                                        MyChar.AddExp((ulong)((1295000 + MyChar.Level * 50000) / 2), false);
                                    else if (MyChar.Level < 110)
                                        MyChar.AddExp((ulong)((1395000 + MyChar.Level * 80000) / 2), false);
                                    else if (MyChar.Level < 115)
                                        MyChar.AddExp((ulong)((1595000 + MyChar.Level * 100000) / 2), false);
                                    else if (MyChar.Level < 120)
                                        MyChar.AddExp((ulong)((1895000 + MyChar.Level * 120000) / 2), false);
                                    else if (MyChar.Level < 125)
                                        MyChar.AddExp((ulong)((2095000 + MyChar.Level * 150000) / 2), false);
                                    else if (MyChar.Level < 130)
                                        MyChar.AddExp((ulong)((2395000 + MyChar.Level * 180000) / 2), false);
                                    else if (MyChar.Level < 255)
                                        MyChar.AddExp((ulong)((2895000 + MyChar.Level * 200000) / 2), false);

                                    Teleport = Program.Rand.Next(1, 5);

                                    if (Teleport == 1)
                                        MyChar.Teleport(2021, 298, 414);
                                    if (Teleport == 2)
                                        MyChar.Teleport(2021, 365, 375);
                                    if (Teleport == 3)
                                        MyChar.Teleport(2021, 302, 324);
                                    if (Teleport == 4)
                                        MyChar.Teleport(2021, 223, 329);

                                    SendPacket(CoServer.MyPackets.SendMsg(MyChar.MyClient.MessageId, "DisCity", MyChar.Name, "Vous devez obtenir 5 Jades d'enfer..", 2000));
                                }
                            }
                            if (CurrentNPC == 600602)
                            {
                                if (Control == 1)
                                {
                                    World.DisMap2.Clear();
                                    if (MyChar.InventoryContains(723085, 5))
                                    {
                                        MyChar.Teleport(2022, 228, 343);
                                        MyChar.DisKO = 0;

                                        foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                                        {
                                            Character Charr = (Character)DE.Value;

                                            if (Charr.LocMap == 2022)
                                                World.DisMap2.Add(Charr.Name, Charr.UID);
                                        }

                                        if (MyChar.MyGuild == null)
                                            World.SendMsgToAll(World.DisMap2.Count + ": " + MyChar.Name + " est entr� dans la deuxi�me carte de Dis City!", "SYSTEM", 2005);
                                        else
                                            World.SendMsgToAll(World.DisMap2.Count + ": " + MyChar.Name + " des " + MyChar.MyGuild.Name + " est entr� dans la deuxi�me carte de Dis City!", "SYSTEM", 2005);

                                        if (World.DisMap2.Count == 1)
                                            World.SendMsgToAll(MyChar.Name + " est entr� dans la SalleInfernale!", "SYSTEM", 2011);


                                        if (World.DisMap2.Count > 59)
                                        {
                                            foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                                            {
                                                Character Chaar = (Character)DE.Value;
                                                if (Chaar.LocMap == 2021)
                                                {
                                                    Chaar.Teleport(1002, 430, 380);
                                                    World.SendMsgToAll("Pour prot�ger les joueurs, " + MyChar.Name + " a t�l�port� les joueurs hors du dang�!", "SYSTEM", 2011);
                                                }
                                            }
                                        }

                                        while (MyChar.InventoryContains(723085, 1))
                                        {
                                            MyChar.RemoveItem(MyChar.ItemNext(723085));
                                        }

                                        if (MyChar.Level < 100)
                                            MyChar.AddExp((ulong)(1295000 + MyChar.Level * 50000), false);
                                        else if (MyChar.Level < 110)
                                            MyChar.AddExp((ulong)(1395000 + MyChar.Level * 80000), false);
                                        else if (MyChar.Level < 115)
                                            MyChar.AddExp((ulong)(1595000 + MyChar.Level * 100000), false);
                                        else if (MyChar.Level < 120)
                                            MyChar.AddExp((ulong)(1895000 + MyChar.Level * 120000), false);
                                        else if (MyChar.Level < 125)
                                            MyChar.AddExp((ulong)(2095000 + MyChar.Level * 150000), false);
                                        else if (MyChar.Level < 130)
                                            MyChar.AddExp((ulong)(2395000 + MyChar.Level * 180000), false);
                                        else if (MyChar.Level < 255)
                                            MyChar.AddExp((ulong)(2895000 + MyChar.Level * 200000), false);

                                        SendPacket(CoServer.MyPackets.SendMsg(MyChar.MyClient.MessageId, "DisCity", MyChar.Name, "Tuez 800 monstres pour les Braves, 900 monstres pour les Guerriers, 1300 monstres pour les Archers, 1000 monstres pour les Taoistes de Feu, 600 monstres pour les Taoistes d�Eau. Je ne peux envoyer que 30 personnes au niveau suivant.", 2000));
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas 5 jades..."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 600603)
                            {
                                if (Control == 1)
                                {
                                    uint DisKo = 0;

                                    if (MyChar.Job > 9 && MyChar.Job < 16)
                                        DisKo = 800;
                                    if (MyChar.Job > 19 && MyChar.Job < 26)
                                        DisKo = 900;
                                    if (MyChar.Job > 39 && MyChar.Job < 46)
                                        DisKo = 1300;
                                    if (MyChar.Job > 129 && MyChar.Job < 136)
                                        DisKo = 600;
                                    if (MyChar.Job > 139 && MyChar.Job < 146)
                                        DisKo = 1000;

                                    World.DisMap3.Clear();

                                    if (MyChar.ItemsInInventory < 40 && MyChar.DisKO >= DisKo)
                                    {
                                        MyChar.Teleport(2023, 301, 653);
                                        MyChar.AddItem("723087-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));

                                        foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                                        {
                                            Character Charr = (Character)DE.Value;

                                            if (Charr.LocMap == 2023)
                                                World.DisMap3.Add(Charr.Name, Charr.UID);
                                        }

                                        if (MyChar.MyGuild == null)
                                            World.SendMsgToAll(World.DisMap3.Count + ": " + MyChar.Name + " est entr� dans la troisi�me carte de Dis City!", "SYSTEM", 2005);
                                        else
                                            World.SendMsgToAll(World.DisMap3.Count + ": " + MyChar.Name + " des " + MyChar.MyGuild.Name + " est entr� dans la troisi�me carte de Dis City!", "SYSTEM", 2005);

                                        if (World.DisMap3.Count == 1)
                                            World.SendMsgToAll(MyChar.Name + " est entr� dans la Clo�treInfernal!", "SYSTEM", 2011);

                                        if (World.DisMap3.Count > 29)
                                        {
                                            foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                                            {
                                                Character Chaar = (Character)DE.Value;
                                                if (Chaar.LocMap == 2022)
                                                {
                                                    Chaar.Teleport(1002, 430, 380);
                                                    World.SendMsgToAll("Pour prot�ger les joueurs, " + MyChar.Name + " a t�l�port� les joueurs hors du dang�!", "SYSTEM", 2011);
                                                }
                                                if (Chaar.LocMap == 2023)
                                                {
                                                    Chaar.Teleport(2024, 150, 283);
                                                }
                                            }
                                        }

                                        if (MyChar.Level < 100)
                                            MyChar.AddExp((ulong)(1295000 + MyChar.Level * 50000), false);
                                        else if (MyChar.Level < 110)
                                            MyChar.AddExp((ulong)(1395000 + MyChar.Level * 80000), false);
                                        else if (MyChar.Level < 115)
                                            MyChar.AddExp((ulong)(1595000 + MyChar.Level * 100000), false);
                                        else if (MyChar.Level < 120)
                                            MyChar.AddExp((ulong)(1895000 + MyChar.Level * 120000), false);
                                        else if (MyChar.Level < 125)
                                            MyChar.AddExp((ulong)(2095000 + MyChar.Level * 150000), false);
                                        else if (MyChar.Level < 130)
                                            MyChar.AddExp((ulong)(2395000 + MyChar.Level * 180000), false);
                                        else if (MyChar.Level < 255)
                                            MyChar.AddExp((ulong)(2895000 + MyChar.Level * 200000), false);

                                        SendPacket(CoServer.MyPackets.SendMsg(MyChar.MyClient.MessageId, "DisCity", MyChar.Name, "Lorsque la 30e personne ou la derni�re personne rentrera dans cette carte vous irez � la prochaine!", 2000));
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas tu� assez de monstre ou votre inventaire est plein!"));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 1315)
                            {
                                MyChar.RemoveItem(MyChar.ItemNext(790001));

                                if (MyChar.Level < 100)
                                    MyChar.AddExp((ulong)((1295000 + MyChar.Level * 50000) * 8), false);
                                else if (MyChar.Level < 110)
                                    MyChar.AddExp((ulong)((1395000 + MyChar.Level * 80000) * 8), false);
                                else if (MyChar.Level < 115)
                                    MyChar.AddExp((ulong)((1595000 + MyChar.Level * 100000) * 8), false);
                                else if (MyChar.Level < 120)
                                    MyChar.AddExp((ulong)((1895000 + MyChar.Level * 120000) * 8), false);
                                else if (MyChar.Level < 125)
                                    MyChar.AddExp((ulong)((2095000 + MyChar.Level * 150000) * 8), false);
                                else if (MyChar.Level < 130)
                                    MyChar.AddExp((ulong)((2395000 + MyChar.Level * 180000) * 8), false);
                                else if (MyChar.Level < 255)
                                    MyChar.AddExp((ulong)((2895000 + MyChar.Level * 200000) * 8), false);

                                byte Dress = 0;
                                Dress = (byte)Program.Rand.Next(1, 22);

                                switch (Dress)
                                {
                                    case 1:
                                        MyChar.AddItem("137320-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 2:
                                        MyChar.AddItem("137330-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 3:
                                        MyChar.AddItem("137340-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 4:
                                        MyChar.AddItem("137420-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 5:
                                        MyChar.AddItem("137430-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 6:
                                        MyChar.AddItem("137440-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 7:
                                        MyChar.AddItem("137520-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 8:
                                        MyChar.AddItem("137530-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 9:
                                        MyChar.AddItem("137540-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 10:
                                        MyChar.AddItem("137620-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 11:
                                        MyChar.AddItem("137630-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 12:
                                        MyChar.AddItem("137640-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 13:
                                        MyChar.AddItem("137720-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 14:
                                        MyChar.AddItem("137730-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 15:
                                        MyChar.AddItem("137740-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 16:
                                        MyChar.AddItem("137820-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 17:
                                        MyChar.AddItem("137830-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 18:
                                        MyChar.AddItem("137840-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 19:
                                        MyChar.AddItem("137920-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 20:
                                        MyChar.AddItem("137930-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                    case 21:
                                        MyChar.AddItem("137940-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                        break;
                                }
                            }
                            if (CurrentNPC == 923)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.CPs >= 27)
                                    {
                                        MyChar.Teleport(700, 50, 50);
                                        MyChar.CPs -= 27;
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas assez de cps"));
                                        SendPacket(CoServer.MyPackets.NPCLink("Zut !", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 9997)
                            {
                                if (Control == 1)
                                {
                                    int Teleport = 0;

                                    if (MyChar.Level < 100)
                                        MyChar.AddExp((ulong)((1295000 + MyChar.Level * 50000) / 2), false);
                                    else if (MyChar.Level < 110)
                                        MyChar.AddExp((ulong)((1395000 + MyChar.Level * 80000) / 2), false);
                                    else if (MyChar.Level < 115)
                                        MyChar.AddExp((ulong)((1595000 + MyChar.Level * 100000) / 2), false);
                                    else if (MyChar.Level < 120)
                                        MyChar.AddExp((ulong)((1895000 + MyChar.Level * 120000) / 2), false);
                                    else if (MyChar.Level < 125)
                                        MyChar.AddExp((ulong)((2095000 + MyChar.Level * 150000) / 2), false);
                                    else if (MyChar.Level < 130)
                                        MyChar.AddExp((ulong)((2395000 + MyChar.Level * 180000) / 2), false);
                                    else if (MyChar.Level < 255)
                                        MyChar.AddExp((ulong)((2895000 + MyChar.Level * 200000) / 2), false);

                                    Teleport = Program.Rand.Next(1, 5);

                                    if (Teleport == 1)
                                        MyChar.Teleport(1081, 107, 111);
                                    if (Teleport == 2)
                                        MyChar.Teleport(1081, 146, 172);
                                    if (Teleport == 3)
                                        MyChar.Teleport(1081, 135, 126);
                                    if (Teleport == 4)
                                        MyChar.Teleport(1081, 124, 85);
                                }
                            }
                            if (CurrentNPC == 9996)
                            {
                                if (Control == 1)
                                {
                                    int Teleport = 0;

                                    if (MyChar.Level < 100)
                                        MyChar.AddExp((ulong)(1295000 + MyChar.Level * 50000), false);
                                    else if (MyChar.Level < 110)
                                        MyChar.AddExp((ulong)(1395000 + MyChar.Level * 80000), false);
                                    else if (MyChar.Level < 115)
                                        MyChar.AddExp((ulong)(1595000 + MyChar.Level * 100000), false);
                                    else if (MyChar.Level < 120)
                                        MyChar.AddExp((ulong)(1895000 + MyChar.Level * 120000), false);
                                    else if (MyChar.Level < 125)
                                        MyChar.AddExp((ulong)(2095000 + MyChar.Level * 150000), false);
                                    else if (MyChar.Level < 130)
                                        MyChar.AddExp((ulong)(2395000 + MyChar.Level * 180000), false);
                                    else if (MyChar.Level < 255)
                                        MyChar.AddExp((ulong)(2895000 + MyChar.Level * 200000), false);

                                    Teleport = Program.Rand.Next(1, 5);

                                    if (Teleport == 1)
                                        MyChar.Teleport(1090, 42, 61);
                                    if (Teleport == 2)
                                        MyChar.Teleport(1090, 98, 63);
                                    if (Teleport == 3)
                                        MyChar.Teleport(1090, 94, 98);
                                    if (Teleport == 4)
                                        MyChar.Teleport(1090, 119, 111);
                                }
                            }
                            if (CurrentNPC == 9995)
                            {
                                if (Control == 1)
                                {
                                    int Teleport = 0;

                                    if (MyChar.Level < 100)
                                        MyChar.AddExp((ulong)((1295000 + MyChar.Level * 50000) * 2), false);
                                    else if (MyChar.Level < 110)
                                        MyChar.AddExp((ulong)((1395000 + MyChar.Level * 80000) * 2), false);
                                    else if (MyChar.Level < 115)
                                        MyChar.AddExp((ulong)((1595000 + MyChar.Level * 100000) * 2), false);
                                    else if (MyChar.Level < 120)
                                        MyChar.AddExp((ulong)((1895000 + MyChar.Level * 120000) * 2), false);
                                    else if (MyChar.Level < 125)
                                        MyChar.AddExp((ulong)((2095000 + MyChar.Level * 150000) * 2), false);
                                    else if (MyChar.Level < 130)
                                        MyChar.AddExp((ulong)((2395000 + MyChar.Level * 180000) * 2), false);
                                    else if (MyChar.Level < 255)
                                        MyChar.AddExp((ulong)((2895000 + MyChar.Level * 200000) * 2), false);

                                    Teleport = Program.Rand.Next(1, 3);

                                    if (Teleport == 1)
                                        MyChar.Teleport(1091, 32, 22);
                                    if (Teleport == 2)
                                        MyChar.Teleport(1091, 20, 31);
                                }
                            }
                            if (CurrentNPC > 9991 && CurrentNPC < 9995)
                            {
                                if (Control == 1)
                                {
                                    World.PktMap1.Clear();
                                    World.PktMap2.Clear();
                                    World.PktMap3.Clear();
                                    foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                                    {
                                        Character Charr = (Character)DE.Value;

                                        if (Charr.LocMap == 1081)
                                            World.PktMap1.Add(Charr.UID);

                                        if (Charr.LocMap == 1090)
                                            World.PktMap2.Add(Charr.UID);

                                        if (Charr.LocMap == 1091)
                                            World.PktMap3.Add(Charr.UID);
                                    }
                                    if (World.PktMap1.Count == 1 && World.PktMap2.Count == 0 && World.PktMap3.Count == 0)
                                    {
                                        DateTime myDateTime = DateTime.Now;
                                        string DayNow = myDateTime.DayOfWeek.ToString();

                                        if (DayNow == "Saturday" && myDateTime.Day != 1)
                                        {
                                            if (MyChar.Level < 100)
                                                MyChar.AddExp((ulong)((1295000 + MyChar.Level * 50000) * 5), false);
                                            else if (MyChar.Level < 110)
                                                MyChar.AddExp((ulong)((1395000 + MyChar.Level * 80000) * 5), false);
                                            else if (MyChar.Level < 115)
                                                MyChar.AddExp((ulong)((1595000 + MyChar.Level * 100000) * 5), false);
                                            else if (MyChar.Level < 120)
                                                MyChar.AddExp((ulong)((1895000 + MyChar.Level * 120000) * 5), false);
                                            else if (MyChar.Level < 125)
                                                MyChar.AddExp((ulong)((2095000 + MyChar.Level * 150000) * 5), false);
                                            else if (MyChar.Level < 130)
                                                MyChar.AddExp((ulong)((2395000 + MyChar.Level * 180000) * 5), false);
                                            else if (MyChar.Level < 255)
                                                MyChar.AddExp((ulong)((2895000 + MyChar.Level * 200000) * 5), false);

                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                            MyChar.Teleport(1002, 430, 380);
                                            World.SendMsgToAll(MyChar.Name + " a gagn� le tournois de pk! F�licitation!", "SYSTEM", 2011);
                                        }
                                        if (myDateTime.Day == 1)
                                        {
                                            if (MyChar.Level < 100)
                                                MyChar.AddExp((ulong)((1295000 + MyChar.Level * 50000) * 5), false);
                                            else if (MyChar.Level < 110)
                                                MyChar.AddExp((ulong)((1395000 + MyChar.Level * 80000) * 5), false);
                                            else if (MyChar.Level < 115)
                                                MyChar.AddExp((ulong)((1595000 + MyChar.Level * 100000) * 5), false);
                                            else if (MyChar.Level < 120)
                                                MyChar.AddExp((ulong)((1895000 + MyChar.Level * 120000) * 5), false);
                                            else if (MyChar.Level < 125)
                                                MyChar.AddExp((ulong)((2095000 + MyChar.Level * 150000) * 5), false);
                                            else if (MyChar.Level < 130)
                                                MyChar.AddExp((ulong)((2395000 + MyChar.Level * 180000) * 5), false);
                                            else if (MyChar.Level < 255)
                                                MyChar.AddExp((ulong)((2895000 + MyChar.Level * 200000) * 5), false);

                                            byte WinGem = (byte)Program.Rand.Next(1, 4);
                                            if (WinGem == 1)
                                                MyChar.AddItem("700003-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                            if (WinGem == 2)
                                                MyChar.AddItem("700013-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                            if (WinGem == 3)
                                                MyChar.AddItem("700033-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));

                                            MyChar.Teleport(1002, 430, 380);
                                            World.SendMsgToAll(MyChar.Name + " a gagn� le tournois de pk! F�licitation!", "SYSTEM", 2011);
                                            World.PktMap1.Clear();
                                            World.PktMap2.Clear();
                                            World.PktMap3.Clear();
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous n'�tes pas le gagnant d�sol�."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            #region Unknown Man - DB Leveling Guy
                            if (CurrentNPC == 3825 || CurrentNPC == 11023)
                            {
                                if (Control == 1)
                                {
                                    uint AddExp = 0;

                                    AddExp = (2895000 + (uint)MyChar.Level * 200000) * 10;

                                    SendPacket(CoServer.MyPackets.NPCSay("Apportez moi une Perle de Dragon, puis je vous aiderai � transmettre l'�nergie de celle-ci. Vous gagnerez: " + AddExp + " points d'exp�rience!"));
                                    SendPacket(CoServer.MyPackets.NPCLink("Veuillez commencer S.V.P.", 2));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(6));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.InventoryContains(1088000, 1))
                                    {
                                        if (MyChar.Level >= 130 && DataBase.NeededXP(MyChar.Level) != 0)
                                        {
                                            MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                            MyChar.AddExp((ulong)(2895000 + MyChar.Level * 200000) * 10, false);
                                        }
                                        else if (MyChar.Level == 137)
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous �tes au plus au niveau, vous ne pouvez plus gagner de point d'exp�rience."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(6));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("D�sol�e, vous n'avez pas de Perle de Dargon."));
                                        SendPacket(CoServer.MyPackets.NPCLink("D�sol�.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(6));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            #endregion
                            if (CurrentNPC == 35016)
                            {
                                if (Control == 1)
                                    SendPacket(CoServer.MyPackets.GeneralData(MyChar, 1, 126));
                                if (Control == 2)
                                    SendPacket(CoServer.MyPackets.GeneralData(MyChar, 1091, 116));
                                if (Control == 21)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Il vous faut 12 Perles de Dragon pour le +10, 25 Perles de Dragon pour le +11 et 40 Perles de Dragon pour le +12."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Arme/Arc/Glaive.", 22));
                                    SendPacket(CoServer.MyPackets.NPCLink("Collier/Sac.", 23));
                                    SendPacket(CoServer.MyPackets.NPCLink("Bague/Bracelet.", 24));
                                    SendPacket(CoServer.MyPackets.NPCLink("Botte.", 25));
                                    SendPacket(CoServer.MyPackets.NPCLink("Armure/Robe.", 26));
                                    SendPacket(CoServer.MyPackets.NPCLink("Casque/Boucle.", 27));
                                    SendPacket(CoServer.MyPackets.NPCLink("Bouclier/Arme.", 28));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }

                                if (Control <= 28 && Control >= 22)
                                {
                                    byte Pos = 0;

                                    if (Control == 22)
                                        Pos = 4;
                                    if (Control == 23)
                                        Pos = 2;
                                    if (Control == 24)
                                        Pos = 6;
                                    if (Control == 25)
                                        Pos = 8;
                                    if (Control == 26)
                                        Pos = 3;
                                    if (Control == 27)
                                        Pos = 1;
                                    if (Control == 28)
                                        Pos = 5;

                                    string[] Splitter = MyChar.Equips[Pos].Split('-');
                                    uint ItemId = uint.Parse(Splitter[0]);
                                    byte Plus = byte.Parse(Splitter[1]);

                                    if (Plus >= 9 && Plus <= 11)
                                    {
                                        byte DBs = 0;
                                        byte DBsCount = 0;

                                        if (Plus == 9)
                                            DBs = 12;
                                        if (Plus == 10)
                                            DBs = 25;
                                        if (Plus == 11)
                                            DBs = 40;

                                        if (MyChar.InventoryContains(1088000, (uint)DBs))
                                        {
                                            while (DBsCount < DBs)
                                            {
                                                MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                                DBsCount++;
                                            }
                                            Plus += 1;

                                            MyChar.GetEquipStats(Pos, true);
                                            MyChar.Equips[Pos] = ItemId + "-" + Plus + "-" + Splitter[2] + "-" + Splitter[3] + "-" + Splitter[4] + "-" + Splitter[5];
                                            MyChar.GetEquipStats(Pos, false);

                                            SendPacket(CoServer.MyPackets.AddItem((long)MyChar.Equips_UIDs[Pos], (int)ItemId, (byte)Plus, byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), Pos, 100, 100));

                                            SendPacket(CoServer.MyPackets.NPCSay("Votre objet a �t� am�lior�!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Merci beaucoup!", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas assez de Perles de Dragon."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Votre objet n'est pas +9 ou votre objet est d�j� +12."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 35015)
                            {
                                if (Control == 1)
                                {
                                    MyChar.Teleport(1767, 94, 64);
                                }
                                if (Control == 101)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Quel arme voulez vous b�nir?"));
                                    SendPacket(CoServer.MyPackets.NPCLink("Main droite.", 2));
                                    SendPacket(CoServer.MyPackets.NPCLink("Main gauche.", 8));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 100)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Quel objet voulez vous b�nir? Sachez que le maximum est de -1% et qu'il vous faudra 25 amulettes!"));
                                    SendPacket(CoServer.MyPackets.NPCLink("Gourde.", 9));
                                    SendPacket(CoServer.MyPackets.NPCLink("Robe.", 10));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control <= 10 && Control >= 2)
                                {
                                    byte Pos = 0;

                                    if (Control == 2)
                                        Pos = 4;
                                    if (Control == 3)
                                        Pos = 2;
                                    if (Control == 4)
                                        Pos = 6;
                                    if (Control == 5)
                                        Pos = 8;
                                    if (Control == 6)
                                        Pos = 3;
                                    if (Control == 7)
                                        Pos = 1;
                                    if (Control == 8)
                                        Pos = 5;
                                    if (Control == 9)
                                        Pos = 7;
                                    if (Control == 10)
                                        Pos = 9;

                                    if (MyChar.Equips[Pos] == null || MyChar.Equips[Pos] == "0")
                                        return;

                                    string[] Splitter = MyChar.Equips[Pos].Split('-');
                                    uint ItemId = uint.Parse(Splitter[0]);
                                    byte ItemBless = byte.Parse(Splitter[2]);
                                    byte RealBless = 0;

                                    if (ItemId == 1050000 || ItemId == 1050001 || ItemId == 1050002)
                                        return;

                                    if (ItemBless > 9 && ItemBless < 18)
                                        RealBless = (byte)(ItemBless - 10);
                                    else if (ItemBless > 19 && ItemBless < 28)
                                        RealBless = (byte)(ItemBless - 20);
                                    else if (ItemBless > 29 && ItemBless < 38)
                                        RealBless = (byte)(ItemBless - 30);
                                    else if (ItemBless > 39 && ItemBless < 48)
                                        RealBless = (byte)(ItemBless - 40);
                                    else
                                        RealBless = ItemBless;

                                    if (RealBless < 7)
                                    {
                                        if ((Pos == 7 || Pos == 9) && RealBless > 0)
                                           return;

                                        byte amulette = 0;

                                        if (RealBless == 0)
                                            amulette = 5;
                                        if (RealBless == 1)
                                            amulette = 1;
                                        if (RealBless == 2)
                                            amulette = 2;
                                        if (RealBless == 3)
                                            amulette = 3;
                                        if (RealBless == 4)
                                            amulette = 4;
                                        if (RealBless == 5)
                                            amulette = 5;
                                        if (RealBless == 6)
                                            amulette = 7;

                                        if (Pos == 7 || Pos == 9)
                                            amulette = 25;

                                        if (MyChar.InventoryContains(700074, amulette))
                                        {
                                            for (byte i = 0; i < amulette; i++)
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));

                                            ItemBless++;

                                            MyChar.GetEquipStats(Pos, true);
                                            MyChar.Equips[Pos] = ItemId + "-" + Splitter[1] + "-" + ItemBless + "-" + Splitter[3] + "-" + Splitter[4] + "-" + Splitter[5];
                                            MyChar.GetEquipStats(Pos, false);

                                            SendPacket(CoServer.MyPackets.AddItem((long)MyChar.Equips_UIDs[Pos], (int)ItemId, byte.Parse(Splitter[1]), (byte)ItemBless, byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), Pos, 100, 100));

                                            SendPacket(CoServer.MyPackets.NPCSay("Votre objet a �t� am�lior�!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Merci beaucoup!", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas assez d'amulette."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Votre �quipement ne peut pas �tre am�lior�."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 1550)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("� quel objet voulez-vous ajouter un trou?"));
                                    SendPacket(CoServer.MyPackets.NPCLink("Casque/Boucle", 3));
                                    SendPacket(CoServer.MyPackets.NPCLink("Collier/Sac", 4));
                                    SendPacket(CoServer.MyPackets.NPCLink("Armure/Robe", 5));
                                    SendPacket(CoServer.MyPackets.NPCLink("Bague/Bracelet", 6));
                                    SendPacket(CoServer.MyPackets.NPCLink("Botte", 7));
                                    SendPacket(CoServer.MyPackets.NPCLink("Bouclier", 13));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je ne fais que passer.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("� quel objet voulez-vous ajouter un trou?"));
                                    SendPacket(CoServer.MyPackets.NPCLink("Casque/Boucle", 8));
                                    SendPacket(CoServer.MyPackets.NPCLink("Collier/Sac", 9));
                                    SendPacket(CoServer.MyPackets.NPCLink("Armure/Robe", 10));
                                    SendPacket(CoServer.MyPackets.NPCLink("Bague/Bracelet", 11));
                                    SendPacket(CoServer.MyPackets.NPCLink("Botte", 12));
                                    SendPacket(CoServer.MyPackets.NPCLink("Bouclier", 14));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je ne fais que passer.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 3 || Control == 4 || Control == 5 || Control == 6 || Control == 7 || Control == 13)
                                {
                                    int into = 0;

                                    if (Control == 3 && MyChar.Equips[1] != null && MyChar.Equips[1] != "0")
                                        into = 1;
                                    else if (Control == 4 && MyChar.Equips[2] != null && MyChar.Equips[2] != "0")
                                        into = 2;
                                    else if (Control == 5 && MyChar.Equips[3] != null && MyChar.Equips[3] != "0")
                                        into = 3;
                                    else if (Control == 6 && MyChar.Equips[6] != null && MyChar.Equips[6] != "0")
                                        into = 6;
                                    else if (Control == 7 && MyChar.Equips[8] != null && MyChar.Equips[8] != "0")
                                        into = 8;
                                    else if (Control == 13 && MyChar.Equips[5] != null && MyChar.Equips[5] != "0")
                                        into = 5;
                                    else
                                        return;

                                    if (MyChar.InventoryContains(1200005, 3))
                                    {
                                        string[] item = MyChar.Equips[into].Split('-');

                                        if (item[0] == "1050000" || item[0] == "1050001" || item[0] == "1050002")
                                            return;

                                        if (item[4] != "0")
                                            return;


                                        MyChar.RemoveItem(MyChar.ItemNext(1200005));
                                        MyChar.RemoveItem(MyChar.ItemNext(1200005));
                                        MyChar.RemoveItem(MyChar.ItemNext(1200005));


                                        item[4] = "255";

                                        MyChar.Equips[into] = item[0] + "-" + item[1] + "-" + item[2] + "-" + item[3] + "-" + item[4] + "-" + item[5];
                                        SendPacket(CoServer.MyPackets.AddItem((long)MyChar.Equips_UIDs[into], int.Parse(item[0]), byte.Parse(item[1]), byte.Parse(item[2]), byte.Parse(item[3]), byte.Parse(item[4]), byte.Parse(item[5]), (byte)into, 100, 100));
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas assez de ForeusesDiamants."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }

                                if (Control == 8 || Control == 9 || Control == 10 || Control == 11 || Control == 12 || Control == 14)
                                {
                                    int into = 0;

                                    if (Control == 8 && MyChar.Equips[1] != null && MyChar.Equips[1] != "0")
                                        into = 1;
                                    else if (Control == 9 && MyChar.Equips[2] != null && MyChar.Equips[2] != "0")
                                        into = 2;
                                    else if (Control == 10 && MyChar.Equips[3] != null && MyChar.Equips[3] != "0")
                                        into = 3;
                                    else if (Control == 11 && MyChar.Equips[6] != null && MyChar.Equips[6] != "0")
                                        into = 6;
                                    else if (Control == 12 && MyChar.Equips[8] != null && MyChar.Equips[8] != "0")
                                        into = 8;
                                    else if (Control == 14 && MyChar.Equips[5] != null && MyChar.Equips[5] != "0")
                                        into = 5;
                                    else
                                        return;

                                    if (MyChar.InventoryContains(1200005, 9))
                                    {
                                        string[] item = MyChar.Equips[into].Split('-');

                                        if (item[0] == "1050000" || item[0] == "1050001" || item[0] == "1050002")
                                            return;

                                        if (item[5] != "0" || item[4] == "0")
                                            return;

                                        for (byte i = 0; i < 9; i++)
                                            MyChar.RemoveItem(MyChar.ItemNext(1200005));

                                        item[5] = "255";

                                        MyChar.Equips[into] = item[0] + "-" + item[1] + "-" + item[2] + "-" + item[3] + "-" + item[4] + "-" + item[5];
                                        SendPacket(CoServer.MyPackets.AddItem((long)MyChar.Equips_UIDs[into], int.Parse(item[0]), byte.Parse(item[1]), byte.Parse(item[2]), byte.Parse(item[3]), byte.Parse(item[4]), byte.Parse(item[5]), (byte)into, 100, 100));
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas assez de ForeusesDiamants."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }

                            if (CurrentNPC == 10064)
                            {
                                if (Control == 1 || Control == 2 || Control == 3)
                                {
                                    if (Control == 2 && MyChar.Equips[1] != "0" && MyChar.Equips[1] != null)
                                    {
                                        string[] Head = MyChar.Equips[1].Split('-');
                                        if (ItemHandler.WeaponType(uint.Parse(Head[0])) == 112 || ItemHandler.WeaponType(uint.Parse(Head[0])) == 117 || ItemHandler.WeaponType(uint.Parse(Head[0])) == 118)
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous ne pouvez pas teindre cette objet!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            ChColor = Control;
                                            SendPacket(CoServer.MyPackets.NPCSay("Voici les sept couleurs que je peux utiliser. Vous pouvez essayer toutes les couleurs sans payer plus. Quel est la meilleur couleur selon vous?"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Orange", 4));
                                            SendPacket(CoServer.MyPackets.NPCLink("Bleu ciel", 5));
                                            SendPacket(CoServer.MyPackets.NPCLink("Rouge", 6));
                                            SendPacket(CoServer.MyPackets.NPCLink("Bleu", 7));
                                            SendPacket(CoServer.MyPackets.NPCLink("Jaune", 8));
                                            SendPacket(CoServer.MyPackets.NPCLink("Mauve", 9));
                                            SendPacket(CoServer.MyPackets.NPCLink("Blanc", 10));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    else if (Control == 3 && MyChar.Equips[5] != "0" && MyChar.Equips[5] != null)
                                    {
                                        string[] Shield = MyChar.Equips[5].Split('-');
                                        if (ItemHandler.WeaponType(uint.Parse(Shield[0])) != 900)
                                        {

                                            SendPacket(CoServer.MyPackets.NPCSay("Vous ne pouvez pas teindre cette objet!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            ChColor = Control;
                                            SendPacket(CoServer.MyPackets.NPCSay("Voici les sept couleurs que je peux utiliser. Vous pouvez essayer toutes les couleurs sans payer plus. Quel est la meilleur couleur selon vous?"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Orange", 4));
                                            SendPacket(CoServer.MyPackets.NPCLink("Bleu ciel", 5));
                                            SendPacket(CoServer.MyPackets.NPCLink("Rouge", 6));
                                            SendPacket(CoServer.MyPackets.NPCLink("Bleu", 7));
                                            SendPacket(CoServer.MyPackets.NPCLink("Jaune", 8));
                                            SendPacket(CoServer.MyPackets.NPCLink("Mauve", 9));
                                            SendPacket(CoServer.MyPackets.NPCLink("Blanc", 10));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    else
                                    {
                                        ChColor = Control;
                                        SendPacket(CoServer.MyPackets.NPCSay("Voici les sept couleurs que je peux utiliser. Vous pouvez essayer toutes les couleurs sans payer plus. Quel est la meilleur couleur selon vous?"));
                                        SendPacket(CoServer.MyPackets.NPCLink("Orange", 4));
                                        SendPacket(CoServer.MyPackets.NPCLink("Bleu ciel", 5));
                                        SendPacket(CoServer.MyPackets.NPCLink("Rouge", 6));
                                        SendPacket(CoServer.MyPackets.NPCLink("Bleu", 7));
                                        SendPacket(CoServer.MyPackets.NPCLink("Jaune", 8));
                                        SendPacket(CoServer.MyPackets.NPCLink("Mauve", 9));
                                        SendPacket(CoServer.MyPackets.NPCLink("Blanc", 10));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 4 || Control == 5 || Control == 6 || Control == 7 || Control == 8 || Control == 9 || Control == 10)
                                {
                                    try
                                    {
                                        string[] item;
                                        string[] item2 = null;
                                        if (ChColor == 3)
                                            item2 = MyChar.Equips[5].Split('-');
                                        string newitem;

                                        if (ChColor == 1 && MyChar.Equips[3] != null && MyChar.Equips[3] != "0")
                                            item = MyChar.Equips[3].Split('-');
                                        else if (ChColor == 2 && MyChar.Equips[1] != null && MyChar.Equips[1] != "0")
                                            item = MyChar.Equips[1].Split('-');
                                        else if (ChColor == 3)
                                            if (MyChar.Equips[5] != null && ItemHandler.WeaponType(uint.Parse(item2[0])) == 900)
                                                item = MyChar.Equips[5].Split('-');
                                            else
                                            {
                                                SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas de bouclier d'�quip�."));
                                                SendPacket(CoServer.MyPackets.NPCLink("Je suis d�sol�.", 255));
                                                SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                                SendPacket(CoServer.MyPackets.NPCFinish());
                                                return;
                                            }
                                        else
                                            return;

                                        newitem = item[0];
                                        newitem = newitem.Remove(newitem.Length - 3, 1);

                                        if (Control == 4)
                                            newitem = newitem.Insert(newitem.Length - 2, "3");
                                        if (Control == 5)
                                            newitem = newitem.Insert(newitem.Length - 2, "4");
                                        if (Control == 6)
                                            newitem = newitem.Insert(newitem.Length - 2, "5");
                                        if (Control == 7)
                                            newitem = newitem.Insert(newitem.Length - 2, "6");
                                        if (Control == 8)
                                            newitem = newitem.Insert(newitem.Length - 2, "7");
                                        if (Control == 9)
                                            newitem = newitem.Insert(newitem.Length - 2, "8");
                                        if (Control == 10)
                                            newitem = newitem.Insert(newitem.Length - 2, "9");

                                        if (ChColor == 1)
                                        {
                                            MyChar.Equips[3] = newitem + "-" + item[1] + "-" + item[2] + "-" + item[3] + "-" + item[4] + "-" + item[5];
                                            SendPacket(CoServer.MyPackets.AddItem((long)MyChar.Equips_UIDs[3], int.Parse(newitem), byte.Parse(item[1]), byte.Parse(item[2]), byte.Parse(item[3]), byte.Parse(item[4]), byte.Parse(item[5]), 3, 100, 100));
                                        }
                                        if (ChColor == 2)
                                        {
                                            MyChar.Equips[1] = newitem + "-" + item[1] + "-" + item[2] + "-" + item[3] + "-" + item[4] + "-" + item[5];
                                            SendPacket(CoServer.MyPackets.AddItem((long)MyChar.Equips_UIDs[1], int.Parse(newitem), byte.Parse(item[1]), byte.Parse(item[2]), byte.Parse(item[3]), byte.Parse(item[4]), byte.Parse(item[5]), 1, 100, 100));
                                        }
                                        if (ChColor == 3)
                                        {
                                            MyChar.Equips[5] = newitem + "-" + item[1] + "-" + item[2] + "-" + item[3] + "-" + item[4] + "-" + item[5];
                                            SendPacket(CoServer.MyPackets.AddItem((long)MyChar.Equips_UIDs[5], int.Parse(newitem), byte.Parse(item[1]), byte.Parse(item[2]), byte.Parse(item[3]), byte.Parse(item[4]), byte.Parse(item[5]), 5, 100, 100));
                                        }
                                        ChColor = 0;
                                    }
                                    catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
                                }
                            }

                            if (CurrentNPC == 10063)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.InventoryContains(1088001, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(1088001));
                                        MyChar.Teleport(1008, 22, 26);
                                    }
                                }
                                if (Control == 2)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Je peux teindre votre armure en noire pour une Perle de Dragon. Vous pourrez toujours la changer de couleur par la suite. Voulez-vous toujours la teindre?"));
                                    SendPacket(CoServer.MyPackets.NPCLink("Oui, voici une Perle de Dragon.", 3));
                                    SendPacket(CoServer.MyPackets.NPCLink("Laisser moi y penser.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 3)
                                {
                                    if (MyChar.Equips[3] != null && MyChar.Equips[3] != "0")
                                    {
                                        if (MyChar.InventoryContains(1088000, 1))
                                        {
                                            MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                            string[] item = MyChar.Equips[3].Split('-');
                                            if (ItemHandler.WeaponType(uint.Parse(item[0])) != 132)
                                            {
                                                string newitem = item[0];
                                                newitem = newitem.Remove(newitem.Length - 3, 1);
                                                newitem = newitem.Insert(newitem.Length - 2, "2");
                                                MyChar.Equips[3] = newitem + "-" + item[1] + "-" + item[2] + "-" + item[3] + "-" + item[4] + "-" + item[5];
                                                SendPacket(CoServer.MyPackets.AddItem((long)MyChar.Equips_UIDs[3], int.Parse(newitem), byte.Parse(item[1]), byte.Parse(item[2]), byte.Parse(item[3]), byte.Parse(item[4]), byte.Parse(item[5]), 3, 100, 100));
                                            }
                                        }
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas une Perle de Dragon."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                }
                            }

                            if (CurrentNPC == 300000)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Vous en avez " + MyChar.VPs));
                                    SendPacket(CoServer.MyPackets.NPCLink("Ok, merci.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Je peux vous donner une m�t�ore pour 5.000 points de vertue et une perle de dragon pour 270.000 points de vertue."));
                                    SendPacket(CoServer.MyPackets.NPCLink("M�t�ore", 3));
                                    SendPacket(CoServer.MyPackets.NPCLink("Perle De Dragon", 4));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 3)
                                {
                                    if (MyChar.VPs >= 5000)
                                    {
                                        if (MyChar.ItemsInInventory < 40)
                                        {
                                            if (MyChar.ItemsInInventory < 40)
                                            {
                                                MyChar.VPs -= 5000;
                                                MyChar.AddItem("1088001-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                            }
                                            else
                                                SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Votre sac est complet!", 2005));
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas 5,000 points de vertue."));
                                        SendPacket(CoServer.MyPackets.NPCLink("D�sol�", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 4)
                                {
                                    if (MyChar.VPs >= 270000)
                                    {
                                        if (MyChar.ItemsInInventory < 40)
                                        {
                                            MyChar.VPs -= 270000;
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        else
                                            SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Votre sac est complet!", 2005));
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas 270,000 points de vertue."));
                                        SendPacket(CoServer.MyPackets.NPCLink("D�sol�", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 11010)
                            {
                                ushort Req = 0;
                                string TheItem = "";
                                switch (Control)
                                {
                                    case 1:
                                        Req = 20;
                                        TheItem = "1088000-0-0-0-0-0";
                                        break;
                                    case 2:
                                        Req = 10;
                                        TheItem = "730003-3-0-0-0-0";
                                        break;
                                    case 3:
                                        Req = 30;
                                        TheItem = "730004-4-0-0-0-0";
                                        break;
                                    case 4:
                                        Req = 90;
                                        TheItem = "730005-5-0-0-0-0";
                                        break;
                                    case 5:
                                        Req = 265;
                                        TheItem = "730006-6-0-0-0-0";
                                        break;
                                    case 6:
                                        Req = 800;
                                        TheItem = "730007-7-0-0-0-0";
                                        break;
                                    case 7:
                                        Req = 2380;
                                        TheItem = "730008-8-0-0-0-0";
                                        break;
                                    case 9:
                                        Req = 15;
                                        TheItem = "721259-0-0-0-0-0";
                                        break;
                                    case 10:
                                        Req = 170;
                                        TheItem = "723701-0-0-0-0-0";
                                        break;
                                    case 11:
                                        Req = 165;
                                        TheItem = "2100045-0-0-0-0-0";
                                        break;
                                    case 12:
                                        Req = 370;
                                        TheItem = "2100025-0-0-0-0-0";
                                        break;
                                    case 13:
                                        Req = 5000;
                                        TheItem = "2100055-0-0-0-0-0";
                                        break;
                                    case 14:
                                        Req = 170;
                                        TheItem = "1200005-0-0-0-0-0";
                                        break;
                                }

                                if (Control >= 1 && Control <= 7 || Control >= 9 && Control <= 14)
                                {
                                    if (MyChar.ItemsInInventory <= 39)
                                    {
                                        if (MyChar.PKPoints >= Req)
                                        {
                                            MyChar.AddItem(TheItem, 0, (uint)Program.Rand.Next(36457836));
                                            MyChar.PKPoints -= Req;
                                        }
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas assez de points de pk!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Je vois", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(200));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Votre inventaire est plein..."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(200));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }

                                if (Control == 8)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Je suis riche en produit en tout genre. Je vois que vous �tes un bandit. Que voulez-vous?"));
                                    SendPacket(CoServer.MyPackets.NPCLink("Sainte Pierre[15 Pkp]", 9));
                                    SendPacket(CoServer.MyPackets.NPCLink("Signe Franchise[170 Pkp]", 10));
                                    SendPacket(CoServer.MyPackets.NPCLink("Bouteille Magic[165 Pkp]", 11));
                                    SendPacket(CoServer.MyPackets.NPCLink("Gourde Magique[370 Pkp]", 12));
                                    SendPacket(CoServer.MyPackets.NPCLink("Prix de bronze[5000 Pkp]", 13));
                                    SendPacket(CoServer.MyPackets.NPCLink("Foreuse Diamant[170 Pkp]", 14));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(200));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                            }
                            if (CurrentNPC == 350050)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.RBCount > 0 && (MyChar.InventoryContains(1088000, 1)))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));

                                        if (MyChar.Job > 9 && MyChar.Job < 16)
                                        {
                                            MyChar.Str = 5;
                                            MyChar.Agi = 2;
                                            MyChar.Vit = 3;
                                            MyChar.Spi = 0;
                                        }
                                        if (MyChar.Job > 19 && MyChar.Job < 26)
                                        {
                                            MyChar.Str = 5;
                                            MyChar.Agi = 2;
                                            MyChar.Vit = 3;
                                            MyChar.Spi = 0;
                                        }
                                        if (MyChar.Job > 39 && MyChar.Job < 46)
                                        {
                                            MyChar.Str = 2;
                                            MyChar.Agi = 7;
                                            MyChar.Vit = 1;
                                            MyChar.Spi = 0;
                                        }
                                        if (MyChar.Job > 129 && MyChar.Job < 136 || MyChar.Job > 139 && MyChar.Job < 146 || MyChar.Job == 100 || MyChar.Job == 101)
                                        {
                                            MyChar.Str = 0;
                                            MyChar.Agi = 2;
                                            MyChar.Vit = 3;
                                            MyChar.Spi = 5;
                                        }

                                        MyChar.StatP = 0;

                                        #region FirstRbStats
                                        if ((MyChar.FirstLevel == 112) || (MyChar.FirstLevel == 113) && (MyChar.FirstJob == 135))
                                            MyChar.StatP += 1;
                                        else if ((MyChar.FirstLevel == 114) || (MyChar.FirstLevel == 115) && (MyChar.FirstJob == 135))
                                            MyChar.StatP += 3;
                                        else if ((MyChar.FirstLevel == 116) || (MyChar.FirstLevel == 117) && (MyChar.FirstJob == 135))
                                            MyChar.StatP += 6;
                                        else if ((MyChar.FirstLevel == 118) || (MyChar.FirstLevel == 119) && (MyChar.FirstJob == 135))
                                            MyChar.StatP += 10;
                                        else if (MyChar.FirstLevel == 120)
                                        {
                                            if (MyChar.FirstJob == 135)
                                                MyChar.StatP += 15;
                                        }
                                        else if (MyChar.FirstLevel == 121)
                                        {
                                            if (MyChar.FirstJob == 135)
                                                MyChar.StatP += 15;
                                            else
                                                MyChar.StatP += 1;
                                        }
                                        else if (MyChar.FirstLevel == 122)
                                        {
                                            if (MyChar.FirstJob == 135)
                                                MyChar.StatP += 21;
                                            else
                                                MyChar.StatP += 3;
                                        }
                                        else if (MyChar.FirstLevel == 123)
                                        {
                                            if (MyChar.FirstJob == 135)
                                                MyChar.StatP += 21;
                                            else
                                                MyChar.StatP += 6;
                                        }
                                        else if (MyChar.FirstLevel == 124)
                                        {
                                            if (MyChar.FirstJob == 135)
                                                MyChar.StatP += 28;
                                            else
                                                MyChar.StatP += 10;
                                        }
                                        else if (MyChar.FirstLevel == 125)
                                        {
                                            if (MyChar.FirstJob == 135)
                                                MyChar.StatP += 28;
                                            else
                                                MyChar.StatP += 15;
                                        }
                                        else if (MyChar.FirstLevel == 126)
                                        {
                                            if (MyChar.FirstJob == 135)
                                                MyChar.StatP += 36;
                                            else
                                                MyChar.StatP += 21;
                                        }
                                        else if (MyChar.FirstLevel == 127)
                                        {
                                            if (MyChar.FirstJob == 135)
                                                MyChar.StatP += 36;
                                            else
                                                MyChar.StatP += 28;
                                        }
                                        else if (MyChar.FirstLevel == 128)
                                        {
                                            if (MyChar.FirstJob == 135)
                                                MyChar.StatP += 45;
                                            else
                                                MyChar.StatP += 36;
                                        }
                                        else if (MyChar.FirstLevel == 129)
                                            MyChar.StatP += 45;
                                        else if (MyChar.FirstLevel >= 130)
                                            MyChar.StatP += (uint)(((MyChar.FirstLevel - 130) * 5) + 55);
                                        #endregion
                                        #region SecRbStats
                                        if ((MyChar.SecondLevel == 112) || (MyChar.SecondLevel == 113) && (MyChar.SecondJob == 135))
                                            MyChar.StatP += 1;
                                        else if ((MyChar.SecondLevel == 114) || (MyChar.SecondLevel == 115) && (MyChar.SecondJob == 135))
                                            MyChar.StatP += 3;
                                        else if ((MyChar.SecondLevel == 116) || (MyChar.SecondLevel == 117) && (MyChar.SecondJob == 135))
                                            MyChar.StatP += 6;
                                        else if ((MyChar.SecondLevel == 118) || (MyChar.SecondLevel == 119) && (MyChar.SecondJob == 135))
                                            MyChar.StatP += 10;
                                        else if (MyChar.SecondLevel == 120)
                                        {
                                            if (MyChar.SecondJob == 135)
                                                MyChar.StatP += 15;
                                        }
                                        else if (MyChar.SecondLevel == 121)
                                        {
                                            if (MyChar.SecondJob == 135)
                                                MyChar.StatP += 15;
                                            else
                                                MyChar.StatP += 1;
                                        }
                                        else if (MyChar.SecondLevel == 122)
                                        {
                                            if (MyChar.SecondJob == 135)
                                                MyChar.StatP += 21;
                                            else
                                                MyChar.StatP += 3;
                                        }
                                        else if (MyChar.SecondLevel == 123)
                                        {
                                            if (MyChar.SecondJob == 135)
                                                MyChar.StatP += 21;
                                            else
                                                MyChar.StatP += 6;
                                        }
                                        else if (MyChar.SecondLevel == 124)
                                        {
                                            if (MyChar.SecondJob == 135)
                                                MyChar.StatP += 28;
                                            else
                                                MyChar.StatP += 10;
                                        }
                                        else if (MyChar.SecondLevel == 125)
                                        {
                                            if (MyChar.SecondJob == 135)
                                                MyChar.StatP += 28;
                                            else
                                                MyChar.StatP += 15;
                                        }
                                        else if (MyChar.SecondLevel == 126)
                                        {
                                            if (MyChar.SecondJob == 135)
                                                MyChar.StatP += 36;
                                            else
                                                MyChar.StatP += 21;
                                        }
                                        else if (MyChar.SecondLevel == 127)
                                        {
                                            if (MyChar.SecondJob == 135)
                                                MyChar.StatP += 36;
                                            else
                                                MyChar.StatP += 28;
                                        }
                                        else if (MyChar.SecondLevel == 128)
                                        {
                                            if (MyChar.SecondJob == 135)
                                                MyChar.StatP += 45;
                                            else
                                                MyChar.StatP += 36;
                                        }
                                        else if (MyChar.SecondLevel == 129)
                                            MyChar.StatP += 45;
                                        else if (MyChar.SecondLevel >= 130)
                                            MyChar.StatP += (uint)(((MyChar.SecondLevel - 130) * 5) + 55);
                                        #endregion
                                        #region ThirdRbStats
                                        if ((MyChar.ThirdLevel == 112 || MyChar.ThirdLevel == 113) && MyChar.ThirdJob == 135)
                                            MyChar.StatP += 1;
                                        else if ((MyChar.ThirdLevel == 114 || MyChar.ThirdLevel == 115) && MyChar.ThirdJob == 135)
                                            MyChar.StatP += 3;
                                        else if ((MyChar.ThirdLevel == 116 || MyChar.ThirdLevel == 117) && MyChar.ThirdJob == 135)
                                            MyChar.StatP += 6;
                                        else if ((MyChar.ThirdLevel == 118 || MyChar.ThirdLevel == 119) && MyChar.ThirdJob == 135)
                                            MyChar.StatP += 10;
                                        else if (MyChar.ThirdLevel == 120)
                                        {
                                            if (MyChar.ThirdJob == 135)
                                                MyChar.StatP += 15;
                                        }
                                        else if (MyChar.ThirdLevel == 121)
                                        {
                                            if (MyChar.ThirdJob == 135)
                                                MyChar.StatP += 15;
                                            else
                                                MyChar.StatP += 1;
                                        }
                                        else if (MyChar.ThirdLevel == 122)
                                        {
                                            if (MyChar.ThirdJob == 135)
                                                MyChar.StatP += 21;
                                            else
                                                MyChar.StatP += 3;
                                        }
                                        else if (MyChar.ThirdLevel == 123)
                                        {
                                            if (MyChar.ThirdJob == 135)
                                                MyChar.StatP += 21;
                                            else
                                                MyChar.StatP += 6;
                                        }
                                        else if (MyChar.ThirdLevel == 124)
                                        {
                                            if (MyChar.ThirdJob == 135)
                                                MyChar.StatP += 28;
                                            else
                                                MyChar.StatP += 10;
                                        }
                                        else if (MyChar.ThirdLevel == 125)
                                        {
                                            if (MyChar.ThirdJob == 135)
                                                MyChar.StatP += 28;
                                            else
                                                MyChar.StatP += 15;
                                        }
                                        else if (MyChar.ThirdLevel == 126)
                                        {
                                            if (MyChar.ThirdJob == 135)
                                                MyChar.StatP += 36;
                                            else
                                                MyChar.StatP += 21;
                                        }
                                        else if (MyChar.ThirdLevel == 127)
                                        {
                                            if (MyChar.ThirdJob == 135)
                                                MyChar.StatP += 36;
                                            else
                                                MyChar.StatP += 28;
                                        }
                                        else if (MyChar.ThirdLevel == 128)
                                        {
                                            if (MyChar.ThirdJob == 135)
                                                MyChar.StatP += 45;
                                            else
                                                MyChar.StatP += 36;
                                        }
                                        else if (MyChar.ThirdLevel == 129)
                                            MyChar.StatP += 45;
                                        else if (MyChar.ThirdLevel >= 130)
                                            MyChar.StatP += (uint)(((MyChar.ThirdLevel - 130) * 5) + 55);
                                        #endregion

                                        uint levelstat = 0;

                                        levelstat = ((uint)MyChar.Level - 15) * 3;
                                        MyChar.StatP += levelstat;
                                        MyChar.StatP += 42;
                                        MyChar.StatP += 30;
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous n'�tes pas qualifi� pour cette action. Revenez quand vous serez rena�t et niveau 70. De plus il vous faudra une Perle de Dragon."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 3381)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Si vous me donnez une Perle de Dragon je peux changer votre taille. "));
                                    SendPacket(CoServer.MyPackets.NPCLink("Voici une Perle de Dragon.", 2));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je n'ai pas de Perle de Dragon.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.InventoryContains(1088000, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));

                                        if (MyChar.Model == 1004)
                                            MyChar.Model -= 1;
                                        else if (MyChar.Model == 1003)
                                            MyChar.Model += 1;
                                        if (MyChar.Model == 2002)
                                            MyChar.Model -= 1;
                                        else if (MyChar.Model == 2001)
                                            MyChar.Model += 1;


                                        SendPacket(CoServer.MyPackets.Vital(MyChar.UID, 12, ulong.Parse(MyChar.Avatar.ToString() + MyChar.Model.ToString())));
                                        World.UpdateSpawn(MyChar);

                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("D�sol�, vous n'avez pas de Perle de Dragon."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Ok, je vois.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }

                            #region FireTaoTrainer
                            if (CurrentNPC == 10000)
                            {
                                if (Control == 100)
                                {
                                    if (MyChar.Job < 146 && MyChar.Job > 139)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Voici les pouvoirs que je peux vous enseigner si vous avez le niveau requis."));
                                        SendPacket(CoServer.MyPackets.NPCLink("�clair: Lv 15", 104));
                                        SendPacket(CoServer.MyPackets.NPCLink("Feu: Lv 40", 105));
                                        SendPacket(CoServer.MyPackets.NPCLink("Volcan: Lv 40", 106));
                                        SendPacket(CoServer.MyPackets.NPCLink("M�ditation: Lv 44", 107));
                                        SendPacket(CoServer.MyPackets.NPCLink("M�t�ore de feu: Lv 52", 103));
                                        SendPacket(CoServer.MyPackets.NPCLink("Anneau de feu: Lv 55", 102));
                                        SendPacket(CoServer.MyPackets.NPCLink("Cercle de feu: Lv 65", 101));
                                        SendPacket(CoServer.MyPackets.NPCLink("Vent: Lv 90", 108));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous n'�tes pas un tao�ste de feu partez!"));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je suis d�sol�.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (MyChar.Job < 146 && MyChar.Job > 139)
                                {
                                    if (Control == 250)
                                    {
                                        if (MyChar.Level >= 70)
                                            if (MyChar.Job < 136 && MyChar.Job > 129)
                                            {
                                                MyChar.LearnSkill(1120, 0);
                                            }
                                    }
                                    if (Control == 101)
                                    {
                                        if (MyChar.Level >= 65)
                                        {
                                            MyChar.LearnSkill(1120, 0);
                                        }
                                    }
                                    if (Control == 102)
                                    {
                                        if (MyChar.Level >= 55)
                                        {
                                            MyChar.LearnSkill(1150, 0);
                                        }
                                    }
                                    if (Control == 103)
                                    {
                                        if (MyChar.Level >= 52)
                                        {
                                            MyChar.LearnSkill(1180, 0);
                                        }
                                    }
                                    if (Control == 104)
                                    {
                                        if (MyChar.Level >= 15)
                                        {
                                            MyChar.LearnSkill(1010, 0);
                                        }
                                    }
                                    if (Control == 105)
                                    {
                                        if (MyChar.Level >= 40)
                                        {
                                            MyChar.LearnSkill(1001, 0);
                                        }
                                    }
                                    if (Control == 106)
                                    {
                                        if (MyChar.Level >= 40)
                                        {
                                            MyChar.LearnSkill(1125, 0);
                                        }
                                    }
                                    if (Control == 107)
                                    {
                                        if (MyChar.Level >= 44)
                                        {
                                            MyChar.LearnSkill(1195, 0);
                                        }
                                    }
                                    if (Control == 108)
                                    {
                                        if (MyChar.Level >= 90)
                                        {
                                            MyChar.LearnSkill(1002, 0);
                                        }
                                    }
                                }
                                if (Control == 1)
                                {
                                    if (MyChar.Job == 100)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Tao�ste vous devez �tre niveau 15."));
                                    }
                                    if (MyChar.Job == 101)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Tao�ste De Feu ou D'eau vous devez �tre niveau 40. Quel classe voulez-vous choisir?"));
                                    }
                                    if (MyChar.Job == 142)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Magicien De Feu vous devez �tre niveau 70."));
                                    }
                                    if (MyChar.Job == 143)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Sorcier De Feu vous devez �tre niveau 100."));
                                    }
                                    if (MyChar.Job == 144)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Enchanteur De Feu vous devez �tre niveau 110."));
                                    }
                                    if (MyChar.Job != 145 && MyChar.Job != 101)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCLink("Je veux avancer.", 3));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je sais, je ne suis pas qualifi�.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Job == 101)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCLink("Tao�ste De Feu", 3));
                                        SendPacket(CoServer.MyPackets.NPCLink("Tao�ste D`eau", 2));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous ne pouvez plus avancer."));
                                        SendPacket(CoServer.MyPackets.NPCLink("D�sol�.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    bool Promoted = false;

                                    if (MyChar.Job == 100 && MyChar.Level >= 15)
                                    {
                                        MyChar.Job = 101;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 101 && MyChar.Level >= 40)
                                    {
                                        MyChar.Job = 132;
                                        Promoted = true;
                                    }

                                    if (Promoted)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("F�licitation, vous avez avancer."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Cool!.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous ne pouvez pas avancer."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }

                                if (Control == 3)
                                {
                                    bool Promoted = false;

                                    if (MyChar.Job == 100 && MyChar.Level >= 15)
                                    {
                                        MyChar.Job = 101;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 101 && MyChar.Level >= 40)
                                    {
                                        MyChar.Job = 142;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 142 && MyChar.Level >= 70)
                                    {
                                        MyChar.Job = 143;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 143 && MyChar.Level >= 100)
                                    {
                                        if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("134287-0-0-0-31-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 144;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 144 && MyChar.Level >= 110)
                                    {
                                        if (MyChar.RBCount == 0)
                                        {
                                            MyChar.AddItem("700002-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("160199-0-0-0-33-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 145;
                                        Promoted = true;
                                    }

                                    if (Promoted)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("F�licitation, vous avez avancer."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Cool!.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous ne pouvez pas avancer."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }

                            #endregion
                            #region WaterTaoTrainer
                            if (CurrentNPC == 30)
                            {
                                if (Control == 100)
                                {
                                    if (MyChar.Job < 136 && MyChar.Job > 129)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Voici les pouvoirs que je peux vous enseigner si vous avez le niveau requis."));
                                        SendPacket(CoServer.MyPackets.NPCLink("�clair: Lv 15", 106));
                                        SendPacket(CoServer.MyPackets.NPCLink("Feu: Lv 40", 107));
                                        SendPacket(CoServer.MyPackets.NPCLink("Volcan: Lv 40", 108));
                                        SendPacket(CoServer.MyPackets.NPCLink("Cure de pluie: Lv 40", 109));
                                        SendPacket(CoServer.MyPackets.NPCLink("Revivre: Lv 40", 110));
                                        SendPacket(CoServer.MyPackets.NPCLink("M�ditation: Lv 44", 103));
                                        SendPacket(CoServer.MyPackets.NPCLink("�toile exacte: Lv 45", 111));
                                        SendPacket(CoServer.MyPackets.NPCLink("Suivante...", 200)); ;
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous n'�tes pas un Tao�ste D'eau partez!"));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je suis d�sol�.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 200)
                                {
                                    if (MyChar.Job < 136 && MyChar.Job > 129)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Voici les pouvoirs que je peux vous enseigner si vous avez le niveau requis."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Bouclier magique: Lv 50", 112));
                                        SendPacket(CoServer.MyPackets.NPCLink("Stigmate rouge: Lv 55", 101));
                                        SendPacket(CoServer.MyPackets.NPCLink("Invisibilit�: Lv 60", 113));
                                        SendPacket(CoServer.MyPackets.NPCLink("Pri�re: Lv 70", 102));
                                        SendPacket(CoServer.MyPackets.NPCLink("Cure avanc�e: Lv 81", 114));
                                        SendPacket(CoServer.MyPackets.NPCLink("Nectar: Lv 94", 115));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous n'�tes pas un Tao�ste D'eau partez!"));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je suis d�sol�.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (MyChar.Job < 136 && MyChar.Job > 129)
                                {

                                    if (Control == 101)
                                    {
                                        if (MyChar.Level >= 55)
                                        {
                                            MyChar.LearnSkill(1095, 0);
                                        }
                                    }
                                    if (Control == 102)
                                    {
                                        if (MyChar.Level >= 70)
                                        {
                                            MyChar.LearnSkill(1100, 0);
                                        }
                                    }
                                    if (Control == 103)
                                    {
                                        if (MyChar.Level >= 44)
                                        {
                                            MyChar.LearnSkill(1195, 0);
                                        }
                                    }
                                    if (Control == 106)
                                    {
                                        if (MyChar.Level >= 15)
                                        {
                                            MyChar.LearnSkill(1010, 0);
                                        }
                                    }
                                    if (Control == 107)
                                    {
                                        if (MyChar.Level >= 40)
                                        {
                                            MyChar.LearnSkill(1001, 0);
                                        }
                                    }
                                    if (Control == 108)
                                    {
                                        if (MyChar.Level >= 40)
                                        {
                                            MyChar.LearnSkill(1125, 0);
                                        }
                                    }
                                    if (Control == 109)
                                    {
                                        if (MyChar.Level >= 40)
                                        {
                                            MyChar.LearnSkill(1055, 0);
                                        }
                                    }
                                    if (Control == 110)
                                    {
                                        if (MyChar.Level >= 40)
                                        {
                                            MyChar.LearnSkill(1050, 0);
                                        }
                                    }
                                    if (Control == 111)
                                    {
                                        if (MyChar.Level >= 45)
                                        {
                                            MyChar.LearnSkill(1085, 0);
                                        }
                                    }
                                    if (Control == 112)
                                    {
                                        if (MyChar.Level >= 50)
                                        {
                                            MyChar.LearnSkill(1090, 0);
                                        }
                                    }
                                    if (Control == 113)
                                    {
                                        if (MyChar.Level >= 60)
                                        {
                                            MyChar.LearnSkill(1075, 0);
                                        }
                                    }
                                    if (Control == 114)
                                    {
                                        if (MyChar.Level >= 81)
                                        {
                                            MyChar.LearnSkill(1175, 0);
                                        }
                                    }
                                    if (Control == 115)
                                    {
                                        if (MyChar.Level >= 94)
                                        {
                                            MyChar.LearnSkill(1170, 0);
                                        }
                                    }
                                }
                                if (Control == 1)
                                {
                                    if (MyChar.Job == 100)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Tao�ste vous devez �tre niveau 15."));
                                    }
                                    if (MyChar.Job == 101)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Tao�ste De Feu ou D'eau vous devez �tre niveau 40. Quel classe voulez-vous choisir?"));
                                    }
                                    if (MyChar.Job == 132)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Magicien D'eau vous devez �tre niveau 70."));
                                    }
                                    if (MyChar.Job == 133)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Sorcier D'eau vous devez �tre niveau 100."));
                                    }
                                    if (MyChar.Job == 134)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Enchanteur D'eau vous devez �tre niveau 110."));
                                    }
                                    if (MyChar.Job != 135 && MyChar.Job != 101)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCLink("Je veux avancer.", 2));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je sais, je ne suis pas qualifi�.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Job == 101)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCLink("Tao�ste De Feu", 3));
                                        SendPacket(CoServer.MyPackets.NPCLink("Tao�ste D`eau", 2));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous ne pouvez plus avancer."));
                                        SendPacket(CoServer.MyPackets.NPCLink("D�sol�.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    bool Promoted = false;

                                    if (MyChar.Job == 100 && MyChar.Level >= 15)
                                    {
                                        MyChar.Job = 101;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 101 && MyChar.Level >= 40)
                                    {
                                        MyChar.Job = 132;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 132 && MyChar.Level >= 70)
                                    {
                                        MyChar.Job = 133;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 133 && MyChar.Level >= 100)
                                    {
                                        if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("134287-0-0-0-31-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 134;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 134 && MyChar.Level >= 110)
                                    {
                                        if (MyChar.RBCount == 0)
                                        {
                                            MyChar.AddItem("700032-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("160199-0-0-0-33-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 135;
                                        Promoted = true;
                                    }

                                    if (Promoted)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("F�licitation, vous avez avancer."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Cool!.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous ne pouvez pas avancer."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }

                                if (Control == 3)
                                {
                                    bool Promoted = false;

                                    if (MyChar.Job == 100 && MyChar.Level >= 15)
                                    {
                                        MyChar.Job = 101;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 101 && MyChar.Level >= 40)
                                    {
                                        MyChar.Job = 142;
                                        Promoted = true;
                                    }

                                    if (Promoted)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("F�licitation, vous avez avancer."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Cool!.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous ne pouvez pas avancer."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }

                            #endregion
                            #region TrojanTrainer
                            if (CurrentNPC == 10022)
                            {
                                if (MyChar.Job < 16 && MyChar.Job > 9 || Control == 255)
                                {
                                    if (Control == 222)
                                    {

                                        SendPacket(CoServer.MyPackets.NPCSay("Voici les pouvoirs que je peux vous enseigner si vous avez le niveau requis."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Cyclone: Lv 15", 34));
                                        SendPacket(CoServer.MyPackets.NPCLink("Exactitude: Lv 15", 35));
                                        SendPacket(CoServer.MyPackets.NPCLink("Hercule: Lv 40", 31));
                                        SendPacket(CoServer.MyPackets.NPCLink("Cure d'esprit: Lv 40", 32));
                                        SendPacket(CoServer.MyPackets.NPCLink("Robot: Lv 40", 33));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());

                                    }
                                    if (Control == 31)
                                    {
                                        if (MyChar.Level >= 40)
                                            MyChar.LearnSkill(1115, 0);
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 32)
                                    {
                                        if (MyChar.Level >= 40)
                                            MyChar.LearnSkill(1190, 0);
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 33)
                                    {
                                        if (MyChar.Level >= 40)
                                            MyChar.LearnSkill(1270, 0);
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 34)
                                    {
                                        if (MyChar.Level >= 15)
                                            MyChar.LearnSkill(1110, 0);
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 35)
                                    {
                                        if (MyChar.Level >= 15)
                                            MyChar.LearnSkill(1015, 0);
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                }

                                if (Control == 1)
                                {
                                    if (MyChar.Job == 10)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Brave vous devez �tre niveau 15."));
                                    }
                                    if (MyChar.Job == 11)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Brave De Force vous devez �tre niveau 40."));
                                    }
                                    if (MyChar.Job == 12)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Brave De Tigre vous devez �tre niveau 70."));
                                    }
                                    if (MyChar.Job == 13)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Brave De Dragon vous devez �tre niveau 100."));
                                    }
                                    if (MyChar.Job == 14)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Brave De Force vous devez �tre niveau 110."));
                                    }
                                    if (MyChar.Job != 15)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCLink("Je veux avancer.", 2));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je sais, je ne suis pas qualifi�.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous ne pouvez plus avancer."));
                                        SendPacket(CoServer.MyPackets.NPCLink("D�sol�.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    bool Promoted = false;

                                    if (MyChar.Job == 10 && MyChar.Level >= 15)
                                    {
                                        MyChar.Job = 11;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 11 && MyChar.Level >= 40)
                                    {
                                        MyChar.Job = 12;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 12 && MyChar.Level >= 70)
                                    {
                                        MyChar.Job = 13;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 13 && MyChar.Level >= 100)
                                    {
                                        if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("130287-0-0-0-11-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 14;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 14 && MyChar.Level >= 110)
                                    {
                                        if (MyChar.RBCount == 0)
                                        {
                                            MyChar.AddItem("700012-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("160199-0-0-0-13-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 15;
                                        Promoted = true;
                                    }

                                    if (Promoted)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("F�licitation, vous avez avancer."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Cool!.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous ne pouvez pas avancer."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            #endregion
                            #region ArcherTrainer
                            if (CurrentNPC == 400)
                            {
                                if (MyChar.Job < 46 && MyChar.Job > 39 || Control == 255)
                                {
                                    if (Control == 20)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Voici les pouvoirs que je peux vous enseigner si vous avez le niveau requis."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Vol(XP Skill): Lv 15", 24));
                                        SendPacket(CoServer.MyPackets.NPCLink("�parpillement: Lv 23", 21));
                                        SendPacket(CoServer.MyPackets.NPCLink("Feu Rapide: Lv 46", 22));
                                        SendPacket(CoServer.MyPackets.NPCLink("Intensification: Lv 71", 23));
                                        SendPacket(CoServer.MyPackets.NPCLink("Fl�cheDePluie: Lv 70", 25));
                                        SendPacket(CoServer.MyPackets.NPCLink("Vol: Lv 70", 27));
                                        SendPacket(CoServer.MyPackets.NPCLink("Vol avanc�: Lv 100", 26));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    if (Control == 21)
                                    {
                                        if (MyChar.Level < 23)
                                        {

                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(8001, 0);
                                            SendPacket(CoServer.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 22)
                                    {
                                        if (MyChar.Level < 46)
                                        {

                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(8000, 0);
                                            SendPacket(CoServer.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 23)
                                    {
                                        if (MyChar.Level < 71)
                                        {

                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(9000, 0);
                                            SendPacket(CoServer.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 24)
                                    {
                                        if (MyChar.Level < 15)
                                        {

                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(8002, 0);
                                            SendPacket(CoServer.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 25)
                                    {
                                        if (MyChar.Level < 70)
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(8030, 0);
                                            SendPacket(CoServer.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }

                                    if (Control == 26)
                                    {
                                        if (MyChar.Level < 100)
                                        {

                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill2(8003, 1);
                                            SendPacket(CoServer.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 27)
                                    {
                                        if (MyChar.Level < 70)
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(8003, 0);
                                            SendPacket(CoServer.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                }
                                else
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Vous n'�tes pas un archer..."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 1)
                                {
                                    if (MyChar.Job == 40)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Archer vous devez �tre niveau 15."));
                                    }
                                    if (MyChar.Job == 41)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Archer De Vautour vous devez �tre niveau 40."));
                                    }
                                    if (MyChar.Job == 42)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Archer De Tigre vous devez �tre niveau 70."));
                                    }
                                    if (MyChar.Job == 43)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Archer De Dragon vous devez �tre niveau 100."));
                                    }
                                    if (MyChar.Job == 44)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Archer D`arc D`or vous devez �tre niveau 110."));
                                    }
                                    if (MyChar.Job != 45)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCLink("Je veux avancer.", 2));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je sais, je ne suis pas qualifi�.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous ne pouvez plus avancer."));
                                        SendPacket(CoServer.MyPackets.NPCLink("D�sol�.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    bool Promoted = false;

                                    if (MyChar.Job == 40 && MyChar.Level >= 15)
                                    {
                                        MyChar.Job = 41;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 41 && MyChar.Level >= 40)
                                    {
                                        MyChar.Job = 42;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 42 && MyChar.Level >= 70)
                                    {
                                        MyChar.Job = 43;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 43 && MyChar.Level >= 100)
                                    {
                                        if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("133277-0-0-0-11-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 44;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 44 && MyChar.Level >= 110)
                                    {
                                        if (MyChar.RBCount == 0)
                                        {
                                            MyChar.AddItem("700012-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("160199-0-0-0-13-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 45;
                                        Promoted = true;
                                    }

                                    if (Promoted)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("F�licitation, vous avez avancer."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Cool!.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous ne pouvez pas avancer."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            #endregion
                            #region WarriorTrainer
                            if (CurrentNPC == 10001)
                            {
                                if (MyChar.Job < 26 && MyChar.Job > 19 || Control == 255)
                                {
                                    if (Control == 150)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Voici les pouvoirs que je peux vous enseigner si vous avez le niveau requis."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Puissance: Lv 3", 154));
                                        SendPacket(CoServer.MyPackets.NPCLink("Exactitude: Lv 3", 155));
                                        SendPacket(CoServer.MyPackets.NPCLink("Bouclier: Lv 15", 156));
                                        SendPacket(CoServer.MyPackets.NPCLink("Hurlement: Lv 15", 157));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    if (Control == 154)
                                    {
                                        if (MyChar.Level < 3)
                                        {

                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(1025, 0);
                                            SendPacket(CoServer.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 155)
                                    {
                                        if (MyChar.Level < 3)
                                        {

                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(1015, 0);
                                            SendPacket(CoServer.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 156)
                                    {
                                        if (MyChar.Level < 15)
                                        {

                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(1020, 0);
                                            SendPacket(CoServer.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 157)
                                    {
                                        if (MyChar.Level < 15)
                                        {

                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(1040, 0);
                                            SendPacket(CoServer.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(CoServer.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                }
                                else
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Vous n'�tes pas un guerrier..."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 1)
                                {
                                    if (MyChar.Job == 20)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Guerrier vous devez �tre niveau 15."));
                                    }
                                    if (MyChar.Job == 21)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Guerrier-Bronze vous devez �tre niveau 40."));
                                    }
                                    if (MyChar.Job == 22)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Guerrier-Argent vous devez �tre niveau 70."));
                                    }
                                    if (MyChar.Job == 23)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Guerrier D`or vous devez �tre niveau 100."));
                                    }
                                    if (MyChar.Job == 24)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Pour avancer en Guerre-Dieu vous devez �tre niveau 110."));
                                    }
                                    if (MyChar.Job != 25)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCLink("Je veux avancer.", 2));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je sais que je ne suis pas pr�t.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous ne pouvez plus avancer."));
                                        SendPacket(CoServer.MyPackets.NPCLink("D�sol�.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    bool Promoted = false;

                                    if (MyChar.Job == 20 && MyChar.Level >= 15)
                                    {
                                        MyChar.Job = 21;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 21 && MyChar.Level >= 40)
                                    {
                                        MyChar.Job = 22;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 22 && MyChar.Level >= 70)
                                    {
                                        MyChar.Job = 23;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 23 && MyChar.Level >= 100)
                                    {
                                        if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("131287-0-0-0-11-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 24;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 24 && MyChar.Level >= 110)
                                    {
                                        if (MyChar.RBCount == 0)
                                        {
                                            MyChar.AddItem("700012-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("160199-0-0-0-13-0", 0, (uint)Program.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 25;
                                        Promoted = true;
                                    }

                                    if (Promoted)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("F�licitation, vous avez avancer."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Cool!.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous ne pouvez pas avancer."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            #endregion
                            if (CurrentNPC == 41)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Si vous me donner 1 Perle de Dragon pour le premier trou ou 5 Perles de Dragon pour le second je vais vous aider."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Ok voici les Perles de Dragon requisent.", 2));
                                    SendPacket(CoServer.MyPackets.NPCLink("J'ai chang� d'id�e.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Veuillez �quiper l'arme dans la main droite. Maintenant je vais rajouter un trou � votre arme."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je suis pr�t.", 3));
                                    SendPacket(CoServer.MyPackets.NPCLink("J'ai chang� d'id�e.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 3)
                                {
                                    if (MyChar.Equips[4] != null || MyChar.Equips[4] != "0")
                                    {
                                        string[] item = MyChar.Equips[4].Split('-');

                                        if (item[4] == "0" && MyChar.InventoryContains(1088000, 1) || item[5] == "0" && MyChar.InventoryContains(1088000, 5))
                                        {
                                            if (item[4] == "0")
                                            {
                                                item[4] = "255";
                                                MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                                MyChar.GetEquipStats(4, true);
                                                MyChar.Equips[4] = item[0] + "-" + item[1] + "-" + item[2] + "-" + item[3] + "-" + item[4] + "-" + item[5];
                                                MyChar.GetEquipStats(4, false);
                                                SendPacket(CoServer.MyPackets.AddItem((long)MyChar.Equips_UIDs[4], int.Parse(item[0]), byte.Parse(item[1]), byte.Parse(item[2]), byte.Parse(item[3]), byte.Parse(item[4]), byte.Parse(item[5]), 4, 100, 100));
                                            }
                                            else if (item[5] == "0")
                                            {
                                                item[5] = "255";
                                                MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                                MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                                MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                                MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                                MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                                MyChar.GetEquipStats(4, true);
                                                MyChar.Equips[4] = item[0] + "-" + item[1] + "-" + item[2] + "-" + item[3] + "-" + item[4] + "-" + item[5];
                                                MyChar.GetEquipStats(4, false);
                                                SendPacket(CoServer.MyPackets.AddItem((long)MyChar.Equips_UIDs[4], int.Parse(item[0]), byte.Parse(item[1]), byte.Parse(item[2]), byte.Parse(item[3]), byte.Parse(item[4]), byte.Parse(item[5]), 4, 100, 100));
                                            }
                                        }
                                        else
                                        {
                                            SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas assez de Perle(s) de Dragon."));
                                            SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                            SendPacket(CoServer.MyPackets.NPCFinish());
                                        }
                                    }
                                }
                            }
                            if (CurrentNPC == 10002)
                            {
                                if (Control == 1 || Control == 111)
                                {
                                    if (Control == 111)
                                    {
                                        SendPacket(CoServer.MyPackets.Vital(MyChar.UID, 27, MyChar.Hair));
                                    }
                                    SendPacket(CoServer.MyPackets.NPCSay("Quelle coupe voulez vous ?"));
                                    SendPacket(CoServer.MyPackets.NPCLink("nouveau style01", 4));
                                    SendPacket(CoServer.MyPackets.NPCLink("nouveau style02", 5));
                                    SendPacket(CoServer.MyPackets.NPCLink("nouveau style03", 6));
                                    SendPacket(CoServer.MyPackets.NPCLink("nouveau style04", 7));
                                    SendPacket(CoServer.MyPackets.NPCLink("nouveau style05", 8));
                                    SendPacket(CoServer.MyPackets.NPCLink("nouveau style06", 9));
                                    SendPacket(CoServer.MyPackets.NPCLink("nouveau style07", 10));
                                    SendPacket(CoServer.MyPackets.NPCLink("suivant", 11));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 11)
                                {
                                    SendPacket(CoServer.MyPackets.NPCSay("Quelle coupe voulez vous ?"));
                                    SendPacket(CoServer.MyPackets.NPCLink("nouveau style08", 12));
                                    SendPacket(CoServer.MyPackets.NPCLink("nouveau style09", 13));
                                    SendPacket(CoServer.MyPackets.NPCLink("nouveau style10", 14));
                                    SendPacket(CoServer.MyPackets.NPCLink("nouveau style11", 15));
                                    SendPacket(CoServer.MyPackets.NPCLink("nouveau style12", 16));
                                    SendPacket(CoServer.MyPackets.NPCLink("pr�c�dent", 1));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 2 || Control == 112)
                                {
                                    if (Control == 112)
                                    {
                                        SendPacket(CoServer.MyPackets.Vital(MyChar.UID, 27, MyChar.Hair));
                                    }
                                    SendPacket(CoServer.MyPackets.NPCSay("Quelle coupe voulez vous ?"));
                                    SendPacket(CoServer.MyPackets.NPCLink("Nostalgique01", 17));
                                    SendPacket(CoServer.MyPackets.NPCLink("Nostalgique02", 18));
                                    SendPacket(CoServer.MyPackets.NPCLink("Nostalgique03", 19));
                                    SendPacket(CoServer.MyPackets.NPCLink("Nostalgique04", 20));
                                    SendPacket(CoServer.MyPackets.NPCLink("Nostalgique05", 21));
                                    SendPacket(CoServer.MyPackets.NPCLink("Nostalgique06", 22));
                                    SendPacket(CoServer.MyPackets.NPCLink("Nostalgique07", 23));
                                    SendPacket(CoServer.MyPackets.NPCLink("J'ai chang� d'avis.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 3 || Control == 113)
                                {
                                    if (Control == 113)
                                    {
                                        SendPacket(CoServer.MyPackets.Vital(MyChar.UID, 27, MyChar.Hair));
                                    }
                                    SendPacket(CoServer.MyPackets.NPCSay("Quelle coupe voulez vous ?"));
                                    SendPacket(CoServer.MyPackets.NPCLink("Sp�cial01", 25));
                                    SendPacket(CoServer.MyPackets.NPCLink("Sp�cial02", 26));
                                    SendPacket(CoServer.MyPackets.NPCLink("Sp�cial03", 27));
                                    SendPacket(CoServer.MyPackets.NPCLink("Sp�cial04", 28));
                                    SendPacket(CoServer.MyPackets.NPCLink("Sp�cial05", 29));
                                    SendPacket(CoServer.MyPackets.NPCLink("J'ai chang� d'avis.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                                if (Control == 254)
                                {
                                    if (MyChar.Silvers >= 500)
                                    {
                                        MyChar.Silvers -= 500;
                                        MyChar.Hair = MyChar.ShowHair;
                                    }
                                }
                                if (Control > 3 && Control < 30 && Control != 11)
                                {
                                    if (MyChar.Silvers >= 500)
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Voila, �tes-vous satisfait?"));
                                        SendPacket(CoServer.MyPackets.NPCLink("Cool!", 254));
                                        if (Control < 17)
                                            SendPacket(CoServer.MyPackets.NPCLink("Je veux la changer.", 111));
                                        else if (Control < 25)
                                            SendPacket(CoServer.MyPackets.NPCLink("Je veux la changer.", 112));
                                        else if (Control < 30)
                                            SendPacket(CoServer.MyPackets.NPCLink("Je veux la changer.", 113));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());

                                        if (Control == 4)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "30");
                                        if (Control == 5)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "31");
                                        if (Control == 6)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "32");
                                        if (Control == 7)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "33");
                                        if (Control == 8)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "34");
                                        if (Control == 9)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "35");
                                        if (Control == 10)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "36");
                                        if (Control == 12)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "37");
                                        if (Control == 13)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "38");
                                        if (Control == 14)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "39");
                                        if (Control == 15)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "40");
                                        if (Control == 16)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "41");

                                        if (Control == 17)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "11");
                                        if (Control == 18)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "12");
                                        if (Control == 19)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "13");
                                        if (Control == 20)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "14");
                                        if (Control == 21)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "15");
                                        if (Control == 22)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "16");
                                        if (Control == 23)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "17");
                                        if (Control == 24)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "10");

                                        if (Control == 25)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "21");
                                        if (Control == 26)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "22");
                                        if (Control == 27)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "23");
                                        if (Control == 28)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "24");
                                        if (Control == 29)
                                            MyChar.ShowHair = ushort.Parse(MyChar.Hair.ToString()[0] + "25");

                                        World.UpdateSpawn(MyChar);
                                    }
                                    else
                                    {
                                        SendPacket(CoServer.MyPackets.NPCSay("Vous n'avez pas assez d'argent."));
                                        SendPacket(CoServer.MyPackets.NPCLink("Je suis d�sol�.", 255));
                                        SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                        SendPacket(CoServer.MyPackets.NPCFinish());
                                    }
                                }
                            }

                            if (CurrentNPC == 45)
                            {
                                if (Control == 1)
                                {
                                    ushort XTo = 429;
                                    ushort YTo = 378;

                                    if (MyChar.PrevMap == 1015)
                                    {
                                        XTo = 717;
                                        YTo = 571;
                                    }
                                    if (MyChar.PrevMap == 1000)
                                    {
                                        XTo = 500;
                                        YTo = 650;
                                    }
                                    if (MyChar.PrevMap == 1011)
                                    {
                                        XTo = 188;
                                        YTo = 264;
                                    }
                                    if (MyChar.PrevMap == 1020)
                                    {
                                        XTo = 565;
                                        YTo = 562;
                                    }

                                    MyChar.Teleport(MyChar.PrevMap, XTo, YTo);
                                }
                            }

                            if (CurrentNPC == 100000000)
                            {
                                if (Control == 1)
                                {
                                    MyChar.Teleport(1039, 217, 215);
                                    SendPacket(CoServer.MyPackets.NPCSay("Vous devez attaquer un poteau/�ventail de votre niveau ou moins. Si vous voulez attaquer un �ventail/poteau d�passant votre niveau, rien ne se passera."));
                                    SendPacket(CoServer.MyPackets.NPCLink("Je vois.", 255));
                                    SendPacket(CoServer.MyPackets.NPCSetFace(30));
                                    SendPacket(CoServer.MyPackets.NPCFinish());
                                }
                            }
                            
                            MyChar.Ready = true;
                            break;
                        }
                    #endregion
                    #region Packet 2036
                    case 2036:
                        {
                            uint MainUID = (uint)((Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + Data[8]);
                            uint Minor1UID = (uint)((Data[15] << 24) + (Data[14] << 16) + (Data[13] << 8) + Data[12]);
                            uint Minor2UID = (uint)((Data[19] << 24) + (Data[18] << 16) + (Data[17] << 8) + Data[16]);
                            uint Gem1 = (uint)((Data[23] << 24) + (Data[22] << 16) + (Data[21] << 8) + Data[20]);
                            uint Gem2 = (uint)((Data[27] << 24) + (Data[26] << 16) + (Data[25] << 8) + Data[24]);

                            string MainItem = MyChar.FindItem(MainUID);
                            string Minor1Item = MyChar.FindItem(Minor1UID);
                            string Minor2Item = MyChar.FindItem(Minor2UID);

                            if (MainItem == null || Minor1Item == null || Minor2Item == null)
                                return;

                            byte MainPlus = 0;
                            byte Minor1Plus = 0;
                            byte Minor2Plus = 0;
                            uint MainItemId = 0;
                            uint Minor1ItemId = 0;
                            uint Minor2ItemId = 0;

                            string[] Splitter;
                            string[] MainItemE;
                            MainItemE = MainItem.Split('-');
                            MainPlus = byte.Parse(MainItemE[1]);
                            MainItemId = uint.Parse(MainItemE[0]);
                            Splitter = Minor1Item.Split('-');
                            Minor1Plus = byte.Parse(Splitter[1]);
                            Minor1ItemId = uint.Parse(Splitter[0]);
                            Splitter = Minor2Item.Split('-');
                            Minor2Plus = byte.Parse(Splitter[1]);
                            Minor2ItemId = uint.Parse(Splitter[0]);

                            if (Minor1Plus == Minor2Plus)
                                if (Minor1Plus == MainPlus || MainPlus == 0 && Minor1Plus == 1)
                                    if (ItemHandler.ItemType2(Minor1ItemId) == ItemHandler.ItemType2(Minor2ItemId) && ItemHandler.ItemType2(Minor2ItemId) == ItemHandler.ItemType2(MainItemId) || (ItemHandler.ItemType(MainItemId) == 4 && ItemHandler.ItemType(Minor1ItemId) == 4 && ItemHandler.ItemType(Minor2ItemId) == 4) || ItemHandler.ItemType(MainItemId) == 5 && ItemHandler.ItemType(Minor1ItemId) == 5 && ItemHandler.ItemType(Minor2ItemId) == 5)
                                    {
                                        MainPlus++;
                                        MyChar.RemoveItem(Minor1UID);
                                        MyChar.RemoveItem(Minor2UID);
                                        MyChar.RemoveItem(MainUID);

                                        if (Gem1 != 0)
                                            MyChar.RemoveItem(Gem1);
                                        if (Gem2 != 0)
                                            MyChar.RemoveItem(Gem2);

                                        MyChar.AddItem(MainItemE[0] + "-" + MainPlus + "-" + MainItemE[2] + "-" + MainItemE[3] + "-" + MainItemE[4] + "-" + MainItemE[5], 0, MainUID);
                                        return;
                                    }

                            if (Minor1Plus == Minor2Plus)
                                if (Minor1Plus == MainPlus || MainPlus == 0 && Minor1Plus == 1)
                                    if (ItemHandler.ItemType2(Minor1ItemId) == 73 || ItemHandler.ItemType2(Minor2ItemId) == 73)
                                        if ((ItemHandler.ItemType2(Minor1ItemId) == 73 && ItemHandler.ItemType2(Minor2ItemId) == 73) || (ItemHandler.ItemType2(Minor1ItemId) == 73 && ItemHandler.ItemType2(Minor2ItemId) != 73 && ItemHandler.ItemType2(MainItemId) == ItemHandler.ItemType2(Minor2ItemId)) || (ItemHandler.ItemType2(Minor2ItemId) == 73 && ItemHandler.ItemType2(Minor1ItemId) != 73 && ItemHandler.ItemType2(MainItemId) == ItemHandler.ItemType2(Minor1ItemId)))
                                        {
                                            MainPlus++;
                                            MyChar.RemoveItem(Minor1UID);
                                            MyChar.RemoveItem(Minor2UID);
                                            MyChar.RemoveItem(MainUID);

                                            if (Gem1 != 0)
                                                MyChar.RemoveItem(Gem1);
                                            if (Gem2 != 0)
                                                MyChar.RemoveItem(Gem2);

                                            MyChar.AddItem(MainItemE[0] + "-" + MainPlus + "-" + MainItemE[2] + "-" + MainItemE[3] + "-" + MainItemE[4] + "-" + MainItemE[5], 0, MainUID);
                                        }

                            break;
                        }
                    #endregion
                    #region Packet 2050
                    case 2050:
                        {
                            if (Data[4] == 3 && MyChar.CPs >= 5)
                            {
                                if (CoServer.RadioOn)
                                {
                                    if (MyChar.CanSend && Status == 0 || Status != 0)
                                    {
                                        if (MyChar.LocMap != 6000 && MyChar.LocMap != 6001)
                                        {
                                            MyChar.CPs -= 5;
                                            byte Len = Data[13];
                                            string Message = "";
                                            for (int i = 0; i < Len; i++)
                                                Message += Convert.ToChar(Data[14 + i]);
                                            World.SendMsgToAll(Message, MyChar.Name, 2500);

                                            MyChar.CanSend = false;
                                            MyChar.Radio.Interval = 60000;
                                            MyChar.Radio.Elapsed += new ElapsedEventHandler(MyChar.Radio_Elapsed);
                                            MyChar.Radio.Start();
                                        }
                                        else
                                            SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous ne pouvez pas envoyez de message dans la prison.", 2005));
                                    }
                                    else
                                        SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous ne pouvez pas envoyez de message pour le moment.", 2005));
                                }
                                else
                                    SendPacket(CoServer.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Le syst�me de radio a �t� d�sactiv� par l'administrateur!", 2005));
                            }
                            break;
                        }
                    #endregion
                    #region Default
                    default:
                        Console.WriteLine("Type: " + PacketId + " Length: " + Data.Length);
                        Console.WriteLine(Program.Dump(Data));
                        break;
                    #endregion
                }

                Data = null;
                MsgPacket = null;
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }


        public void SendPacket(byte[] Dat)
        {
            try
            {
                if (ListenSock != null)
                    if (ListenSock.Connection.Connected)
                    {
                        int Len = Dat[0] + (Dat[1] << 8);
                        if (Dat.Length != Len)
                            return;

                        System.Threading.Monitor.TryEnter(this, new TimeSpan(0, 0, 0, 5, 0));

                        byte[] Data = new byte[Dat.Length];
                        Dat.CopyTo(Data, 0);

                        Crypto.Encrypt(Data);
                        ListenSock.Connection.Send(Data);

                        System.Threading.Monitor.Exit(this);
                        Data = null;
                    }
            }
            catch { }
        }

        public void Drop()
        {
            try
            {
                Online = false;
                if (MyChar != null)
                {
                    MyChar.Screen.Disconnect();
                    if (MyChar.MyShop != null)
                        MyChar.MyShop.Destroy();

                    if (MyChar.TheTimer != null)
                    {
                        MyChar.TheTimer.Stop();
                        MyChar.TheTimer.Dispose();
                        MyChar.TheTimer = null;
                    }

                    if (MyChar.LocMap == 1036 || MyChar.LocMap == 1091 || MyChar.LocMap == 1081 || MyChar.LocMap == 1090 || MyChar.LocMap == 2021 || MyChar.LocMap == 2022 || MyChar.LocMap == 2023 || MyChar.LocMap == 2024 || MyChar.LocMap == 1039)
                        MyChar.Teleport(1002, 431, 379);

                    if (MyChar.LocMap == 1038)
                    {
                        if (MyChar.CurHP == 0)
                            MyChar.Teleport(1505, 155, 218);
                        else
                            MyChar.Teleport(1002, 431, 379);
                    }

                    if (MyChar.CurHP == 0 && MyChar.LocMap != 1038)
                        MyChar.Revive(true);

                    MyChar.Save();

                    foreach (DictionaryEntry DE in MyChar.Friends)
                    {
                        uint FriendID = (uint)DE.Key;
                        if (World.AllChars.ContainsKey(FriendID))
                        {
                            Character Friend = World.AllChars[FriendID];

                            Friend.MyClient.SendPacket(CoServer.MyPackets.FriendEnemyPacket(MyChar.UID, MyChar.Name, 14, 0));
                            Friend.MyClient.SendPacket(CoServer.MyPackets.FriendEnemyPacket(MyChar.UID, MyChar.Name, 15, 0));
                            Friend.SendSysMsg("Votre ami " + MyChar.Name + " vient de quitter le jeu.");
                        }
                    }

                    foreach (Character Player in World.AllChars.Values)
                    {
                        if (Player.Enemies.Contains(MyChar.UID))
                        {
                            Player.MyClient.SendPacket(CoServer.MyPackets.FriendEnemyPacket(MyChar.UID, MyChar.Name, 18, 0));
                            Player.MyClient.SendPacket(CoServer.MyPackets.FriendEnemyPacket(MyChar.UID, MyChar.Name, 19, 0));
                        }
                    }

                    if (MyChar.Trading)
                    {
                        Character Who = (Character)World.AllChars[MyChar.TradingWith];
                        Who.MyClient.SendPacket(CoServer.MyPackets.TradePacket(MyChar.TradingWith, 5));
                        Who.TradingSilvers = 0;
                        Who.TradingCPs = 0;
                        Who.TradeOK = false;
                        Who.Trading = false;
                        Who.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Who.MyClient.MessageId, "SYSTEM", Who.Name, "�change rat�e!", 2005));
                    }

                    if (MyChar.TeamLeader)
                        MyChar.TeamDismiss();

                    if (MyChar.MyTeamLeader != null && !MyChar.TeamLeader)
                        MyChar.MyTeamLeader.TeamRemove(MyChar, false);

                    MyChar.Inventory = null;
                    MyChar.Equips = null;
                    MyChar.TCWH = null;
                    MyChar.PCWH = null;
                    MyChar.ACWH = null;
                    MyChar.DCWH = null;
                    MyChar.BIWH = null;
                    MyChar.MAWH = null;
                    MyChar.Skills.Clear();
                    MyChar.Skill_Exps.Clear();
                    MyChar.Profs.Clear();
                    MyChar.Prof_Exps.Clear();
                    MyChar.Friends.Clear();
                    MyChar.Enemies.Clear();

                    if (World.AllChars.ContainsKey(MyChar.UID))
                        World.AllChars.Remove(MyChar.UID);

                    MyChar = null;
                }

                if (ListenSock != null)
                    ListenSock.Disconnect();
                ListenSock = null;
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }
    }
}
