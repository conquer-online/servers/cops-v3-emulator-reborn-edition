using System;
using System.Collections.Generic;
using System.Collections;
using System.Net;
using INI_CORE_DLL;
using DMapReader;
using CoS_Math;

namespace COServer
{
    public class World
    {
        public static ArrayList PlayersPraying = new ArrayList();
        public static Dictionary<uint, Character> AllChars = new Dictionary<uint, Character>();
        public static Dictionary<uint, Monster> AllMobs = new Dictionary<uint, Monster>();
        public static Dictionary<uint, Shop> AllShops = new Dictionary<uint, Shop>();
        public static Guild PoleHolder;
        public static Hashtable GWScores = new Hashtable();
        //Pkt Map
        public static bool PktOn = false;
        public static ArrayList PktMap1 = new ArrayList();
        public static ArrayList PktMap2 = new ArrayList();
        public static ArrayList PktMap3 = new ArrayList();
        //Dis
        public static bool DisOn = false;
        public static Dictionary<string, uint> DisMap1 = new Dictionary<string, uint>();
        public static Dictionary<string, uint> DisMap2 = new Dictionary<string, uint>();
        public static Dictionary<string, uint> DisMap3 = new Dictionary<string, uint>();
        public static Dictionary<string, uint> DisMap4 = new Dictionary<string, uint>();
        public static ArrayList DisBoss = new ArrayList();
        //GW
        public static bool GWOn = false;
        public static bool GBOn = false;
        public static bool JailOn = false;
        public static bool LGateDead = false;
        public static bool RGateDead = false;

        public static void LevelUp(Character Player)
        {
            try
            {
                lock (AllChars)
                {
                    foreach (Character User in AllChars.Values)
                    {
                        if (User.LocMap == Player.LocMap)
                            if (MyMath.CanSee(Player.LocX, Player.LocY, User.LocX, User.LocY))
                                User.MyClient.SendPacket(CoServer.MyPackets.GeneralData(Player, 0, 92));
                    }
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void ShowEffect(Character Player, string Effect)
        {
            try
            {
                lock (AllChars)
                {
                    foreach (Character User in AllChars.Values)
                    {
                        if (User.LocMap == Player.LocMap)
                            if (MyMath.CanSee(Player.LocX, Player.LocY, User.LocX, User.LocY))
                                User.MyClient.SendPacket(CoServer.MyPackets.String(Player.UID, 10, Effect));
                    }
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void ShowEffect(Monster Mob, string Effect)
        {
            try
            {
                lock (AllChars)
                {
                    foreach (Character User in AllChars.Values)
                    {
                        if (User.LocMap == Mob.LocMap)
                            if (MyMath.CanSee(Mob.LocX, Mob.LocY, User.LocX, User.LocY))
                                User.MyClient.SendPacket(CoServer.MyPackets.String(Mob.UID, 10, Effect));
                    }
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void CharSkillUse(Character Attacker, Character Attacked, uint Damage, ushort SkillId, byte SkillLvl)
        {
            try
            {
                lock (AllChars)
                {
                    foreach (Character User in AllChars.Values)
                    {
                        if (User.LocMap == Attacker.LocMap)
                            if (MyMath.CanSeeBig(Attacker.LocX, Attacker.LocY, User.LocX, User.LocY))
                                User.MyClient.SendPacket(CoServer.MyPackets.CharSkillUse(Attacker, Attacked, Damage, SkillId, SkillLvl));
                    }
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void SpawnNPC(SingleNPC NPC)
        {
            try
            {
                lock (AllChars)
                {
                    foreach (Character User in AllChars.Values)
                    {
                        if (NPC.LocMap == User.LocMap)
                            if (MyMath.CanSee(NPC.LocX, NPC.LocY, User.LocX, User.LocY))
                                if (!User.Screen.Contains(NPC.UID))
                                    User.Screen.Add(NPC, true);
                    }
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void RemoveEntity(SingleNPC NPC)
        {
            try
            {
                lock (AllChars)
                {
                    foreach (Character User in AllChars.Values)
                    {
                        if (User.Screen.Contains(NPC.UID))
                            User.Screen.Remove(NPC, true);
                    }
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void RemoveEntity(Monster Mob)
        {
            try
            {
                lock (AllChars)
                {
                    foreach (Character User in AllChars.Values)
                    {
                        if (User.Screen.Contains(Mob.UID))
                            User.Screen.Remove(Mob, true);
                    }
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void UsingSkill(Character Player, short SkillId, byte SkillLvl, uint Target, uint Damage, short X, short Y)
        {
            if (SkillId != 1110 && SkillId != 1100 && SkillId != 1015)
            {
                if (Player.LocMap != 1039)
                    Player.AddSkillExp(SkillId, Damage);
                else
                    Player.AddSkillExp(SkillId, Damage / 10);

                if (SkillId == 1095 || SkillId == 1075 || SkillId == 3080 || SkillId == 1090 || SkillId == 1085)
                    if (Player.LocMap != 1039)
                        Player.AddSkillExp(SkillId, 10);
                    else
                        Player.AddSkillExp(SkillId, 1);

                if (SkillId == 1190)
                    if (Player.LocMap != 1039)
                        Player.AddSkillExp(SkillId, 100);
                    else
                        Player.AddSkillExp(SkillId, 10);
            }

            try
            {
                lock (AllChars)
                {
                    foreach (Character User in AllChars.Values)
                    {
                        if (Player.LocMap == User.LocMap)
                            if (MyMath.CanSee(Player.LocX, Player.LocY, User.LocX, User.LocY))
                                if (SkillId == 1002 || SkillId == 1190)
                                    User.MyClient.SendPacket(CoServer.MyPackets.SkillUse(Player, null, null, null, 0, 0, SkillId, SkillLvl, 1, Target, Damage));
                                else
                                    User.MyClient.SendPacket(CoServer.MyPackets.SkillUse(Player, null, null, null, X, Y, SkillId, SkillLvl, 2, Target, Damage));
                    }
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void UsingSkill(Character User, Hashtable MobTargets, Hashtable NPCTargets, Hashtable PlTargets, short AimX, short AimY, short SkillId, byte SkillLvl)
        {
            foreach (Character Charr in AllChars.Values)
            {
                if (Charr.MyClient.Online)
                    if (User.LocMap == Charr.LocMap)
                        if (MyMath.CanSee(User.LocX, User.LocY, Charr.LocX, Charr.LocY))
                            Charr.MyClient.SendPacket(CoServer.MyPackets.SkillUse(User, MobTargets, PlTargets, NPCTargets, AimX, AimY, SkillId, SkillLvl, 0, 0, 0));
            }

            if (MobTargets.Count < 1 && NPCTargets.Count < 1 && PlTargets.Count < 1)
                return;

            uint GotXP = 0;
            uint GotSkilExp = 0;

            foreach (DictionaryEntry DE in PlTargets)
            {
                Character Player = (Character)DE.Key;
                uint Damage = (uint)DE.Value;

                if (!Other.CanPK(User.LocMap))
                    if (!User.BlueName)
                        if (!Player.BlueName)
                            if (Player.PKPoints < 100)
                                if (!Other.CanPK(Player.LocMap))
                                    User.BlueName = true;

                if (Player.GetHitDie(Damage))
                {
                    if (User.PTarget != null)
                        if (Player == User.PTarget)
                        {
                            User.PTarget = null;
                            User.Attacking = false;
                        }
                    PVP(User, Player, 14, Damage);
                    if (!Other.CanPK(User.LocMap))
                    {
                        if (!Player.BlueName)
                            if (Player.PKPoints >= 30 && Player.PKPoints < 100) //Red Name
                            {
                                User.GotBlueName = DateTime.Now;
                                User.BlueName = true;
                            }
                            if (Player.PKPoints < 30) //White Name
                            {
                                User.GotBlueName = DateTime.Now;
                                User.BlueName = true;

                                if (User.MyGuild != null)
                                {
                                    if (User.MyGuild.Enemies.Contains(Player.GuildID))
                                        User.PKPoints += 3;
                                    else if (User.Enemies.Contains(Player.UID))
                                        User.PKPoints += 5;
                                    else
                                        User.PKPoints += 10;
                                }
                                else
                                {
                                    if (User.Enemies.Contains(Player.UID))
                                        User.PKPoints += 5;
                                    else
                                        User.PKPoints += 10;
                                }

                                if (!Player.Enemies.Contains(User.UID))
                                {
                                    Network.MsgFriend Msg = new Network.MsgFriend();
                                    Player.MyClient.SendPacket(Msg.Create(User.UID, User.Name, true, 19));
                                    Player.AddEnemy(User);
                                }

                                if (User.PKPoints > 30000)
                                    User.PKPoints = 30000;
                            }

                        ulong PkXp = Player.Exp / 100;
                        if (PkXp < 0)
                            PkXp = Player.Exp;
                        Player.Exp -= PkXp;
                        User.AddExp(PkXp, false);
                    }
                }
            }

            foreach (DictionaryEntry DE in NPCTargets)
            {
                SingleNPC TGO = (SingleNPC)DE.Key;
                uint Damage = (uint)DE.Value;

                uint TGOHP = TGO.CurHP;
                double ExpQuality = 1;

                if (TGO.Level + 5 < User.Level)
                    ExpQuality = 0.1;

                if (TGO.GetDamageDie(Damage, User))
                {
                    GotXP += (uint)(TGOHP * ExpQuality / 10);
                    if (SkillId != 5030)
                        GotSkilExp += (uint)(TGOHP / 100);
                    else
                        GotSkilExp += (uint)(5 * (SkillLvl + 1)); ;
                }
                else
                {
                    GotXP += (uint)(Damage * ExpQuality / 10);
                    if (SkillId != 5030)
                        GotSkilExp += (uint)(Damage / 100);
                    else
                        GotSkilExp += (uint)(5 * (SkillLvl + 1)); ;
                }
            }

            foreach (DictionaryEntry DE in MobTargets)
            {
                Monster Mob = (Monster)DE.Key;
                uint Damage = (uint)DE.Value;
                uint MobCurHP = Mob.CurHP;

                if (Mob.GetDamage(User, Damage))
                {
                    if (User.CycloneOn || User.SMOn)
                    {
                        User.KO++;
                        User.MyClient.SendPacket(CoServer.MyPackets.SendMsg(User.MyClient.MessageId, "SYSTEM", User.Name, "Kills:" + User.KO, 2005));
                    }
                    if (User.LocMap == 2022)
                    {
                        ushort DisRequest = 0;

                        if (User.Job > 9 && User.Job < 16)
                            DisRequest = 800;
                        if (User.Job > 19 && User.Job < 26)
                            DisRequest = 900;
                        if (User.Job > 39 && User.Job < 46)
                            DisRequest = 1300;
                        if (User.Job > 129 && User.Job < 136)
                            DisRequest = 600;
                        if (User.Job > 139 && User.Job < 146)
                            DisRequest = 1000;

                        if (Mob.Name == "TrollInfernal")
                        {
                            User.DisKO += 3;
                            User.MyClient.SendPacket(CoServer.MyPackets.SendMsg(User.MyClient.MessageId, "SYSTEM", User.Name, "Dis Kills:" + User.DisKO + "/" + DisRequest, 2005));
                        }
                        else
                        {
                            User.DisKO++;
                            User.MyClient.SendPacket(CoServer.MyPackets.SendMsg(User.MyClient.MessageId, "SYSTEM", User.Name, "Dis Kills:" + User.DisKO + "/" + DisRequest, 2005));
                        }

                        if (User.DisKO >= DisRequest)
                            User.MyClient.SendPacket(CoServer.MyPackets.SendMsg(User.MyClient.MessageId, "SYSTEM", User.Name, "Vous avez tu� assez de monstre pour la qu�te.", 2011));
                    }

                    if (Mob.Name == "PlutoFinal")
                    {
                        User.AddItem("790001-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                        User.Teleport(1020, 531, 482);
                        User.MyClient.CurrentNPC = 1315;
                        User.MyClient.SendPacket(CoServer.MyPackets.NPCSay("Vous avez vaincu PlutoFinal, pour vous remercier je vais vous donner un grand cadeau."));
                        User.MyClient.SendPacket(CoServer.MyPackets.NPCLink("Merci!", 1));
                        User.MyClient.SendPacket(CoServer.MyPackets.NPCSetFace(6));
                        User.MyClient.SendPacket(CoServer.MyPackets.NPCFinish());

                        foreach (KeyValuePair<uint, Character> DI in AllChars)
                        {
                            Character Chaar = (Character)DI.Value;
                            if (Chaar.Name != User.Name)
                                if (Chaar.LocMap == 2021 || Chaar.LocMap == 2022 || Chaar.LocMap == 2023 || Chaar.LocMap == 2024)
                                    Chaar.Teleport(1002, 430, 380);
                        }
                        World.SendMsgToAll(User.Name + " des " + User.MyGuild.Name + " a d�truit PlutoFinal! Les joueurs ont �t� t�l�port�s pour leurs protection.", "SYSTEM", 2011);
                    }
                    if (/*User.Level >= 70 && */User.LocMap != 1767)
                    {
                        foreach (Character Member in User.Team)
                        {
                            if (!Member.Alive)
                                continue;

                            double PlvlExp = 1;
                            byte WatXP = 1;
                            byte ExpPot = 1;
                            byte MarriageXP = 1;

                            if (Member.Job == 133 || Member.Job == 134 || Member.Job == 135)
                                WatXP = 2;
                            if (Member.Spouse == User.Spouse)
                                MarriageXP = 2;
                            if (Mob.Level - 20 <= (short)Member.Level)
                                PlvlExp = 0.1;
                            if (Member.dexp == 1)
                                ExpPot = 2;
                            else if (Member.dexp == 2)
                                ExpPot = 3;
                            else if (Member.dexp == 3)
                                ExpPot = 5;

                            if (Member != null)
                                if (Member.LocMap == User.LocMap)
                                    if (MyMath.GetDistance(Member.LocX, Member.LocY, User.LocX, User.LocY) < 30)
                                    {
                                        double TheXP = 0;
                                        TheXP = ((((((52 + (Member.Level * 30)) * PlvlExp) * WatXP) * ExpPot) * MarriageXP) * Member.AddExpPc);
                                        Member.AddExp((ulong)TheXP, false);
                                        Member.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Member.MyClient.MessageId, "SYSTEM", Member.Name, Member.Name + " a gagn� " + TheXP + " points d'exp�rience.", 2005));
                                        if (Member.Level < 70 && User.MyTeamLeader.Level >= 70)
                                        {
                                            User.MyTeamLeader.VPs += (uint)((Member.Level * 17 / 13 * 12 / 2) + Member.Level * 3);
                                            User.MyTeamLeader.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Member.MyClient.MessageId, "SYSTEM", User.MyTeamLeader.Name, User.MyTeamLeader.Name + " a gagn� " + ((Member.Level * 17 / 13 * 12 / 2) + Member.Level * 3) + " points de vertu.", 2003));
                                            foreach (Character AllMember in User.Team)
                                            {
                                                if (AllMember != null)
                                                    AllMember.MyClient.SendPacket(CoServer.MyPackets.SendMsg(AllMember.MyClient.MessageId, "SYSTEM", AllMember.Name, User.MyTeamLeader.Name + " a gagn�" + ((Member.Level * 17 / 13 * 12 / 2) + Member.Level * 3) + " points de vertu.", 2003));
                                            }
                                        }
                                    }
                        }
                    }
                    if (User.MobTarget == Mob)
                    {
                        User.TargetUID = 0;
                        User.MobTarget = null;
                        User.Attacking = false;
                    }
                    User.XpCircle++;

                    if (User.CycloneOn || User.SMOn)
                        User.ExtraXP += 820;

                    if (User.MobTarget != null)
                        if (Mob == User.MobTarget)
                            User.MobTarget = null;

                    GotXP += BattleSystem.AdjustExp(MobCurHP, User, Mob);
                    if (SkillId != 5030)
                        GotSkilExp += BattleSystem.AdjustExp(MobCurHP, User, Mob);
                    else
                        GotSkilExp += (uint)(5 * (SkillLvl + 1));

                    AttackMob(User, Mob, 14, 0);

                    MobDissappear(Mob);
                    Mob.Death = DateTime.Now;
                }
                else
                {
                    GotXP += BattleSystem.AdjustExp(Damage, User, Mob);
                    if (SkillId != 5030)
                        GotSkilExp += BattleSystem.AdjustExp(Damage, User, Mob);
                    else
                        GotSkilExp += (uint)(5 * (SkillLvl + 1));
                }
            }
            User.AddExp((ulong)GotXP, true);
            User.AddSkillExp((short)SkillId, GotSkilExp);
        }

        public static void SpawnMob(Monster Mob)
        {
            try
            {
                lock (AllChars)
                {
                    foreach (Character User in AllChars.Values)
                    {
                        if (Mob.LocMap == User.LocMap)
                            if (MyMath.CanSee(Mob.LocX, Mob.LocY, User.LocX, User.LocY))
                                if (!User.Screen.Contains(Mob.UID))
                                    User.Screen.Add(Mob, true);
                    }
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); } 
        }

        public static void MobWalks(Monster Mob, byte Dir, bool Run)
        {
            try
            {
                lock (AllChars)
                {
                    foreach (Character User in AllChars.Values)
                    {
                        if (User.Screen.Contains(Mob.UID))
                            User.MyClient.SendPacket(CoServer.MyPackets.Walk(Mob.UID, Dir, Run));
                        else
                        {
                            if (Mob.LocMap == User.LocMap)
                                if (MyMath.CanSee(Mob.LocX, Mob.LocY, User.LocX, User.LocY))
                                    if (!User.Screen.Contains(Mob.UID))
                                        User.Screen.Add(Mob, true);
                        }
                    }
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void MobJumps(Monster Mob)
        {
            try
            {
                lock (AllChars)
                {
                    foreach (Character User in AllChars.Values)
                    {
                        if (User.Screen.Contains(Mob.UID))
                            User.MyClient.SendPacket(CoServer.MyPackets.GeneralData(Mob, (uint)(Mob.PrevX | (Mob.PrevY << 16)), 133));
                        else
                        {
                            if (Mob.LocMap == User.LocMap)
                                if (MyMath.CanSee(Mob.LocX, Mob.LocY, User.LocX, User.LocY))
                                    if (!User.Screen.Contains(Mob.UID))
                                        User.Screen.Add(Mob, true);
                        }
                    }
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void ItemDrops(DroppedItem Item)
        {
            try
            {
                lock (AllChars)
                {
                    foreach (Character User in AllChars.Values)
                    {
                        if (Item.Map == User.LocMap)
                            if (MyMath.CanSee(Item.X, Item.Y, User.LocX, User.LocY))
                                User.MyClient.SendPacket(CoServer.MyPackets.ItemDrop(Item.UID, Item.ItemId, Item.X, Item.Y));
                    }
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void ItemDissappears(DroppedItem Item)
        {
            try
            {
                lock (AllChars)
                {
                    foreach (Character User in AllChars.Values)
                    {
                        if (Item.Map == User.LocMap)
                            if (MyMath.CanSee(Item.X, Item.Y, User.LocX, User.LocY))
                                User.MyClient.SendPacket(CoServer.MyPackets.ItemDropRemove(Item.UID));
                    }
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void SurroundDroppedItems(Character Player, bool Check)
        {
            try
            {
                lock (DroppedItems.AllDroppedItems)
                {
                    foreach (DroppedItem Item in DroppedItems.AllDroppedItems.Values)
                    {
                        if (Item != null)
                            if (Item.Map == Player.LocMap)
                                if (MyMath.CanSee(Player.LocX, Player.LocY, Item.X, Item.Y))
                                    if (!MyMath.CanSee(Player.PrevX, Player.PrevY, Item.X, Item.Y) || !Check)
                                        Player.MyClient.SendPacket(CoServer.MyPackets.ItemDrop(Item.UID, Item.ItemId, Item.X, Item.Y));
                    }
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void MobReSpawn(Monster Mob)
        {
            foreach (Character User in AllChars.Values)
            {
                if (Mob.LocMap == User.LocMap)
                    if (MyMath.CanSee(Mob.LocX, Mob.LocY, User.LocX, User.LocY))
                    {
                        if (!User.Screen.Contains(Mob.UID))
                            User.Screen.Add(Mob, true);
                        if (Mob.LocMap == 1767 || Mob.LocMap == 2021 || Mob.LocMap == 2022 || Mob.LocMap == 2023 || Mob.LocMap == 2024)
                        {
                            User.MyClient.SendPacket(CoServer.MyPackets.SpawnMob(Mob));
                            User.MyClient.SendPacket(CoServer.MyPackets.String(Mob.UID, 10, "MBGhost"));
                        }
                        else
                        {
                            User.MyClient.SendPacket(CoServer.MyPackets.SpawnMob(Mob));
                            User.MyClient.SendPacket(CoServer.MyPackets.String(Mob.UID, 10, "MBStandard"));
                        }
                    }
            }
        }

        public static void MobDissappear(Monster Mob)
        {
            foreach (Character User in AllChars.Values)
            {
                if (Mob.LocMap == User.LocMap)
                    if (MyMath.CanSee(Mob.LocX, Mob.LocY, User.LocX, User.LocY))
                        User.MyClient.SendPacket(CoServer.MyPackets.Vital(Mob.UID, 26, 2080));
            }
        }

        public static void MobAttacksCharSkill(Monster Mob, Character Attacked, uint Dmg, ushort SkillID, byte SkillLvl)
        {
            foreach (KeyValuePair<uint, Character> DE in AllChars)
            {
                Character Charr = (Character)DE.Value;
                if (Charr.MyClient.Online)
                    if (MyMath.CanSeeBig(Attacked.LocX, Attacked.LocY, Charr.LocX, Charr.LocY))
                        Charr.MyClient.SendPacket(CoServer.MyPackets.MobSkillUse(Mob, Attacked, Dmg, SkillID, SkillLvl));
            }
        }

        public static void MobAttacksCharSkill(Monster Mob, Character Attacked, ushort SkillID, byte SkillLvl)
        {
            foreach (KeyValuePair<uint, Character> DE in AllChars)
            {
                Character Charr = (Character)DE.Value;
                if (Charr.MyClient.Online)
                    if (MyMath.CanSeeBig(Attacked.LocX, Attacked.LocY, Charr.LocX, Charr.LocY))
                        Charr.MyClient.SendPacket(CoServer.MyPackets.MobSkillUse(Mob, Attacked, 0, SkillID, SkillLvl));
            }
        }

        public static void MobAttacksChar(Monster Mob, Character Attacked, byte AtkType, uint Dmg)
        {
            if (Mob == null)
                return;

            foreach (KeyValuePair<uint, Character> DE in AllChars)
            {
                Character Charr = (Character)DE.Value;

                if (Charr.MyClient.Online)
                    if (MyMath.CanSee(Attacked.LocX, Attacked.LocY, Charr.LocX, Charr.LocY))
                        Charr.MyClient.SendPacket(CoServer.MyPackets.Attack(Mob.UID, Attacked.UID, Attacked.LocX, Attacked.LocY, AtkType, Dmg));
            }
        }

        public static void PlAttacksTG(SingleNPC NPC, Character Me, byte AtkType, uint Dmg)
        {
            if (NPC == null)
                return;

            foreach (KeyValuePair<uint, Character> DE in AllChars)
            {
                Character Charr = (Character)DE.Value;

                if (Charr.MyClient.Online)
                    if (MyMath.CanSee(Me.LocX, Me.LocY, Charr.LocX, Charr.LocY))
                        Charr.MyClient.SendPacket(CoServer.MyPackets.Attack(Me.UID, NPC.UID, NPC.LocX, NPC.LocY, AtkType, Dmg));
            }
        }

        public static void PVP(Character Me, Character Attacked, byte AtkType, uint Dmg)
        {
            if (Attacked == null)
                return;

            foreach (KeyValuePair<uint, Character> DE in AllChars)
            {
                Character Charr = (Character)DE.Value;
                if (MyMath.CanSeeBig(Me.LocX, Me.LocY, Charr.LocX, Charr.LocY))
                    if (Charr.MyClient.Online)
                        Charr.MyClient.SendPacket(CoServer.MyPackets.Attack(Me.UID, Attacked.UID, Attacked.LocX, Attacked.LocY, AtkType, Dmg));
            }
        }

        public static void MVM(Monster Me, Monster Attacked, byte AtkType, uint Dmg)
        {
            if (Attacked == null)
                return;

            foreach (KeyValuePair<uint, Character> DE in AllChars)
            {
                Character Charr = (Character)DE.Value;
                if (MyMath.CanSeeBig(Me.LocX, Me.LocY, Charr.LocX, Charr.LocY))
                    if (Charr.MyClient.Online)
                        Charr.MyClient.SendPacket(CoServer.MyPackets.Attack(Me.UID, Attacked.UID, Attacked.LocX, Attacked.LocY, AtkType, Dmg));
            }
        }
        public static void AttackMiss(Character Me, byte AtkType, ushort X, ushort Y)
        {
            foreach (KeyValuePair<uint, Character> DE in AllChars)
            {
                Character Charr = (Character)DE.Value;
                if (Charr != null)
                    if (MyMath.CanSee(Me.LocX, Me.LocY, Charr.LocX, Charr.LocY))
                        if (Charr.MyClient.Online)
                            Charr.MyClient.SendPacket(CoServer.MyPackets.Attack(Me.UID, 0, X, Y, AtkType, 0));
            }
        }
        public static void AttackMob(Character Player, Monster Mob, byte AtkType, uint Dmg)
        {
            try
            {
                lock (AllChars)
                {
                    foreach (Character User in AllChars.Values)
                    {
                        if (Player.LocMap == User.LocMap)
                            if (MyMath.CanSeeBig(Player.LocX, Player.LocY, User.LocX, User.LocY))
                                User.MyClient.SendPacket(CoServer.MyPackets.Attack(Player.UID, Mob.UID, Mob.LocX, Mob.LocY, AtkType, Dmg));
                    }
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void UpdateSpawn(Character Me)
        {
            foreach (KeyValuePair<uint, Character> DE in AllChars)
            {
                Character Charr = (Character)DE.Value;

                if (Me.LocMap == Charr.LocMap)
                    if (MyMath.CanSee(Me.LocX, Me.LocY, Charr.LocX, Charr.LocY))
                    {
                        if (Me.MyGuild != null)
                            Charr.MyClient.SendPacket(CoServer.MyPackets.GuildName(Me.GuildID, Me.MyGuild.Name));
                        Charr.MyClient.SendPacket(CoServer.MyPackets.SpawnEntity(Me));

                        if (Me.MyGuild != null)
                            Charr.MyClient.SendPacket(CoServer.MyPackets.GuildName(Me.GuildID, Me.MyGuild.Name));
                    }
            }
        }

        public static void SendMsgToAll(string Message, string From, short MsgType)
        {
            foreach (Character Player in AllChars.Values)
            {
                Player.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Player.MyClient.MessageId, From, "All", Message, MsgType));
            }
        }

        public static void SendMsgToChar(string Message, string From, short MsgType, string For)
        {
            foreach (KeyValuePair<uint, Character> DE in AllChars)
            {
                Character Charr = (Character)DE.Value;
                if (Charr.Name == For)
                    if (Charr.MyClient.Online)
                        Charr.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Charr.MyClient.MessageId, From, "All", Message, MsgType));
            }
        }

        private static string CheckMsg(Character Sender, string Message)
        {
            bool NeedToLog = false;
            foreach (string Word in DataBase.FiltredWords)
            {
                if (Message.ToLower().Contains(Word.ToLower()))
                {
                    char[] TmpArray = Word.ToCharArray();
                    for (byte i = 0; i < TmpArray.Length; i++)
                        TmpArray[i] = (char)'*';

                    string TmpString = new string(TmpArray);
                    Message = Message.ToLower().Replace(Word.ToLower(), TmpString);

                    TmpArray = null;
                    TmpString = null;
                    NeedToLog = true;
                }
            }
            if (NeedToLog)
                CoServer.WriteChat(Sender, Message);
            return Message;
        }

        public static void Chat(Character Sender, string Receiver, string Message, byte[] Data, ushort Channel)
        {
            if (CoServer.ChatFiltrer)
                Message = CheckMsg(Sender, Message);

            switch (Channel)
            {
                case 2000: //Talk
                    {
                        foreach (Character Player in AllChars.Values)
                        {
                            if (Sender != Player)
                                if (Sender.LocMap == Player.LocMap)
                                    if (MyMath.CanSee(Player.LocX, Player.LocY, Sender.LocX, Sender.LocY))
                                        Player.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Player.MyClient.MessageId, Sender.Name, Receiver, Message, (short)Channel));
                        }
                        break;
                    }
                case 2001: //Whisper
                    {
                        bool Sent = false;
                        foreach (Character Player in AllChars.Values)
                        {
                            if (Sender != Player)
                                if (Player.SeeWhisper)
                                    Player.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Player.MyClient.MessageId, Sender.Name, Receiver, Message, (short)Channel));

                            if (Sender != Player)
                                if (Player.Name == Receiver)
                                {
                                    Player.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Player.MyClient.MessageId, Sender.Name, Receiver, Message, (short)Channel));
                                    Sent = true;
                                    break;
                                }
                        }
                        if (!Sent)
                            Sender.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Sender.MyClient.MessageId, "SYSTEM", Sender.Name, "Le personnage est hors-ligne pour le moment.", 0x7d0));
                        break;
                    }
                case 2003: //Team
                    {
                        foreach (Character Member in Sender.Team)
                        {
                            if (Member != null)
                                Member.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Member.MyClient.MessageId, Sender.Name, Receiver, Message, (short)Channel));
                        }
                        if (!Sender.TeamLeader && Sender.MyTeamLeader != null)
                            Sender.MyTeamLeader.MyClient.SendPacket(Data);
                        break;
                    }
                case 2004: //Guild
                    Sender.MyGuild.GuildMessage(Sender, Message);
                    break;
                case 2009: //Friends
                    {
                        foreach (DictionaryEntry DE in Sender.Friends)
                        {
                            uint FriendID = (uint)DE.Key;
                            if (AllChars.ContainsKey(FriendID))
                            {
                                Character Friend = (Character)AllChars[FriendID];
                                if (Friend != null)
                                    Friend.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Friend.MyClient.MessageId, Sender.Name, Receiver, Message, (short)Channel));
                            }
                        }
                        break;
                    }
                case 2013: //Ghost
                    {
                        foreach (Character Player in AllChars.Values)
                        {
                            if (Sender != Player)
                                if (Sender.LocMap == Player.LocMap)
                                    if (MyMath.CanSee(Player.LocX, Player.LocY, Sender.LocX, Sender.LocY))
                                        if (!Player.Alive || Player.Job < 136 && Player.Job > 130)
                                            Player.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Player.MyClient.MessageId, Sender.Name, Receiver, Message, (short)Channel));
                        }
                        break;
                    }
                default:
                    Sender.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Sender.MyClient.MessageId, "SYSTEM", Sender.Name, "Not implemented yet.", 0x7d0));
                    break;
            }
        }

        public static void SaveAllChars()
        {
            try
            {
                if (AllChars.Count > 0)
                    foreach (Character Player in AllChars.Values)
                    {
                        Player.Save();
                    }
                Guilds.SaveAllGuilds();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void SendAllGuild(Character TheChar)
        {
            try
            {
                foreach (Guild TheGuild in Guilds.AllGuilds.Values)
                {
                    TheChar.MyClient.SendPacket(CoServer.MyPackets.GuildName(TheGuild.UniqId, TheGuild.Name));
                    if (TheChar.MyGuild != null && TheGuild != null)
                    {
                        if (TheChar.MyGuild == TheGuild)
                            TheChar.MyClient.SendPacket(CoServer.MyPackets.SendGuild(TheGuild.UniqId, 1));
                        else if (TheChar.MyGuild.Allies.Contains(TheGuild.UniqId))
                            TheChar.MyClient.SendPacket(CoServer.MyPackets.SendGuild(TheGuild.UniqId, 7));
                        else if (TheChar.MyGuild.Enemies.Contains(TheGuild.UniqId))
                            TheChar.MyClient.SendPacket(CoServer.MyPackets.SendGuild(TheGuild.UniqId, 9));
                        else
                            TheChar.MyClient.SendPacket(CoServer.MyPackets.SendGuild(TheGuild.UniqId, 8));
                    }
                    else
                        TheChar.MyClient.SendPacket(CoServer.MyPackets.SendGuild(TheGuild.UniqId, 8));
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }
    }
}