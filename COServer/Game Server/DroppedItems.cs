using System;
using System.Collections;
using System.Timers;

namespace COServer
{
    public class DroppedItems
    {
        public static Hashtable AllDroppedItems = new Hashtable();
        public static uint LastUID { get { if (m_LastUID > int.MaxValue) m_LastUID = 0; m_LastUID++; return m_LastUID; } }
        private static uint m_LastUID = 0;

        public static DroppedItem DropItem(uint Owner, string Item, uint X, uint Y, uint Map, uint Money)
        {
            lock (AllDroppedItems)
            {
                uint UniqId = LastUID;

                DroppedItem ITEM = new DroppedItem(Owner, UniqId, Item, X, Y, Map, Money);
                AllDroppedItems.Add(UniqId, ITEM);

                return ITEM;
            }
        }

        public static ushort[] GetCoord(Character TheChar)
        {
            ushort[] Coord = new ushort[3];
            ushort TheMap = TheChar.LocMap;
            ushort TheX = TheChar.LocX;
            ushort TheY = TheChar.LocY;

            while (true)
            {
                TheMap = TheChar.LocMap;
                TheX = (ushort)(TheChar.LocX + Program.Rand.Next(-1, 2));
                TheY = (ushort)(TheChar.LocY + Program.Rand.Next(-1, 2));

                if (Other.PlaceFree((short)TheMap, (short)TheX, (short)TheY, true))
                {
                    Coord[0] = TheMap;
                    Coord[1] = TheX;
                    Coord[2] = TheY;
                    return Coord;
                }
            }
        }
    }

    public class DroppedItem
    {
        public uint UID;
        public string Item;
        public uint ItemId;
        public uint X;
        public uint Y;
        public uint Map;
        public uint Money;
        public bool Other = false;
        public uint Owner;
        public Timer OtherCanTake = new Timer();
        public Timer DestroyTimer = new Timer();

        public DroppedItem(uint TheOwner, uint uid, string item, uint x, uint y, uint map, uint money)
        {
            Owner = TheOwner;
            UID = uid;
            Item = item;
            X = x;
            Y = y;
            Map = map;
            Money = money;
            if (Owner == 0)
                Other = true;

            if (Money != 0)
                DestroyTimer.Interval = 20000;
            else
                DestroyTimer.Interval = 30000;
            DestroyTimer.Elapsed += new ElapsedEventHandler(Destroy);
            DestroyTimer.Start();

            if (Money != 0)
                OtherCanTake.Interval = 10000;
            else
                OtherCanTake.Interval = 15000;
            OtherCanTake.Elapsed += new ElapsedEventHandler(RemoveProtection);
            OtherCanTake.Start();

            string[] Splitter = item.Split('-');
            ItemId = uint.Parse(Splitter[0]);
        }

        ~DroppedItem()
        {
            Item = null;
            OtherCanTake = null;
            DestroyTimer = null;
        }

        public void Destroy(object source, ElapsedEventArgs e)
        {
            lock (DroppedItems.AllDroppedItems)
            {
                if (OtherCanTake != null)
                {
                    OtherCanTake.Stop();
                    OtherCanTake.Dispose();
                    OtherCanTake = null;
                }
                DestroyTimer.Stop();
                DestroyTimer.Dispose();
                DestroyTimer = null;
                DroppedItems.AllDroppedItems.Remove(UID);
                World.ItemDissappears(this);
            }
        }

        public void RemoveProtection(object source, ElapsedEventArgs e)
        {
            OtherCanTake.Stop();
            OtherCanTake.Dispose();
            OtherCanTake = null;
            Other = true;
        }
    }
}
