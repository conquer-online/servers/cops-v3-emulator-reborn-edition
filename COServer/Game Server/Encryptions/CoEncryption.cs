using System;

namespace COServer
{
    //The Conquer Online Game Encryption [5017-]
    public class CoEncryption
    {
        internal class CryptCounter
        {
            ushort m_Counter = 0;

            public byte Key2
            {
                get { return (byte)(m_Counter >> 8); }
            }

            public byte Key1
            {
                get { return (byte)(m_Counter & 0xFF); }
            }

            public void Increment()
            {
                m_Counter++;
            }

            public void Reset()
            {
                m_Counter = 0;
            }
        }

        private CryptCounter _decryptCounter;
        private CryptCounter _encryptCounter;
        private byte[] _cryptKey1 = new byte[0x100];
        private byte[] _cryptKey2 = new byte[0x100];

        private byte[] _cryptKey3;
        private byte[] _cryptKey4;
        private bool _useAlt;

        public unsafe CoEncryption()
        {
            _decryptCounter = new CryptCounter();
            _encryptCounter = new CryptCounter();

            //Not original! From my COSAC release!
            Int32 P = 0x13FA0F9D;
            Int32 G = 0x6D5C7962;

            Byte* pBufPKey = (Byte*)&P;
            Byte* pBufGKey = (Byte*)&G;

            for (Int16 i = 0; i < 0x100; i++)
            {
                _cryptKey1[i + 0] = pBufPKey[0];
                _cryptKey2[i] = pBufGKey[0];
                pBufPKey[0] = (Byte)((pBufPKey[1] + (Byte)(pBufPKey[0] * pBufPKey[2])) * pBufPKey[0] + pBufPKey[3]);
                pBufGKey[0] = (Byte)((pBufGKey[1] - (Byte)(pBufGKey[0] * pBufGKey[2])) * pBufGKey[0] + pBufGKey[3]);
            }
            //End
        }

        ~CoEncryption()
        {
            _decryptCounter = null;
            _encryptCounter = null;

            _cryptKey1 = null;
            _cryptKey2 = null;
            _cryptKey3 = null;
            _cryptKey4 = null;
        }

        public void GenerateKeys(byte[] Token, byte[] AccountId)
        {
            _cryptKey3 = new byte[0x100];
            _cryptKey4 = new byte[0x100];

            uint tmp1 = BitConverter.ToUInt32(Rotate(Token), 0) + BitConverter.ToUInt32(Rotate(AccountId), 0);
            byte[] tmpKey1 = Rotate(BitConverter.GetBytes(tmp1));

            tmpKey1[2] ^= 0x43;
            tmpKey1[3] ^= 0x21;

            for (sbyte i = 0; i < 4; i++)
                tmpKey1[i] ^= Token[i];

            uint tmp2 = BitConverter.ToUInt32(Rotate(tmpKey1), 0) * BitConverter.ToUInt32(Rotate(tmpKey1), 0);
            byte[] tmpKey2 = Rotate(BitConverter.GetBytes(tmp2));

            //Build the 3rd Key
            for (int i = 0; i < 0x100; i++)
                _cryptKey3[i] = (byte)(_cryptKey1[i] ^ tmpKey1[3 - (i % 4)]);

            //Build the 4th Key
            for (int i = 0; i < 0x100; i++)
                _cryptKey4[i] = (byte)(_cryptKey2[i] ^ tmpKey2[3 - (i % 4)]);
        }

        private byte[] Rotate(byte[] Bytes)
        {
            byte[] TheBytes = new byte[Bytes.Length];
            for (int i = 0; i < Bytes.Length; i++)
                TheBytes[i] = Bytes[(Bytes.Length - 1) - i];
            return TheBytes;
        }

        public void EnableAlternateKeys()
        {
            _useAlt = true;
            _encryptCounter.Reset();
        }

        public void DisableAlternateKeys()
        {
            _useAlt = false;
            _encryptCounter.Reset();
            _decryptCounter.Reset();
        }

        public void Encrypt(byte[] buffer)
        {
            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] ^= (byte)0x7D;
                buffer[i] = (byte)(buffer[i] >> 4 | buffer[i] << 4);
                buffer[i] ^= (byte)(_cryptKey1[_encryptCounter.Key1] ^ _cryptKey2[_encryptCounter.Key2]);
                _encryptCounter.Increment();
            }
        }

        public void Decrypt(byte[] buffer)
        {
            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] ^= (byte)0x7D;
                buffer[i] = (byte)(buffer[i] >> 4 | buffer[i] << 4);
                if (_useAlt)
                    buffer[i] ^= (byte)(_cryptKey4[_decryptCounter.Key2] ^ _cryptKey3[_decryptCounter.Key1]);
                else
                    buffer[i] ^= (byte)(_cryptKey2[_decryptCounter.Key2] ^ _cryptKey1[_decryptCounter.Key1]);
                _decryptCounter.Increment();
            }
        }
    }
}
