using System;
using System.Collections.Generic;

namespace COServer
{
    public class Guilds
    {
        public static Dictionary<ushort, Guild> AllGuilds = new Dictionary<ushort, Guild>();

        public static void NewGuild(ushort GuildID, string GuildName, Character Creator)
        {
            foreach (Guild Guild in AllGuilds.Values)
            {
                if (Guild.Name == GuildName)
                    return;
            }

            if (!AllGuilds.ContainsKey(GuildID))
            {
                AllGuilds.Add(GuildID, new Guild(GuildID, GuildName, new GuildMember().Create(Creator, 500000), new Dictionary<uint, GuildMember>(), new Dictionary<uint, GuildMember>(), new List<ushort>(), new List<ushort>(), 1000000, "Nouvelle Guilde", 1));
                World.SendMsgToAll(Creator.Name + " a r�ussi � cr�er " + GuildName, "SYSTEM", 2000);
                DataBase.SaveGuild(AllGuilds[GuildID]);
            }
        }

        public static void SaveAllGuilds()
        {
            foreach (Guild Guild in AllGuilds.Values)
            {
                DataBase.SaveGuild(Guild);
            }
        }
    }
}
