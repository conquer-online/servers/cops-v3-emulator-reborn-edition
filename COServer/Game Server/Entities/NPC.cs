using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Timers;
using CoS_Math;

namespace COServer
{
    public class NPCs
    {
        public static Dictionary<uint, SingleNPC> AllNPCs = new Dictionary<uint, SingleNPC>();

        public static void SpawnAllNPCs()
        {
            try
            {
                AllNPCs.Add(699999, new SingleNPC(699999, 180, 2, 0, 438, 377, 1002, 0));

                foreach (uint[] NPC in DataBase.NPCs)
                {
                    SingleNPC npc = new SingleNPC(NPC[0], NPC[1], (byte)NPC[2], (byte)NPC[3], (short)NPC[4], (short)NPC[5], (short)NPC[6], (byte)NPC[7]);
                    AllNPCs.Add(npc.UID, npc);
                }
                DataBase.NPCs = null;

                SingleNPC npcc = new SingleNPC(600501, 1450, 2, 0, (short)DataBase.GC1X, (short)DataBase.GC1Y, (short)DataBase.GC1Map, 0);
                AllNPCs.Add(600501, npcc);

                npcc = new SingleNPC(600502, 1460, 2, 0, (short)DataBase.GC2X, (short)DataBase.GC2Y, (short)DataBase.GC2Map, 0);
                AllNPCs.Add(600502, npcc);

                npcc = new SingleNPC(600503, 1470, 2, 0, (short)DataBase.GC3X, (short)DataBase.GC3Y, (short)DataBase.GC3Map, 0);
                AllNPCs.Add(600503, npcc);

                npcc = new SingleNPC(600504, 1480, 2, 0, (short)DataBase.GC4X, (short)DataBase.GC4Y, (short)DataBase.GC4Map, 0);
                AllNPCs.Add(600504, npcc);                

            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }
    }

    public class SingleNPC : ConquerObject
    {
        public uint Type;
        public string Name;
        public byte Flags;
        public uint MaxHP = 10000;
        public uint CurHP = 10000;
        public byte Sob;
        public byte Level;
        public byte Dodge = 25;

        public SingleNPC(uint uid, uint type, byte flags, byte dir, short x, short y, short map, byte sob)
        {
            UID = uid;
            Type = type;
            Flags = flags;
            Direction = dir;
            LocX = (ushort)x;
            LocY = (ushort)y;
            LocMap = (ushort)map;
            Sob = sob;
            if (Flags == 21)
                Level = (byte)((Type - 420) / 6 + 20);
            if (Flags == 22)
                Level = (byte)((Type - 430) / 6 + 20);
            if (Type == 1500)
                Level = 125;
            if (Type == 1520)
                Level = 125;

            if (Sob == 2)
            {
                MaxHP = 30000000;
                CurHP = 30000000;
            }
            if (Sob == 3)
            {
                MaxHP = 10000000;
                CurHP = 10000000;
            }
        }

        public bool GetDamageDie(uint Damage, Character Attacker)
        {
            if (Damage >= CurHP)
            {
                World.RemoveEntity(this);
                if (Sob != 3)
                    CurHP = MaxHP;
                else
                    CurHP = 1;

                if (Sob == 2 && World.GWOn == true)
                {
                    int Highest = 0;
                    Guild Winner = null;
                    Guild Looser = null;

                    foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                    {
                        Character Char = (Character)DE.Value;
                        if (Char != null)
                        {
                            if (Char.TGTarget != null && Char.TGTarget == this)
                                Char.TGTarget = null;
                        }
                    }

                    SingleNPC Npc = (SingleNPC)NPCs.AllNPCs[701701];
                    if (Npc != null)
                    {
                        if (Npc.Type == 250)
                            Npc.Type -= 10;
                        Npc.CurHP = Npc.MaxHP;
                        World.RemoveEntity(Npc);
                        World.SpawnNPC(Npc);
                    }

                    Npc = (SingleNPC)NPCs.AllNPCs[701702];
                    if (Npc != null)
                    {
                        if (Npc.Type == 280)
                            Npc.Type -= 10;
                        Npc.CurHP = Npc.MaxHP;
                        World.RemoveEntity(Npc);
                        World.SpawnNPC(Npc);
                    }

                    foreach (Guild AGuild in Guilds.AllGuilds.Values)
                    {
                        if (AGuild.HoldingPole == true)
                        {
                            Looser = AGuild;
                        }
                        AGuild.HoldingPole = false;
                        if (AGuild.PoleDamage > Highest)
                        {
                            Highest = (int)AGuild.PoleDamage;
                            Winner = AGuild;
                            AGuild.Fund += (uint)AGuild.PoleDamage;
                            if (Looser != null)
                            {
                                Looser.Fund -= (uint)AGuild.PoleDamage;
                                if (Looser.Fund <= 0)
                                    Looser.Disband(true);
                            }
                        }
                        AGuild.PoleDamage = 0;
                    }
                    if (Winner != null)
                    {
                        Winner.HoldingPole = true;
                        Guilds.SaveAllGuilds();
                        World.PoleHolder = Winner;
                        World.SendMsgToAll(Winner.Name + " a gagn� la Guerre de Guilde!", "SYSTEM", 2011);
                    }
                    World.GWScores.Clear();
                    Attacker.TGTarget = null;
                    Attacker.Attacking = false;
                }
                if (Sob == 3 && Type == 240)
                {
                    World.LGateDead = true;
                    Type += 10;
                    Attacker.TGTarget = null;
                    Attacker.Attacking = false;
                }
                if (Sob == 3 && Type == 270)
                {
                    World.RGateDead = true;
                    Type += 10;
                    Attacker.TGTarget = null;
                    Attacker.Attacking = false;
                }
                if (Type == 250)
                    World.LGateDead = true;
                if (Type == 270)
                    World.RGateDead = true;

                World.SpawnNPC(this);
                return true;
            }
            else
            {
                if (Sob == 2 && World.GWOn == true)
                {
                    if (World.PoleHolder != null)
                        if (Attacker.MyGuild == World.PoleHolder)
                            Damage = 0;

                    if (Attacker.MyGuild != null)
                    {
                        if (Attacker.MyGuild != World.PoleHolder)
                            Attacker.MyGuild.PoleDamage += (int)Damage;
                        if (World.GWScores.Contains(Attacker.MyGuild.UniqId))
                            World.GWScores.Remove(Attacker.MyGuild.UniqId);

                        World.GWScores.Add(Attacker.MyGuild.UniqId, Attacker.MyGuild.PoleDamage);
                    }
                }
                CurHP -= Damage;

                return false;
            }
        }
    }
}
