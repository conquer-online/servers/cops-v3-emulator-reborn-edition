﻿using System;
using System.Collections.Generic;
using System.Timers;
using System.IO;
using COServer.AI;
using CoS_Math;

namespace COServer
{
    public class Mobs
    {
        public static void GetAllDrops()
        {
            try
            {
                DataBase.MobDrops = new Dictionary<ushort, Drop>(DataBase.Mobs.Length);
                foreach (string[] Monster in DataBase.Mobs)
                {
                    ushort Id = ushort.Parse(Monster[0]);

                    if (File.Exists(System.Windows.Forms.Application.StartupPath + "\\Drops\\" + Id.ToString() + ".ini"))
                        DataBase.MobDrops.Add(Id, new Drop().Create(System.Windows.Forms.Application.StartupPath + "\\Drops\\" + Id.ToString() + ".ini"));
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void SpawnAllMobs()
        {
            try
            {
                uint MobsSpawned = 0;
                uint UID = 800000;

                for (int i = 0; i < DataBase.MobSpawns.Length; i++)
                {
                    Console.Write("\r");
                    Console.Write("Spawning monsters... {0}%", i * 100 / DataBase.MobSpawns.Length);

                    uint[] ThisSpawn = DataBase.MobSpawns[i];
                    string[] ThisMob = null;

                    foreach (string[] FindId in DataBase.Mobs)
                    {
                        if (FindId[0] == ThisSpawn[1].ToString())
                            ThisMob = FindId;
                    }

                    if (ThisMob != null)
                    {
                        for (int n = 0; n < (int)ThisSpawn[2]; n++)
                        {
                            ushort spawn_x = (ushort)Program.Rand.Next((ushort)Math.Min(ThisSpawn[3], ThisSpawn[5]), (ushort)Math.Max(ThisSpawn[3], ThisSpawn[5]));
                            ushort spawn_y = (ushort)Program.Rand.Next((ushort)Math.Min(ThisSpawn[4], ThisSpawn[6]), (ushort)Math.Max(ThisSpawn[4], ThisSpawn[6]));

                            while (World.AllMobs.ContainsKey(UID))
                                UID++;

                            Monster Mob = new Monster(
                                ushort.Parse(ThisMob[0]),
                                UID,
                                ThisMob[1],
                                byte.Parse(ThisMob[2]),
                                ushort.Parse(ThisMob[3]),
                                (ushort)ThisSpawn[7],
                                spawn_x,
                                spawn_y,
                                ushort.Parse(ThisMob[4]),
                                ushort.Parse(ThisMob[5]),
                                uint.Parse(ThisMob[6]),
                                uint.Parse(ThisMob[7]),
                                ushort.Parse(ThisMob[8]),
                                byte.Parse(ThisMob[9]),
                                byte.Parse(ThisMob[10]),
                                byte.Parse(ThisMob[11]),
                                ushort.Parse(ThisMob[12]),
                                ushort.Parse(ThisMob[13]),
                                ushort.Parse(ThisMob[14]),
                                byte.Parse(ThisMob[15]),
                                uint.Parse(ThisMob[16]),
                                uint.Parse(ThisMob[17]),
                                uint.Parse(ThisMob[18]),
                                ushort.Parse(ThisMob[19]));

                            World.AllMobs.Add(UID, Mob);

                            MobsSpawned++;
                            UID++;
                        }
                    }
                    else
                        Program.WriteLine("Monster " + ThisSpawn[1] + " doesn't exist for spawn " + ThisSpawn[0] + "!");
                }
                DataBase.MobSpawns = null;
                Console.Write("\r");
                Console.WriteLine("Spawning monsters... Ok!");
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void CheckSpawns()
        {
            int MonsterC = World.AllMobs.Count;
            List<Monster> ToRemove = new List<Monster>();
            lock (World.AllMobs)
            {
                foreach (Monster Monster in World.AllMobs.Values)
                {
                    ushort DMap = 0;
                    if (DataBase.AllMaps.ContainsKey(Monster.LocMap))
                        DMap = DataBase.AllMaps[Monster.LocMap].DMap;

                    if (DataBase.AllDMaps.ContainsKey(DMap)) //DataBase contains the DMap
                    {
                        if (!DataBase.AllDMaps[DMap].CanAcess(Monster.LocX, Monster.LocY)) //The location isn't acessible
                            ToRemove.Add(Monster);
                    }
                }
            }

            Monster[] TmpArray = ToRemove.ToArray();
            for (int i = 0; i < TmpArray.Length; i++)
            {
                TmpArray[i].AI.Stop();
                TmpArray[i].Drop = null;
                TmpArray[i].AI = null;
                World.RemoveEntity(TmpArray[i]);
                World.AllMobs.Remove(TmpArray[i].UID);
                TmpArray[i] = null;
            }
            ToRemove.Clear();
            TmpArray = null;

            Console.WriteLine("{0} monsters destroyed. The server has now {1} monsters!", MonsterC - World.AllMobs.Count, World.AllMobs.Count);
            Program.MCompressor.Optimize();
        }
    }

    public enum MonsterType : byte
    {
        Summon = 0,
        Normal = 1,
        Magic = 2,
        Boss = 3,
        Guard = 4,
        Reviver = 5,
        GuildBeast = 6
    }

    public class Monster : ConquerObject
    {
        public MonsterAI AI;
        public Drop Drop;
        public ushort Id;

        public string Name;
        public byte Type;
        public ushort Look;
        public ushort Level;

        public ushort XStart;
        public ushort YStart;
        public ushort PrevX;
        public ushort PrevY;

        public ushort CurHP;
        public ushort MaxHP;

        public ushort Defense;
        public byte MDef;
        public uint MinAtk;
        public uint MaxAtk;
        public byte Dodge;
        public ushort BattleLvl;

        public ushort SkillId;

        public byte AtkRange;
        public byte ViewRange;
        public ushort AtkSpeed;
        public ushort MoveSpeed;

        public bool Alive;
        private bool Revive;

        private uint Money;
        private uint PotHP;
        private uint PotMP;

        private Timer MyTimer = new Timer();
        public Character Target = null;
        public DateTime Death;

        public Monster(ushort m_Id, uint m_UID, string m_Name, byte m_Type, ushort m_Look, ushort m_LocMap, ushort m_LocX, ushort m_LocY, ushort m_Level, ushort m_Life, uint m_MinAtk, uint m_MaxAtk, ushort m_Defence, byte m_Dodge, byte m_AtkRange, byte m_ViewRange, ushort m_AtkSpeed, ushort m_MoveSpeed, ushort m_SkillId, byte m_MDef, uint m_Money, uint m_PotHP, uint m_PotMP, ushort m_BattleLvl)
        {
            Id = m_Id;
            UID = m_UID;
            Name = m_Name;
            Type = m_Type;
            Look = m_Look;
            Level = m_Level;
            CurHP = m_Life;
            MaxHP = m_Life;
            MinAtk = m_MinAtk;
            MaxAtk = m_MaxAtk;
            Defense = m_Defence;
            Dodge = m_Dodge;
            AtkRange = m_AtkRange;
            ViewRange = m_ViewRange;
            AtkSpeed = m_AtkSpeed;
            MoveSpeed = m_MoveSpeed;
            SkillId = m_SkillId;
            MDef = m_MDef;
            Money = m_Money;
            PotHP = m_PotHP;
            PotMP = m_PotMP;
            BattleLvl = m_BattleLvl;

            LocMap = m_LocMap;
            LocX = m_LocX;
            LocY = m_LocY;
            XStart = LocX;
            YStart = LocY;
            PrevX = LocX;
            PrevY = LocY;

            Alive = true;

            MyTimer.Interval = 500;
            MyTimer.Elapsed += new ElapsedEventHandler(TimerElapsed);
            MyTimer.Start();

            AI = new MonsterAI(this);
            if (DataBase.MobDrops.ContainsKey(Id))
                Drop = DataBase.MobDrops[Id];
        }

        public Monster(ushort m_Id, uint m_UID, ushort m_LocMap, ushort m_LocX, ushort m_LocY)
        {
            foreach (string[] MobInf in DataBase.Mobs)
            {
                if (MobInf[0] == m_Id.ToString())
                {
                    Id = m_Id;
                    UID = m_UID;
                    Name = MobInf[1];
                    Type = byte.Parse(MobInf[2]);
                    Look = ushort.Parse(MobInf[3]);
                    Level = ushort.Parse(MobInf[4]);
                    CurHP = ushort.Parse(MobInf[5]);
                    MaxHP = ushort.Parse(MobInf[5]);
                    MinAtk = uint.Parse(MobInf[6]);
                    MaxAtk = uint.Parse(MobInf[7]);
                    Defense = ushort.Parse(MobInf[8]);
                    Dodge = byte.Parse(MobInf[9]);
                    AtkRange = byte.Parse(MobInf[10]);
                    ViewRange = byte.Parse(MobInf[11]);
                    AtkSpeed = ushort.Parse(MobInf[12]);
                    MoveSpeed = ushort.Parse(MobInf[13]);
                    SkillId = ushort.Parse(MobInf[14]);
                    MDef = byte.Parse(MobInf[15]);
                    Money = uint.Parse(MobInf[16]);
                    PotHP = uint.Parse(MobInf[17]);
                    PotMP = uint.Parse(MobInf[18]);
                    BattleLvl = ushort.Parse(MobInf[19]);

                    LocMap = m_LocMap;
                    LocX = m_LocX;
                    LocY = m_LocY;
                    XStart = LocX;
                    YStart = LocY;
                    PrevX = LocX;
                    PrevY = LocY;

                    Alive = true;

                    MyTimer.Interval = 500;
                    MyTimer.Elapsed += new ElapsedEventHandler(TimerElapsed);
                    MyTimer.Start();

                    AI = new MonsterAI(this);
                    if (DataBase.MobDrops.ContainsKey(Id))
                        Drop = DataBase.MobDrops[Id];
                    return;
                }
            }
            Program.WriteLine("Try to create an inexistant monster! MonsterId: " + m_Id);
        }

        ~Monster()
        {
            AI = null;
            Drop = null;
            Target = null;

            Name = null;

            MyTimer.Dispose();
            MyTimer = null;
        }

        public bool IsGreen(Character Player) { return (Player.Level - Level) >= 3; }
        public bool IsWhite(Character Player) { return (Player.Level - Level) >= 0 && (Player.Level - Level) < 3; }
        public bool IsRed(Character Player) { return (Player.Level - Level) >= -4 && (Player.Level - Level) < 0; }
        public bool IsBlack(Character Player) { return (Player.Level - Level) < -4; }

        private void TimerElapsed(object source, ElapsedEventArgs e)
        {
            if (HavePlayer() && !AI.IsThinking)
                AI.Start();

            if (!HavePlayer() && AI.IsThinking)
                AI.Stop();

            if (!Alive)
            {
                if (DateTime.Now > Death.AddMilliseconds(3000))
                    Dissappear();

                if (AI.IsThinking)
                    AI.Stop();

                if (Revive)
                {
                    if (DateTime.Now > Death.AddMilliseconds(10000))
                        ReSpawn();
                }
            }
        }

        private bool HavePlayer()
        {
            lock (World.AllChars)
            {
                if (World.AllChars.Count == 0)
                    return false;

                foreach (Character Player in World.AllChars.Values)
                {
                    if (LocMap == Player.LocMap)
                        if (MyMath.CanSeeBig(LocX, LocY, Player.LocX, Player.LocY))
                            return true;
                }

                return false;
            }
        }

        public bool GetDamage(Character Attacker, uint Damage)
        {
            if (CurHP > Damage)
            {
                CurHP -= (ushort)Damage;

                return false;
            }
            else
            {
                CurHP = 0;
                Alive = false;
                Revive = false;

                if (Type != 4)
                {
                    uint MoneyDrops = 0;

                    //Money Drop
                    if (Money != 0 && MyMath.Success(65) && !Other.NoDrop(LocMap))
                    {
                        int DropTimes = 1;
                        if (MyMath.Success(50))
                            DropTimes = Program.Rand.Next(1, 6);

                        for (int i = 0; i < DropTimes; i++)
                        {
                            MoneyDrops = (uint)Program.Rand.Next((int)Money, (int)(Money * 2));

                            if (MyMath.Success(90))
                                MoneyDrops = (uint)Program.Rand.Next((int)Money, (int)(Money * 3 * DataBase.Rate.Money));
                            if (MyMath.Success(70))
                                MoneyDrops = (uint)Program.Rand.Next((int)Money, (int)(Money * 5 * DataBase.Rate.Money));
                            if (MyMath.Success(50))
                                MoneyDrops = (uint)Program.Rand.Next((int)Money, (int)(Money * 7 * DataBase.Rate.Money));
                            if (MyMath.Success(30))
                                MoneyDrops = (uint)Program.Rand.Next((int)Money, (int)(Money * 8 * DataBase.Rate.Money));
                            if (MyMath.Success(15))
                                MoneyDrops = (uint)Program.Rand.Next((int)Money, (int)(Money * 10 * DataBase.Rate.Money));

                            string Item = "";
                            if (MoneyDrops <= 10) //Silver
                                Item = "1090000-0-0-0-0-0";
                            else if (MoneyDrops <= 100) //Sycee
                                Item = "1090010-0-0-0-0-0";
                            else if (MoneyDrops <= 1000) //Gold
                                Item = "1090020-0-0-0-0-0";
                            else if (MoneyDrops <= 2000) //GoldBullion
                                Item = "1091000-0-0-0-0-0";
                            else if (MoneyDrops <= 5000) //GoldBar
                                Item = "1091010-0-0-0-0-0";
                            else if (MoneyDrops > 5000) //GoldBars
                                Item = "1091020-0-0-0-0-0";
                            else //Error
                                Item = "1090000-0-0-0-0-0";

                            short[] Location = Other.GetCoords(LocMap, LocX, LocY);
                            DroppedItem item = DroppedItems.DropItem(Attacker.UID, Item, (uint)Location[0], (uint)Location[1], (uint)LocMap, MoneyDrops);
                            World.ItemDrops(item);
                        }
                    }

                    //CPs Drop
                    if (MyMath.Success(10) || Attacker.LuckTime > 0 && MyMath.Success(12.5))
                        Attacker.CPs += (uint)(3 * DataBase.Rate.CPs);

                    if (MyMath.Success(7.5) || Attacker.LuckTime > 0 && MyMath.Success(10))
                        Attacker.CPs += (uint)(5 * DataBase.Rate.CPs);

                    if (MyMath.Success(5) || Attacker.LuckTime > 0 && MyMath.Success(7.5))
                        Attacker.CPs += (uint)(10 * DataBase.Rate.CPs);

                    //Potion Drop
                    if (PotHP != 0 && MyMath.Success(5))
                    {
                        string Item = PotHP.ToString() + "-0-0-0-0-0";
                        short[] Location = Other.GetCoords(LocMap, LocX, LocY);

                        DroppedItem item = DroppedItems.DropItem(Attacker.UID, Item, (uint)Location[0], (uint)Location[1], LocMap, 0);
                        World.ItemDrops(item);
                    }
                    if (PotMP != 0 && MyMath.Success(5))
                    {
                        string Item = PotMP.ToString() + "-0-0-0-0-0";
                        short[] Location = Other.GetCoords(LocMap, LocX, LocY);

                        DroppedItem item = DroppedItems.DropItem(Attacker.UID, Item, (uint)Location[0], (uint)Location[1], LocMap, 0);
                        World.ItemDrops(item);
                    }

                    //Special Drop
                    if (Attacker.LuckTime > 0 && CoServer.LuckyDrop)
                        Other.LuckyDrop(Attacker);

                    //Item Drop
                    if (Drop != null)
                    {
                        //Drop Meteor
                        if (MyMath.Success(Drop.Rate.Meteor * DataBase.Rate.Meteor))
                        {
                            string Item = "1088001-0-0-0-0-0";
                            short[] Location = Other.GetCoords(LocMap, LocX, LocY);

                            DroppedItem item = DroppedItems.DropItem(Attacker.UID, Item, (uint)Location[0], (uint)Location[1], LocMap, 0);
                            World.ItemDrops(item);
                        }

                        //Drop DragonBall
                        if (MyMath.Success(Drop.Rate.DragonBall * DataBase.Rate.DragonBall))
                        {
                            string Item = "1088000-0-0-0-0-0";
                            short[] Location = Other.GetCoords(LocMap, LocX, LocY);

                            DroppedItem item = DroppedItems.DropItem(Attacker.UID, Item, (uint)Location[0], (uint)Location[1], LocMap, 0);
                            World.ItemDrops(item);
                        } 

                        //Drop Gem
                        if (MyMath.Success(Drop.Rate.Gem * DataBase.Rate.Gem))
                        {
                            string Item = "7000" + Program.Rand.Next(0, 7).ToString() + "1-0-0-0-0-0";
                            short[] Location = Other.GetCoords(LocMap, LocX, LocY);

                            DroppedItem item = DroppedItems.DropItem(Attacker.UID, Item, (uint)Location[0], (uint)Location[1], LocMap, 0);
                            World.ItemDrops(item);
                        } 

                        //Drop Equip
                        if (MyMath.Success(75))
                        {
                            uint ItemId = Drop.GenerateEquip();
                            byte Plus = 0;
                            byte Bless = 0;
                            byte Soc1 = 0;
                            byte Soc2 = 0;

                            if (MyMath.Success(Drop.Rate.Craft * DataBase.Rate.Craft))
                                Plus = 1;


                            if (ItemHandler.ItemType(ItemId) == 4 || ItemHandler.ItemType(ItemId) == 5)
                            {
                                if (MyMath.Success(50 / ItemHandler.ItemQuality(ItemId)))
                                {
                                    Soc1 = 255;
                                    if (MyMath.Success(25 / ItemHandler.ItemQuality(ItemId)))
                                        Soc2 = 255;
                                }
                            }

                            if (MyMath.Success(2.5))
                                Bless = (byte)Program.Rand.Next(1, 6);

                            if (ItemId != 0)
                            {
                                string Item = ItemId.ToString() + "-" + Plus.ToString() + "-" + Bless.ToString() + "-0-" + Soc1.ToString() + "-" + Soc2.ToString();
                                short[] Location = Other.GetCoords(LocMap, LocX, LocY);

                                DroppedItem item = DroppedItems.DropItem(Attacker.UID, Item, (uint)Location[0], (uint)Location[1], (uint)LocMap, 0);
                                World.ItemDrops(item);
                            }
                        }

                        //Drop Special Item
                        if (Drop.Specials.Count > 0)
                        {
                            foreach (KeyValuePair<uint, double> KV in Drop.Specials)
                            {
                                if (MyMath.Success(KV.Value))
                                {
                                    string Item = KV.Key.ToString() + "-0-0-0-0-0";
                                    short[] Location = Other.GetCoords(LocMap, LocX, LocY);

                                    DroppedItem item = DroppedItems.DropItem(Attacker.UID, Item, (uint)Location[0], (uint)Location[1], LocMap, 0);
                                    World.ItemDrops(item);
                                }
                            }
                        }
                    }
                }
                return true;
            }
        }

        public void Dissappear()
        {
            if (Type == 3 || Type == 6)
            {
                World.AllMobs.Remove(UID);
                Revive = false;
            }
            else
                Revive = true;
            World.RemoveEntity(this);
        }

        public void ReSpawn()
        {
            CurHP = MaxHP;
            Alive = true;
            LocX = (ushort)XStart;
            LocY = (ushort)YStart;
            PrevX = LocX;
            PrevY = LocY;
            World.RemoveEntity(this);
            World.MobReSpawn(this);
            Revive = false;
        }
    }
}
