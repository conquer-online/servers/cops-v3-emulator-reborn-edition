﻿using System;

namespace COServer
{
    public class ConquerObject
    {
        const uint NpcId_Min = 1;
        const uint Dyn_NpcId_Min = 700000;
        const uint Dyn_NpcId_Max = 799999;
        const uint NpcId_Max = 799999;
        const uint MonsterId_Min = 800000;
        const uint MonsterId_Max = 999999;
        const uint PlayerId_Min = 1000000;
        const uint PlayerId_Max = 1999999999;

        private uint m_UID = 0;

        private ushort m_LocMap;
        private ushort m_LocX;
        private ushort m_LocY;

        private byte m_Direction;

        public virtual uint UID { get { return m_UID; } set { this.m_UID = value; } }

        public virtual ushort LocMap { get { return m_LocMap; } set { this.m_LocMap = value; } }
        public virtual ushort LocX { get { return m_LocX; } set { this.m_LocX = value; } }
        public virtual ushort LocY { get { return m_LocY; } set { this.m_LocY = value; } }

        public virtual byte Direction { get { return m_Direction; } set { this.m_Direction = value; } }

        public bool IsNPC() { return UID >= NpcId_Min && UID <= NpcId_Max; }
        public bool IsDynNPC() { return UID >= Dyn_NpcId_Min && UID <= Dyn_NpcId_Max; }
        public bool IsMonster() { return UID >= MonsterId_Min && UID <= MonsterId_Max; }
        public bool IsPlayer() { return UID >= PlayerId_Min && UID <= PlayerId_Max; }
    }
}
