using System;
using System.Collections.Generic;
using System.Collections;
using System.Threading;
using System.Timers;
using System.Text;
using System.Net;
using System.IO;
using COServer.AI;
using CoS_Math;

namespace COServer
{
    public class Character : ConquerObject
    {
        public Client MyClient;
        public Screen Screen;
        public Shop MyShop;

        public uint Model = 0;
        public uint RealModel = 0;
        public uint MobModel = 0;
        public ushort Avatar = 0;
        public ushort RealAvatar = 0;
        public byte dexp = 0;
        public uint dexptime = 0;
        public uint SkillExpNull = 0;
        public byte Stamina = 0;
        public bool CanSend = true;
        public bool SeeWhisper;
        public byte LotoCount = 0;

        public string Name = "";
        public string Spouse = "Non";
        public bool Attacking = false;

        public TimeSpan AutoClick_Time;
        public double AutoClick_Count = 0;
        public bool AutoClick_Check = false;
        public DateTime AutoClick_Send;

        public byte FirstJob = 0;
        public byte FirstLevel = 0;
        public byte SecondJob = 0;
        public byte SecondLevel = 0;
        public byte ThirdJob = 0;
        public byte ThirdLevel = 0;

        public uint KO = 0;
        public uint OldKO = 0;
        public uint DisKO = 0;

        public uint BanNB = 0;

        public uint WHPWcheck = 0;
        public string WHPW = "";

        public uint WHSilvers = 0;
        public uint VPs = 0;

        private byte m_Level = 0;
        private byte m_Job = 0;
        private ulong m_Exp = 0;
        private uint m_Silvers = 0;
        private uint m_CPs = 0;
        private byte m_RBCount = 0;
        private ushort m_PKPoints = 0;
        private ushort m_Str = 0;
        private ushort m_Agi = 0;
        private ushort m_Vit = 0;
        private ushort m_Spi = 0;
        private uint m_StatP = 0;

        private ushort m_CurHP = 0;
        public ushort MaxHP = 0;
        public ushort BaseHP = 0;
        private ushort m_CurMP = 0;
        public ushort MaxMP = 0;
        public ushort BaseMP = 0;

        private byte m_Action = 100;
        private byte m_PKMode = 0;

        private ushort m_Hair = 0;
        private ushort m_ShowHair = 0;
        private byte m_XPCircle = 0;

        public StatusFlag Flags;
        public bool MShieldBuff;
        public bool Shield;
        public bool StigBuff;
        public bool DodgeBuff;
        public bool AccuracyBuff;
        public bool FreezeBuff;
        public bool InvisibleBuff;
        public bool IronShirt;
        public bool Restore;
        public bool Accuracy;

        public byte Level
        {
            get { return m_Level; }
            set { m_Level = value; Screen.SendPacket(CoServer.MyPackets.Vital(UID, 13, m_Level), true); }
        }

        public byte Job
        {
            get { return m_Job; }
            set { m_Job = value; MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 7, m_Job)); }
        }

        public ulong Exp
        {
            get { return m_Exp; }
            set { m_Exp = value; MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 5, m_Exp)); }
        }

        public uint Silvers
        {
            get { return m_Silvers; }
            set { m_Silvers = value; MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 4, m_Silvers)); }
        }

        public uint CPs
        {
            get { return m_CPs; }
            set { m_CPs = value; MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 30, m_CPs)); }
        }

        public byte RBCount
        {
            get { return m_RBCount; }
            set
            { 
                m_RBCount = value;
                if (m_RBCount >= 3)
                    Flags.RebornFlag = true;
                else
                    Flags.RebornFlag = false;
                Screen.SendPacket(CoServer.MyPackets.Vital(UID, 23, m_RBCount), true);
            }
        }

        public ushort PKPoints
        {
            get { return m_PKPoints; }
            set
            { 
                m_PKPoints = value;
                if (m_PKPoints >= 30 && m_PKPoints < 100)
                {
                    Flags.BlackName = false;
                    Flags.RedName = true;
                }
                if (m_PKPoints >= 100)
                {
                    Flags.RedName = false;
                    Flags.BlackName = true;
                }
                MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 6, m_PKPoints));
            }
        }

        public ushort Str
        {
            get { return m_Str; }
            set { m_Str = value; MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 16, m_Str)); }
        }

        public ushort Agi
        {
            get { return m_Agi; }
            set { m_Agi = value; MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 17, m_Agi)); }
        }

        public ushort Vit
        {
            get { return m_Vit; }
            set { m_Vit = value; MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 15, m_Vit)); }
        }

        public ushort Spi
        {
            get { return m_Spi; }
            set { m_Spi = value; MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 14, m_Spi)); }
        }

        public uint StatP
        {
            get { return m_StatP; }
            set { m_StatP = value; MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 11, m_StatP)); }
        }

        public ushort CurHP
        {
            get { return m_CurHP; }
            set { m_CurHP = value; MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 0, m_CurHP)); }
        }

        public ushort CurMP
        {
            get { return m_CurMP; }
            set { m_CurMP = value; MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 2, m_CurMP)); }
        }

        public byte Action
        {
            get { return m_Action; }
            set { m_Action = value; Screen.SendPacket(CoServer.MyPackets.GeneralData(this, m_Action, 81), true); }
        }

        public byte PKMode
        {
            get { return m_PKMode; }
            set { m_PKMode = value; MyClient.SendPacket(CoServer.MyPackets.GeneralData(this, m_PKMode, 96)); }
        }

        public ushort Hair
        {
            get { return m_Hair; }
            set { m_Hair = value; Screen.SendPacket(CoServer.MyPackets.Vital(UID, 27, m_Hair), true); }
        }

        public ushort ShowHair
        {
            get { return m_ShowHair; }
            set { m_ShowHair = value; Screen.SendPacket(CoServer.MyPackets.Vital(UID, 27, m_ShowHair), true); }
        }

        public byte XpCircle
        {
            get { return m_XPCircle; }
            set
            {
                m_XPCircle = value;
                if (m_XPCircle > 100)
                    m_XPCircle = 100;
                if (m_XPCircle != 100)
                    Flags.XPList = false;
                else
                    Flags.XPList = true;

                LastXPC = DateTime.Now;
                MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 28, m_XPCircle));
            }
        }

        public bool BlueName
        {
            get { return Flags.Flashing; }
            set
            { 
                Flags.Flashing = value; 
                if (value)
                    GotBlueName = DateTime.Now;
            }
        }

        public bool Alive
        {
            get { return !Flags.Dead; }
            set
            {
                Flags.Dead = !value;
                Flags.Frozen = !value;
                if (!value)
                {
                    Death = DateTime.Now;
                    Die();
                }
            }
        }

        public ushort PrevX = 0;
        public ushort PrevY = 0;
        public ushort RealAgi = 0;
        public double AddExpPc = 1;
        public double AddProfPc = 1;
        public double AddSpellPc = 1;
        public double AddMythique = 1;

        public uint Defense = 0;
        public uint MDefense = 0;
        public uint MagicBlock = 0;
        public double MinAtk = 0;
        public double MaxAtk = 0;
        public double MAtk = 0;
        public double Bless = 1;
        public byte Dodge = 0;
        public ushort ADS_Attack = 0;

        public uint LuckTime = 0;
        public DateTime PrayCasted = DateTime.Now;
        public ushort PrayX = 0;
        public ushort PrayY = 0;
        public bool CastingPray = false;
        public bool Praying = false;
        public bool CanPray = false;

        public ushort PrevMap = 0;

        public Hashtable Skills = new Hashtable();
        public Hashtable Skill_Exps = new Hashtable();
        public Hashtable Profs = new Hashtable();
        public Hashtable Prof_Exps = new Hashtable();
        public Hashtable Friends = new Hashtable();
        public Hashtable Enemies = new Hashtable();

        public string[] Equips = new string[10];
        public string[] Inventory = new string[42];
        public string[] TCWH = new string[20];
        public string[] PCWH = new string[20];
        public string[] BIWH = new string[20];
        public string[] ACWH = new string[20];
        public string[] DCWH = new string[20];
        public string[] MAWH = new string[40];
        public string[] AZWH = new string[20];

        public uint[] Equips_UIDs = new uint[10];
        public uint[] Inventory_UIDs = new uint[41];
        public uint[][] WHIDs = new uint[7][];

        public byte ItemsInInventory = 0;
        public byte AddedSkills = 0;
        public byte TCWHCount = 0;
        public byte PCWHCount = 0;
        public byte BIWHCount = 0;
        public byte ACWHCount = 0;
        public byte DCWHCount = 0;
        public byte MAWHCount = 0;
        public byte AZWHCount = 0;

        public Monster MobTarget = null;
        public Monster Guard = null;
        public Character PTarget = null;
        public SingleNPC TGTarget = null;
        public byte AtkType = 0;
        public ushort Potency = 0;
        public ushort SkillLooping = 0;
        public ushort SkillLoopingX = 0;
        public ushort SkillLoopingY = 0;
        public uint SkillLoopingTarget = 0;
        public bool TeamLeader = false;
        public bool SMOn = false;
        public bool CycloneOn = false;
        public bool ReflectOn = false;
        public bool RobotOn = false;
        public ArrayList Team = new ArrayList(6);
        public Character MyTeamLeader = null;
        public byte PlayersInTeam = 0;
        public byte TeamCount = 0;
        public bool JoinForbidden = false;
        public bool Trading = false;
        public uint TradingWith = 0;
        public ArrayList MyTradeSide = new ArrayList(20);
        public uint TradingSilvers = 0;
        public uint TradingCPs = 0;
        public bool TradeOK = false;
        public byte MyTradeSideCount = 0;
        public bool Flying = false;

        public DateTime RestoreTime = DateTime.Now;

        public bool GMTransform = false;
        public DateTime GMTransformed = DateTime.Now;
        public bool Transform = false;

        public uint TargetUID = 0;
        public uint RequestFriendWith = 0;

        public int GuildDonation = 0;
        public ushort GuildID = 0;
        public byte GuildPosition = 0;

        public Guild MyGuild;
        public DateTime LastSkill;
        public DateTime LastAttack;
        public DateTime Death;
        public DateTime LastSave;
        public DateTime GotBlueName;
        public DateTime LostPKP;
        public DateTime LastXPC;
        public DateTime LastSwing = DateTime.Now;
        public DateTime AccuracyActivated = DateTime.Now;
        public DateTime LastTargetting = DateTime.Now;
        public DateTime LastGWList = DateTime.Now;
        public DateTime FlyActivated = DateTime.Now;
        public DateTime Stigged = DateTime.Now;
        public DateTime Dodged = DateTime.Now;
        public DateTime Shielded = DateTime.Now;
        public DateTime IronShirted = DateTime.Now;
        public DateTime MShielded = DateTime.Now;
        public DateTime Invisibility = DateTime.Now;
        public DateTime ExpBuffed = DateTime.Now;
        public DateTime Freezed = DateTime.Now;
        public DateTime KOActivated = DateTime.Now;
        public ulong ExtraXP = 0;
        public uint TimeSkill = 0;
        public uint TimeSkill2 = 0;
        public byte FlyType = 0;
        public bool Mining;
        public byte StigLevel = 0;
        public byte DodgeLevel = 0;
        public byte InvLevel = 0;
        public byte MShieldLevel = 0;
        public byte AccuLevel = 0;

        public System.Timers.Timer TheTimer = new System.Timers.Timer();
        public System.Timers.Timer Radio = new System.Timers.Timer();

        private bool Saving = false;
        //public SuperSoldierAI AI;

        public void Radio_Elapsed(object sender, ElapsedEventArgs e)
        {
            CanSend = true;
            Radio.Stop();
        }

        public void SwingPickAxe()
        {
            if (Mining)
            {
                string[] Splitter = Equips[4].Split('-');
                if (LocMap >= 1023 && LocMap <= 1028 && Splitter[0] == "562000" || Splitter[0] == "562001")
                {
                    LastSwing = DateTime.Now;
                    Screen.SendPacket(CoServer.MyPackets.GeneralData(this, 99, 99), true);

                    if (ItemsInInventory < 40)
                        if (MyMath.Success(30))
                        {
                            uint ItemId = (uint)(1072010 + Program.Rand.Next(10)); //Iron

                            if (MyMath.Success(50) && LocMap != 1028)
                                ItemId = (uint)(1072020 + Program.Rand.Next(10)); //Copper

                            if (MyMath.Success(40) && LocMap != 1028 && LocMap != 1025)
                                ItemId = (uint)(1072040 + Program.Rand.Next(10)); //Silver

                            if (MyMath.Success(25) && LocMap != 1028 || MyMath.Success(10))
                                ItemId = (uint)(1072050 + Program.Rand.Next(10)); //Gold

                            if (MyMath.Success(10) && LocMap == 1028)
                                ItemId = 1072031; //Euxite

                            if (MyMath.Success(2.5))
                            {
                                ItemId = (uint)(700001 + Program.Rand.Next(8) * 10);
                                if (MyMath.Success(45))
                                    ItemId++;
                                if (MyMath.Success(25))
                                    ItemId++;
                            }

                            if (ItemId != 0)
                                AddItem(ItemId.ToString() + "-0-0-0-0-0", 0, (uint)Program.Rand.Next(23453246));
                        }
                }
                else
                    Mining = false;
            }
        }

        public void GemEffect()
        {
            if (Other.ChanceSuccess(25))
                for (byte Pos = 0; Pos < 9; Pos++)
                    if (Equips[Pos] != "0" && Equips[Pos] != null)
                    {
                        string[] Splitter = Equips[Pos].Split('-');

                        byte ItemGem1 = byte.Parse(Splitter[4]);
                        byte ItemGem2 = byte.Parse(Splitter[5]);
                        if (ItemGem1 == 3 || ItemGem2 == 3)
                            if (Other.ChanceSuccess(10))
                            {
                                World.ShowEffect(this, "phoenix");
                                return;
                            }
                        if (ItemGem1 == 13 || ItemGem2 == 13)
                            if (Other.ChanceSuccess(10))
                            {
                                World.ShowEffect(this, "goldendragon");
                                return;
                            }
                        if (ItemGem1 == 23 || ItemGem2 == 23)
                            if (Other.ChanceSuccess(10))
                            {
                                World.ShowEffect(this, "fastflash");
                                return;
                            }
                        if (ItemGem1 == 33 || ItemGem2 == 33)
                            if (Other.ChanceSuccess(10))
                            {
                                World.ShowEffect(this, "rainbow");
                                return;
                            }
                        if (ItemGem1 == 43 || ItemGem2 == 43)
                            if (Other.ChanceSuccess(10))
                            {
                                World.ShowEffect(this, "goldenkylin");
                                return;
                            }
                        if (ItemGem1 == 53 || ItemGem2 == 53)
                            if (Other.ChanceSuccess(10))
                            {
                                World.ShowEffect(this, "purpleray");
                                return;
                            }
                        if (ItemGem1 == 63 || ItemGem2 == 73)
                            if (Other.ChanceSuccess(10))
                            {
                                World.ShowEffect(this, "moon");
                                return;
                            }
                        if (ItemGem1 == 73 || ItemGem2 == 73)
                            if (Other.ChanceSuccess(10))
                            {
                                World.ShowEffect(this, "recovery");
                                return;
                            }
                    }
        }

        public void BlessEffect()
        {
            if (Other.ChanceSuccess(25))
                for (byte Pos = 0; Pos < 9; Pos++)
                    if (Equips[Pos] != "0" && Equips[Pos] != null)
                    {
                        string[] Splitter = Equips[Pos].Split('-');

                        byte ItemBless = byte.Parse(Splitter[2]);
                        if (ItemBless == 1)
                            if (Other.ChanceSuccess(15))
                            {
                                World.ShowEffect(this, "Aegis1");
                                return;
                            }
                        if (ItemBless == 2 || ItemBless == 3)
                            if (Other.ChanceSuccess(15))
                            {
                                World.ShowEffect(this, "Aegis2");
                                return;
                            }
                        if (ItemBless == 4 || ItemBless == 5)
                            if (Other.ChanceSuccess(15))
                            {
                                World.ShowEffect(this, "Aegis3");
                                return;
                            }
                        if (ItemBless >= 6)
                            if (Other.ChanceSuccess(15))
                            {
                                World.ShowEffect(this, "Aegis4");
                                return;
                            }
                    }
        }

        public bool AllSuper()
        {
            if (Equips[1] != null && Equips[2] != null && Equips[3] != null && Equips[4] != null && Equips[8] != null && Equips[6] != null)
            {
                string TheEquip1 = Equips[1];
                string TheEquip2 = Equips[2];
                string TheEquip3 = Equips[3];
                string TheEquip4 = Equips[4];
                string TheEquip8 = Equips[8];
                string TheEquip6 = Equips[6];

                string[] Splitter1 = TheEquip1.Split('-');
                uint ItemId1 = uint.Parse(Splitter1[0]);

                string[] Splitter2 = TheEquip2.Split('-');
                uint ItemId2 = uint.Parse(Splitter2[0]);

                string[] Splitter3 = TheEquip3.Split('-');
                uint ItemId3 = uint.Parse(Splitter3[0]);

                string[] Splitter4 = TheEquip4.Split('-');
                uint ItemId4 = uint.Parse(Splitter4[0]);

                string[] Splitter8 = TheEquip8.Split('-');
                uint ItemId8 = uint.Parse(Splitter8[0]);

                string[] Splitter6 = TheEquip6.Split('-');
                uint ItemId6 = uint.Parse(Splitter6[0]);

                if (ItemHandler.ItemQuality(ItemId1) == 9 && ItemHandler.ItemQuality(ItemId2) == 9 && ItemHandler.ItemQuality(ItemId3) == 9 && ItemHandler.ItemQuality(ItemId4) == 9 && ItemHandler.ItemQuality(ItemId8) == 9 && ItemHandler.ItemQuality(ItemId6) == 9)
                    return true;

                return false;
            }
            return false;
        }

        public bool ArmorSuper()
        {
            if (Equips[3] != null)
            {
                string TheEquip3 = Equips[3];

                string[] Splitter3 = TheEquip3.Split('-');
                uint ItemId3 = uint.Parse(Splitter3[0]);

                if (ItemHandler.ItemQuality(ItemId3) == 9)
                    return true;

                return false;
            }
            return false;
        }

        void TimerElapsed(object source, ElapsedEventArgs e)
        {
            if (CPs > 999998 && MyClient.Status < 6)
            {
                World.SendMsgToAll(Name + " a �t� banni du serveur car il avait " + CPs.ToString() + "CPs...", "SYSTEM", 2011);
                CoServer.WriteCheat(this, "CPS_CHEAT[" + CPs.ToString() + "]");
                DataBase.Ban(MyClient.Account);
                MyClient.Drop();
            }

            if (AutoClick_Check)
                if (DateTime.Now > AutoClick_Send.AddSeconds(45))
                {
                    World.SendMsgToAll(Name + " a �t� banni du serveur car il utilisait un autoclick!", "SYSTEM", 2011);
                    CoServer.WriteCheat(this, "AUTOCLICK_CHEAT");
                    DataBase.Ban(MyClient.Account);
                    MyClient.Drop();
                }

            if (AutoClick_Count > 5)
            {
                AutoClick_Count = 0;
                MyClient.CurrentNPC = 1000010;
                MyClient.SendPacket(CoServer.MyPackets.NPCSay("Vous �tes soupsonn� d'utiliser un AutoClick. En utilisez-vous un?"));
                MyClient.SendPacket(CoServer.MyPackets.NPCLink("Non!", 255));
                MyClient.SendPacket(CoServer.MyPackets.NPCSetFace(6));
                MyClient.SendPacket(CoServer.MyPackets.NPCFinish());

                AutoClick_Check = true;
                AutoClick_Send = DateTime.Now;
            }

            if (StigBuff)
                if (DateTime.Now > Stigged.AddSeconds(20 + StigLevel * 5))
                {
                    Flags.Stigged = false;
                    StigBuff = false;
                }

            if (AccuracyBuff)
                if (DateTime.Now > AccuracyActivated.AddSeconds(20 + AccuLevel * 5))
                {
                    Flags.Accuracy = false;
                    AccuracyBuff = false;
                }

            if (Accuracy)
                if (DateTime.Now > AccuracyActivated.AddSeconds(200))
                {
                    Flags.Accuracy = false;
                    Accuracy = false;
                }

            if (DodgeBuff)
                if (DateTime.Now > Dodged.AddSeconds(20 + DodgeLevel * 5 * 2))
                    DodgeBuff = false;

            if (InvisibleBuff)
                if (DateTime.Now > Invisibility.AddSeconds(20 + InvLevel * 5 * 2))
                {
                    Flags.Invisible = false;
                    InvisibleBuff = false;
                }

            if (MShieldBuff)
                if (DateTime.Now > MShielded.AddSeconds(20 + MShieldLevel * 5))
                {
                    Flags.Shielded = false;
                    MShieldBuff = false;
                }

            if (Shield)
                if (DateTime.Now > Shielded.AddSeconds(120))
                {
                    Flags.Shielded = false;
                    Shield = false;
                }

            if (IronShirt)
                if (DateTime.Now > IronShirted.AddSeconds(10))
                {
                    Flags.Shielded = false;
                    IronShirt = false;
                }

            if (Restore)
            {
                if (CurHP < MaxHP)
                {
                    CurHP += (ushort)(MaxHP / 15);
                    if (CurHP > MaxHP)
                        CurHP = MaxHP;
                }
                if (DateTime.Now > RestoreTime.AddSeconds(50))
                {
                    Flags.Restoring = false;
                    Restore = false;
                }
            }

            if (GMTransform)
                if (DateTime.Now > GMTransformed.AddSeconds(ushort.MaxValue))
                {
                    Transform = false;
                    GMTransform = false;
                    MyClient.SendPacket(CoServer.MyPackets.CharacterInfo(this));
                    GetEquipStats(1, true);
                    GetEquipStats(2, true);
                    GetEquipStats(3, true);
                    GetEquipStats(4, true);
                    GetEquipStats(5, true);
                    GetEquipStats(6, true);
                    GetEquipStats(7, true);
                    GetEquipStats(8, true);
                    GetEquipStats(9, true);
                    MinAtk = Str;
                    MaxAtk = Str;
                    Calculation.SetMaxHP(this, false);
                    Calculation.SetPotency(this);
                    SendEquips(true);
                    World.UpdateSpawn(this);
                }
                else
                {
                    MaxHP = 65535;
                    MinAtk = 10000000;
                    MaxAtk = 10000000;
                    Defense = 15000000;
                    MAtk = 0;
                    MDefense = 100;
                    MagicBlock = 0;
                    Dodge = 100;
                }

            if (FreezeBuff)
                if (DateTime.Now > Freezed.AddSeconds(30))
                {
                    GetEquipStats(1, true);
                    GetEquipStats(2, true);
                    GetEquipStats(3, true);
                    GetEquipStats(4, true);
                    GetEquipStats(5, true);
                    GetEquipStats(6, true);
                    GetEquipStats(7, true);
                    GetEquipStats(8, true);
                    GetEquipStats(9, true);
                    MinAtk = Str;
                    MaxAtk = Str;
                    GetEquipStats(1, false);
                    GetEquipStats(2, false);
                    GetEquipStats(3, false);
                    GetEquipStats(4, false);
                    GetEquipStats(5, false);
                    GetEquipStats(6, false);
                    GetEquipStats(7, false);
                    GetEquipStats(8, false);
                    GetEquipStats(9, false);
                    FreezeBuff = false;
                }

            //----Lucky Time
            //--Gain LuckyTime--
            if (CastingPray)//Caster
                if (LuckTime < (2 * 60 * 60 * 1000))
                {
                    LuckTime += 1500;
                    if (LuckTime >= (2 * 60 * 60 * 1000))
                        LuckTime = (2 * 60 * 60 * 1000);

                    MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 29, LuckTime));
                }
            if (Praying)//Others
                if (LuckTime < (2 * 60 * 60 * 1000))
                {
                    LuckTime += 500;
                    if (LuckTime >= (2 * 60 * 60 * 1000))
                        LuckTime = (2 * 60 * 60 * 1000);

                    MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 29, LuckTime));
                }
            //------------------
            //--Lose LuckyTime--
            if (!Praying && !CastingPray)
                if (LuckTime > 0)
                {
                    LuckTime -= 500;
                    MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 29, LuckTime));
                }
            //------------------
            //--Caster Stops Luckytime--
            if (CastingPray)
            {
                if (LocX != PrayX || LocY != PrayY || !Alive || Attacking)
                {
                    Flags.CastingPray = false;
                    Flags.Praying = false;
                    CastingPray = false;
                    Praying = false;

                    MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 29, LuckTime));
                    PrayX = 0;
                    PrayY = 0;
                    World.PlayersPraying.Remove(this);
                }
            }
            //--------------------------
            //--Others Start Praying--
            foreach (Character Caster in World.PlayersPraying)
            {
                if (LocMap == Caster.LocMap)
                    if (this != Caster)
                        if (Caster.CastingPray)
                            if ((MyMath.GetDistance(LocX, LocY, Caster.LocX, Caster.LocY) < 4) || (LocX == Caster.LocX && LocY == Caster.LocY))
                                if (!Praying && !CastingPray)
                                {
                                    Thread.Sleep(TimeSpan.FromSeconds(3));
                                    {
                                        if (!Mining)
                                        {
                                            Flags.Praying = true;
                                            Praying = true;
                                            MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 29, LuckTime));
                                        }
                                    }
                                }
            }
            //------------------------
            //--Others Stop Praying--
            foreach (Character Caster in World.PlayersPraying)
            {
                if (LocMap == Caster.LocMap)
                    if (this != Caster)
                        if (MyMath.GetDistance(LocX, LocY, Caster.LocX, Caster.LocY) > 3 || !Caster.CastingPray)
                        {
                            if (Praying)
                            {
                                Flags.Praying = false;
                                Praying = false;
                                MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 29, LuckTime));
                            }
                        }
            }
            //------------------------
            //--Change The Map--
            foreach (Character Caster in World.PlayersPraying)
            {
                if (LocMap != Caster.LocMap)
                {
                    Flags.Praying = false;
                    Praying = false;
                    MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 29, LuckTime));
                }
            }
            //------------------------

            if (LocMap == 1038)
                if (DateTime.Now > LastGWList.AddMilliseconds(5000))
                {
                    LastGWList = DateTime.Now;
                    SendGuildWar();
                }
            if (Action == 250)
                if (Stamina < 100)
                {
                    Stamina += 8;
                    if (Stamina > 100)
                        Stamina = 100;
                    MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 9, Stamina));
                }
            if (Action == 100)
                if (Stamina < 100)
                {
                    if (Flying == false)
                        Stamina += 1;
                    if (Stamina > 100)
                        Stamina = 100;
                    MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 9, Stamina));
                }
            if (Action == 230)
            {
                if (AllSuper())
                {
                    if (Job <= 16 && Job >= 9)
                    {
                        World.ShowEffect(this, "warrior");
                        Action = 100;
                    }
                    if (Job <= 26 && Job >= 19)
                    {
                        World.ShowEffect(this, "fighter");
                        Action = 100;

                    }
                    if (Job <= 46 && Job >= 39)
                    {
                        World.ShowEffect(this, "archer");
                        Action = 100;
                    }
                    if (Job <= 146 && Job >= 100)
                    {
                        World.ShowEffect(this, "taoist");
                        Action = 100;
                    }
                }
                if (!AllSuper() && ArmorSuper())
                {
                    if (Job <= 16 && Job >= 9)
                    {
                        World.ShowEffect(this, "warrior-s");
                        Action = 100;
                    }
                    if (Job <= 26 && Job >= 19)
                    {
                        World.ShowEffect(this, "fighter-s");
                        Action = 100;

                    }
                    if (Job <= 46 && Job >= 39)
                    {
                        World.ShowEffect(this, "archer-s");
                        Action = 100;
                    }
                    if (Job <= 146 && Job >= 100)
                    {
                        World.ShowEffect(this, "taoist-s");
                        Action = 100;
                    }
                }
            }

            if (LocMap == 1505)
                if (World.JailOn == false)
                    Teleport(1002, 430, 380);

            if (Flying)
            {
                if (Equips[4] == null || Equips[4] == "0")
                {
                    if (Attacking)
                    {
                        Flags.Flying = false;
                        Flying = false;
                        FlyType = 0;
                        BanNB += 1;
                        World.SendMsgToAll(Name + " a �t� envoyez en prison pour avoir utilis� le bug de voler sans arc! Il/Elle a �t� banni " + BanNB + " fois. Attention si vous �tes banni trop souvent vous resterez en prison!", "SYSTEM", 2011);
                        Teleport(6001, 28, 71);
                        Save();
                    }
                }
                if (DateTime.Now > FlyActivated.AddSeconds(40 + FlyType * 20))
                {
                    Flags.Flying = false;
                    Flying = false;
                    FlyType = 0;
                }
            }

            if (DateTime.Now > LastSwing.AddMilliseconds(2500))
                SwingPickAxe();

            if (!CycloneOn)
            {
                if (AtkType == 2 || AtkType == 21)
                {
                    if (DateTime.Now > LastAttack.AddMilliseconds(1000))
                    {
                        if (Attacking)
                            Attack();
                    }
                }
                else if (Attacking)
                    Attack();
            }
            else if (PTarget != null && !PTarget.Flying || AtkType != 2 || PTarget == null)
                Attack();

            if (FirstJob == 25 && Job > 19 && Job < 26 || SecondJob == 25)
                ReflectOn = true;

            if (Alive && !Flags.XPList)
                if (DateTime.Now > LastXPC.AddMilliseconds(3000))
                    AddXPC();

            if (Alive && Flags.XPList)
                if (DateTime.Now > LastXPC.AddSeconds(20))
                    AddXPC();

            if (DateTime.Now > LastSave.AddMilliseconds(15000))
                Save();

            if (DateTime.Now > KOActivated.AddMilliseconds(ExtraXP))
                if (SMOn || CycloneOn)
                {
                    Flags.Cyclone = false;
                    Flags.SuperMan = false;
                    SMOn = false;
                    CycloneOn = false;

                    if (KO >= 100)
                    {
                        World.SendMsgToAll(Name + " a tu� " + KO + " monstres avec sont XPSkill!", "SYSTEM", 2000);
                        Save();
                        KO = 0;
                    }
                    else
                    {
                        if (KO >= 1)
                        {
                            MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous avez tu� " + KO + " monstres avec votre XPSkill!", 2005));
                            Save();
                            KO = 0;
                        }
                    }
                }

            if (DateTime.Now > GotBlueName.AddMilliseconds(25000))
                if (BlueName)
                    BlueName = false;

            if (DateTime.Now > LostPKP.AddMilliseconds(120000))
                if (PKPoints > 0)
                    PKTimer_Elapsed();
        }

        public Character()
        {
            TheTimer.Interval = 500;
            TheTimer.Elapsed += new ElapsedEventHandler(TimerElapsed);
            TheTimer.Start();
            LastAttack = DateTime.Now;
            LastXPC = DateTime.Now;
            LastSave = DateTime.Now;
            LostPKP = DateTime.Now;

            WHIDs[0] = new uint[21];
            WHIDs[1] = new uint[21];
            WHIDs[2] = new uint[21];
            WHIDs[3] = new uint[21];
            WHIDs[4] = new uint[21];
            WHIDs[5] = new uint[41];
            WHIDs[6] = new uint[21];

            Screen = new Screen(this);
            Flags = new StatusFlag(this);
        }

        ~Character()
        {
            MyClient = null;
            Screen = null;
            MyShop = null;
            MyGuild = null;
            Flags = null;
            MyTeamLeader = null;
            PTarget = null;
            TGTarget = null;
            MobTarget = null;

            Name = null;
            Spouse = null;

            Skills.Clear();
            Skills = null;
            Skill_Exps.Clear();
            Skill_Exps = null;
            Profs.Clear();
            Profs = null;
            Prof_Exps.Clear();
            Prof_Exps = null;
            Friends.Clear();
            Friends = null;
            Enemies.Clear();
            Enemies = null;
            Team.Clear();
            Team = null;
            MyTradeSide.Clear();
            MyTradeSide = null;

            Equips = null;
            Equips_UIDs = null;
            Inventory = null;
            Inventory_UIDs = null;
            WHIDs = null;
            TCWH = null;
            PCWH = null;
            BIWH = null;
            ACWH = null;
            DCWH = null;
            MAWH = null;
            AZWH = null;

            if (TheTimer != null)
            {
                TheTimer.Dispose();
                TheTimer = null;
            }
            if (Radio != null)
            {
                Radio.Dispose();
                Radio = null;
            }
        }

        public void PKTimer_Elapsed()
        {
            LostPKP = DateTime.Now;
            if (PKPoints > 0)
                PKPoints--;
        }

        public void RBStuff()
        {
            for (byte i = 1; i < 9; i++)
            {
                if (Equips[i] == null || Equips[i] == "") continue;
                string I = Equips[i];
                string[] II = I.Split('-');
                uint IID = uint.Parse(II[0]);
                byte Quality = (byte)ItemHandler.ItemQuality(IID);

                if (i == 1)
                {
                    string NewID = "";
                    if (Other.ItemInfo(IID).Id >= 117493 && Other.ItemInfo(IID).Id <= 117499)
                    {
                        NewID = II[0].Remove(4, 2);
                        NewID = 1173 + "0" + Quality.ToString();

                        Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                        II[0] = NewID;
                        MyClient.SendPacket(CoServer.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                    }
                    else
                    {
                        if (ItemHandler.WeaponType(IID) == 111 || ItemHandler.WeaponType(IID) == 113 || ItemHandler.WeaponType(IID) == 114 || ItemHandler.WeaponType(IID) == 118 || ItemHandler.WeaponType(IID) == 117)
                        {
                            NewID = II[0].Remove(4, 2);
                            NewID = NewID + "0" + Quality.ToString();

                            Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                            II[0] = NewID;
                            MyClient.SendPacket(CoServer.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                        }
                        else if (ItemHandler.WeaponType(IID) == 112)
                        {
                            byte Type = byte.Parse(II[0][4].ToString());
                            byte Color = byte.Parse(II[0][3].ToString());
                            NewID = "11" + Type.ToString() + Color.ToString() + "0" + Quality.ToString();
                            Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                            II[0] = NewID;
                            MyClient.SendPacket(CoServer.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                        }
                    }
                }
                else if (i == 2)
                {
                    string NewID = "";

                    NewID = II[0].Remove(3, 3);
                    NewID += "00" + Quality.ToString();
                    Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                    II[0] = NewID;
                    MyClient.SendPacket(CoServer.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                }
                else if (i == 3)
                {
                    string NewID = "";
                    if (ItemHandler.WeaponType(IID) == 130 || ItemHandler.WeaponType(IID) == 131 || ItemHandler.WeaponType(IID) == 133 || ItemHandler.WeaponType(IID) == 134)
                    {
                        NewID = II[0].Remove(4, 2);
                        NewID = NewID + "0" + Quality.ToString();

                        Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                        II[0] = NewID;
                        MyClient.SendPacket(CoServer.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                    }
                    else if (ItemHandler.WeaponType(IID) == 135 || ItemHandler.WeaponType(IID) == 136 || ItemHandler.WeaponType(IID) == 138 || ItemHandler.WeaponType(IID) == 139)
                    {
                        byte Type = byte.Parse(II[0][2].ToString());
                        byte Color = byte.Parse(II[0][3].ToString());
                        Type -= 5;
                        NewID = "13" + Type.ToString() + Color.ToString() + "0" + Quality.ToString();
                        Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                        II[0] = NewID;
                        MyClient.SendPacket(CoServer.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                    }
                }
                else if (i == 4)
                {
                    string NewID = "";

                    NewID = II[0].Remove(3, 3);
                    NewID += "02" + Quality.ToString();
                    Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                    II[0] = NewID;
                    MyClient.SendPacket(CoServer.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                }
                else if (i == 5)
                {
                    string NewID = "";

                    if (ItemHandler.WeaponType(IID) == 900)
                    {
                        NewID = II[0].Remove(4, 2);
                        NewID += "0" + Quality.ToString();
                        Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                        II[0] = NewID;
                        MyClient.SendPacket(CoServer.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                    }
                    else if (ItemHandler.ItemType(IID) == 4 || ItemHandler.ItemType(IID) == 5)
                    {
                        NewID = II[0].Remove(3, 3);
                        NewID += "02" + Quality.ToString();
                        Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                        II[0] = NewID;
                        MyClient.SendPacket(CoServer.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                    }
                }
                else if (i == 6)
                {
                    string NewID = "";

                    NewID = II[0].Remove(3, 3);
                    NewID += "01" + Quality.ToString();
                    Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                    II[0] = NewID;
                    MyClient.SendPacket(CoServer.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                }
                else if (i == 8)
                {
                    string NewID = "";

                    NewID = II[0].Remove(3, 3);
                    NewID += "01" + Quality.ToString();
                    Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                    II[0] = NewID;
                    MyClient.SendPacket(CoServer.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                }

            }
        }

        public void ReBorn(byte ToJob)
        {
            RBCount++;
            if (RBCount == 1)
            {
                FirstLevel = Level;
                FirstJob = Job;
                Save();
            }
            if (RBCount == 2)
            {
                SecondLevel = Level;
                SecondJob = Job;
                Save();
            }

            try
            {
                #region RbStats
                StatP = 0;
                if ((Level == 112 || Level == 113) && Job == 135)
                    StatP += 1;
                else if ((Level == 114) || (Level == 115) && Job == 135)
                    StatP += 3;
                else if ((Level == 116 || Level == 117) && Job == 135)
                    StatP += 6;
                else if ((Level == 118 || Level == 119) && Job == 135)
                    StatP += 10;
                else if (Level == 120)
                {
                    if (Job == 135)
                        StatP += 15;
                }
                else if (Level == 121)
                {
                    if (Job == 135)
                        StatP += 15;
                    else
                        StatP += 1;
                }
                else if (Level == 122)
                {
                    if (Job == 135)
                        StatP += 21;
                    else
                        StatP += 3;
                }
                else if (Level == 123)
                {
                    if (Job == 135)
                        StatP += 21;
                    else
                        StatP += 6;
                }
                else if (Level == 124)
                {
                    if (Job == 135)
                        StatP += 28;
                    else
                        StatP += 10;
                }
                else if (Level == 125)
                {
                    if (Job == 135)
                        StatP += 28;
                    else
                        StatP += 15;
                }
                else if (Level == 126)
                {
                    if (Job == 135)
                        StatP += 36;
                    else
                        StatP += 21;
                }
                else if (Level == 127)
                {
                    if (Job == 135)
                        StatP += 36;
                    else
                        StatP += 28;
                }
                else if (Level == 128)
                {
                    if (Job == 135)
                        StatP += 45;
                    else
                        StatP += 36;
                }
                else if (Level == 129)
                    StatP += 45;
                else if (Level >= 130)
                    StatP += (uint)(((Level - 130) * 5) + 55);
                #endregion
                #region FirstRbStats
                if (RBCount == 2)
                {
                    if ((FirstLevel == 112 || FirstLevel == 113) && FirstJob == 135)
                        StatP += 1;
                    else if ((FirstLevel == 114 || FirstLevel == 115) && FirstJob == 135)
                        StatP += 3;
                    else if ((FirstLevel == 116 || FirstLevel == 117) && FirstJob == 135)
                        StatP += 6;
                    else if ((FirstLevel == 118 || FirstLevel == 119) && FirstJob == 135)
                        StatP += 10;
                    else if (FirstLevel == 120)
                    {
                        if (FirstJob == 135)
                            StatP += 15;
                    }
                    else if (FirstLevel == 121)
                    {
                        if (FirstJob == 135)
                            StatP += 15;
                        else
                            StatP += 1;
                    }
                    else if (FirstLevel == 122)
                    {
                        if (FirstJob == 135)
                            StatP += 21;
                        else
                            StatP += 3;
                    }
                    else if (FirstLevel == 123)
                    {
                        if (FirstJob == 135)
                            StatP += 21;
                        else
                            StatP += 6;
                    }
                    else if (FirstLevel == 124)
                    {
                        if (FirstJob == 135)
                            StatP += 28;
                        else
                            StatP += 10;
                    }
                    else if (FirstLevel == 125)
                    {
                        if (FirstJob == 135)
                            StatP += 28;
                        else
                            StatP += 15;
                    }
                    else if (FirstLevel == 126)
                    {
                        if (FirstJob == 135)
                            StatP += 36;
                        else
                            StatP += 21;
                    }
                    else if (FirstLevel == 127)
                    {
                        if (FirstJob == 135)
                            StatP += 36;
                        else
                            StatP += 28;
                    }
                    else if (FirstLevel == 128)
                    {
                        if (FirstJob == 135)
                            StatP += 45;
                        else
                            StatP += 36;
                    }
                    else if (FirstLevel == 129)
                        StatP += 45;
                    else if (FirstLevel >= 130)
                        StatP += (uint)(((FirstLevel - 130) * 5) + 55);
                }
                #endregion

                StatP += 30;
                StatP += 42;

                Level = 15;
                Exp = 0;
                foreach (DictionaryEntry DE in Skills)
                {
                    short SkillId = (short)DE.Key;
                    MyClient.SendPacket(CoServer.MyPackets.GeneralData(this, (ushort)SkillId, 109)); //108 -> Prof
                }
                Skills.Clear();
                Skill_Exps.Clear();

                #region Skills
                if (RBCount == 1)
                {
                    if (Job == 25)//Warrior
                    {
                        FirstJob = 25;
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(4000, 1);//SummonGuard
                        }
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(1015, 0);//Accuracy
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(1320, 0);//FlyingMoon
                            LearnSkill(4000, 1);//SummonGuard
                        }
                        if (ToJob == 21)//Warrior
                        {
                            //LearnSkill(3060, 1);//Reflect
                            LearnSkill(4000, 1);//SummonGuard
                        }
                        if (ToJob == 142)//FireTaoist
                        {
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(4000, 1);//SummonGuard
                        }
                        if (ToJob == 132)//WaterTaoist
                        {
                            LearnSkill(1025, 0);//Superman
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(4000, 1);//SummonGuard
                        }
                    }
                    if (Job == 15)//Trojan
                    {
                        FirstJob = 15;
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(1190, 1);//SpiritualHealing
                            LearnSkill(4000, 1);//SummonGuard
                        }
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(3050, 1);//CruelShade
                            LearnSkill(4000, 1);//SummonGuard
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(1190, 1);//SpiritualHealing
                            LearnSkill(5100, 0);//IronShirt
                            LearnSkill(4000, 1);//SummonGuard
                        }
                        if (ToJob == 142)//FireTaoist
                        {
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(1190, 1);//SpiritualHealing
                            LearnSkill(1270, 1);//Robot
                            LearnSkill(4000, 1);//SummonGuard
                        }
                        if (ToJob == 132)//WaterTaoist
                        {
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(1190, 1);//SpiritualHealing
                            LearnSkill(1270, 1);//Robot
                            LearnSkill(4000, 1);//SummonGuard
                        }
                    }
                    if (Job == 45)//Archer
                    {
                        FirstJob = 45;
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(5000, 0);//FreezingArrow
                            LearnSkill(4000, 1);//SummonGuard
                        }
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(4000, 1);//SummonGuard
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(4000, 1);//SummonGuard
                        }
                        if (ToJob == 142)//FireTaoist
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(4000, 1);//SummonGuard
                        }
                        if (ToJob == 132)//WaterTaoist
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(4000, 1);//SummonGuard
                        }
                    }
                    if (Job == 145)//FireTaoist
                    {
                        FirstJob = 145;
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(4000, 1);//SummonGuard
                        }
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(4000, 1);//SummonGuard
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(4000, 1);//SummonGuard
                        }
                        if (ToJob == 142)//FireTaoist
                        {
                            LearnSkill(3080, 1);//Dodge
                            LearnSkill(4000, 1);//SummonGuard
                        }
                        if (ToJob == 132)//WaterTaoist
                        {
                            LearnSkill(1120, 1);//FireCircle
                            LearnSkill(4000, 1);//SummonGuard
                        }
                    }
                    if (Job == 135)//WaterTaoist
                    {
                        FirstJob = 135;
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1075, 1);//Invisibility
                            LearnSkill(1090, 1);//Magic Sheild
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(1350, 1);//DivineHare
                        }
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1085, 1);//Star of Accuracy
                            LearnSkill(1090, 1);//Magic Sheild
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(1350, 1);//DivineHare
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1085, 1);//Star of Accuracy
                            LearnSkill(1090, 1);//Magic Sheild
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(1350, 1);//DivineHare
                        }
                        if (ToJob == 142)//FireTaoist
                        {
                            LearnSkill(1175, 1);//Adv. Cure
                            LearnSkill(1075, 1);//Invisibility
                            LearnSkill(1050, 0);//Revive
                            LearnSkill(1055, 1);//HealingRain
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(1350, 1);//DivineHare
                        }
                        if (ToJob == 132)//WaterTaoist
                        {
                            LearnSkill(3090, 1);//Pervade
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(1350, 1);//DivineHare
                        }
                    }
                }
                #endregion
                #region Skills
                if (RBCount == 2 || RBCount == 3)
                {
                    if (FirstJob == 15)//Trojan
                    {
                        if (Job == 15)//Trojan
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(3050, 1);//CruelShade
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1110, 0);//Cyclone		
                                LearnSkill(3050, 1);//CruelShade
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(5100, 0);//IronShirt
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(3050, 1);//CruelShade
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTaoist
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(3050, 1);//CruelShade
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTaoist
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(3050, 1);//CruelShade
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 25)//Warrior
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1110, 0);//Cyclone		
                                LearnSkill(5100, 0);//IronShirt
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1015, 0);//Accuracy
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1320, 1);//FlyingMoon
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1110, 0);//Cyclone		
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(5100, 0);//IronShirt
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTaoist
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(5100, 0);//IronShirt
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTaoist
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(5100, 0);//IronShirt
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 45)//Archer
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(5000, 0);//FreezingArrow
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTaoist
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTaoist
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 135)//WaterTaoist
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarofAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarofAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTaoist
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(3090, 1);//Pervade	
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTaoist
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1050, 0);//Revive
                                LearnSkill(1175, 1);//AdvancedCure
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1170, 1);//Nectar
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 145)//FireTaoist
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTaoist
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1120, 1);//FireCircle
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTaoist
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(3080, 0);//Dodge	
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                    }
                    if (FirstJob == 25)//Warrior
                    {
                        if (Job == 15)//Trojan
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1320, 0);//Flying Moon
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(3050, 1);//CruelShade
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(5100, 0);//IronShirt
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1320, 0);//FlyingMoon
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTao
                            {
                                LearnSkill(1320, 0);//FlyingMoon
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTao
                            {
                                LearnSkill(1320, 0);//FlyingMoon
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 25)//Warrior
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1015, 0);//Accuracy
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1320, 0);//FlyingMoon
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTao
                            {
                                LearnSkill(1025, 0);//SuperMan
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTao
                            {
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 45)//Archer
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(5000, 0);//FreezingArrow
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTao
                            {
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(5000, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTao
                            {
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(5000, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 135)//WaterTao
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarOfAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//M�ditation
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1280, 1);//WaterElf
                                LearnSkill(1350, 1);//DivineHare
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarOfAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//M�ditation
                                LearnSkill(1280, 1);//WaterElf
                                LearnSkill(1350, 1);//DivineHare
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//M�ditation
                                LearnSkill(1280, 1);//WaterElf
                                LearnSkill(1350, 1);//DivineHare
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTao
                            {
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1050, 0);//XPRevive
                                LearnSkill(1175, 1);//AdvancedCure
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1055, 1);//HealingRain
                                LearnSkill(1280, 1);//WaterElf
                                LearnSkill(1350, 1);//DivineHare
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTao
                            {
                                LearnSkill(1025, 0);//SuperMan
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1280, 1);//WaterElf
                                LearnSkill(1350, 1);//DivineHare
                                LearnSkill(3090, 1);//Pervade
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 145)//FireTao
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//M�ditation
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//M�ditation
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//M�ditation
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTao
                            {
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(3080, 0);//Dodge
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTao
                            {
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1120, 1);//FireCicle
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                    }
                    if (FirstJob == 45)//Archer
                    {
                        if (Job == 15)//Trojan
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(3050, 1);//CruelShade
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(5100, 0);//IronShirt
                                LearnSkill(1270, 1);//Robot			
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot			
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTaoist
                            {
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot			
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTaoist
                            {
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot			
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 25)//Warrior
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(1015, 0);//Accuracy
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1320, 1);//FlyingMoon			
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(5002, 0);//Poison		
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1040, 0);//Roar		
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTaoist
                            {
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(1025, 0);//Superman			
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1040, 0);//Roar		
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTaoist
                            {
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1040, 0);//Roar		
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 45)//Archer
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(5000, 0);//FreezingArrow			
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(5000, 0);//FreezingArrow			
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(5000, 0);//FreezingArrow			
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTaoist
                            {
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(5000, 0);//FreezingArrow			
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTaoist
                            {
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(5000, 0);//FreezingArrow			
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 135)//WaterTaoist
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarofAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(1280, 1);//WaterElf			
                                LearnSkill(1350, 1);//DivineHare		
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarofAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(1280, 1);//WaterElf			
                                LearnSkill(1350, 1);//DivineHare		
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(1280, 1);//WaterElf			
                                LearnSkill(1350, 1);//DivineHare		
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTaoist
                            {
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(1280, 1);//WaterElf			
                                LearnSkill(1350, 1);//DivineHare
                                LearnSkill(3090, 1);//Pervade			
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTaoist
                            {
                                LearnSkill(1050, 0);//Revive
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(1055, 1);//HealingRain			
                                LearnSkill(1175, 1);//AdvancedCure			
                                LearnSkill(1280, 1);//WaterElf			
                                LearnSkill(1350, 1);//DivineHare			
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 145)//FireTaoist
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1005, 1);//Cure	
                                LearnSkill(1195, 1);//Meditation	
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(1000, 1);//Thunder	
                                LearnSkill(1001, 1);//Fire			
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1005, 1);//Cure	
                                LearnSkill(1195, 1);//Meditation	
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(1000, 1);//Thunder	
                                LearnSkill(1001, 1);//Fire			
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1005, 1);//Cure	
                                LearnSkill(1195, 1);//Meditation	
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(1000, 1);//Thunder	
                                LearnSkill(1001, 1);//Fire			
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTaoist
                            {
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(1120, 1);//FireCircle		
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTaoist
                            {
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(3080, 0);//Dodge		
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                    }
                    if (FirstJob == 135)//WaterTao
                    {

                        if (Job == 15)//Trojan
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarOfAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditadtion
                                LearnSkill(3050, 1);//CruelShade
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarOfAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditadtion
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(5100, 0);//IronShirt
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarOfAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditadtion
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTao
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarOfAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditadtion
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTao
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarOfAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditadtion
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 25)//Warrior
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarOfAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditadtion
                                LearnSkill(1015, 0);//Accuracy
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1320, 1);//FlyingMoon
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarOfAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditadtion
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarOfAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditadtion
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTao
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarOfAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditadtion
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTao
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarOfAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditadtion
                                LearnSkill(1025, 0);//Superman
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 45)//Archer
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(5000, 0);//FreezingArrow
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTao
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTao
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 135)//WaterTao
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarOfAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditadtion
                                LearnSkill(3090, 1);//Pervade
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarOfAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditadtion
                                LearnSkill(3090, 1);//Pervade
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarOfAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditadtion
                                LearnSkill(3090, 1);//Pervade
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTao
                            {
                                LearnSkill(1050, 0);//Revive
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1055, 1);//HealingRain
                                LearnSkill(1175, 1);//AdvancedCure
                                LearnSkill(3090, 1);//Pervade
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTao
                            {
                                LearnSkill(3090, 1);//Pervade
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 145)//FireTao
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1050, 0);//Revive
                                LearnSkill(1175, 1);//AdvancedCure
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1055, 1);//HealingRain
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1050, 0);//Revive
                                LearnSkill(1175, 1);//AdvancedCure
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1055, 1);//HealingRain
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1050, 0);//Revive
                                LearnSkill(1175, 1);//AdvancedCure
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1055, 1);//HealingRain
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTao
                            {
                                LearnSkill(1050, 0);//Revive
                                LearnSkill(1175, 1);//AdvancedCure
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1055, 1);//HealingRain
                                LearnSkill(3080, 1);//Dodge
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTao
                            {
                                LearnSkill(1050, 0);//Revive
                                LearnSkill(1175, 1);//AdvancedCure
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1055, 1);//HealingRain
                                LearnSkill(1120, 1);//FireCircle
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                    }
                    if (FirstJob == 145)//Fire
                    {
                        if (Job == 15)//Trojan
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(3050, 1);//CrualShade
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1110, 0);//Cyclone		
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(5100, 0);//IronShirt
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTaoist
                            {
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTaoist
                            {
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 25)//Warrior
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(1015, 0);//Accuracy
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1320, 1);//FlyingMoon
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTaoist
                            {
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(1025, 0);//Superman
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTaoist
                            {
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 45)//Archer
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(5000, 0);//FreezingArrow
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTaoist
                            {
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTaoist
                            {
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 135)//WaterTaoist
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(1085, 1);//StarOfAccuracy
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(1085, 1);//StarOfAccuracy
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTaoist
                            {
                                LearnSkill(1120, 1);//FireCircle
                                LearnSkill(3090, 1);//Pervade
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTaoist
                            {
                                LearnSkill(1050, 0);//Revivre
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1055, 1);//HealingRain
                                LearnSkill(1175, 1);//AdvancedCure
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 145)//FireTaoist
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(3080, 1);//Dodge
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(3080, 1);//Dodge
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(3080, 1);//Dodge
                                LearnSkill(1000, 1);//Thunder
                                LearnSkill(1001, 1);//Fire
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTaoist
                            {
                                LearnSkill(3080, 1);//Dodge
                                LearnSkill(1120, 1);//FireCircle
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTaoist
                            {
                                LearnSkill(3080, 1);//Dodge
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                    }
                }
                #endregion

                Job = ToJob;

                if (Job == 11)
                {
                    Str = 5;
                    Agi = 2;
                    Vit = 3;
                    Spi = 0;
                }
                if (Job == 21)
                {
                    Str = 5;
                    Agi = 2;
                    Vit = 3;
                    Spi = 0;
                }
                if (Job == 41)
                {
                    Str = 2;
                    Agi = 7;
                    Vit = 1;
                    Spi = 0;
                }
                if (Job == 132)
                {
                    Str = 0;
                    Agi = 2;
                    Vit = 3;
                    Spi = 5;
                }
                if (Job == 142)
                {
                    Str = 0;
                    Agi = 2;
                    Vit = 3;
                    Spi = 5;
                }
                MinAtk = Str;
                MaxAtk = Str;
                Calculation.SetMaxHP(this, false);
                RBStuff();
                CurHP = MaxHP;
                if (RBCount == 1)
                    AddItem("720028-0-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));

                if (RBCount == 1)
                    World.SendMsgToAll(Name + " a complet� la premi�re renaissance!", "SYSTEM", 2011);
                if (RBCount == 2)
                    World.SendMsgToAll(Name + " a complet� la deuxi�me renaissance!", "SYSTEM", 2011);
                Save();
                //MyClient.Drop();
            }
            catch (Exception Exc) { Console.WriteLine(Exc); }
        }

        public bool TeamAdd(Character Player)
        {
            if (PlayersInTeam < 6)
            {
                Player.MyTeamLeader = this;
                Player.MyClient.SendPacket(CoServer.MyPackets.PlayerJoinsTeam(this));
                for (int i = 0; i < Team.Count; i++)
                {
                    Character Member = (Character)Team[i];
                    if (Member != null)
                    {
                        Member.MyClient.SendPacket(CoServer.MyPackets.PlayerJoinsTeam(Player));
                        Member.Team.Add(Player);

                        Player.MyClient.SendPacket(CoServer.MyPackets.PlayerJoinsTeam(Member));
                    }
                }
                Team.Add(Player);
                Player.Team = Team;
                MyClient.SendPacket(CoServer.MyPackets.PlayerJoinsTeam(Player));
                Player.MyClient.SendPacket(CoServer.MyPackets.PlayerJoinsTeam(Player));
                PlayersInTeam++;
                return true;
            }
            else
                return false;
        }

        public void TeamRemove(Character Player, bool Kick)
        {
            Player.MyTeamLeader = null;
            if (Kick)
                Player.MyClient.SendPacket(CoServer.MyPackets.TeamPacket(Player.UID, 7));
            else
                Player.MyClient.SendPacket(CoServer.MyPackets.TeamPacket(Player.UID, 2));

            if (Kick)
                MyClient.SendPacket(CoServer.MyPackets.TeamPacket(Player.UID, 7));
            else
                MyClient.SendPacket(CoServer.MyPackets.TeamPacket(Player.UID, 2));

            Team.Remove(Player);

            for (int i = 0; i < Team.Count; i++)
            {
                Character Member = (Character)Team[i];
                if (Member != null)
                {
                    if (Kick)
                        Member.MyClient.SendPacket(CoServer.MyPackets.TeamPacket(Player.UID, 7));
                    else
                        Member.MyClient.SendPacket(CoServer.MyPackets.TeamPacket(Player.UID, 2));

                    Member.Team.Remove(Player);

                    Player.MyClient.SendPacket(CoServer.MyPackets.TeamPacket(Member.UID, 2));
                }
            }
            Player.Team = new ArrayList(4);
            PlayersInTeam--;
        }

        public void TeamDismiss()
        {
            TeamLeader = false;
            MyTeamLeader = null;
            PlayersInTeam = 0;
            Flags.TeamLeader = false;

            for (int i = 0; i < Team.Count; i++)
            {
                Character Member = (Character)Team[i];
                Member.MyTeamLeader = null;
                Member.Team = new ArrayList(6);
                Member.MyClient.SendPacket(CoServer.MyPackets.TeamPacket(UID, 6));
                Member.MyClient.SendPacket(CoServer.MyPackets.TeamPacket(Member.UID, 6));
            }
            MyClient.SendPacket(CoServer.MyPackets.TeamPacket(UID, 6));
        }


        public string FindItem(uint ItemUID)
        {
            int Count = 0;
            foreach (uint item in Inventory_UIDs)
            {
                if (item == ItemUID)
                    return Inventory[Count];


                Count++;
            }
            return null;
        }

        public string FindWHItem(uint ItemUID, byte WH)
        {
            int Count = 0;
            foreach (uint item in WHIDs[WH])
            {
                if (item == ItemUID)
                {
                    if (WH == 0)
                        return TCWH[Count];
                    else if (WH == 1)
                        return PCWH[Count];
                    else if (WH == 2)
                        return ACWH[Count];
                    else if (WH == 3)
                        return DCWH[Count];
                    else if (WH == 4)
                        return BIWH[Count];
                    else if (WH == 5)
                        return MAWH[Count];
                    else if (WH == 6)
                        return AZWH[Count];
                    else
                        return null;
                }
                Count++;
            }
            return null;
        }

        public void AddXPC() { XpCircle++; }

        public void Revive(bool Tele)
        {
            if (Alive)
                return;

            Stamina = 100;
            CurHP = MaxHP;

            Flags.RemoveFlags();
            XpCircle = 0;

            MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 12, ulong.Parse(Avatar.ToString() + Model.ToString())));
            MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 9, Stamina));

            if (Tele)
            {
                ushort RbMap = 0;
                foreach (Map Map in DataBase.AllMaps.Values)
                {
                    if (Map.MapId == LocMap)
                    {
                        RbMap = Map.RbMap;
                        break;
                    }
                }
                foreach (Map Map in DataBase.AllMaps.Values)
                {
                    if (Map.MapId == RbMap)
                    {
                        Teleport(Map.MapId, Map.PortalX, Map.PortalY);
                        break;
                    }
                }
            }
            else
                Screen.SendPacket(CoServer.MyPackets.Vital(UID, 12, ulong.Parse(Avatar.ToString() + Model.ToString())), false);
            Alive = true;
        }

        public void Die()
        {
            try
            {
                Flags.RemoveFlags();
                Flags.Dead = true;
                Flags.Frozen = true;
                XpCircle = 0;

                if (MyClient.Status < 7)
                {
                    if (Level > 15 && !Other.CanPK(LocMap))
                    {
                        //------ Items Drop -------
                        if (ItemsInInventory > 0)
                        {
                            byte MaxC = 4;
                            if (ItemsInInventory < 4)
                                MaxC = ItemsInInventory;
                            byte ItemC = (byte)Program.Rand.Next(1, MaxC);
                            for (byte Count = 0; Count < ItemC; Count++)
                            {
                                byte Pos = (byte)Program.Rand.Next(ItemsInInventory);
                                string Itemtype = Inventory[Pos];
                                uint ItemUID = Inventory_UIDs[Pos];

                                if (Itemtype != "" && Itemtype != "0" && ItemUID != 0)
                                {
                                    short[] Location = Other.GetCoords(LocMap, LocX, LocY);

                                    DroppedItem TheItem = DroppedItems.DropItem(0, Itemtype, (uint)Location[0], (uint)Location[1], (uint)LocMap, 0);
                                    World.ItemDrops(TheItem);

                                    RemoveItem(ItemUID);
                                }
                            }
                        }
                        //------ Money Drop -------
                        int Money = (int)Silvers / 10;
                        if (Money < 0)
                            Money = 0;

                        uint MoneyDrop = (uint)Program.Rand.Next(0, Money);

                        if (MoneyDrop > 0)
                        {
                            string Item = "";

                            if (MoneyDrop <= 10) //Silver
                                Item = "1090000-0-0-0-0-0";
                            else if (MoneyDrop <= 100) //Sycee
                                Item = "1090010-0-0-0-0-0";
                            else if (MoneyDrop <= 1000) //Gold
                                Item = "1090020-0-0-0-0-0";
                            else if (MoneyDrop <= 2000) //GoldBullion
                                Item = "1091000-0-0-0-0-0";
                            else if (MoneyDrop <= 5000) //GoldBar
                                Item = "1091010-0-0-0-0-0";
                            else if (MoneyDrop > 5000) //GoldBars
                                Item = "1091020-0-0-0-0-0";
                            else //Error
                                Item = "1090000-0-0-0-0-0";

                            short[] Location = Other.GetCoords(LocMap, LocX, LocY);
                            DroppedItem item = DroppedItems.DropItem(0, Item, (uint)Location[0], (uint)Location[1], (uint)LocMap, MoneyDrop);
                            World.ItemDrops(item);

                            Silvers -= MoneyDrop;
                        }
                    }

                    if (PKPoints >= 30 && !Other.CanPK(LocMap))
                    {
                        if (Other.ChanceSuccess(50))
                        {
                            byte Pos = (byte)Program.Rand.Next(0, 9);
                            if (Equips[Pos] == null || Equips[Pos] == "0")
                                return;

                            uint ItemId = Equips_UIDs[Pos];
                            UnEquip(Pos);

                            int Count = 0;
                            foreach (uint TheUID in Inventory_UIDs)
                            {
                                if (TheUID == ItemId)
                                {
                                    string TheItem = Inventory[Count];
                                    short[] Location = Other.GetCoords(LocMap, LocX, LocY);

                                    DroppedItem Item = DroppedItems.DropItem(0, TheItem, (uint)Location[0], (uint)Location[1], (uint)LocMap, 0);
                                    World.ItemDrops(Item);
                                    RemoveItem(ItemId);
                                }
                                Count++;
                            }
                        }
                    }
                }

                Screen.SendPacket(CoServer.MyPackets.Disguise(UID, GhostMesh(), 0, 0), true);
                //MyClient.SendPacket(CoServer.MyPackets.Death(this));
                //World.UpdateSpawn(this);

                if (!Other.CanPK(LocMap))
                {
                    if (PKPoints > 100)
                    {
                        Teleport(6000, 028, 071);
                        World.SendMsgToAll(Name + " a �t� envoy� en prison!", "SYSTEM", 2008);
                    }
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        private uint GhostMesh()
        {
            byte EMesh = 0;

            if (Model == 1003 || Model == 1004)
                EMesh = 98;
            else
                EMesh = 99;

            return (uint)((EMesh * 0x989680) + (Avatar * 0x2710) + Model);
        }

        public bool GetHitDie(uint Damage)
        {
            if (Action == 250)
            {
                if (Stamina >= 35)                
                    Stamina -= 25;                
                else
                    Stamina = 0;
                MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 9, Stamina));
            }
            Action = 100;
            if (Damage >= CurHP)
            {
                CurHP = 0;
                Alive = false;

                return true;
            }
            else
            {
                CurHP -= (ushort)Damage;
                return false;
            }
        }

        public void GetReflect(uint Damage)
        {
            World.PVP(this, this, 2, Damage);
            if (Damage >= CurHP)
            {
                CurHP = 0;
                Alive = false;
            }
            else
                CurHP -= (ushort)Damage;
        }

        public void XPEnd()
        {
            Flags.Cyclone = false;
            Flags.SuperMan = false;
            CycloneOn = false;
            SMOn = false;
            //AccuracyOn = false;
        }

        public void UseSkill(ushort SkillId, ushort X, ushort Y, uint TargID)
        {
            if (!Alive)
                return;
            Ready = false;
            if (Skills.Contains((short)SkillId))
                if (DataBase.SkillsDone.ContainsKey(SkillId))
                {
                    Action = 100;
                    byte SkillLvl = (byte)Skills[(short)SkillId];
                    Hashtable MobTargets = new Hashtable();
                    Hashtable PlayerTargets = new Hashtable();
                    Hashtable NPCTargets = new Hashtable();

                    ushort[] SkillAttributes = DataBase.SkillAttributes[SkillId][SkillLvl];
                    #region SkillAttributes8
                    if (SkillAttributes[0] == 8)
                    {
                        Character Target = (Character)World.AllChars[TargID];

                        if (Target == null)
                            return;

                        if (SkillId == 1095 && CurMP >= SkillAttributes[4]) //Stig
                        {
                            if (Shield || MShieldBuff)
                                return;

                            if (LocMap != 1039)
                                CurMP -= (ushort)SkillAttributes[4];
                            Target.Flags.Stigged = true;
                            Target.StigBuff = true;
                            Target.Stigged = DateTime.Now;
                            Target.StigLevel = SkillLvl;
                            Target.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Target.MyClient.MessageId, "System", Target.Name, "Stig activ�: +" + (10 + StigLevel * 5) + " % attaque de plus durant " + (20 + StigLevel * 5) + " secondes.", 2005));
                        }

                        if (SkillId == 1085 && CurMP >= SkillAttributes[4]) //StarofAccuracy
                        {
                            if (Shield || MShieldBuff)
                                return;

                            if (LocMap != 1039)
                                CurMP -= (ushort)SkillAttributes[4];
                            Target.Flags.Accuracy = true;
                            Target.AccuracyBuff = true;
                            Target.AccuracyActivated = DateTime.Now;
                            Target.AccuLevel = SkillLvl;
                            Target.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Target.MyClient.MessageId, "System", Target.Name, "�toile exacte activ�: +" + (10 + AccuLevel * 5) + " % dext�rit� de plus durant " + (20 + AccuLevel * 5) + " secondes.", 2005));
                        }

                        if (SkillId == 1090 && CurMP >= SkillAttributes[4]) //MShield
                        {
                            if (Shield || MShieldBuff)
                                return;

                            if (LocMap != 1039)
                                CurMP -= (ushort)SkillAttributes[4];
                            Target.Flags.Shielded = true;
                            Target.MShieldBuff = true;
                            Target.MShielded = DateTime.Now;
                            Target.MShieldLevel = SkillLvl;
                            Target.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Target.MyClient.MessageId, "System", Target.Name, "Bouclier Magique activ�: +" + (10 + MShieldLevel * 5) + " % d�fense durant " + (20 + MShieldLevel * 5) + " secondes.", 2005));
                        }

                        if (SkillId == 3080 && CurMP >= SkillAttributes[4]) //Dodge
                        {
                            if (Shield || MShieldBuff)
                                return;

                            if (LocMap != 1039)
                                CurMP -= (ushort)SkillAttributes[4];
                            Target.DodgeBuff = true;
                            Target.Dodged = DateTime.Now;
                            Target.DodgeLevel = SkillLvl;
                            Target.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Target.MyClient.MessageId, "System", Target.Name, "Dodge activ�: +" + (20 + DodgeLevel * 5) + " % esquive de plus durant " + (20 + DodgeLevel * 5 * 2) + " secondes.", 2005));

                        }

                        if (SkillId == 1075 && CurMP >= SkillAttributes[4]) //Invisibility
                        {
                            if (Shield || MShieldBuff)
                                return;

                            if (LocMap != 1039)
                                CurMP -= (ushort)SkillAttributes[4];
                            Target.Flags.Invisible = true;
                            Target.InvisibleBuff = true;
                            Target.Invisibility = DateTime.Now;
                            Target.InvLevel = SkillLvl;
                            Target.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Target.MyClient.MessageId, "System", Target.Name, "Invisibilit� activ�: Vous �tes invisible durant " + (20 + InvLevel * 5) + " secondes.", 2005));
                        }

                        if (SkillId == 1100 && CurMP >= SkillAttributes[4]) //Pri�re
                        {
                            if (!Other.NoRez(LocMap))
                            {
                                if (!Target.Alive)
                                {
                                    if (LocMap != 1039)
                                        CurMP -= (ushort)SkillAttributes[4];
                                    Target.Revive(false);
                                }
                            }
                        }

                        if (SkillId == 1050) //Revive
                        {
                            if (!Other.NoRez(LocMap))
                            {
                                if (!Target.Alive)
                                {
                                    Target.Revive(false);
                                    XpCircle = 0;
                                }
                            }
                        }

                        if (SkillId == 5000)
                        {
                            Target.MAtk = 1;
                            Target.MinAtk = 1;
                            Target.MaxAtk = 1;
                            Target.FreezeBuff = true;
                            Target.Freezed = DateTime.Now;
                        }

                        World.UsingSkill(this, (short)SkillId, SkillLvl, TargID, 0, (short)Target.LocX, (short)Target.LocY);
                    }
                    #endregion
                    #region SkillAttributes7
                    if (SkillAttributes[0] == 7)
                    {
                        if (SkillId == 8003 && Stamina >= 100) //Fly avanc�
                        {
                            if (Shield || MShieldBuff)
                                return;

                            Flying = true;
                            Flags.Flying = true;

                            if (SkillLvl == 0)
                                FlyType = 0;
                            else if (SkillLvl == 1)
                                FlyType = 1;

                            if (LocMap != 1039)
                                Stamina = 0;

                            FlyActivated = DateTime.Now;

                            MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 9, Stamina));
                        }
                        if (SkillId == 8002) //XP fly
                        {
                            if (Shield || MShieldBuff)
                                return;

                            Flying = true;
                            Flags.Flying = true;
                            FlyType = 0;
                            XpCircle = 0;

                            FlyActivated = DateTime.Now;
                        }
                        if (SkillId == 1040) //Roar
                        {
                            foreach (Character Member in Team)
                            {
                                if (Member.Name != Name)
                                    if (Member.LocMap == LocMap)
                                        if (MyMath.CanSee(LocX, LocY, Member.LocX, Member.LocY))
                                        {
                                            Member.XpCircle += 20;
                                            Member.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Member.MyClient.MessageId, "SYSTEM", Member.Name, "Vous gagnez 20 points par le hurlement de votre �quipier " + Name + "!", 2005));
                                            PlayerTargets.Add(Member, 0);
                                        }
                            }
                            if (MyTeamLeader != null)
                            {
                                if (MyTeamLeader.Name != Name)
                                    if (MyTeamLeader.LocMap == LocMap)
                                        if (MyMath.CanSee(LocX, LocY, MyTeamLeader.LocX, MyTeamLeader.LocY))
                                        {
                                            MyTeamLeader.XpCircle += 20;
                                            MyTeamLeader.MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyTeamLeader.MyClient.MessageId, "SYSTEM", MyTeamLeader.Name, "Vous gagnez 20 points par le hurlement de votre �quipier " + Name + "!", 2005));
                                            PlayerTargets.Add(MyTeamLeader, 0);
                                        }
                            }
                            XpCircle = 0;
                        }

                        if (SkillId == 3321)
                        {
                            CurHP = 65535;
                            MobModel = uint.Parse("223" + Avatar.ToString() + Model.ToString());
                            Transform = true;
                            GMTransform = true;
                            GMTransformed = DateTime.Now;
                            Screen.SendPacket(CoServer.MyPackets.Disguise(UID, MobModel, 65535, 65535), true);
                        }

                        if (SkillId == 9876 && Stamina >= 100)//Bless
                        {
                            if (!CastingPray && !Praying)
                            {
                                if (!Mining)
                                {
                                    PrayCasted = DateTime.Now;
                                    CastingPray = true;
                                    Flags.CastingPray = true;

                                    if (LocMap != 1039)
                                        Stamina = 0;

                                    PrayX = LocX;
                                    PrayY = LocY;

                                    if (World.PlayersPraying.Contains(this))
                                    {
                                        World.PlayersPraying.Remove(this);
                                        World.PlayersPraying.Add(this);
                                    }
                                    else
                                        World.PlayersPraying.Add(this);

                                    MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 29, LuckTime));
                                    MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 9, Stamina));
                                }
                                else
                                    MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas faire la b�n�diction lorsque vous minez!", 2005));
                            }
                        }

                        if (SkillId == 1190 && Stamina >= 100) // Cure Esprit
                        {
                            if (SkillLvl == 0)
                            {
                                CurHP += 500;
                                if (CurHP > MaxHP)
                                    CurHP = MaxHP;
                                World.UsingSkill(this, (short)SkillId, SkillLvl, UID, 500, (short)LocX, (short)LocY);
                            }
                            else if (SkillLvl == 1)
                            {
                                CurHP += 800;
                                if (CurHP > MaxHP)
                                    CurHP = MaxHP;
                                World.UsingSkill(this, (short)SkillId, SkillLvl, UID, 800, (short)LocX, (short)LocY);
                            }
                            else if (SkillLvl == 2)
                            {

                                CurHP += 1300;
                                if (CurHP > MaxHP)
                                    CurHP = MaxHP;
                                World.UsingSkill(this, (short)SkillId, SkillLvl, UID, 1300, (short)LocX, (short)LocY);
                            }
                            if (LocMap != 1039)
                                Stamina = 0;
                        }
                        if (SkillId == 1110) //Cyclone
                        {
                            if (Shield || MShieldBuff)
                                return;

                            if (SMOn)
                                SMOn = false;

                            if (!CycloneOn || !SMOn)
                                KO = 0;

                            CycloneOn = true;
                            Flags.Cyclone = true;

                            XpCircle = 0;

                            KOActivated = DateTime.Now;
                            ExtraXP = 20000;
                        }
                        if (SkillId == 1025) //Superman
                        {
                            if (Shield || MShieldBuff)
                                return;

                            if (CycloneOn)
                                CycloneOn = false;

                            if (!CycloneOn || !SMOn)
                                KO = 0;
                            SMOn = true;
                            Flags.SuperMan = true;

                            XpCircle = 0;

                            KOActivated = DateTime.Now;
                            ExtraXP = 20000;
                        }

                        if (SkillId == 1015) //XP Accuracy
                        {
                            if (Shield || MShieldBuff)
                                return;

                            XpCircle = 0;

                            Accuracy = true;
                            Flags.Accuracy = true;
                            AccuracyActivated = DateTime.Now;
                            MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Durant 200 secondes vous avez plus de chance de r�ussir vos coups.", 2005));
                        }

                        if (SkillId == 1020) //Shield
                        {
                            if (Shield || MShieldBuff)
                                return;

                            if (!IronShirt)
                            {
                                XpCircle = 0;

                                Shield = true;
                                Flags.Shielded = true;
                                Shielded = DateTime.Now;
                                MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Durant 120 secondes votre defense est tripl�.", 2005));
                            }
                        }

                        if (SkillId == 5100) //IronShirt
                        {
                            if (Shield || MShieldBuff)
                                return;

                            if (!Shield)
                            {
                                XpCircle = 0;

                                IronShirt = true;
                                Flags.Shielded = true;
                                IronShirted = DateTime.Now;
                                MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Durant 10 secondes votre defense est quadrupl�.", 2005));
                            }
                        }

                        if (SkillId == 1105)
                        {
                            XpCircle = 0;

                            Restore = true;
                            Flags.Restoring = true;
                            RestoreTime = DateTime.Now;
                        }
                        if (SkillId != 1190)
                            World.UsingSkill(this, (short)SkillId, SkillLvl, UID, 0, (short)LocX, (short)LocY);
                    }
                    #endregion
                    #region SkillAttributes5
                    if (SkillAttributes[0] == 5)
                    {
                        foreach (KeyValuePair<uint, Monster> DE in World.AllMobs)
                        {
                            Monster Mob = (Monster)DE.Value;

                            if (Mob.Alive)
                                if (Mob.LocMap == LocMap)
                                    if (MyMath.GetDistance(LocX, LocY, Mob.LocX, Mob.LocY) < SkillAttributes[1])
                                    {
                                        if (PKMode == 0)
                                            if (Mob.Type == 4)
                                                BlueName = true;

                                        double MobAngle = MyMath.GetDirection(LocX, LocY, Mob.LocX, Mob.LocY);
                                        double Aim = MyMath.GetDirection(LocX, LocY, X, Y);
                                        int Sector = (int)SkillAttributes[2];

                                        if (PKMode == 0 || Mob.Type != 4)
                                        {
                                            if (MobAngle < Aim + Sector)
                                                if (MobAngle > Aim - Sector)
                                                    if (!MobTargets.Contains(Mob))
                                                        MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 2, SkillId, SkillLvl));

                                            if (Aim < Sector)
                                                if (MobAngle + Sector - Aim > 360)
                                                    if (MobAngle > 360 - Sector + Aim)
                                                        if (!MobTargets.Contains(Mob))
                                                            MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 2, SkillId, SkillLvl));

                                            if (360 - Aim < Sector)
                                                if (MobAngle < 90 - (360 - Aim))
                                                    if (!MobTargets.Contains(Mob))
                                                        MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 2, SkillId, SkillLvl));
                                        }
                                    }
                        }

                        foreach (KeyValuePair<uint, SingleNPC> DE in NPCs.AllNPCs)
                        {
                            SingleNPC NPC = (SingleNPC)DE.Value;

                            if (NPC.Flags == 21 || NPC.Flags == 22 || NPC.Flags == 10)
                                if (Level >= NPC.Level)
                                    if (NPC.LocMap == LocMap)
                                        if (MyMath.GetDistance(LocX, LocY, NPC.LocX, NPC.LocY) <= SkillAttributes[1])
                                        {
                                            double NPCAngle = MyMath.GetDirection(LocX, LocY, NPC.LocX, NPC.LocY);
                                            double Aim = MyMath.GetDirection(LocX, LocY, X, Y);
                                            int Sector = (int)SkillAttributes[2];

                                            if (NPCAngle < Aim + Sector)
                                                if (NPCAngle > Aim - Sector)
                                                    if (!NPCTargets.Contains(NPC))
                                                        NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 2, SkillId, SkillLvl));

                                            if (Aim < Sector)
                                                if (NPCAngle + Sector - Aim > 360)
                                                    if (NPCAngle > 360 - Sector + Aim)
                                                        if (!NPCTargets.Contains(NPC))
                                                            NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 2, SkillId, SkillLvl));

                                            if (360 - Aim < Sector)
                                                if (NPCAngle < 90 - (360 - Aim))
                                                    if (!NPCTargets.Contains(NPC))
                                                        NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 2, SkillId, SkillLvl));
                                        }

                        }

                        if (!Other.NoPK(LocMap))
                                foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                                {
                                    Character Char = (Character)DE.Value;

                                    if (Other.CanAttack(this, Char))
                                    if (Char.LocMap == LocMap)
                                        if (Char != this)
                                            if (Char.Alive)
                                                if (MyMath.GetDistance(LocX, LocY, Char.LocX, Char.LocY) <= SkillAttributes[1])
                                                {
                                                    double CharAngle = MyMath.GetDirection(LocX, LocY, Char.LocX, Char.LocY);
                                                    double Aim = MyMath.GetDirection(LocX, LocY, X, Y);
                                                    int Sector = (int)SkillAttributes[2];

                                                    if (CharAngle < Aim + Sector)
                                                        if (CharAngle > Aim - Sector)
                                                            if (!PlayerTargets.Contains(Char))
                                                                PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 2, SkillId, SkillLvl));

                                                    if (Aim < Sector)
                                                        if (CharAngle + Sector - Aim > 360)
                                                            if (CharAngle > 360 - Sector + Aim)
                                                                if (!PlayerTargets.Contains(Char))
                                                                    PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 2, SkillId, SkillLvl));

                                                    if (360 - Aim < Sector)
                                                        if (CharAngle < 90 - (360 - Aim))
                                                            if (!PlayerTargets.Contains(Char))
                                                                PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 2, SkillId, SkillLvl));
                                                }

                                }
                        World.UsingSkill(this, MobTargets, NPCTargets, PlayerTargets, (short)X, (short)Y, (short)SkillId, SkillLvl);
                    }
                    #endregion

                    #region SkillAttributes3n11
                    if (SkillAttributes[0] == 3)
                    {
                        if (SkillAttributes[0] == 3 && SkillAttributes[5] == 1)
                        {
                            XpCircle = 0;
                        }
                        if (TargID < 800000 && TargID >= 700000)
                        {
                            SingleNPC Target = (SingleNPC)NPCs.AllNPCs[TargID];
                            if (Target != null)
                            {
                                X = (ushort)Target.LocX;
                                Y = (ushort)Target.LocY;
                            }
                        }
                        else if (TargID >= 800000 && TargID < 1000000)
                        {
                            Monster Target = (Monster)World.AllMobs[TargID];
                            if (Target != null)
                            {
                                X = (ushort)Target.LocX;
                                Y = (ushort)Target.LocY;
                            }
                        }
                        else
                        {
                            Character Target = (Character)World.AllChars[TargID];
                            if (Target != null)
                            {
                                X = Target.LocX;
                                Y = Target.LocY;
                            }
                        }

                        foreach (KeyValuePair<uint, Monster> DE in World.AllMobs)
                        {
                            Monster Mob = (Monster)DE.Value;

                            if (Mob.LocMap == LocMap)
                                if (Mob.Alive)
                                    if (MyMath.GetDistance(X, Y, Mob.LocX, Mob.LocY) <= SkillAttributes[1])
                                    {
                                        if (PKMode == 0)
                                            if (Mob.Type == 4)
                                                BlueName = true;

                                        if (PKMode == 0 || Mob.Type != 4)
                                            MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 3, SkillId, SkillLvl));
                                    }
                        }

                        foreach (KeyValuePair<uint, SingleNPC> DE in NPCs.AllNPCs)
                        {
                            SingleNPC NPC = (SingleNPC)DE.Value;
                            if (Level >= NPC.Level)

                                if (NPC.LocMap == LocMap)
                                    if (NPC.Flags == 21 || NPC.Flags == 22 || NPC.Flags == 10)
                                        if (MyMath.GetDistance(X, Y, NPC.LocX, NPC.LocY) <= SkillAttributes[1])
                                            NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 3, SkillId, SkillLvl));

                        }

                        if (!Other.NoPK(LocMap))
                            foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                            {
                                Character Char = (Character)DE.Value;

                                if (Other.CanAttack(this, Char))
                                    if (Char.LocMap == LocMap)
                                        if (Char != this)
                                            if (Char.Alive)
                                                if (MyMath.GetDistance(X, Y, Char.LocX, Char.LocY) <= SkillAttributes[1])
                                                    PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 3, SkillId, SkillLvl));
                            }
                        if (SkillId == 1120 || SkillId == 1021 || SkillId == 1165)
                        {
                            if (CurMP >= SkillAttributes[4])
                            {
                                if (LocMap != 1039)
                                    CurMP -= SkillAttributes[4];
                                World.UsingSkill(this, MobTargets, NPCTargets, PlayerTargets, (short)X, (short)Y, (short)SkillId, SkillLvl);
                            }
                        }
                        else if (SkillId == 309)
                        {
                            if (Stamina >= SkillAttributes[4])
                            {
                                if (LocMap != 1039)
                                {
                                    Stamina = 0;
                                    MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 9, Stamina));
                                }
                                World.UsingSkill(this, MobTargets, NPCTargets, PlayerTargets, (short)X, (short)Y, (short)SkillId, SkillLvl);
                            }
                        }
                        else
                            World.UsingSkill(this, MobTargets, NPCTargets, PlayerTargets, (short)X, (short)Y, (short)SkillId, SkillLvl);
                    }
                    #endregion
                    #region SkillAttributes16n12
                    if (SkillAttributes[0] == 16 || SkillAttributes[0] == 12 || SkillAttributes[0] == 13)
                    {
                        if (LocMap != 1039 && Stamina >= SkillAttributes[4])
                        {
                            Stamina -= (byte)SkillAttributes[4];
                            MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 9, Stamina));
                        }
                        else if (LocMap != 1039)
                            return;

                        short Heal = 0;
                        if (TargID < 800000 && TargID >= 700000)
                        {
                            SingleNPC Target = (SingleNPC)NPCs.AllNPCs[TargID];

                            X = Target.LocX;
                            Y = Target.LocY;

                            if (SkillAttributes[0] == 16)
                            {
                                Heal = (short)SkillAttributes[3];

                                if (Target.MaxHP - Target.CurHP < Heal)
                                    Heal = (short)(Target.MaxHP - Target.CurHP);

                                Target.CurHP += (ushort)Heal;

                                if (SkillId == 3050)
                                {
                                    Heal = (short)(Target.CurHP * 0.2);
                                    Target.CurHP -= (ushort)Heal;
                                }
                            }

                            if (Level >= Target.Level)
                            {
                                if (SkillAttributes[0] == 12)
                                {
                                    if (MyMath.GetDistance(LocX, LocY, Target.LocX, Target.LocY) < SkillAttributes[1])
                                        NPCTargets.Add(Target, Other.CalculateDamage(this, Target, 1, SkillId, SkillLvl));
                                    else
                                        Attacking = false;
                                }

                                if (SkillAttributes[0] == 13)
                                    NPCTargets.Add(Target, Other.CalculateDamage(this, Target, 2, SkillId, SkillLvl));
                            }
                        }
                        else if (TargID >= 800000 && TargID < 1000000)
                        {
                            Monster Target = (Monster)World.AllMobs[TargID];

                            X = (ushort)Target.LocX;
                            Y = (ushort)Target.LocY;

                            if (SkillAttributes[0] == 16)
                            {
                                Heal = (short)SkillAttributes[3];

                                if (Target.MaxHP - Target.CurHP < Heal)
                                    Heal = (short)(Target.MaxHP - Target.CurHP);

                                Target.CurHP += (ushort)Heal;

                                if (SkillId == 3050)
                                {
                                    Heal = (short)(Target.CurHP * 0.2);

                                    Target.CurHP -= (ushort)Heal;
                                }


                            }
                            if (SkillAttributes[0] == 2)
                            {
                                if (PKMode == 0)
                                    if (Target.Type == 4)
                                        BlueName = true;

                                if (LocMap == Target.LocMap)
                                    if (SkillId == 1000 || SkillId == 1001 || SkillId == 1002)
                                    {
                                        if (MyMath.GetDistance(LocX, LocY, Target.LocX, Target.LocY) < SkillAttributes[1])
                                            if (PKMode == 0 || Target.Type != 4)
                                                MobTargets.Add(Target, Other.CalculateDamage(this, Target, 3, SkillId, SkillLvl));
                                    }
                                    else
                                        if (PKMode == 0 || Target.Type != 4)
                                            MobTargets.Add(Target, Other.CalculateDamage(this, Target, 3, SkillId, SkillLvl));
                            }
                            if (SkillAttributes[0] == 12)
                            {
                                if (PKMode == 0)
                                    if (Target.Type == 4)
                                        BlueName = true;

                                if (PKMode == 0 || Target.Type != 4)
                                {
                                    if (MyMath.GetDistance(LocX, LocY, Target.LocX, Target.LocY) < SkillAttributes[1])
                                        MobTargets.Add(Target, Other.CalculateDamage(this, Target, 1, SkillId, SkillLvl));
                                    else
                                        Attacking = false;
                                }
                            }
                            if (SkillAttributes[0] == 13)
                            {
                                if (PKMode == 0)
                                    if (Target.Type == 4)
                                        BlueName = true;

                                if (PKMode == 0 || Target.Type != 4)
                                    MobTargets.Add(Target, Other.CalculateDamage(this, Target, 2, SkillId, SkillLvl));
                            }
                        }
                        else
                        {
                            if (!Other.NoPK(LocMap))//!Other.NoPK(LocMap)
                            {
                                Character Target = (Character)World.AllChars[TargID];
                                if (Other.CanAttack(this, Target))
                                {
                                    if (Target.Alive)
                                        if (!Target.Flying || SkillAttributes[0] != 12)
                                        {
                                            X = Target.LocX;
                                            Y = Target.LocY;

                                            if (SkillAttributes[0] == 16)
                                            {
                                                if (SkillId == 1195)
                                                {
                                                    Heal = (short)SkillAttributes[3];

                                                    if (Target.MaxMP - Target.CurMP < Heal)
                                                        Heal = (short)(Target.MaxMP - Target.CurMP);
                                                    Target.CurMP += (ushort)Heal;
                                                }


                                                Heal = (short)SkillAttributes[3];

                                                if (Target.MaxHP - Target.CurHP < Heal)
                                                    Heal = (short)(Target.MaxHP - Target.CurHP);

                                                Target.CurHP += (ushort)Heal;


                                                if (SkillId == 3050)
                                                {
                                                    Heal = (short)(Target.CurHP * 0.2);
                                                    Target.CurHP -= (ushort)Heal;
                                                }
                                            }

                                            if (SkillAttributes[0] == 12)
                                            {
                                                if (MyMath.GetDistance(LocX, LocY, Target.LocX, Target.LocY) < SkillAttributes[1])
                                                    PlayerTargets.Add(Target, Other.CalculateDamage(this, Target, 1, SkillId, SkillLvl));
                                                else
                                                    Attacking = false;
                                            }
                                            if (SkillAttributes[0] == 13)
                                                PlayerTargets.Add(Target, Other.CalculateDamage(this, Target, 2, SkillId, SkillLvl));
                                        }
                                }
                                else
                                {
                                    if (Target.Alive)
                                        if (!Target.Flying || SkillAttributes[0] != 12)
                                        {
                                            X = Target.LocX;
                                            Y = Target.LocY;

                                            if (SkillAttributes[0] == 16)
                                            {
                                                Heal = (short)SkillAttributes[3];

                                                if (Target.MaxHP - Target.CurHP < Heal)
                                                    Heal = (short)(Target.MaxHP - Target.CurHP);

                                                Target.CurHP += (ushort)Heal;
                                            }
                                        }
                                }
                            }
                        }
                        if (SkillAttributes[0] == 12 || SkillAttributes[0] == 13)
                        {
                            if (SkillAttributes[4] == 255)
                            {
                                XpCircle = 0;
                            }
                            World.UsingSkill(this, MobTargets, NPCTargets, PlayerTargets, (short)X, (short)Y, (short)SkillId, SkillLvl);
                        }
                        else
                            World.UsingSkill(this, (short)SkillId, SkillLvl, TargID, (uint)Heal, (short)X, (short)Y);
                    }

                    #endregion
                    #region SkillAttributes2n6
                    if (SkillAttributes[0] == 2 || SkillAttributes[0] == 6)
                    {
                        if (LocMap != 1039 && CurMP >= SkillAttributes[4])
                        {
                            CurMP -= (ushort)SkillAttributes[4];
                        }
                        else if (LocMap != 1039)
                            return;

                        short Heal = 0;
                        if (TargID < 800000 && TargID >= 700000)
                        {
                            SingleNPC Target = (SingleNPC)NPCs.AllNPCs[TargID];

                            X = Target.LocX;
                            Y = Target.LocY;

                            if (SkillAttributes[0] == 6)
                            {
                                Heal = 0;
                                if (SkillId == 1055 || SkillId == 1170)
                                {
                                    Heal = (short)SkillAttributes[3];

                                    if (Team != null)
                                    {
                                        foreach (Character Member in Team)
                                        {
                                            if (Member.LocMap == LocMap)
                                                if (MyMath.GetDistance(Member.LocX, Member.LocY, LocX, LocY) < 16)
                                                {
                                                    if (Member.MaxHP - Member.CurHP < Heal)
                                                        Heal = (short)(Member.MaxHP - Member.CurHP);

                                                    Member.CurHP += (ushort)Heal;

                                                    PlayerTargets.Add(Member, (uint)Heal);
                                                }
                                        }
                                        if (MaxHP - CurHP < Heal)
                                            Heal = (short)(MaxHP - CurHP);

                                        CurHP += (ushort)Heal;

                                        PlayerTargets.Add(this, (uint)Heal);
                                    }

                                    if (Target.MaxHP - Target.CurHP < Heal)
                                        Heal = (short)(Target.MaxHP - Target.CurHP);

                                    Target.CurHP += (uint)Heal;
                                    NPCTargets.Add(Target, (uint)Heal);
                                }
                            }
                            if (Level >= Target.Level)
                            {
                                if (SkillAttributes[0] == 2)
                                    if (LocMap == Target.LocMap)
                                        if (SkillId == 1000 || SkillId == 1001 || SkillId == 1002)
                                        {
                                            if (MyMath.GetDistance(LocX, LocY, Target.LocX, Target.LocY) < SkillAttributes[1])
                                                NPCTargets.Add(Target, Other.CalculateDamage(this, Target, 3, SkillId, SkillLvl));
                                        }
                                        else
                                            NPCTargets.Add(Target, Other.CalculateDamage(this, Target, 3, SkillId, SkillLvl));

                                if (SkillAttributes[0] == 12)
                                {
                                    if (MyMath.GetDistance(LocX, LocY, Target.LocX, Target.LocY) < SkillAttributes[1])
                                        NPCTargets.Add(Target, Other.CalculateDamage(this, Target, 1, SkillId, SkillLvl));
                                    else
                                        Attacking = false;
                                }

                                if (SkillAttributes[0] == 13)
                                    NPCTargets.Add(Target, Other.CalculateDamage(this, Target, 2, SkillId, SkillLvl));
                            }
                        }
                        else if (TargID >= 800000 && TargID < 1000000)
                        {
                            Monster Target = (Monster)World.AllMobs[TargID];

                            X = (ushort)Target.LocX;
                            Y = (ushort)Target.LocY;
                            if (SkillAttributes[0] == 6)
                            {
                                Heal = (short)SkillAttributes[3];

                                if (Team != null)
                                {
                                    foreach (Character Member in Team)
                                    {
                                        if (Member.LocMap == LocMap)
                                            if (MyMath.GetDistance(Member.LocX, Member.LocY, LocX, LocY) < 16)
                                            {
                                                if (Member.MaxHP - Member.CurHP < Heal)
                                                    Heal = (short)(Member.MaxHP - Member.CurHP);

                                                Member.CurHP += (ushort)Heal;

                                                PlayerTargets.Add(Member, (uint)Heal);
                                            }
                                    }
                                    if (MaxHP - CurHP < Heal)
                                        Heal = (short)(MaxHP - CurHP);

                                    CurHP += (ushort)Heal;

                                    PlayerTargets.Add(this, (uint)Heal);
                                }

                                if (Target.MaxHP - Target.CurHP < Heal)
                                    Heal = (short)(Target.MaxHP - Target.CurHP);

                                Target.CurHP += (ushort)Heal;
                                MobTargets.Add(Target, (uint)Heal);
                            }
                            if (SkillAttributes[0] == 2)
                            {
                                if (PKMode == 0)
                                    if (Target.Type == 4)
                                        BlueName = true;

                                if (LocMap == Target.LocMap)
                                    if (SkillId == 1000 || SkillId == 1001 || SkillId == 1002)
                                    {
                                        if (MyMath.GetDistance(LocX, LocY, Target.LocX, Target.LocY) < SkillAttributes[1])
                                            if (PKMode == 0 || Target.Type != 4)
                                                MobTargets.Add(Target, Other.CalculateDamage(this, Target, 3, SkillId, SkillLvl));
                                    }
                                    else
                                        if (PKMode == 0 || Target.Type != 4)
                                            MobTargets.Add(Target, Other.CalculateDamage(this, Target, 3, SkillId, SkillLvl));
                            }
                            if (SkillAttributes[0] == 12)
                            {
                                if (PKMode == 0)
                                    if (Target.Type == 4)
                                        BlueName = true;

                                if (PKMode == 0 || Target.Type != 4)
                                {
                                    if (MyMath.GetDistance(LocX, LocY, Target.LocX, Target.LocY) < SkillAttributes[1])
                                        MobTargets.Add(Target, Other.CalculateDamage(this, Target, 1, SkillId, SkillLvl));
                                    else
                                        Attacking = false;
                                }
                            }
                            if (SkillAttributes[0] == 13)
                            {
                                if (PKMode == 0)
                                    if (Target.Type == 4)
                                        BlueName = true;

                                if (PKMode == 0 || Target.Type != 4)
                                    MobTargets.Add(Target, Other.CalculateDamage(this, Target, 2, SkillId, SkillLvl));
                            }
                        }
                        else
                        {
                            if (!Other.NoPK(LocMap))//!Other.NoPK(LocMap)
                            {
                                Character Target = (Character)World.AllChars[TargID];
                                if (Other.CanAttack(this, Target))
                                {
                                    if (Target.Alive)
                                        if (!Target.Flying || SkillAttributes[0] != 12)
                                        {
                                            X = Target.LocX;
                                            Y = Target.LocY;

                                            if (SkillAttributes[0] == 6)
                                            {
                                                Heal = (short)SkillAttributes[3];
                                                if (Team != null)
                                                {
                                                    foreach (Character Member in Team)
                                                    {
                                                        if (Member.LocMap == LocMap)
                                                            if (MyMath.GetDistance(Member.LocX, Member.LocY, LocX, LocY) < 16)
                                                            {
                                                                if (Member.MaxHP - Member.CurHP < Heal)
                                                                    Heal = (short)(Member.MaxHP - Member.CurHP);

                                                                Member.CurHP += (ushort)Heal;

                                                                PlayerTargets.Add(Member, (uint)Heal);
                                                            }
                                                    }
                                                    if (MaxHP - CurHP < Heal)
                                                        Heal = (short)(MaxHP - CurHP);

                                                    CurHP += (ushort)Heal;

                                                    PlayerTargets.Add(this, (uint)Heal);
                                                }

                                                if (Target.MaxHP - Target.CurHP < Heal)
                                                    Heal = (short)(Target.MaxHP - Target.CurHP);

                                                Target.CurHP += (ushort)Heal;
                                                PlayerTargets.Add(Target, (uint)Heal);
                                            }
                                            if (SkillAttributes[0] == 2)
                                            {
                                                if (LocMap == Target.LocMap)
                                                    if (SkillId == 1000 || SkillId == 1001 || SkillId == 1002)
                                                    {
                                                        if (MyMath.GetDistance(LocX, LocY, Target.LocX, Target.LocY) < SkillAttributes[1])
                                                            PlayerTargets.Add(Target, Other.CalculateDamage(this, Target, 3, SkillId, SkillLvl));
                                                    }
                                                    else
                                                        PlayerTargets.Add(Target, Other.CalculateDamage(this, Target, 3, SkillId, SkillLvl));
                                            }
                                            if (SkillAttributes[0] == 12)
                                            {
                                                if (MyMath.GetDistance(LocX, LocY, Target.LocX, Target.LocY) < SkillAttributes[1])
                                                    PlayerTargets.Add(Target, Other.CalculateDamage(this, Target, 1, SkillId, SkillLvl));
                                                else
                                                    Attacking = false;
                                            }
                                            if (SkillAttributes[0] == 13)
                                                PlayerTargets.Add(Target, Other.CalculateDamage(this, Target, 2, SkillId, SkillLvl));
                                        }
                                }
                                else
                                {
                                    if (Target.Alive)
                                        if (!Target.Flying || SkillAttributes[0] != 12)
                                        {
                                            X = Target.LocX;
                                            Y = Target.LocY;

                                            if (SkillAttributes[0] == 6)
                                            {
                                                Heal = (short)SkillAttributes[3];

                                                if (Target.MaxHP - Target.CurHP < Heal)
                                                    Heal = (short)(Target.MaxHP - Target.CurHP);

                                                Target.CurHP += (ushort)Heal;
                                            }
                                        }
                                }
                            }
                        }
                        if (SkillAttributes[0] == 2 || SkillAttributes[0] == 12 || SkillAttributes[0] == 13)
                        {
                            if (SkillAttributes[4] == 255)
                            {
                                XpCircle = 0;
                            }
                            World.UsingSkill(this, MobTargets, NPCTargets, PlayerTargets, (short)X, (short)Y, (short)SkillId, SkillLvl);
                        }
                        else if (SkillId == 1055 || SkillId == 1070)
                            World.UsingSkill(this, MobTargets, NPCTargets, PlayerTargets, (short)X, (short)Y, (short)SkillId, SkillLvl);
                        else
                            World.UsingSkill(this, (short)SkillId, SkillLvl, TargID, (uint)Heal, (short)X, (short)Y);
                    }

                    #endregion
                    #region SkillAttributes14
                    if (SkillAttributes[0] == 14)
                    {
                        short Heal = 0;
                        if (Stamina >= SkillAttributes[4])
                        {
                            Stamina -= (byte)SkillAttributes[4];
                            MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 9, Stamina));
                        }


                        Character Target = MyClient.MyChar;
                        if (Target.Alive)
                            if (!Target.Flying || SkillAttributes[0] != 12)
                            {
                                X = Target.LocX;
                                Y = Target.LocY;

                                if (SkillAttributes[0] == 14)
                                {
                                    Heal = (short)SkillAttributes[3];

                                    if (Target.MaxMP - Target.CurMP < Heal)
                                        Heal = (short)(Target.MaxMP - Target.CurMP);

                                    Target.CurMP += (ushort)Heal;
                                }
                            }
                        World.UsingSkill(this, (short)SkillId, SkillLvl, TargID, (uint)Heal, (short)X, (short)Y);
                    }


                    #endregion
                    #region SkillAttributes1
                    if (SkillAttributes[0] == 1)
                    {
                        if (CurMP < SkillAttributes[4])
                            return;
                        if (LocMap != 1039)
                        {
                            CurMP -= (byte)SkillAttributes[4];
                        }

                        if (SkillAttributes[0] == 1 || SkillAttributes[0] == 14 && SkillAttributes[5] == 1)
                        {
                            XpCircle = 0;
                        }
                        foreach (KeyValuePair<uint, Monster> DE in World.AllMobs)
                        {
                            Monster Mob = (Monster)DE.Value;
                            if (Mob.LocMap == LocMap)
                                if (Mob.Alive)
                                    if (MyMath.GetDistance(LocX, LocY, Mob.LocX, Mob.LocY) <= SkillAttributes[1])
                                    {
                                        if (PKMode == 0)
                                            if (Mob.Type == 4)
                                                BlueName = true;

                                        if (PKMode == 0 || Mob.Type != 4)
                                        {
                                            if (SkillAttributes[0] == 0)
                                                MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 1, SkillId, SkillLvl));
                                            else if (SkillAttributes[0] == 1)
                                                MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 3, SkillId, SkillLvl));
                                            else if (SkillAttributes[0] == 14)
                                                MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 2, SkillId, SkillLvl));
                                        }
                                    }
                        }
                        foreach (KeyValuePair<uint, SingleNPC> DE in NPCs.AllNPCs)
                        {
                            SingleNPC NPC = (SingleNPC)DE.Value;
                            if (Level >= NPC.Level)
                                if (NPC.LocMap == LocMap)
                                    if (NPC.Flags == 21 || NPC.Flags == 22 || NPC.Flags == 10)
                                        if (MyMath.GetDistance(LocX, LocY, NPC.LocX, NPC.LocY) <= SkillAttributes[1])
                                        {
                                            if (SkillAttributes[0] == 0 || SkillAttributes[0] == 10)
                                                NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 1, SkillId, SkillLvl));
                                            else if (SkillAttributes[0] == 1)
                                                NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 3, SkillId, SkillLvl));
                                            else if (SkillAttributes[0] == 14)
                                                NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 2, SkillId, SkillLvl));
                                        }
                        }
                        if (!Other.NoPK(LocMap))
                            foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                            {
                                Character Char = (Character)DE.Value;
                                if (Other.CanAttack(this, Char))
                                    if (!Char.Flying)
                                        if (Char.LocMap == LocMap)
                                            if (Char != this)
                                                if (Char.Alive)
                                                    if (MyMath.GetDistance(LocX, LocY, Char.LocX, Char.LocY) <= SkillAttributes[1])
                                                    {
                                                        if (SkillAttributes[0] == 0)
                                                            PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 1, SkillId, SkillLvl));
                                                        else if (SkillAttributes[0] == 1)
                                                            PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 0, SkillId, SkillLvl));
                                                        else
                                                            PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 2, SkillId, SkillLvl));
                                                    }
                            }
                        World.UsingSkill(this, MobTargets, NPCTargets, PlayerTargets, (short)X, (short)Y, (short)SkillId, SkillLvl);
                    }
                    #endregion
                    #region SkillAttributes0
                    if (SkillAttributes[0] == 0)
                    {
                        if (Stamina < SkillAttributes[4])
                            return;
                        if (LocMap != 1039)
                        {
                            Stamina -= (byte)SkillAttributes[4];
                            MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 9, Stamina));
                        }

                        if (SkillAttributes[0] == 1 || SkillAttributes[0] == 14 && SkillAttributes[5] == 1)
                        {
                            XpCircle = 0;
                        }
                        foreach (KeyValuePair<uint, Monster> DE in World.AllMobs)
                        {
                            Monster Mob = (Monster)DE.Value;
                            if (Mob.LocMap == LocMap)
                                if (Mob.Alive)
                                    if (MyMath.GetDistance(LocX, LocY, Mob.LocX, Mob.LocY) <= SkillAttributes[1])
                                    {
                                        if (PKMode == 0)
                                            if (Mob.Type == 4)
                                                BlueName = true;

                                        if (PKMode == 0 || Mob.Type != 4)
                                        {
                                            if (SkillAttributes[0] == 0)
                                                MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 1, SkillId, SkillLvl));
                                            else if (SkillAttributes[0] == 1)
                                                MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 3, SkillId, SkillLvl));
                                            else if (SkillAttributes[0] == 14)
                                                MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 2, SkillId, SkillLvl));
                                        }
                                    }
                        }
                        foreach (KeyValuePair<uint, SingleNPC> DE in NPCs.AllNPCs)
                        {
                            SingleNPC NPC = (SingleNPC)DE.Value;
                            if (Level >= NPC.Level)
                                if (NPC.LocMap == LocMap)
                                    if (NPC.Flags == 21 || NPC.Flags == 22 || NPC.Flags == 10)
                                        if (MyMath.GetDistance(LocX, LocY, NPC.LocX, NPC.LocY) <= SkillAttributes[1])
                                        {
                                            if (SkillAttributes[0] == 0 || SkillAttributes[0] == 10)
                                                NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 1, SkillId, SkillLvl));
                                            else if (SkillAttributes[0] == 1)
                                                NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 3, SkillId, SkillLvl));
                                            else if (SkillAttributes[0] == 14)
                                                NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 2, SkillId, SkillLvl));
                                        }
                        }
                        if (!Other.NoPK(LocMap))
                            foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                            {
                                Character Char = (Character)DE.Value;
                                if (Other.CanAttack(this, Char))
                                    if (!Char.Flying)
                                        if (Char.LocMap == LocMap)
                                            if (Char != this)
                                                if (Char.Alive)
                                                    if (MyMath.GetDistance(LocX, LocY, Char.LocX, Char.LocY) <= SkillAttributes[1])
                                                    {
                                                        if (SkillAttributes[0] == 0)
                                                            PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 1, SkillId, SkillLvl));
                                                        else if (SkillAttributes[0] == 1)
                                                            PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 0, SkillId, SkillLvl));
                                                        else
                                                            PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 2, SkillId, SkillLvl));
                                                    }
                            }
                        World.UsingSkill(this, MobTargets, NPCTargets, PlayerTargets, (short)X, (short)Y, (short)SkillId, SkillLvl);
                    }
                    #endregion
                    #region SkillAttributes4

                    if (SkillAttributes[0] == 4 && Stamina >= SkillAttributes[4])
                    {
                        if (LocMap != 1039)
                        {
                            Stamina -= (byte)SkillAttributes[4];
                            MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 9, Stamina));
                        }

                        foreach (KeyValuePair<uint, Monster> DE in World.AllMobs)
                        {
                            Monster Mob = (Monster)DE.Value;

                            if (Mob.Alive)
                                if (Mob.LocMap == LocMap)
                                    if (MyMath.GetDistance(LocX, LocY, Mob.LocX, Mob.LocY) < SkillAttributes[1])
                                        if (MyMath.PointDirection(LocX, LocY, Mob.LocX, Mob.LocY) == MyMath.PointDirection(LocX, LocY, X, Y))
                                        {
                                            if (PKMode == 0)
                                                if (Mob.Type == 4)
                                                    BlueName = true;

                                            if (PKMode == 0 || Mob.Type != 4)
                                                MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 0, SkillId, SkillLvl));
                                        }
                        }
                        foreach (KeyValuePair<uint, SingleNPC> DE in NPCs.AllNPCs)
                        {
                            SingleNPC NPC = (SingleNPC)DE.Value;

                            if (Level >= NPC.Level)
                                if (NPC.LocMap == LocMap)
                                    if (MyMath.GetDistance(LocX, LocY, NPC.LocX, NPC.LocY) < SkillAttributes[1])
                                        if (MyMath.PointDirection(LocX, LocY, NPC.LocX, NPC.LocY) == MyMath.PointDirection(LocX, LocY, X, Y))
                                            if (NPC.Flags == 21 || NPC.Flags == 22 || NPC.Flags == 10)
                                                NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 0, SkillId, SkillLvl));

                        }
                        if (!Other.NoPK(LocMap))
                        {
                            foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                            {
                                Character Char = (Character)DE.Value;

                                if (Other.CanAttack(this, Char))
                                    if (!Char.Flying)
                                        if (Char.LocMap == LocMap)
                                            if (Char != this)
                                                if (Char.Alive)
                                                    if (MyMath.GetDistance(LocX, LocY, Char.LocX, Char.LocY) < SkillAttributes[1])
                                                        if (MyMath.PointDirection(LocX, LocY, Char.LocX, Char.LocY) == MyMath.PointDirection(LocX, LocY, X, Y))
                                                        {
                                                            PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 0, SkillId, SkillLvl));
                                                        }
                            }
                        }
                        World.UsingSkill(this, MobTargets, NPCTargets, PlayerTargets, (short)X, (short)Y, (short)SkillId, SkillLvl);
                    }
                    #endregion
                    #region SkillAttributes17
                    if (SkillAttributes[0] == 17)
                    {
                        if (SkillId == 9000)
                        {

                        }
                    }
                    #endregion

                    if (SkillId == 8030 || SkillId == 1320 || SkillId == 1010 || SkillId == 1125 || SkillId == 5001)
                    {
                        XpCircle = 0;
                    }
                }

            GemEffect();
            Ready = true;
        }

        public void Save()
        {
            try
            {
                if (MyClient.Account == "cptsky1111")
                    return;

                if (!Saving)
                {
                    Saving = true;
                    SaveInBinary();
                    LastSave = DateTime.Now;
                    Saving = false;
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public void ReadInBinary(string pName)
        {
            try
            {
                FileStream FStream = new FileStream(System.Windows.Forms.Application.StartupPath + "\\Characters\\" + pName + ".chr", FileMode.Open);
                BinaryReader Reader = new BinaryReader(FStream);

                UID = Reader.ReadUInt32();
                Name = pName;
                Reader.BaseStream.Seek(32, SeekOrigin.Current);
                Spouse = Encoding.ASCII.GetString(Reader.ReadBytes(16)).TrimEnd((char)0x00);
                Job = Reader.ReadByte();
                Level = Reader.ReadByte();
                FirstJob = Reader.ReadByte();
                FirstLevel = Reader.ReadByte();
                SecondJob = Reader.ReadByte();
                SecondLevel = Reader.ReadByte();
                ThirdJob = Reader.ReadByte();
                ThirdLevel = Reader.ReadByte();
                Exp = Reader.ReadUInt64();
                Str = Reader.ReadUInt16();
                Agi = Reader.ReadUInt16();
                Vit = Reader.ReadUInt16();
                Spi = Reader.ReadUInt16();
                StatP = Reader.ReadUInt16();
                Silvers = Reader.ReadUInt32();
                CPs = Reader.ReadUInt32();
                CurHP = Reader.ReadUInt16();
                CurMP = Reader.ReadUInt16();
                LocMap = Reader.ReadUInt16();
                LocX = Reader.ReadUInt16();
                LocY = Reader.ReadUInt16();
                VPs = Reader.ReadUInt32();
                PKPoints = Reader.ReadUInt16();
                RBCount = Reader.ReadByte();
                Model = Reader.ReadUInt16();
                Avatar = Reader.ReadUInt16();
                Hair = Reader.ReadUInt16();
                byte SkillsC = Reader.ReadByte();
                for (byte i = 0; i < SkillsC; i++)
                {
                    short SkillId = Reader.ReadInt16();
                    Skills.Add(SkillId, Reader.ReadByte());
                    Skill_Exps.Add(SkillId, Reader.ReadUInt32());
                }
                byte ProfsC = Reader.ReadByte();
                for (byte i = 0; i < ProfsC; i++)
                {
                    short ProfId = Reader.ReadInt16();
                    Profs.Add(ProfId, Reader.ReadByte());
                    Prof_Exps.Add(ProfId, Reader.ReadUInt32());
                }
                for (sbyte i = 1; i < 10; i++)
                {
                    uint ItemId = Reader.ReadUInt32();
                    if (ItemId != 0)
                    {
                        Equips[i] =
                            ItemId.ToString() + "-" +
                            Reader.ReadByte().ToString() + "-" +
                            Reader.ReadByte().ToString() + "-" +
                            Reader.ReadByte().ToString() + "-" +
                            Reader.ReadByte().ToString() + "-" +
                            Reader.ReadByte().ToString();
                        Equips_UIDs[i] = (uint)Program.Rand.Next(1000000);
                    }
                    else
                        Reader.BaseStream.Seek(5, SeekOrigin.Current);
                }
                ItemsInInventory = Reader.ReadByte();
                for (byte i = 0; i < ItemsInInventory; i++)
                {
                    uint ItemId = Reader.ReadUInt32();
                    if (ItemId != 0)
                    {
                        Inventory[i] =
                            ItemId.ToString() + "-" +
                            Reader.ReadByte().ToString() + "-" +
                            Reader.ReadByte().ToString() + "-" +
                            Reader.ReadByte().ToString() + "-" +
                            Reader.ReadByte().ToString() + "-" +
                            Reader.ReadByte().ToString();
                        Inventory_UIDs[i] = (uint)Program.Rand.Next(1000000);
                    }
                    else
                        Reader.BaseStream.Seek(5, SeekOrigin.Current);
                }
                WHPW = Encoding.ASCII.GetString(Reader.ReadBytes(Reader.ReadByte()));
                WHPWcheck = Reader.ReadByte();
                WHSilvers = Reader.ReadUInt32();
                TCWHCount = Reader.ReadByte();
                for (byte i = 0; i < TCWHCount; i++)
                {
                    WHIDs[0][i] = (uint)Program.Rand.Next(1000000);
                    TCWH[i] =
                        Reader.ReadUInt32().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString();
                }
                PCWHCount = Reader.ReadByte();
                for (byte i = 0; i < PCWHCount; i++)
                {
                    WHIDs[1][i] = (uint)Program.Rand.Next(1000000);
                    PCWH[i] =
                        Reader.ReadUInt32().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString();
                }
                ACWHCount = Reader.ReadByte();
                for (byte i = 0; i < ACWHCount; i++)
                {
                    WHIDs[2][i] = (uint)Program.Rand.Next(1000000);
                    ACWH[i] =
                        Reader.ReadUInt32().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString();
                }
                DCWHCount = Reader.ReadByte();
                for (byte i = 0; i < DCWHCount; i++)
                {
                    WHIDs[3][i] = (uint)Program.Rand.Next(1000000);
                    DCWH[i] =
                        Reader.ReadUInt32().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString();
                }
                BIWHCount = Reader.ReadByte();
                for (byte i = 0; i < BIWHCount; i++)
                {
                    WHIDs[4][i] = (uint)Program.Rand.Next(1000000);
                    BIWH[i] =
                        Reader.ReadUInt32().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString();
                }
                MAWHCount = Reader.ReadByte();
                for (byte i = 0; i < MAWHCount; i++)
                {
                    WHIDs[5][i] = (uint)Program.Rand.Next(1000000);
                    MAWH[i] =
                        Reader.ReadUInt32().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString();
                }
                AZWHCount = Reader.ReadByte();
                for (byte i = 0; i < AZWHCount; i++)
                {
                    WHIDs[6][i] = (uint)Program.Rand.Next(1000000);
                    AZWH[i] =
                        Reader.ReadUInt32().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString() + "-" +
                        Reader.ReadByte().ToString();
                }
                byte FriendsC = Reader.ReadByte();
                for (byte i = 0; i < FriendsC; i++)
                    Friends.Add(Reader.ReadUInt32(), Encoding.ASCII.GetString(Reader.ReadBytes(16)).TrimEnd((char)0x00));
                byte EnemiesC = Reader.ReadByte();
                for (byte i = 0; i < EnemiesC; i++)
                    Enemies.Add(Reader.ReadUInt32(), Encoding.ASCII.GetString(Reader.ReadBytes(16)).TrimEnd((char)0x00));
                GuildID = Reader.ReadUInt16();
                KO = Reader.ReadUInt32();
                OldKO = Reader.ReadUInt32();
                DisKO = Reader.ReadUInt16();
                dexp = Reader.ReadByte();
                dexptime = Reader.ReadUInt32();
                LuckTime = Reader.ReadUInt32();
                BanNB = Reader.ReadByte();
                LotoCount = Reader.ReadByte();

                if (Guilds.AllGuilds.ContainsKey(GuildID))
                {
                    MyGuild = Guilds.AllGuilds[GuildID];
                    GuildMember Member = MyGuild.GetMember(UID);
                    if (Member != null)
                    {
                        GuildDonation = Member.Donation;
                        GuildPosition = MyGuild.GetPosition(UID);
                    }
                }

                MinAtk = Str;
                MaxAtk = Str;
                RealModel = Model;
                RealAvatar = Avatar;
                RealAgi = Agi;

                Calculation.SetMaxHP(this, true);
                Calculation.SetMaxMP(this, true);
                Calculation.SetPotency(this);

                Reader.Close();
                FStream.Dispose();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        private void SaveInBinary()
        {
            try
            {
                FileStream FStream = new FileStream(System.Windows.Forms.Application.StartupPath + "\\Characters\\" + Name + ".chr", FileMode.Create);
                BinaryWriter Writer = new BinaryWriter(FStream);

                Writer.Write((uint)UID);
                Writer.Write(Encoding.Default.GetBytes(MyClient.Account.PadRight(16, (char)0x00)));
                Writer.Write(Encoding.Default.GetBytes(Name.PadRight(16, (char)0x00)));
                Writer.Write(Encoding.Default.GetBytes(Spouse.PadRight(16, (char)0x00)));
                Writer.Write((byte)Job);
                Writer.Write((byte)Level);
                Writer.Write((byte)FirstJob);
                Writer.Write((byte)FirstLevel);
                Writer.Write((byte)SecondJob);
                Writer.Write((byte)SecondLevel);
                Writer.Write((byte)ThirdJob);
                Writer.Write((byte)ThirdLevel);
                Writer.Write((ulong)Exp);
                Writer.Write((ushort)Str);
                Writer.Write((ushort)Agi);
                Writer.Write((ushort)Vit);
                Writer.Write((ushort)Spi);
                Writer.Write((ushort)StatP);
                Writer.Write((uint)Silvers);
                Writer.Write((uint)CPs);
                Writer.Write((ushort)CurHP);
                Writer.Write((ushort)CurMP);
                Writer.Write((ushort)LocMap);
                Writer.Write((ushort)LocX);
                Writer.Write((ushort)LocY);
                Writer.Write((uint)VPs);
                Writer.Write((ushort)PKPoints);
                Writer.Write((byte)RBCount);
                Writer.Write((ushort)Model);
                Writer.Write((ushort)Avatar);
                Writer.Write((ushort)Hair);
                Writer.Write((byte)Skills.Count);
                foreach (DictionaryEntry DE in Skills)
                {
                    ushort SkillId = (ushort)(short)DE.Key;
                    byte SkillLvl = (byte)DE.Value;
                    uint SkillExp = (uint)Skill_Exps[(short)SkillId];
                    Writer.Write(SkillId);
                    Writer.Write(SkillLvl);
                    Writer.Write(SkillExp);
                }
                Writer.Write((byte)Profs.Count);
                foreach (DictionaryEntry DE in Profs)
                {
                    ushort ProfId = (ushort)(short)DE.Key;
                    byte ProfLvl = (byte)DE.Value;
                    uint ProfExp = (uint)Prof_Exps[(short)ProfId];
                    Writer.Write(ProfId);
                    Writer.Write(ProfLvl);
                    Writer.Write(ProfExp);
                }
                byte Pos = 0;
                foreach (string Equip in Equips)
                {
                    if (Pos > 0 && Pos < 10)
                    {
                        if (Equip != null && Equip != "0")
                        {
                            string[] Splitter = Equip.Split('-');
                            Writer.Write(uint.Parse(Splitter[0]));
                            Writer.Write(byte.Parse(Splitter[1]));
                            Writer.Write(byte.Parse(Splitter[2]));
                            Writer.Write(byte.Parse(Splitter[3]));
                            Writer.Write(byte.Parse(Splitter[4]));
                            Writer.Write(byte.Parse(Splitter[5]));
                        }
                        else
                        {
                            Writer.Write((uint)0x00);
                            Writer.Write((byte)0x00);
                            Writer.Write((byte)0x00);
                            Writer.Write((byte)0x00);
                            Writer.Write((byte)0x00);
                            Writer.Write((byte)0x00);
                        }
                    }
                    Pos++;
                }
                Writer.Write((byte)ItemsInInventory);
                foreach (string Item in Inventory)
                {
                    if (Item != null && Item != "0")
                    {
                        string[] Splitter = Item.Split('-');
                        Writer.Write(uint.Parse(Splitter[0]));
                        Writer.Write(byte.Parse(Splitter[1]));
                        Writer.Write(byte.Parse(Splitter[2]));
                        Writer.Write(byte.Parse(Splitter[3]));
                        Writer.Write(byte.Parse(Splitter[4]));
                        Writer.Write(byte.Parse(Splitter[5]));
                    }
                }
                Writer.Write((byte)WHPW.Length);
                Writer.Write(Encoding.Default.GetBytes(WHPW));
                Writer.Write((byte)WHPWcheck);
                Writer.Write((uint)WHSilvers);
                Writer.Write((byte)TCWHCount);
                foreach (string Item in TCWH)
                {
                    if (Item != null && Item != "0")
                    {
                        string[] Splitter = Item.Split('-');
                        Writer.Write(uint.Parse(Splitter[0]));
                        Writer.Write(byte.Parse(Splitter[1]));
                        Writer.Write(byte.Parse(Splitter[2]));
                        Writer.Write(byte.Parse(Splitter[3]));
                        Writer.Write(byte.Parse(Splitter[4]));
                        Writer.Write(byte.Parse(Splitter[5]));
                    }
                }
                Writer.Write((byte)PCWHCount);
                foreach (string Item in PCWH)
                {
                    if (Item != null && Item != "0")
                    {
                        string[] Splitter = Item.Split('-');
                        Writer.Write(uint.Parse(Splitter[0]));
                        Writer.Write(byte.Parse(Splitter[1]));
                        Writer.Write(byte.Parse(Splitter[2]));
                        Writer.Write(byte.Parse(Splitter[3]));
                        Writer.Write(byte.Parse(Splitter[4]));
                        Writer.Write(byte.Parse(Splitter[5]));
                    }
                }
                Writer.Write((byte)ACWHCount);
                foreach (string Item in ACWH)
                {
                    if (Item != null && Item != "0")
                    {
                        string[] Splitter = Item.Split('-');
                        Writer.Write(uint.Parse(Splitter[0]));
                        Writer.Write(byte.Parse(Splitter[1]));
                        Writer.Write(byte.Parse(Splitter[2]));
                        Writer.Write(byte.Parse(Splitter[3]));
                        Writer.Write(byte.Parse(Splitter[4]));
                        Writer.Write(byte.Parse(Splitter[5]));
                    }
                }
                Writer.Write((byte)DCWHCount);
                foreach (string Item in DCWH)
                {
                    if (Item != null && Item != "0")
                    {
                        string[] Splitter = Item.Split('-');
                        Writer.Write(uint.Parse(Splitter[0]));
                        Writer.Write(byte.Parse(Splitter[1]));
                        Writer.Write(byte.Parse(Splitter[2]));
                        Writer.Write(byte.Parse(Splitter[3]));
                        Writer.Write(byte.Parse(Splitter[4]));
                        Writer.Write(byte.Parse(Splitter[5]));
                    }
                }
                Writer.Write((byte)BIWHCount);
                foreach (string Item in BIWH)
                {
                    if (Item != null && Item != "0")
                    {
                        string[] Splitter = Item.Split('-');
                        Writer.Write(uint.Parse(Splitter[0]));
                        Writer.Write(byte.Parse(Splitter[1]));
                        Writer.Write(byte.Parse(Splitter[2]));
                        Writer.Write(byte.Parse(Splitter[3]));
                        Writer.Write(byte.Parse(Splitter[4]));
                        Writer.Write(byte.Parse(Splitter[5]));
                    }
                }
                Writer.Write((byte)MAWHCount);
                foreach (string Item in MAWH)
                {
                    if (Item != null && Item != "0")
                    {
                        string[] Splitter = Item.Split('-');
                        Writer.Write(uint.Parse(Splitter[0]));
                        Writer.Write(byte.Parse(Splitter[1]));
                        Writer.Write(byte.Parse(Splitter[2]));
                        Writer.Write(byte.Parse(Splitter[3]));
                        Writer.Write(byte.Parse(Splitter[4]));
                        Writer.Write(byte.Parse(Splitter[5]));
                    }
                }
                Writer.Write((byte)AZWHCount);
                foreach (string Item in AZWH)
                {
                    if (Item != null && Item != "0")
                    {
                        string[] Splitter = Item.Split('-');
                        Writer.Write(uint.Parse(Splitter[0]));
                        Writer.Write(byte.Parse(Splitter[1]));
                        Writer.Write(byte.Parse(Splitter[2]));
                        Writer.Write(byte.Parse(Splitter[3]));
                        Writer.Write(byte.Parse(Splitter[4]));
                        Writer.Write(byte.Parse(Splitter[5]));
                    }
                }
                Writer.Write((byte)Friends.Count);
                foreach (DictionaryEntry DE in Friends)
                {
                    Writer.Write((uint)DE.Key);
                    Writer.Write(Encoding.Default.GetBytes(((string)DE.Value).PadRight(16, (char)0x00)));
                }
                Writer.Write((byte)Enemies.Count);
                foreach (DictionaryEntry DE in Enemies)
                {
                    Writer.Write((uint)DE.Key);
                    Writer.Write(Encoding.Default.GetBytes(((string)DE.Value).PadRight(16, (char)0x00)));
                }
                Writer.Write((ushort)GuildID);
                Writer.Write((uint)KO);
                Writer.Write((uint)OldKO);
                Writer.Write((ushort)DisKO);
                Writer.Write((byte)dexp);
                Writer.Write((uint)dexptime);
                Writer.Write((uint)LuckTime);
                Writer.Write((byte)BanNB);
                Writer.Write((byte)LotoCount);

                Writer.Close();
                FStream.Dispose();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public bool Ready = true;

        public bool InventoryContains(uint Id, uint Amount)
        {
            Ready = false;
            uint Count = 0;

            foreach (string item in Inventory)
            {
                if (item != null && item != "")
                {
                    string[] Splitter = item.Split('-');
                    if (uint.Parse(Splitter[0]) == Id)
                        Count++;
                }
            }

            Ready = true;

            if (Count >= Amount)
                return true;
            else
                return false;
        }

        public uint ItemNext(uint Id)
        {
            Ready = false;
            int Count = 0;
            uint IUID = 0;

            foreach (string item in Inventory)
            {
                if (item != null && item != "")
                {
                    string[] Splitter = item.Split('-');
                    if (uint.Parse(Splitter[0]) == Id)
                    {
                        IUID = (uint)Inventory_UIDs[Count];
                        break;
                    }
                }
                Count++;
            }

            Ready = true;

            return IUID;
        }

        public string GetItem(uint ItemUID)
        {
            byte Pos = 0;
            foreach (uint UniqId in Inventory_UIDs)
            {
                if (UniqId == ItemUID)
                    return Inventory[Pos];
                Pos++;
            }
            return null;
        }

        public bool AddExp(ulong Amount, bool CountMisc)
        {
            if (DataBase.NeededXP(Level) == 0)
                return false;

            double BonusXp = AddExpPc + ((double)(Potency) / 100);
            ulong ExpPot = 1;
            ulong Lucky = 1;
            ulong Rb = 1;

            if (dexp == 1)
                ExpPot = 2;
            else if (dexp == 2)
                ExpPot = 3;
            else if (dexp == 3)
                ExpPot = 5;

            if (RBCount == 2)
                Rb = 3;

            if (LuckTime > 0 && Other.ChanceSuccess(10))
            {
                Lucky = 2;
                World.ShowEffect(this, "LuckyGuy");
            }

            Ready = false;

            if (DataBase.NeededXP(Level) != 0)
            {
                if (CountMisc && LocMap != 1767)
                    Exp += (ulong)(((Amount * DataBase.ExpRate * BonusXp * ExpPot) * Lucky) / Rb);
                else
                    Exp += (Amount / Rb);
            }

            bool Leveled = false;

            if (Exp > DataBase.NeededXP(Level) && DataBase.NeededXP(Level) != 0)
            {
                Leveled = true;
                while (Exp > DataBase.NeededXP(Level) && DataBase.NeededXP(Level) != 0)
                {
                    Exp -= DataBase.NeededXP(Level);
                    Level++;
                    if (Level == 3 || Level == 20)
                    {
                        if (Job < 16 && Job > 9)
                            LearnSkill(1110, 0);
                        if (Job < 16 && Job > 9 || Job < 26 && Job > 19)
                            LearnSkill(1015, 0);
                        if (Job < 26 && Job > 19)
                            LearnSkill(1025, 0);
                        XpCircle = 100;
                    }

                    if (RBCount == 0 && Level <= 120)
                        DataBase.GetStats(this);
                    else
                        StatP += 3;
                }
            }

            if (Leveled)
            {
                if (MyGuild != null)
                    MyGuild.Refresh(UID, Level);

                if (Level == 130)
                {
                    if (RBCount == 1)
                        if (FirstLevel > 130)
                            StatP += (uint)((FirstLevel - Level) * 3);

                    if (RBCount == 2)
                        if (SecondLevel > 130)
                            StatP += (uint)((SecondLevel - Level) * 3);

                    if (RBCount == 1)
                        if (FirstLevel > 130)
                            Level = FirstLevel;

                    if (RBCount == 2)
                        if (SecondLevel > 130)
                            Level = SecondLevel;
                }
                GetEquipStats(1, true);
                GetEquipStats(2, true);
                GetEquipStats(3, true);
                GetEquipStats(4, true);
                GetEquipStats(5, true);
                GetEquipStats(6, true);
                GetEquipStats(7, true);
                GetEquipStats(8, true);
                GetEquipStats(9, true);
                Calculation.SetMaxHP(this, false);
                Calculation.SetMaxMP(this, false);
                MinAtk = Str;
                MaxAtk = Str;
                Calculation.SetPotency(this);
                GetEquipStats(1, false);
                GetEquipStats(2, false);
                GetEquipStats(3, false);
                GetEquipStats(4, false);
                GetEquipStats(5, false);
                GetEquipStats(6, false);
                GetEquipStats(7, false);
                GetEquipStats(8, false);
                GetEquipStats(9, false);

                CurHP = MaxHP;
                World.LevelUp(this);
            }
            Ready = true;

            if (Leveled)
                return true;
            else
                return false;
        }

        public void AddSkillExp(short Type, double Amount)
        {
            Ready = false;
            Amount *= DataBase.ProfRate;

            if (Type >= 4000 && Type <= 4070 || Type == 9000)
                Amount = 1;
            if (Type == 3321 || Type == 1220)
                return;

            if (Skills.Contains((short)Type))
            {

                bool SkillLeveled = false;
                byte Skill_Lv = (byte)Skills[(short)Type];
                uint Skill_Exp;
                if (Skill_Exps.Contains((short)Type))
                    Skill_Exp = (uint)Skill_Exps[(short)Type];
                else
                    Skill_Exp = 0;

                int MaxSkillLv = 9;

                MaxSkillLv = DataBase.SkillsDone[(ushort)Type];

                Amount += (Amount * AddSpellPc);
                Skill_Exp += (uint)Amount;

                if (Skill_Exp >= DataBase.NeededSkillExp((short)Type, Skill_Lv) && Skill_Lv < MaxSkillLv)
                {
                    Skill_Lv++;
                    Skill_Exp = 0;
                    SkillLeveled = true;
                }
                if (SkillLeveled)
                {
                    Skills.Remove((short)Type);
                    Skills.Add((short)Type, Skill_Lv);
                    MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Le niveau de votre attaque a �t� augment�.", 2005));
                }
                if (Skill_Exps.Contains((short)Type))
                    Skill_Exps.Remove((short)Type);
                Skill_Exps.Add(Type, Skill_Exp);
                MyClient.SendPacket(CoServer.MyPackets.LearnSkill(Type, Skill_Lv, Skill_Exp));
            }

            Ready = true;
        }

        public void AddProfExp(short Type, double Amount)
        {
            Ready = false;
            Amount *= DataBase.ProfRate;
            if (Prof_Exps.Contains(Type))
            {
                bool ProfLeveled = false;
                byte Prof_Lev = (byte)Profs[Type];
                uint Prof_Exp = (uint)Prof_Exps[Type];

                if (Prof_Lev < 20)
                {
                    Prof_Exp += (uint)Amount;
                    if (Prof_Lev >= 20)
                    {

                        Prof_Lev = 20;
                        Prof_Exp = 0;
                        ProfLeveled = false;

                    }
                    if (Prof_Exp > DataBase.NeededProfXP(Prof_Lev))
                    {
                        Prof_Lev++;
                        Prof_Exp = 0;
                        ProfLeveled = true;
                    }
                    if (ProfLeveled)
                    {
                        Profs.Remove(Type);
                        Profs.Add(Type, Prof_Lev);
                        MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Le niveau de votre comp�tence a �t� augment�.", 2005));
                    }
                    Prof_Exps.Remove(Type);
                    Prof_Exps.Add(Type, Prof_Exp);
                    MyClient.SendPacket(CoServer.MyPackets.Prof(Type, Prof_Lev, Prof_Exp));
                }
            }
            else
            {
                byte Lev = 1;
                uint PExp = 0;
                Profs.Add(Type, Lev);
                Prof_Exps.Add(Type, PExp);
                MyClient.SendPacket(CoServer.MyPackets.Prof(Type, Lev, PExp));
            }
            Ready = true;
        }

        public bool WeaponSkill()
        {
            string[] Splitter;
            bool Use = false;
            int Hand = 1;

            if (Equips[5] != null && Equips[5] != "0")
                if (Other.ChanceSuccess(50))
                    Hand = 2;

            if (Equips[4] != null && Equips[4] != "0" && Hand == 1)
            {
                Splitter = Equips[4].Split('-');
                int WepType = (int)ItemHandler.WeaponType(uint.Parse(Splitter[0]));

                if (WepType == 480)
                {
                    if (Skills.Contains((short)7020))
                    {
                        byte SkillLvl = (byte)Skills[(short)7020];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)7020][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(7020, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 420 && !Use || WepType == 421 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)5030))
                        {
                            byte SkillLvl = (byte)Skills[(short)5030];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)5030][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(5030, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 561 && !Use)
                {
                    if (Skills.Contains((short)5010))
                    {
                        byte SkillLvl = (byte)Skills[(short)5010];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)5010][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(5010, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 560 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)1260))
                        {
                            byte SkillLvl = (byte)Skills[(short)1260];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)1260][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(1260, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 490 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)1290))
                        {
                            byte SkillLvl = (byte)Skills[(short)1290];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)1290][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(1290, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 481 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)7030))
                        {
                            byte SkillLvl = (byte)Skills[(short)7030];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)7030][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(7030, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 410 && !Use)//found weapon skills!
                {
                    if (Skills.Contains((short)1220))
                    {
                        byte SkillLvl = (byte)Skills[(short)1220];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)1220][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(1220, 0, 0, TargetUID);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 500 && !Use)//FreezingArrow && Poison
                {
                    if (Skills.Contains((short)5000))
                    {
                        byte SkillLvl = (byte)Skills[(short)5000];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)5000][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(5000, 0, 0, TargetUID);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 540 && !Use)
                {
                    if (Skills.Contains((short)1300))
                    {
                        byte SkillLvl = (byte)Skills[(short)1300];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)1300][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(1300, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 580 && !Use)
                {
                    if (Skills.Contains((short)5020))
                    {
                        byte SkillLvl = (byte)Skills[(short)5020];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)5020][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(5020, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 510 && !Use)
                {
                    if (Skills.Contains((short)1250))
                    {
                        byte SkillLvl = (byte)Skills[(short)1250];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)1250][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(1250, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 530 && !Use)
                {
                    if (Skills.Contains((short)5050))
                    {
                        byte SkillLvl = (byte)Skills[(short)5050];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)5050][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(5050, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 430 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)7000))
                        {
                            byte SkillLvl = (byte)Skills[(short)7000];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)7000][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(7000, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 450 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)7010))
                        {
                            byte SkillLvl = (byte)Skills[(short)7010];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)7010][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(7010, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 440 && !Use)
                {
                    if (Skills.Contains((short)7040))
                    {
                        byte SkillLvl = (byte)Skills[(short)7040];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)7040][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(7040, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 460 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)5040))
                        {
                            byte SkillLvl = (byte)Skills[(short)5040];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)5040][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(5040, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
            }
            if (Equips[5] != null && Equips[5] != "0" && Hand == 2)
            {
                Splitter = Equips[5].Split('-');
                int WepType = (int)ItemHandler.WeaponType(uint.Parse(Splitter[0]));

                if (WepType == 480 && !Use)
                {
                    if (Skills.Contains((short)7020))
                    {
                        byte SkillLvl = (byte)Skills[(short)7020];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)7020][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(7020, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 420 && !Use || WepType == 421 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)5030))
                        {
                            byte SkillLvl = (byte)Skills[(short)5030];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)5030][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(5030, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 561 && !Use)
                {
                    if (Skills.Contains((short)5010))
                    {
                        byte SkillLvl = (byte)Skills[(short)5010];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)7020][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(5010, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 560 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)1260))
                        {
                            byte SkillLvl = (byte)Skills[(short)1260];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)1260][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(1260, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 490 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)1290))
                        {
                            byte SkillLvl = (byte)Skills[(short)1290];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)1290][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(1290, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 481 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)7030))
                        {
                            byte SkillLvl = (byte)Skills[(short)7030];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)7030][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(7030, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 540 && !Use)
                {
                    if (Skills.Contains((short)1300))
                    {
                        byte SkillLvl = (byte)Skills[(short)1300];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)1300][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(1300, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 580 && !Use)
                {
                    if (Skills.Contains((short)5020))
                    {
                        byte SkillLvl = (byte)Skills[(short)5020];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)5020][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(5020, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 510 && !Use)
                {
                    if (Skills.Contains((short)1250))
                    {
                        byte SkillLvl = (byte)Skills[(short)1250];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)1250][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(1250, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 530 && !Use)
                {
                    if (Skills.Contains((short)5050))
                    {
                        byte SkillLvl = (byte)Skills[(short)5050];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)5050][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(5050, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 430 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)7000))
                        {
                            byte SkillLvl = (byte)Skills[(short)7000];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)7000][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(7000, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 450 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)7010))
                        {
                            byte SkillLvl = (byte)Skills[(short)7010];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)7010][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(7010, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 440 && !Use)
                {
                    if (Skills.Contains((short)7040))
                    {
                        byte SkillLvl = (byte)Skills[(short)7040];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)7040][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(7040, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 460 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)5040))
                        {
                            byte SkillLvl = (byte)Skills[(short)5040];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)5040][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(5040, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 410 && !Use)
                {
                    if (Skills.Contains((short)1220))
                    {
                        byte SkillLvl = (byte)Skills[(short)1220];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)1220][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(1220, 0, 0, TargetUID);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 500 && !Use)//FreezingArrow && Poison
                {
                    if (Skills.Contains((short)5000))
                    {
                        byte SkillLvl = (byte)Skills[(short)5000];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)5000][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(5000, 0, 0, TargetUID);
                            Use = true;
                        }
                    }
                }
            }
            return Use;
        }

        public void MissAttack(Monster Mob)
        {
            World.AttackMiss(this, AtkType, Mob.LocX, Mob.LocY);
        }
        public void MissAttack(SingleNPC NPC)
        {
            World.AttackMiss(this, AtkType, NPC.LocX, NPC.LocY);
        }
        public void MissAttack(Character Player)
        {
            World.AttackMiss(this, AtkType, Player.LocX, Player.LocY);
        }

        public void Attack()
        {
            LastAttack = DateTime.Now;
            if (!Alive)
            {
                MobTarget = null;
                SkillLoopingTarget = 0;
                TGTarget = null;
                PTarget = null;
                return;
            }

            if (PTarget != null)
                if (AtkType == 2 || AtkType == 25)
                {
                    if (!SMOn && !Accuracy && !AccuracyBuff)
                    {
                        if (!Other.ChanceSuccess(RealAgi / 1.7 + Math.Abs((110 - Level) / 2)))
                        {
                            MissAttack(PTarget);
                            return;
                        }
                    }
                    else if (SMOn || AccuracyBuff)
                    {
                        if (!Other.ChanceSuccess(RealAgi + Math.Abs((110 - Level) / 2)))
                        {
                            MissAttack(PTarget);
                            return;
                        }
                    }
                    else
                    {
                        if (!Other.ChanceSuccess(RealAgi * 1.2 + Math.Abs((110 - Level) / 2)))
                        {
                            MissAttack(PTarget);
                            return;
                        }
                    }
                }
            if (TGTarget != null)
                if (AtkType == 2 || AtkType == 25)
                {
                    if (!SMOn && !Accuracy && !AccuracyBuff)
                    {
                        if (!Other.ChanceSuccess(RealAgi / 1.7 + Math.Abs((110 - Level) / 2)))
                        {
                            MissAttack(TGTarget);
                            return;
                        }
                    }
                    else if (SMOn || AccuracyBuff)
                    {
                        if (!Other.ChanceSuccess(RealAgi + Math.Abs((110 - Level) / 2)))
                        {
                            MissAttack(TGTarget);
                            return;
                        }
                    }
                    else
                    {
                        if (!Other.ChanceSuccess(RealAgi * 1.2 + Math.Abs((110 - Level) / 2)))
                        {
                            MissAttack(TGTarget);
                            return;
                        }
                    }
                }
            if (MobTarget != null)
                if (AtkType == 2 || AtkType == 25)
                {
                    if (!SMOn && !Accuracy && !AccuracyBuff)
                    {
                        if (!Other.ChanceSuccess(RealAgi / 2 + Math.Abs((110 - Level) / 2)))
                        {
                            MissAttack(MobTarget);
                            return;
                        }
                    }
                    else if (SMOn || AccuracyBuff)
                    {
                        if (!Other.ChanceSuccess(RealAgi + Math.Abs((110 - Level) / 2)))
                        {
                            MissAttack(MobTarget);
                            return;
                        }
                    }
                    else
                    {
                        if (!Other.ChanceSuccess(RealAgi * 1.2 + Math.Abs((110 - Level) / 2)))
                        {
                            MissAttack(MobTarget);
                            return;
                        }
                    }
                }

            if (AtkType == 25 || AtkType == 2)
            {
                if (!WeaponSkill())
                {
                    if (MobTarget != null)
                        if (MobTarget.Alive)
                            if (MobTarget.LocMap == LocMap)
                                if (MyMath.GetDistance(LocX, LocY, MobTarget.LocX, MobTarget.LocY) < 6 || AtkType == 25 && MyMath.GetDistance(LocX, LocY, MobTarget.LocX, MobTarget.LocY) < 16)
                                {
                                    double Damage = MyMath.Generate((int)MinAtk, (int)MaxAtk);

                                    if (StigBuff)
                                        Damage = (Damage * (double)(1 + ((double)(10 + StigLevel) / 100)));

                                    Damage = BattleSystem.AdjustDamageUser2Monster((uint)Damage, this, MobTarget);

                                    if (AtkType == 2)
                                        Damage -= MobTarget.Defense;

                                    //if (AtkType == 25)
                                    //    Damage *= ((double)(100 - (MobTarget.Dodge / 1.5)) / 100);

                                    if (SMOn && MobTarget.Type != 6)
                                        Damage *= 10;

                                    if (SMOn && MobTarget.Type == 6)
                                        Damage *= 2;

                                    if (LuckTime > 0 && MyMath.Success(10))
                                    {
                                        Damage *= 2;
                                        World.ShowEffect(this, "LuckyGuy");
                                    }

                                    if (Damage < 1)
                                        Damage = 1;

                                    Damage = BattleSystem.AdjustMinDamageUser2Monster((uint)Damage, this, MobTarget);

                                    double ProfExpAdd = 0;
                                    uint MobCurHP = MobTarget.CurHP;

                                    if (MobTarget.Type == 5)
                                        Damage = 0;

                                    if (Potency < MobTarget.BattleLvl)
                                        Damage /= 100;

                                    if (MobTarget.LocMap == 2021 || MobTarget.LocMap == 2022 || MobTarget.LocMap == 2023 || MobTarget.LocMap == 2024)
                                        Damage /= 10;

                                    if (MobTarget.Type == 4)
                                        BlueName = true;

                                    if (MobTarget.GetDamage(this, (uint)Damage))
                                    {
                                        if (CycloneOn || SMOn)
                                        {
                                            KO++;
                                            MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Kills:" + KO, 2005));
                                        }
                                        if (LocMap == 2022)
                                        {
                                            ushort DisRequest = 0;

                                            if (Job > 9 && Job < 16)
                                                DisRequest = 800;
                                            if (Job > 19 && Job < 26)
                                                DisRequest = 900;
                                            if (Job > 39 && Job < 46)
                                                DisRequest = 1300;
                                            if (Job > 129 && Job < 136)
                                                DisRequest = 600;
                                            if (Job > 139 && Job < 146)
                                                DisRequest = 1000;

                                            if (MobTarget.Name == "TrollInfernal")
                                            {
                                                DisKO += 3;
                                                MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Dis Kills:" + DisKO + "/" + DisRequest, 2005));
                                            }
                                            else
                                            {
                                                DisKO++;
                                                MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Dis Kills:" + DisKO + "/" + DisRequest, 2005));
                                            }

                                            if (DisKO >= DisRequest)
                                                MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous avez tu� assez de monstre pour la qu�te.", 2011));
                                        }

                                        if (MobTarget.Name == "PlutoFinal")
                                        {
                                            AddItem("790001-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                                            Teleport(1020, 531, 482);
                                            MyClient.CurrentNPC = 1315;
                                            MyClient.SendPacket(CoServer.MyPackets.NPCSay("Vous avez vaincu PlutoFinal, pour vous remercier je vais vous donner un grand cadeau."));
                                            MyClient.SendPacket(CoServer.MyPackets.NPCLink("Merci!", 1));
                                            MyClient.SendPacket(CoServer.MyPackets.NPCSetFace(6));
                                            MyClient.SendPacket(CoServer.MyPackets.NPCFinish());

                                            foreach (KeyValuePair<uint, Character> DE in World.AllChars)
                                            {
                                                Character Chaar = (Character)DE.Value;
                                                if (Chaar.Name != Name)
                                                    if (Chaar.LocMap == 2021 || Chaar.LocMap == 2022 || Chaar.LocMap == 2023 || Chaar.LocMap == 2024)
                                                        Chaar.Teleport(1002, 430, 380);
                                            }
                                            World.SendMsgToAll(Name + " des " + MyGuild.Name + " a d�truit PlutoFinal! Les joueurs ont �t� t�l�port�s pour leurs protection.", "SYSTEM", 2011);
                                        }

                                        Attacking = false;
                                        if (/*Level >= 70 && */LocMap != 1767)
                                        {
                                            foreach (Character Member in Team)
                                            {
                                                if (!Member.Alive)
                                                    continue;

                                                double PlvlExp = 1;
                                                byte WatXP = 1;
                                                byte ExpPot = 1;
                                                byte MarriageXP = 1;

                                                if (Member.Job == 133 || Member.Job == 134 || Member.Job == 135)
                                                    WatXP = 2;
                                                if (Member.Spouse == Spouse)
                                                    MarriageXP = 2;
                                                if (MobTarget.Level - 20 <= Member.Level)
                                                    PlvlExp = 0.1;
                                                if (Member.dexp == 1)
                                                    ExpPot = 2;
                                                else if (Member.dexp == 2)
                                                    ExpPot = 3;
                                                else if (Member.dexp == 3)
                                                    ExpPot = 5;

                                                if (Member != null)
                                                    if (Member.LocMap == LocMap)
                                                        if (MyMath.GetDistance(Member.LocX, Member.LocY, LocX, LocY) < 30)
                                                        {
                                                            double TheXP = 0;
                                                            TheXP = ((((((52 + (Member.Level * 30)) * PlvlExp) * WatXP) * ExpPot) * MarriageXP) * AddExpPc);
                                                            Member.AddExp((ulong)TheXP, false);
                                                            Member.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Member.MyClient.MessageId, "SYSTEM", Member.Name, Member.Name + " a gagn� " + TheXP + " points d'exp�rience.", 2005));
                                                            if (Member.Level < 70 && MyTeamLeader.Level >= 70)
                                                            {
                                                                MyTeamLeader.VPs += (uint)((Member.Level * 17 / 13 * 12 / 2) + Member.Level * 3);
                                                                MyTeamLeader.MyClient.SendPacket(CoServer.MyPackets.SendMsg(Member.MyClient.MessageId, "SYSTEM", MyTeamLeader.Name, MyTeamLeader.Name + " a gagn� " + ((Member.Level * 17 / 13 * 12 / 2) + Member.Level * 3) + " points de vertu.", 2003));
                                                                foreach (Character AllMember in Team)
                                                                {
                                                                    if (AllMember != null)
                                                                        AllMember.MyClient.SendPacket(CoServer.MyPackets.SendMsg(AllMember.MyClient.MessageId, "SYSTEM", AllMember.Name, MyTeamLeader.Name + " a gagn�" + ((Member.Level * 17 / 13 * 12 / 2) + Member.Level * 3) + " points de vertu.", 2003));
                                                                }
                                                            }
                                                        }
                                            }
                                        }

                                        XpCircle++;
                                        if (CycloneOn || SMOn)
                                            ExtraXP += 820;

                                        TargetUID = 0;
                                        AddExp(BattleSystem.AdjustExp((uint)MobCurHP, this, MobTarget), true);
                                        ProfExpAdd = MobCurHP;
                                        MobTarget.Death = DateTime.Now;

                                        if (Equips[4] != null && Equips[4] != "0")
                                        {
                                            string[] Splitter2 = Equips[4].Split('-');
                                            if (ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 4 || ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 5 || ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 9)
                                                AddProfExp((short)ItemHandler.WeaponType(uint.Parse(Splitter2[0])), (double)(ProfExpAdd * AddProfPc));
                                        }
                                        if (Equips[5] != null && Equips[5] != "0")
                                        {
                                            string[] Splitter2 = Equips[5].Split('-');
                                            if (ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 4 || ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 5 || ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 9)
                                                AddProfExp((short)ItemHandler.WeaponType(uint.Parse(Splitter2[0])), (double)(ProfExpAdd * AddProfPc));
                                        }

                                        World.AttackMob(this, MobTarget, AtkType, (uint)Damage);
                                        World.AttackMob(this, MobTarget, 14, 0);

                                        World.MobDissappear(MobTarget);
                                        MobTarget.Death = DateTime.Now;

                                        MobTarget = null;
                                    }
                                    else
                                    {
                                        AddExp(BattleSystem.AdjustExp((uint)Damage, this, MobTarget), true);

                                        ProfExpAdd = Damage;

                                        if (Equips[4] != null && Equips[4] != "0")
                                        {
                                            string[] Splitter2 = Equips[4].Split('-');
                                            if (ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 4 || ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 5 || ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 9)
                                                AddProfExp((short)ItemHandler.WeaponType(uint.Parse(Splitter2[0])), (double)(ProfExpAdd * AddProfPc));
                                        }
                                        if (Equips[5] != null && Equips[5] != "0")
                                        {
                                            string[] Splitter2 = Equips[5].Split('-');
                                            if (ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 4 || ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 5 || ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 9)
                                                AddProfExp((short)ItemHandler.WeaponType(uint.Parse(Splitter2[0])), (double)(ProfExpAdd * AddProfPc));
                                        }

                                        World.AttackMob(this, MobTarget, AtkType, (uint)Damage);
                                    }
                                }
                                else
                                    MobTarget = null;

                    if (PTarget != null)
                        if (Other.CanAttack(this, PTarget))
                            if (PTarget.Alive)
                                if (LocMap == PTarget.LocMap)
                                    if (AtkType == 2 && MyMath.GetDistance(LocX, LocY, PTarget.LocX, PTarget.LocY) < 5 || AtkType == 25 && MyMath.GetDistance(LocX, LocY, PTarget.LocX, PTarget.LocY) < 16)
                                    {
                                        double reborn = 1;
                                        double Shield = 1;
                                        double IronShirt = 1;
                                        double BonusDef = 0;
                                        double ExtraDodge = 0;

                                        double Damage = MyMath.Generate((int)(MinAtk), (int)(MaxAtk));

                                        if (PTarget.MShieldBuff)
                                            BonusDef = (PTarget.Defense * (10 + PTarget.MShieldLevel * 5) / 100);

                                        if (StigBuff)
                                            Damage = (Damage * (double)(1 + ((double)(10 + StigLevel) / 100)));

                                        if (PTarget.DodgeBuff)
                                            ExtraDodge = (20 + PTarget.DodgeLevel * 5);

                                        if (SMOn)
                                            Damage *= 2;

                                        if (PTarget.RBCount == 1)
                                            reborn = 0.7;
                                        else if (PTarget.RBCount == 2)
                                            reborn = 0.5;

                                        if (PTarget.Shield)
                                            Shield = 3;

                                        if (PTarget.IronShirt)
                                            IronShirt = 4;

                                        if (AtkType == 25)
                                        {

                                            byte Dodge = (byte)(PTarget.Dodge + ExtraDodge);
                                            if (Dodge > 100)
                                                Dodge = 100;

                                            Damage *= (double)((100 - Dodge) / 100);
                                            Damage *= 0.12;

                                            if (CoServer.ADS)
                                                Damage += (ADS_Attack * 0.25);
                                        }
                                        else
                                            Damage -= ((PTarget.Defense + BonusDef) * Shield * IronShirt);
                                        Damage *= reborn;
                                        Damage *= PTarget.Bless;
                                        Damage *= PTarget.AddMythique;

                                        if (LuckTime > 0 && Other.ChanceSuccess(10))
                                        {
                                            Damage *= 2;
                                            World.ShowEffect(this, "LuckyGuy");
                                        }

                                        if (PTarget.LuckTime > 0 && Other.ChanceSuccess(10))
                                        {
                                            Damage = 1;
                                            World.ShowEffect(PTarget, "LuckyGuy");
                                        }

                                        if (Damage < 1)
                                            Damage = 1;

                                        if (PTarget.ReflectOn && Other.ChanceSuccess(15))
                                        {
                                            if (Damage > 2000)
                                                Damage = 2000;

                                            GetReflect((uint)Damage);
                                            World.ShowEffect(this, "WeaponReflect");
                                            Damage = 0;
                                        }
                                        PTarget.BlessEffect();

                                        if (PTarget.PKPoints < 100)
                                            if (!PTarget.BlueName)
                                                if (!Other.CanPK(LocMap))
                                                    BlueName = true;

                                        World.PVP(this, PTarget, AtkType, (uint)Damage);

                                        if (PTarget.GetHitDie((uint)Damage))
                                        {
                                            Attacking = false;
                                            if (!Other.CanPK(LocMap))
                                            {
                                                if (!PTarget.BlueName)
                                                    if (PTarget.PKPoints >= 30 && PTarget.PKPoints < 100) //Red Name
                                                    {
                                                        GotBlueName = DateTime.Now;
                                                        BlueName = true;
                                                    }
                                                    else if (PTarget.PKPoints < 30) //White Name
                                                    {
                                                        GotBlueName = DateTime.Now;
                                                        BlueName = true;

                                                        if (MyGuild != null)
                                                        {
                                                            if (MyGuild.Enemies.Contains(PTarget.GuildID))
                                                                PKPoints += 3;
                                                            else if (Enemies.Contains(PTarget.UID))
                                                                PKPoints += 5;
                                                            else
                                                                PKPoints += 10;
                                                        }
                                                        else
                                                        {
                                                            if (Enemies.Contains(PTarget.UID))
                                                                PKPoints += 5;
                                                            else
                                                                PKPoints += 10;
                                                        }

                                                        if (!PTarget.Enemies.Contains(UID))
                                                        {
                                                            Network.MsgFriend Msg = new Network.MsgFriend();
                                                            PTarget.MyClient.SendPacket(Msg.Create(UID, Name, true, 19));
                                                            PTarget.AddEnemy(this);
                                                        }

                                                        if (PKPoints > 30000)
                                                            PKPoints = 30000;
                                                    }

                                                ulong PkXp = PTarget.Exp / 100;
                                                if (PkXp < 0)
                                                    PkXp = PTarget.Exp;
                                                PTarget.Exp -= PkXp;
                                                AddExp(PkXp, false);
                                            }

                                            World.PVP(this, PTarget, 14, 0);
                                            PTarget.PTarget = null;
                                            PTarget.MobTarget = null;
                                            PTarget = null;
                                        }
                                    }
                                    else
                                        PTarget = null;

                    if (TGTarget != null)
                        if (Level >= TGTarget.Level)
                        {
                                double Damage = MyMath.Generate((int)(MinAtk), (int)(MaxAtk));
                                uint THP = TGTarget.CurHP;

                                if (StigBuff)
                                    Damage = (Damage * (double)(1 + ((double)(10 + StigLevel) / 100)));

                                if (AtkType == 25)
                                    Damage *= ((double)(100 - TGTarget.Dodge) / 100);

                                if (LuckTime > 0 && Other.ChanceSuccess(10))
                                {
                                    Damage *= 2;
                                    World.ShowEffect(this, "LuckyGuy");
                                }

                                if (Damage < 1)
                                    Damage = 1;

                                if (GuildID != 0)
                                {
                                    if (TGTarget.Sob == 2 && !MyGuild.HoldingPole)
                                    {
                                        Silvers += (uint)(Damage / 100);
                                        GuildDonation += (int)(Damage / 100);
                                        MyGuild.Refresh(UID, (int)GuildDonation);
                                        MyClient.SendPacket(CoServer.MyPackets.GuildInfo(MyGuild, this));
                                    }
                                }

                                double ExpQuality = 1;

                                if (TGTarget.Level + 5 < Level)
                                    ExpQuality = 0.1;

                                if (TGTarget.GetDamageDie((uint)Damage, this))
                                {
                                    AddExp((ulong)(THP / 10 * ExpQuality), true);
                                    uint ProfExpAdd = THP / 10;
                                    if (Equips[4] != null && Equips[4] != "0")
                                    {
                                        string[] Splitter2 = Equips[4].Split('-');
                                        if (ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 4 || ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 5 || ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 9)
                                            AddProfExp((short)ItemHandler.WeaponType(uint.Parse(Splitter2[0])), (uint)(ProfExpAdd * AddProfPc));
                                    }
                                    if (Equips[5] != null && Equips[5] != "0")
                                    {
                                        string[] Splitter2 = Equips[5].Split('-');
                                        if (ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 4 || ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 5 || ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 9)
                                            AddProfExp((short)ItemHandler.WeaponType(uint.Parse(Splitter2[0])), (uint)(ProfExpAdd * AddProfPc));
                                    };
                                    World.PlAttacksTG(TGTarget, this, (byte)AtkType, (uint)Damage);
                                    World.PlAttacksTG(TGTarget, this, 14, (uint)Damage);
                                }
                                else
                                {
                                    AddExp((ulong)(Damage / 10 * ExpQuality), true);
                                    uint ProfExpAdd = (uint)Damage / 10;

                                    if (Equips[4] != null && Equips[4] != "0")
                                    {
                                        string[] Splitter2 = Equips[4].Split('-');
                                        if (ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 4 || ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 5 || ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 9)
                                            AddProfExp((short)ItemHandler.WeaponType(uint.Parse(Splitter2[0])), (uint)(ProfExpAdd * AddProfPc));
                                    }
                                    if (Equips[5] != null && Equips[5] != "0")
                                    {
                                        string[] Splitter2 = Equips[5].Split('-');
                                        if (ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 4 || ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 5 || ItemHandler.ItemType(uint.Parse(Splitter2[0])) == 9)
                                            AddProfExp((short)ItemHandler.WeaponType(uint.Parse(Splitter2[0])), (uint)(ProfExpAdd * AddProfPc));
                                    };
                                    World.PlAttacksTG(TGTarget, this, (byte)AtkType, (uint)Damage);
                                }
                        }
                        else
                            MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas attaquer car vous n'avez pas le bon niveau. Allez � la place de votre niveau.", 2000));
                }
                GemEffect();
            }
            else if (AtkType == 21)
            {
                if (SkillLooping != 0)
                {
                    if (SkillLoopingTarget != 0)
                    {
                        if (SkillLoopingTarget >= 800000 && SkillLoopingTarget < 1000000)
                        {
                            Monster Targ = (Monster)World.AllMobs[SkillLoopingTarget];
                            if (Targ == null || !Targ.Alive)
                                return;
                            else
                                UseSkill(SkillLooping, (ushort)Targ.LocX, (ushort)Targ.LocY, SkillLoopingTarget);

                        }
                        else if (SkillLoopingTarget < 800000 && SkillLoopingTarget >= 700000)
                        {
                            SingleNPC Targ = (SingleNPC)NPCs.AllNPCs[SkillLoopingTarget];
                            if (Targ == null)
                                return;
                            else
                                UseSkill(SkillLooping, (ushort)Targ.LocX, (ushort)Targ.LocY, SkillLoopingTarget);

                        }
                        else
                        {
                            Character Targ = (Character)World.AllChars[SkillLoopingTarget];
                            if (Targ == null || !Targ.Alive)
                                return;
                            else
                                UseSkill(SkillLooping, Targ.LocX, Targ.LocY, SkillLoopingTarget);
                        }
                    }
                    else
                        UseSkill(SkillLooping, SkillLoopingX, SkillLoopingY, 0);
                }
                else
                    SkillLooping = 0;
            }
        }

        public void GetEquipStats(byte Pos, bool UnEquip)
        {
            Ready = false;

            Calculation.SetAttack(this);
            Calculation.SetDefence(this);
            Calculation.SetPotency(this);
            Calculation.SetMaxHP(this, false);
            Calculation.SetMaxMP(this, false);

            if (Equips[Pos] != "0" && Equips[Pos] != null)
            {
                string[] Splitter = Equips[Pos].Split('-');

                uint ItemId = uint.Parse(Splitter[0]);
                byte ItemPlus = byte.Parse(Splitter[1]);
                byte ItemEnchant = byte.Parse(Splitter[3]);
                byte ItemGem1 = byte.Parse(Splitter[4]);
                byte ItemGem2 = byte.Parse(Splitter[5]);

                string PItemID = ItemHandler.GetBonusId(ItemId, Pos).ToString();
                string[] PItem = new string[10] { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0" };

                foreach (string[] Item in DataBase.DBPlusInfo)
                {
                    if (PItemID == Item[0])
                        if (ItemPlus == byte.Parse(Item[1]))
                        {
                            PItem = Item;
                            break;
                        }
                }

                ushort AddHP = (ushort)(ItemEnchant + ushort.Parse(PItem[2]) + ItemHandler.GetItemHP(ItemId));
                ushort AddMP = (ushort)ItemHandler.GetItemMP(ItemId);

                if (!UnEquip)
                {
                    MaxHP += AddHP;
                    MaxMP += AddMP;

                    //RainbowGem
                    if (ItemGem1 == 31)
                        AddExpPc += 0.1;
                    if (ItemGem2 == 31)
                        AddExpPc += 0.1;
                    if (ItemGem1 == 32)
                        AddExpPc += 0.15;
                    if (ItemGem2 == 32)
                        AddExpPc += 0.15;
                    if (ItemGem1 == 33)
                        AddExpPc += 0.25;
                    if (ItemGem2 == 33)
                        AddExpPc += 0.25;

                    //VioletGem
                    if (ItemGem1 == 51)
                        AddProfPc += 0.3;
                    if (ItemGem2 == 51)
                        AddProfPc += 0.3;
                    if (ItemGem1 == 52)
                        AddProfPc += 0.5;
                    if (ItemGem2 == 52)
                        AddProfPc += 0.5;
                    if (ItemGem1 == 53)
                        AddProfPc += 1;
                    if (ItemGem2 == 53)
                        AddProfPc += 1;

                    //MoonGem
                    if (ItemGem1 == 61)
                        AddSpellPc += 1.15;
                    if (ItemGem2 == 61)
                        AddSpellPc += 1.15;
                    if (ItemGem1 == 62)
                        AddSpellPc += 1.30;
                    if (ItemGem2 == 62)
                        AddSpellPc += 1.30;
                    if (ItemGem1 == 63)
                        AddSpellPc += 1.50;
                    if (ItemGem2 == 63)
                        AddSpellPc += 1.50;
                }
                else
                {
                    MaxHP -= AddHP;
                    MaxMP -= AddMP;

                    //RainbowGem
                    if (ItemGem1 == 31)
                        AddExpPc -= 0.1;
                    if (ItemGem2 == 31)
                        AddExpPc -= 0.1;
                    if (ItemGem1 == 32)
                        AddExpPc -= 0.15;
                    if (ItemGem2 == 32)
                        AddExpPc -= 0.15;
                    if (ItemGem1 == 33)
                        AddExpPc -= 0.25;
                    if (ItemGem2 == 33)
                        AddExpPc -= 0.25;

                    //VioletGem
                    if (ItemGem1 == 51)
                        AddProfPc -= 0.3;
                    if (ItemGem2 == 51)
                        AddProfPc -= 0.3;
                    if (ItemGem1 == 52)
                        AddProfPc -= 0.5;
                    if (ItemGem2 == 52)
                        AddProfPc -= 0.5;
                    if (ItemGem1 == 53)
                        AddProfPc -= 1;
                    if (ItemGem2 == 53)
                        AddProfPc -= 1;

                    //MoonGem
                    if (ItemGem1 == 61)
                        AddSpellPc -= 1.15;
                    if (ItemGem2 == 61)
                        AddSpellPc -= 1.15;
                    if (ItemGem1 == 62)
                        AddSpellPc -= 1.30;
                    if (ItemGem2 == 62)
                        AddSpellPc -= 1.30;
                    if (ItemGem1 == 63)
                        AddSpellPc -= 1.50;
                    if (ItemGem2 == 63)
                        AddSpellPc -= 1.50;
                }
            }
            Ready = true;
        }

        public void UnEquip(byte From)
        {
            if (ItemsInInventory > 40)
                return;
            if (MyClient.Status == 7)
                return;

            Ready = false;
            try
            {
                GetEquipStats(From, true);

                MyClient.SendPacket(CoServer.MyPackets.RemoveItem((long)Equips_UIDs[From], From, 6));
                AddItem(Equips[From], 0, Equips_UIDs[From]);
                Equips[From] = null;
                Equips_UIDs[From] = 0;

                Calculation.SetAttack(this);
                Calculation.SetDefence(this);
                Calculation.SetPotency(this);
            }
            catch (Exception Exc)
            {
                Program.WriteLine(Exc.ToString());
            }
            Ready = true;
        }

        public void UseItem(ulong ItemUID, string Item)
        {
            Ready = false;
            string[] ItemParts = Item.Split('-');

            if (ItemParts[0] == "720027")
            {
                if (ItemsInInventory <= 30)
                {
                    for (byte i = 0; i < 10; i++)
                        AddItem("1088001-0-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "720028")
            {
                if (ItemsInInventory <= 30)
                {
                    for (byte i = 0; i < 10; i++)
                        AddItem("1088000-0-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "722059") //Bo�teDM�t�ores
            {
                if (ItemsInInventory <= 30)
                {
                    for (byte i = 0; i < 10; i++)
                        AddItem("720027-0-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "722060") //Bo�teDPerles
            {
                if (ItemsInInventory <= 30)
                {
                    for (byte i = 0; i < 10; i++)
                        AddItem("720028-0-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "723712")
            {
                if (ItemsInInventory <= 35)
                {
                    for (byte i = 0; i < 5; i++)
                        AddItem("730001-1-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "723728")
            {
                if (ItemsInInventory <= 35)
                {
                    for (byte i = 0; i < 5; i++)
                        AddItem("730002-2-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "723729")
            {
                if (ItemsInInventory <= 35)
                {
                    for (byte i = 0; i < 5; i++)
                        AddItem("730003-3-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "723730")
            {
                if (ItemsInInventory <= 35)
                {
                    for (byte i = 0; i < 5; i++)
                        AddItem("730004-4-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "723713")
            {
                Silvers += 300000;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723714")
            {
                Silvers += 800000;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723715")
            {
                Silvers += 1200000;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723716")
            {
                Silvers += 1800000;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723717")
            {
                Silvers += 5000000;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723718")
            {
                Silvers += 20000000;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723719")
            {
                Silvers += 25000000;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723720")
            {
                Silvers += 80000000;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723721")
            {
                Silvers += 100000000;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723722")
            {
                Silvers += 300000000;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723723")
            {
                Silvers += 500000000;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "720028")
            {
                if (ItemsInInventory <= 30)
                {
                    for (byte i = 0; i < 10; i++)
                        AddItem("1088000-0-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "721540")
            {
                if (ItemsInInventory <= 36)
                {
                    for (byte i = 0; i < 3; i++)
                        AddItem("1088001-0-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                    if (Other.ChanceSuccess(50))
                        AddItem("1088000-0-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "723700") // ExpBall
            {
                if (Level < 100)
                    AddExp((ulong)(1295000 + Level * 50000), false);
                else if (Level < 110)
                    AddExp((ulong)(1395000 + Level * 80000), false);
                else if (Level < 115)
                    AddExp((ulong)(1595000 + Level * 100000), false);
                else if (Level < 120)
                    AddExp((ulong)(1895000 + Level * 120000), false);
                else if (Level < 125)
                    AddExp((ulong)(2095000 + Level * 150000), false);
                else if (Level < 130)
                    AddExp((ulong)(2395000 + Level * 180000), false);
                else if (Level < 255)
                    AddExp((ulong)(2895000 + Level * 200000), false);

                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723017")//PotXp
            {
                dexptime = 3600;
                dexp = 1;
                MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 19, dexptime));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "720021") //GuildTransport1
            {
                if (MyGuild == World.PoleHolder && GuildPosition == 100)
                    if (!Other.CanPK(LocMap))
                    {
                        SingleNPC OldNPC = new SingleNPC(614, 1450, 2, 0, (short)DataBase.GC1X, (short)DataBase.GC1Y, (short)DataBase.GC1Map, 0);
                        NPCs.AllNPCs.Remove(614);
                        World.RemoveEntity(OldNPC);

                        DataBase.ChangeGC("GC1", LocMap, LocX, LocY);

                        SingleNPC NewNPC = new SingleNPC(614, 1450, 2, 0, (short)DataBase.GC1X, (short)DataBase.GC1Y, (short)DataBase.GC1Map, 0);
                        NPCs.AllNPCs.Add(614, NewNPC);
                        World.SpawnNPC(NewNPC);

                        RemoveItem(ItemUID);
                    }
            }
            else if (ItemParts[0] == "720022") //GuildTransport2
            {
                if (MyGuild == World.PoleHolder && GuildPosition == 100)
                    if (!Other.CanPK(LocMap))
                    {
                        SingleNPC OldNPC = new SingleNPC(615, 1460, 2, 0, (short)DataBase.GC2X, (short)DataBase.GC2Y, (short)DataBase.GC2Map, 0);
                        NPCs.AllNPCs.Remove(615);
                        World.RemoveEntity(OldNPC);

                        DataBase.ChangeGC("GC2", LocMap, LocX, LocY);

                        SingleNPC NewNPC = new SingleNPC(615, 1460, 2, 0, (short)DataBase.GC2X, (short)DataBase.GC2Y, (short)DataBase.GC2Map, 0);
                        NPCs.AllNPCs.Add(615, NewNPC);
                        World.SpawnNPC(NewNPC);

                        RemoveItem(ItemUID);
                    }
            }
            else if (ItemParts[0] == "720023") //GuildTransport3
            {
                if (MyGuild == World.PoleHolder && GuildPosition == 100)
                    if (!Other.CanPK(LocMap))
                    {
                        SingleNPC OldNPC = new SingleNPC(616, 1470, 2, 0, (short)DataBase.GC3X, (short)DataBase.GC3Y, (short)DataBase.GC3Map, 0);
                        NPCs.AllNPCs.Remove(616);
                        World.RemoveEntity(OldNPC);

                        DataBase.ChangeGC("GC3", LocMap, LocX, LocY);

                        SingleNPC NewNPC = new SingleNPC(616, 1470, 2, 0, (short)DataBase.GC3X, (short)DataBase.GC3Y, (short)DataBase.GC3Map, 0);
                        NPCs.AllNPCs.Add(616, NewNPC);
                        World.SpawnNPC(NewNPC);

                        RemoveItem(ItemUID);
                    }
            }
            else if (ItemParts[0] == "720024") //GuildTransport4
            {
                if (MyGuild == World.PoleHolder && GuildPosition == 100)
                    if (!Other.CanPK(LocMap))
                    {
                        SingleNPC OldNPC = new SingleNPC(617, 1480, 2, 0, (short)DataBase.GC4X, (short)DataBase.GC4Y, (short)DataBase.GC4Map, 0);
                        NPCs.AllNPCs.Remove(617);
                        World.RemoveEntity(OldNPC);

                        DataBase.ChangeGC("GC4", LocMap, LocX, LocY);

                        SingleNPC NewNPC = new SingleNPC(617, 1480, 2, 0, (short)DataBase.GC4X, (short)DataBase.GC4Y, (short)DataBase.GC4Map, 0);
                        NPCs.AllNPCs.Add(617, NewNPC);
                        World.SpawnNPC(NewNPC);

                        RemoveItem(ItemUID);
                    }
            }
            else if (ItemParts[0] == "723701") //ExemptionToken
            {
                if (RBCount == 1)
                {
                    if (Level >= 120)
                    {
                        if (Job == 15 || Job == 25 || Job == 45 || Job == 135 || Job == 145)
                        {
                            MyClient.CurrentNPC = 128;
                            MyClient.SendPacket(CoServer.MyPackets.NPCSay("F�licitation, vous avez tous les pr�requis! En quel classe voulez-vous rena�tre?"));
                            MyClient.SendPacket(CoServer.MyPackets.NPCLink("Brave.", 11));
                            MyClient.SendPacket(CoServer.MyPackets.NPCLink("Guerrier.", 21));
                            MyClient.SendPacket(CoServer.MyPackets.NPCLink("Archer.", 41));
                            MyClient.SendPacket(CoServer.MyPackets.NPCLink("Tao�ste d'eau.", 132));
                            MyClient.SendPacket(CoServer.MyPackets.NPCLink("Tao�ste de feu.", 142));
                            MyClient.SendPacket(CoServer.MyPackets.NPCLink("Je dois r�fl�chir.", 255));
                            MyClient.SendPacket(CoServer.MyPackets.NPCSetFace(6));
                            MyClient.SendPacket(CoServer.MyPackets.NPCFinish());
                        }
                        else
                        {
                            MyClient.SendPacket(CoServer.MyPackets.NPCSay("Vous devez rentrer en fonction avant de rena�tre pour la seconde fois!"));
                            MyClient.SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                            MyClient.SendPacket(CoServer.MyPackets.NPCSetFace(6));
                            MyClient.SendPacket(CoServer.MyPackets.NPCFinish());
                        }
                    }
                    else
                    {
                        MyClient.SendPacket(CoServer.MyPackets.NPCSay("Vous devez �tre niveau 120 minimum!"));
                        MyClient.SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                        MyClient.SendPacket(CoServer.MyPackets.NPCSetFace(6));
                        MyClient.SendPacket(CoServer.MyPackets.NPCFinish());
                    }
                }
                else
                {
                    MyClient.SendPacket(CoServer.MyPackets.NPCSay("Vous n'�tes pas de premi�re renaissance je ne peux pas vous aider!"));
                    MyClient.SendPacket(CoServer.MyPackets.NPCLink("Ok.", 255));
                    MyClient.SendPacket(CoServer.MyPackets.NPCSetFace(6));
                    MyClient.SendPacket(CoServer.MyPackets.NPCFinish());
                }
            }
            else if (ItemParts[0] == "1000000")
            {
                CurHP += 20;
                if (CurHP > MaxHP)
                    CurHP = MaxHP;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1000010")
            {
                CurHP += 100;
                if (CurHP > MaxHP)
                    CurHP = MaxHP;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1000020")
            {
                CurHP += 250;
                if (CurHP > MaxHP)
                    CurHP = MaxHP;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1000030")
            {
                CurHP += 500;
                if (CurHP > MaxHP)
                    CurHP = MaxHP;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1002000")
            {
                CurHP += 800;
                if (CurHP > MaxHP)
                    CurHP = MaxHP;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1002010")
            {
                CurHP += 1200;
                if (CurHP > MaxHP)
                    CurHP = MaxHP;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1002020")
            {
                CurHP += 2000;
                if (CurHP > MaxHP)
                    CurHP = MaxHP;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1001000")
            {
                CurMP += 70;
                if (CurMP > MaxMP)
                    CurMP = MaxMP;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1001010")
            {
                CurMP += 200;
                if (CurMP > MaxMP)
                    CurMP = MaxMP;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1001020")
            {
                CurMP += 450;
                if (CurMP > MaxMP)
                    CurMP = MaxMP;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1001030")
            {
                CurMP += 1000;
                if (CurMP > MaxMP)
                    CurMP = MaxMP;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1001040")
            {
                CurMP += 2000;
                if (CurMP > MaxMP)
                    CurMP = MaxMP;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1002030")
            {
                CurMP += 3000;
                if (CurMP > MaxMP)
                    CurMP = MaxMP;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1002040")
            {
                CurMP += 4500;
                if (CurMP > MaxMP)
                    CurMP = MaxMP;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1002050")
            {
                CurHP += 3000;
                if (CurHP > MaxHP)
                    CurHP = MaxHP;
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "720010")
            {
                if (ItemsInInventory <= 37)
                {
                    for (byte i = 0; i < 3; i++)
                        AddItem("1000030-0-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "720011")
            {
                if (ItemsInInventory <= 37)
                {
                    for (byte i = 0; i < 3; i++)
                        AddItem("1002000-0-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "720012")
            {
                if (ItemsInInventory <= 37)
                {
                    for (byte i = 0; i < 3; i++)
                        AddItem("1002010-0-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "720013")
            {
                if (ItemsInInventory <= 37)
                {
                    for (byte i = 0; i < 3; i++)
                        AddItem("1002020-0-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "723711")
            {
                if (ItemsInInventory <= 35)
                {
                    for (byte i = 0; i < 5; i++)
                        AddItem("1088001-0-0-0-0-0", 0, (uint)Program.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "723583")
            {
                if (Model == 1004)
                    Model -= 1;
                else if (Model == 1003)
                    Model += 1;
                if (Model == 2002)
                    Model -= 1;
                else if (Model == 2001)
                    Model += 1;


                MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 12, ulong.Parse(Avatar.ToString() + Model.ToString())));
                World.UpdateSpawn(this);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1060020")
            {
                if (CurHP != 0)
                {
                    if (LocMap == 6001 || LocMap == 6000 || LocMap == 1505 || LocMap == 1038)
                    {
                        RemoveItem(ItemUID);
                        CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin ici!", 2005);
                    }
                    else
                    {
                        Teleport(1002, 429, 378);
                        RemoveItem(ItemUID);
                    }
                }
                else
                    CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin lorsque vous �tes mort!", 2005);
            }
            else if (ItemParts[0] == "1060021")
            {
                if (CurHP != 0)
                {
                    if (LocMap == 6001 || LocMap == 6000 || LocMap == 1505 || LocMap == 1038)
                    {
                        RemoveItem(ItemUID);
                        CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin ici!", 2005);
                    }
                    else
                    {
                        Teleport(1000, 500, 650);
                        RemoveItem(ItemUID);
                    }
                }
                else
                    CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin lorsque vous �tes mort!", 2005);
            }
            else if (ItemParts[0] == "1060022")
            {
                if (CurHP != 0)
                {
                    if (LocMap == 6001 || LocMap == 6000 || LocMap == 1505 || LocMap == 1038)
                    {
                        RemoveItem(ItemUID);
                        CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin ici!", 2005);
                    }
                    else
                    {
                        Teleport(1020, 565, 562);
                        RemoveItem(ItemUID);
                    }
                }
                else
                    CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin lorsque vous �tes mort!", 2005);
            }
            else if (ItemParts[0] == "1060023")
            {
                if (CurHP != 0)
                {
                    if (LocMap == 6001 || LocMap == 6000 || LocMap == 1505 || LocMap == 1038)
                    {
                        RemoveItem(ItemUID);
                        CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin ici!", 2005);
                    }
                    else
                    {
                        Teleport(1011, 188, 264);
                        RemoveItem(ItemUID);
                    }
                }
                else
                    CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin lorsque vous �tes mort!", 2005);
            }
            else if (ItemParts[0] == "1060024")
            {
                if (CurHP != 0)
                {
                    if (LocMap == 6001 || LocMap == 6000 || LocMap == 1505 || LocMap == 1038)
                    {
                        RemoveItem(ItemUID);
                        CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin ici!", 2005);
                    }
                    else
                    {
                        Teleport(1015, 717, 571);
                        RemoveItem(ItemUID);
                    }
                }
                else
                    CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin lorsque vous �tes mort!", 2005);
            }
            else if (ItemParts[0] == "1060030")
            {
                Hair = ushort.Parse("3" + Hair.ToString()[1] + Hair.ToString()[2]);
                RemoveItem(ItemUID);
                World.UpdateSpawn(this);
            }
            else if (ItemParts[0] == "1060040")
            {
                Hair = ushort.Parse("9" + Hair.ToString()[1] + Hair.ToString()[2]);
                RemoveItem(ItemUID);
                World.UpdateSpawn(this);
            }
            else if (ItemParts[0] == "1060050")
            {
                Hair = ushort.Parse("8" + Hair.ToString()[1] + Hair.ToString()[2]);
                RemoveItem(ItemUID);
                World.UpdateSpawn(this);
            }
            else if (ItemParts[0] == "1060060")
            {
                Hair = ushort.Parse("7" + Hair.ToString()[1] + Hair.ToString()[2]);
                RemoveItem(ItemUID);
                World.UpdateSpawn(this);
            }
            else if (ItemParts[0] == "1060070")
            {
                Hair = ushort.Parse("6" + Hair.ToString()[1] + Hair.ToString()[2]);
                RemoveItem(ItemUID);
                World.UpdateSpawn(this);
            }
            else if (ItemParts[0] == "1060080")
            {
                Hair = ushort.Parse("5" + Hair.ToString()[1] + Hair.ToString()[2]);
                RemoveItem(ItemUID);
                World.UpdateSpawn(this);
            }
            else if (ItemParts[0] == "1060090")
            {
                Hair = ushort.Parse("4" + Hair.ToString()[1] + Hair.ToString()[2]);
                RemoveItem(ItemUID);
                World.UpdateSpawn(this);
            }
            else if (ItemParts[0] == "723727")
            {
                if (PKPoints <= 30)
                    PKPoints = 0;
                else
                    PKPoints -= 30;

                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723584")
            {
                string[] item = Equips[3].Split('-');
                if (ItemHandler.WeaponType(uint.Parse(item[0])) != 132)
                {
                    string newitem = item[0];
                    newitem = newitem.Remove(newitem.Length - 3, 1);
                    newitem = newitem.Insert(newitem.Length - 2, "2");
                    Equips[3] = newitem + "-" + item[1] + "-" + item[2] + "-" + item[3] + "-" + item[4] + "-" + item[5];
                    MyClient.SendPacket(CoServer.MyPackets.AddItem((long)Equips_UIDs[3], int.Parse(newitem), byte.Parse(item[1]), byte.Parse(item[2]), byte.Parse(item[3]), byte.Parse(item[4]), byte.Parse(item[5]), 3, 100, 100));
                }
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1060101")
            {
                if (Job >= 143)
                {
                    LearnSkill(1165, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "1060100")
            {
                if (Job >= 143)
                {
                    LearnSkill(1160, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "725025")
            {
                if (Job < 26 && Job > 19 && Level >= 40)
                {
                    LearnSkill(1320, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "725026")
            {
                LearnSkill(5010, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725027")
            {
                LearnSkill(5020, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725028")
            {
                if (Job > 130 && Level >= 70)
                {
                    LearnSkill(5001, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "725029")
            {
                LearnSkill(5030, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725030")
            {
                LearnSkill(5040, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725031")
            {
                LearnSkill(5050, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725040")
            {
                LearnSkill(7000, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725041")
            {
                LearnSkill(7010, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725042")
            {
                LearnSkill(7020, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725043")
            {
                LearnSkill(7030, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725044")
            {
                LearnSkill(7040, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725045")
            {
                LearnSkill(1220, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725018")
            {
                LearnSkill(1380, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725019")
            {
                LearnSkill(1385, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725020")
            {
                LearnSkill(1390, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725021")
            {
                LearnSkill(1395, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725022")
            {
                LearnSkill(1400, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725023")
            {
                LearnSkill(1405, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725024")
            {
                LearnSkill(1410, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725000")
            {
                if (Spi >= 20)
                {
                    LearnSkill(1000, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "725001")
            {
                if (Spi >= 80)
                {
                    LearnSkill(1001, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "725002")
            {
                if (Spi >= 160 && Job >= 143)
                {
                    LearnSkill(1002, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "725003")
            {
                if (Spi >= 30)
                {
                    LearnSkill(1005, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "725004")
            {
                if (Spi >= 25)
                {
                    LearnSkill(1010, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "725005")
            {
                LearnSkill(1045, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725010")
            {
                LearnSkill(1046, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725011")
            {
                LearnSkill(1250, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725012")
            {
                LearnSkill(1260, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725013")
            {
                LearnSkill(1290, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725014")
            {
                LearnSkill(1300, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725015")
            {
                if (Job > 129 && Job < 136)
                {
                    LearnSkill(1350, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "725016")
            {
                LearnSkill(1360, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "720393") //ExpPill
            {
                MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Votre exp�rience est 3x plus grande lors de l'xp!", 2005));
                dexptime = 7200;
                dexp = 2;
                MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 19, dexptime));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "720394") //ExpAmrita
            {
                MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Votre exp�rience est 5x plus grande lors de l'xp!", 2005));
                dexptime = 7200;
                dexp = 3;
                MyClient.SendPacket(CoServer.MyPackets.Vital(UID, 19, dexptime));
                RemoveItem(ItemUID);
            }
            else
                MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser cet objet.", 2005));
            Ready = true;
        }

        public void RemoveItem(ulong UID)
        {
            int count = 0;
            foreach (ulong uid in Inventory_UIDs)
            {
                count++;
                if (UID == uid)
                {
                    count--;
                    byte Do = (byte)(ItemsInInventory - count);
                    for (int p = 0; p < Do; p++)
                    {
                        Inventory[count + p] = Inventory[count + p + 1];
                        Inventory_UIDs[count + p] = Inventory_UIDs[count + p + 1];
                    }
                    Inventory[ItemsInInventory] = null;
                    Inventory_UIDs[ItemsInInventory] = 0;
                    ItemsInInventory -= 1;

                    MyClient.SendPacket(CoServer.MyPackets.RemoveItem((long)UID, 0, 3));
                    break;
                }
            }
        }

        public void LearnSkill(short SkillId, byte SkillLvl)
        {
            Ready = false;

            if (!Skills.Contains((short)SkillId))
            {
                Skills.Add((short)SkillId, SkillLvl);
                Skill_Exps.Add((short)SkillId, SkillExpNull);
            }

            MyClient.SendPacket(CoServer.MyPackets.LearnSkill(SkillId, SkillLvl, 0));
            Ready = true;
        }

        public void LearnSkill2(short SkillId, byte SkillLvl)
        {
            Ready = false;

            if (!Skills.Contains((short)SkillId))
            {
                Skills.Add((short)SkillId, SkillLvl);
                Skill_Exps.Add((short)SkillId, SkillExpNull);
            }
            else
            {
                Skills.Remove((short)SkillId);
                Skill_Exps.Remove((short)SkillId);
                Skills.Add((short)SkillId, SkillLvl);
                Skill_Exps.Add((short)SkillId, SkillExpNull);
            }

            MyClient.SendPacket(CoServer.MyPackets.LearnSkill(SkillId, SkillLvl, 0));
            Ready = true;
        }

        public void SendProfs()
        {
            Ready = false;
            IDictionaryEnumerator IE = Profs.GetEnumerator();

            while (IE.MoveNext())
            {
                short prof_id = (short)IE.Key;
                byte prof_lvl = (byte)IE.Value;
                uint prof_exp = (uint)Prof_Exps[prof_id];

                MyClient.SendPacket(CoServer.MyPackets.Prof(prof_id, prof_lvl, prof_exp));
            }
            Ready = true;
        }

        public void SendSkills()
        {
            Ready = false;
            IDictionaryEnumerator IE = Skills.GetEnumerator();

            while (IE.MoveNext())
            {
                short skill_id = (short)IE.Key;
                byte skill_lvl = (byte)IE.Value;
                uint skill_exp = (uint)Skill_Exps[skill_id];

                MyClient.SendPacket(CoServer.MyPackets.LearnSkill(skill_id, skill_lvl, skill_exp));
            }
            Ready = true;
        }

        public void RemoveWHItem(ulong UID)
        {
            Ready = false;
            int count = 0;
            int whcount = 0;

            foreach (uint[] wh in WHIDs)
            {
                count = 0;

                foreach (uint uid in wh)
                {
                    if (uid == UID)
                    {
                        if (whcount == 0)
                        {
                            byte Do = (byte)(TCWHCount - count - 1);
                            for (int p = 0; p < Do; p++)
                            {
                                TCWH[count + p] = TCWH[count + p + 1];
                                WHIDs[whcount][count + p] = WHIDs[whcount][count + p + 1];
                            }
                            TCWH[TCWHCount - 1] = null;
                            WHIDs[whcount][TCWHCount - 1] = 0;
                            TCWHCount -= 1;
                        }
                        else if (whcount == 1)
                        {
                            byte Do = (byte)(PCWHCount - count - 1);
                            for (int p = 0; p < Do; p++)
                            {
                                PCWH[count + p] = PCWH[count + p + 1];
                                WHIDs[whcount][count + p] = WHIDs[whcount][count + p + 1];
                            }
                            PCWH[PCWHCount - 1] = null;
                            WHIDs[whcount][PCWHCount - 1] = 0;
                            PCWHCount -= 1;
                        }
                        else if (whcount == 2)
                        {
                            byte Do = (byte)(ACWHCount - count - 1);
                            for (int p = 0; p < Do; p++)
                            {
                                ACWH[count + p] = ACWH[count + p + 1];
                                WHIDs[whcount][count + p] = WHIDs[whcount][count + p + 1];
                            }
                            ACWH[ACWHCount - 1] = null;
                            WHIDs[whcount][ACWHCount - 1] = 0;
                            ACWHCount -= 1;
                        }
                        else if (whcount == 3)
                        {
                            byte Do = (byte)(DCWHCount - count - 1);
                            for (int p = 0; p < Do; p++)
                            {
                                DCWH[count + p] = DCWH[count + p + 1];
                                WHIDs[whcount][count + p] = WHIDs[whcount][count + p + 1];
                            }
                            DCWH[DCWHCount - 1] = null;
                            WHIDs[whcount][DCWHCount - 1] = 0;
                            DCWHCount -= 1;
                        }
                        else if (whcount == 4)
                        {
                            byte Do = (byte)(BIWHCount - count - 1);
                            for (int p = 0; p < Do; p++)
                            {
                                BIWH[count + p] = BIWH[count + p + 1];
                                WHIDs[whcount][count + p] = WHIDs[whcount][count + p + 1];
                            }
                            BIWH[BIWHCount - 1] = null;
                            WHIDs[whcount][BIWHCount - 1] = 0;
                            BIWHCount -= 1;
                        }
                        else if (whcount == 5)
                        {
                            byte Do = (byte)(MAWHCount - count - 1);
                            for (int p = 0; p < Do; p++)
                            {
                                MAWH[count + p] = MAWH[count + p + 1];
                                WHIDs[whcount][count + p] = WHIDs[whcount][count + p + 1];
                            }
                            MAWH[MAWHCount - 1] = null;
                            WHIDs[whcount][MAWHCount - 1] = 0;
                            MAWHCount -= 1;
                        }
                        else if (whcount == 6)
                        {
                            byte Do = (byte)(AZWHCount - count - 1);
                            for (int p = 0; p < Do; p++)
                            {
                                AZWH[count + p] = AZWH[count + p + 1];
                                WHIDs[whcount][count + p] = WHIDs[whcount][count + p + 1];
                            }
                            AZWH[AZWHCount - 1] = null;
                            WHIDs[whcount][AZWHCount - 1] = 0;
                            AZWHCount -= 1;
                        }
                    }
                    count++;
                }
                whcount++;
            }


            Ready = true;
        }

        public void AddWHItem(string ItemInf, uint UID, byte WHID)
        {
            if (WHID == 0)
            {
                WHIDs[WHID][TCWHCount] = UID;
                TCWH[TCWHCount] = ItemInf;
                TCWHCount++;
            }
            else if (WHID == 1)
            {
                WHIDs[WHID][PCWHCount] = UID;
                PCWH[PCWHCount] = ItemInf;
                PCWHCount++;
            }
            else if (WHID == 2)
            {
                WHIDs[WHID][ACWHCount] = UID;
                ACWH[ACWHCount] = ItemInf;
                ACWHCount++;
            }
            else if (WHID == 3)
            {
                WHIDs[WHID][DCWHCount] = UID;
                DCWH[DCWHCount] = ItemInf;
                DCWHCount++;
            }
            else if (WHID == 4)
            {
                WHIDs[WHID][BIWHCount] = UID;
                BIWH[BIWHCount] = ItemInf;
                BIWHCount++;
            }
            else if (WHID == 5)
            {
                WHIDs[WHID][MAWHCount] = UID;
                MAWH[MAWHCount] = ItemInf;
                MAWHCount++;
            }
            else if (WHID == 6)
            {
                WHIDs[WHID][AZWHCount] = UID;
                AZWH[AZWHCount] = ItemInf;
                AZWHCount++;
            }
        }

        public void AddItem(string ItemInfo, byte ToPos, uint UID)
        {
            if (ItemInfo == null || ItemInfo == "")
                return;

            string[] Splitter = ItemInfo.Split('-');

            int ItemId;
            byte Plus = 0;
            byte Bless = 0;
            byte Enchant = 0;
            byte Soc1 = 0;
            byte Soc2 = 0;

            ItemId = int.Parse(Splitter[0]);
            Plus = byte.Parse(Splitter[1]);
            Bless = byte.Parse(Splitter[2]);
            Enchant = byte.Parse(Splitter[3]);
            Soc1 = byte.Parse(Splitter[4]);
            Soc2 = byte.Parse(Splitter[5]);


            if (ToPos == 0)
            {
                Inventory[ItemsInInventory] = ItemInfo;
                Inventory_UIDs[ItemsInInventory] = UID;
                ItemsInInventory++;
            }
            else if (ToPos < 10)
            {
                Equips[ToPos] = ItemInfo;
                Equips_UIDs[ToPos] = UID;
                GetEquipStats(ToPos, false);
            }

            MyClient.SendPacket(CoServer.MyPackets.AddItem((long)UID, ItemId, Plus, Bless, Enchant, Soc1, Soc2, ToPos, 100, 100));
        }

        public void SendInventory()
        {
            Ready = false;
            string[] Splitter;

            int ItemId;
            byte Plus = 0;
            byte Bless = 0;
            byte Enchant = 0;
            byte Soc1 = 0;
            byte Soc2 = 0;

            int count = 0;
            foreach (string item in Inventory)
            {
                if (item != null)
                {
                    Splitter = item.Split('-');

                    ItemId = int.Parse(Splitter[0]);
                    Plus = byte.Parse(Splitter[1]);
                    Bless = byte.Parse(Splitter[2]);
                    Enchant = byte.Parse(Splitter[3]);
                    Soc1 = byte.Parse(Splitter[4]);
                    Soc2 = byte.Parse(Splitter[5]);

                    MyClient.SendPacket(CoServer.MyPackets.AddItem((long)Inventory_UIDs[count], ItemId, Plus, Bless, Enchant, Soc1, Soc2, 0, 100, 100));
                }
                count++;
            }
            Ready = true;
        }

        public void SendEquips(bool GetStats)
        {
            try
            {
                Ready = false;
                string[] Splitter;

                int ItemId;
                byte Plus = 0;
                byte Bless = 0;
                byte Enchant = 0;
                byte Soc1 = 0;
                byte Soc2 = 0;

                int count = 0;
                foreach (string item in Equips)
                {
                    if (item != null)
                    {
                        Splitter = item.Split('-');

                        ItemId = int.Parse(Splitter[0]);
                        Plus = byte.Parse(Splitter[1]);
                        Bless = byte.Parse(Splitter[2]);
                        Enchant = byte.Parse(Splitter[3]);
                        Soc1 = byte.Parse(Splitter[4]);
                        Soc2 = byte.Parse(Splitter[5]);

                        MyClient.SendPacket(CoServer.MyPackets.AddItem((long)Equips_UIDs[count], ItemId, Plus, Bless, Enchant, Soc1, Soc2, (byte)count, 100, 100));
                    }
                    count++;
                }
                if (GetStats)
                {
                    GetEquipStats(1, false);
                    GetEquipStats(2, false);
                    GetEquipStats(3, false);
                    GetEquipStats(4, false);
                    GetEquipStats(5, false);
                    GetEquipStats(6, false);
                    GetEquipStats(7, false);
                    GetEquipStats(8, false);
                    GetEquipStats(9, false);
                }
                Ready = true;
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public void SendFriends()
        {
            foreach (DictionaryEntry DE in Friends)
            {
                uint FriendId = (uint)DE.Key;
                string FriendName = (string)DE.Value;

                if (World.AllChars.ContainsKey(FriendId))
                {
                    Character Friend = World.AllChars[FriendId];

                    MyClient.SendPacket(CoServer.MyPackets.FriendEnemyPacket(FriendId, FriendName, 15, 1));
                    Friend.MyClient.SendPacket(CoServer.MyPackets.FriendEnemyPacket(UID, Name, 14, 0));
                    Friend.MyClient.SendPacket(CoServer.MyPackets.FriendEnemyPacket(UID, Name, 15, 1));
                    Friend.SendSysMsg("Votre ami " + Name + " vient de se connecter.");
                }
                else
                    MyClient.SendPacket(CoServer.MyPackets.FriendEnemyPacket(FriendId, FriendName, 15, 0));
            }
        }

        public void SendEnemies()
        {
            foreach (DictionaryEntry DE in Enemies)
            {
                uint EnemyId = (uint)DE.Key;
                string EnemyName = (string)DE.Value;

                if (World.AllChars.ContainsKey(EnemyId))
                    MyClient.SendPacket(CoServer.MyPackets.FriendEnemyPacket(EnemyId, EnemyName, 19, 1));
                else
                    MyClient.SendPacket(CoServer.MyPackets.FriendEnemyPacket(EnemyId, EnemyName, 19, 0));
            }
        }

        public static string[] ShuffleGuildScores()
        {
            try
            {
                string[] ret = new string[5];
                DictionaryEntry[] Vals = new DictionaryEntry[5];

                for (sbyte i = 0; i < 5; i++)
                {
                    Vals[i] = new DictionaryEntry();
                    Vals[i].Key = (ushort)0;
                    Vals[i].Value = (int)0;
                }

                foreach (DictionaryEntry Score in World.GWScores)
                {
                    sbyte Pos = -1;
                    for (sbyte i = 0; i < 5; i++)
                    {
                        if ((int)Score.Value > (int)Vals[i].Value)
                        {
                            Pos = i;
                            break;
                        }
                    }
                    if (Pos == -1)
                        continue;

                    for (sbyte i = 4; i > Pos; i--)
                        Vals[i] = Vals[i - 1];

                    Vals[Pos] = Score;
                }

                for (sbyte i = 0; i < 5; i++)
                {
                    if ((ushort)Vals[i].Key == 0)
                    {
                        ret[i] = "";
                        continue;
                    }
                    Guild eGuild = (Guild)Guilds.AllGuilds[(ushort)Vals[i].Key];
                    ret[i] = "No  " + (i + 1).ToString() + ": " + eGuild.Name + "(" + (int)Vals[i].Value + ")";
                }

                return ret;
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); return null; }
        }


        public void SendGuildWar()
        {
            string[] g = ShuffleGuildScores();
            byte C = 0;

            foreach (string t in g)
            {
                if (t != "")
                {
                    if (C == 0)
                        MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", "ALLUSERS", t, 0x83C));
                    else
                        MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", "ALLUSERS", t, 0x83D));
                }
                C++;
            }
        }

        public void Teleport(ushort Map, ushort X, ushort Y)
        {
            Attacking = false;
            PTarget = null;
            MobTarget = null;
            TGTarget = null;

            if (LocMap != 700 && LocMap != 1036)
                PrevMap = LocMap;

            if (MyShop != null)
                MyShop.Destroy();

            LocMap = Map;
            LocX = X;
            LocY = Y;

            if (DataBase.AllMaps.ContainsKey(Map))
            {
                Map ThisMap = DataBase.AllMaps[Map];
                MyClient.SendPacket(CoServer.MyPackets.GeneralData(this, ThisMap.DMap, 74));
                MyClient.SendPacket(CoServer.MyPackets.GeneralData(this, ThisMap.Color, 104));
                if (ThisMap.Weather != 0)
                    MyClient.SendPacket(CoServer.MyPackets.Weather(ThisMap.Weather, 150, 75, 0));
                if (ThisMap.Flags != 0)
                    MyClient.SendPacket(CoServer.MyPackets.MapInfo(ThisMap.MapId, ThisMap.DMap, ThisMap.Flags));
            }
            else
                MyClient.SendPacket(CoServer.MyPackets.GeneralData(this, LocMap, 74));

            Screen.ChangeMap();
            World.SurroundDroppedItems(this, false);

            World.ShowEffect(this, "moveback");

            if (LocMap == 1038)            
                SendGuildWar();
        }

        public void UsePortal()
        {
            ushort NewMap = LocMap;
            ushort NewX = (ushort)(LocX + 3);
            ushort NewY = (ushort)(LocY - 2);

            foreach (ushort[] Portal in DataBase.Portals)
            {
                if (Portal[0] == LocMap)
                    if (MyMath.GetDistance(LocX, LocY, Portal[1], Portal[2]) <= 6)
                    {
                        NewMap = Portal[3];
                        NewX = Portal[4];
                        NewY = Portal[5];
                        break;
                    }
            }

            Teleport(NewMap, NewX, NewY);
        }

        public void CheckPortal()
        {
            foreach (ushort[] Portal in DataBase.Portals)
            {
                if (Portal[0] == LocMap)
                    if (MyMath.GetDistance(LocX, LocY, Portal[1], Portal[2]) <= 3)
                    {
                        LocMap = Portal[3];
                        LocX = Portal[4];
                        LocY = Portal[5];
                        break;
                    }
            }
        }

        public void SendSysMsg(string Message)
        {
            MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, Message, 0x7D5));
        }

        public void AddFriend(Character Who)
        {
            if (!Friends.Contains(Who.UID))
                Friends.Add(Who.UID, Who.Name);
        }

        public void DelFriend(uint FriendId)
        {
            if (Friends.Contains(FriendId))
                Friends.Remove(FriendId);
        }

        public bool FriendExist(uint FriendId)
        {
            return Friends.Contains(FriendId);
        }

        public void AddEnemy(Character Who)
        {
            if (!Enemies.Contains(Who.UID))
                Enemies.Add(Who.UID, Who.Name);
        }

        public void DelEnemy(uint EnemyId)
        {
            if (Enemies.Contains(EnemyId))
                Enemies.Remove(EnemyId);
        }
    }
}
