﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace COServer
{
    public class ScriptHandler
    {
        public static Dictionary<UInt32, NPCScript> AllScripts = new Dictionary<UInt32, NPCScript>();

        public static void GetAllScripts()
        {
            try
            {
                AllScripts = new Dictionary<UInt32, NPCScript>();

                String[] ScriptFiles = Directory.GetFiles(System.Windows.Forms.Application.StartupPath + "\\NPCs", "*.kcs");
                for (Int32 i = 0; i < ScriptFiles.Length; i++)
                {
                    try
                    {
                        UInt32 UniqId = 0;
                        NPCScript Script = null;

                        Script = new NPCScript(ScriptFiles[i]);
                        UInt32.TryParse(ScriptFiles[i].Replace(System.Windows.Forms.Application.StartupPath + "\\NPCs\\", "").Replace(".kcs", ""), out UniqId);

                        if (!AllScripts.ContainsKey(UniqId))
                            AllScripts.Add(UniqId, Script);
                    }
                    catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
                }
                Program.MCompressor.Optimize();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }
    }

    public class NPCScript
    {
        #region Structures
        public struct Header
        {
            public Int32 UniqId;
            public String Name;
            public Int16 Face;
        }

        public struct Binder
        {
            public Page[] Pages;
        }

        public struct Page
        {
            public Requirement[] Requirements;
            public Reward[] Rewards;
            public String Text;
            public Option[] Options;
        }

        public struct Requirement
        {
            public Byte Type;
            public Byte Operator;
            public Int32 IntValue;
            public String StrValue;
        }

        public struct Reward
        {
            public Byte Type;
            public Byte Operator;
            public Int32 IntValue;
            public String StrValue;
        }

        public struct Option
        {
            public Byte UniqId;
            public String Text;
        }
        #endregion
        #region Enumerations
        public enum Operator : byte
        {
            Addition = 0,           // +
            Subtraction = 1,        // -
            Multiplication = 2,     // *
            Divison = 3,            // /
            LessThan = 10,          // <
            GreaterThan = 11,       // >
            LessThanOrEqual = 12,   // <=
            GreaterThanOrEqual = 13,// >=
            Equal = 20,             // ==
            Inequal = 21,           // !=
        }

        public enum RequirementType : byte
        {
            CurHP = 0,
            MaxHP = 1,
            CurMP = 2,
            MaxMP = 3,
            Money = 4,
            Exp = 5,
            PKPoint = 6,
            Job = 7,
            StatP = 11,
            Model = 12,
            Level = 13,
            Spirit = 14,
            Vitality = 15,
            Strength = 16,
            Agility = 17,
            Hair = 27,
            CPs = 30,
            InvContains = 100,
            InvCount = 101
        }

        public enum RewardType : byte
        {
            CurHP = 0,
            MaxHP = 1,
            CurMP = 2,
            MaxMP = 3,
            Money = 4,
            Exp = 5,
            PKPoint = 6,
            Job = 7,
            StatP = 11,
            Model = 12,
            Level = 13,
            Spirit = 14,
            Vitality = 15,
            Strength = 16,
            Agility = 17,
            Hair = 27,
            CPs = 30,
            AddItem = 100,
            DelItem = 101,
            Teleport = 102
        }
        #endregion
        #region Private Members
        private Header m_Header = new Header();
        private Binder[] m_Binders = new Binder[0];
        #endregion

        public NPCScript(string File)
        {
            try
            {
                FileStream FStream = new FileStream(File, FileMode.Open, FileAccess.Read);
                BinaryReader BReader = new BinaryReader(FStream);

                if (Encoding.ASCII.GetString(BReader.ReadBytes(0x03)) == "KCS")
                {
                    m_Header = new Header()
                    {
                        UniqId = BReader.ReadInt32(),
                        Name = Encoding.Default.GetString(BReader.ReadBytes(32)).TrimEnd((Char)0x00),
                        Face = BReader.ReadInt16()
                    };

                    m_Binders = new Binder[BReader.ReadByte()];
                    for (Byte i = 0; i < m_Binders.Length; i++)
                    {
                        m_Binders[i] = new Binder()
                        {
                            Pages = new Page[BReader.ReadByte()]
                        };

                        for (Byte x = 0; x < m_Binders[i].Pages.Length; x++)
                        {
                            m_Binders[i].Pages[x] = new Page()
                            {
                                Requirements = new Requirement[BReader.ReadByte()],
                                Rewards = new Reward[BReader.ReadByte()],
                                Options = new Option[BReader.ReadByte()],
                                Text = null
                            };

                            for (Byte y = 0; y < m_Binders[i].Pages[x].Requirements.Length; y++)
                            {
                                m_Binders[i].Pages[x].Requirements[y] = new Requirement()
                                {
                                    Type = BReader.ReadByte(),
                                    Operator = BReader.ReadByte()
                                };
                                if (BReader.ReadBoolean())
                                    m_Binders[i].Pages[x].Requirements[y].StrValue = Encoding.Default.GetString(BReader.ReadBytes(32)).TrimEnd((Char)0x00);
                                else
                                    m_Binders[i].Pages[x].Requirements[y].IntValue = BReader.ReadInt32();
                            }

                            for (Byte y = 0; y < m_Binders[i].Pages[x].Rewards.Length; y++)
                            {
                                m_Binders[i].Pages[x].Rewards[y] = new Reward()
                                {
                                    Type = BReader.ReadByte(),
                                    Operator = BReader.ReadByte()
                                };
                                if (BReader.ReadBoolean())
                                    m_Binders[i].Pages[x].Rewards[y].StrValue = Encoding.Default.GetString(BReader.ReadBytes(32)).TrimEnd((Char)0x00);
                                else
                                    m_Binders[i].Pages[x].Rewards[y].IntValue = BReader.ReadInt32();
                            }

                            if (BReader.ReadBoolean())
                                m_Binders[i].Pages[x].Text = Encoding.Default.GetString(BReader.ReadBytes(BReader.ReadInt16())).TrimEnd((Char)0x00);

                            for (Byte y = 0; y < m_Binders[i].Pages[x].Options.Length; y++)
                            {
                                m_Binders[i].Pages[x].Options[y] = new Option()
                                {
                                    UniqId = BReader.ReadByte(),
                                    Text = Encoding.Default.GetString(BReader.ReadBytes(BReader.ReadByte())).TrimEnd((Char)0x00)
                                };
                            }
                        }
                    }
                }
                BReader.Close();
                FStream.Close();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        ~NPCScript()
        {
            m_Binders = null;
        }

        public void Execute(Client Client, Byte BinderId)
        {
            try
            {
                if (m_Binders.Length <= BinderId)
                    return;

                for (Byte i = 0; i < m_Binders[BinderId].Pages.Length; i++)
                {
                    Page Page = m_Binders[BinderId].Pages[i];

                    bool ChangePage = false;
                    foreach (Requirement Requirement in Page.Requirements)
                    {
                        if (!CheckRequirement(Requirement, Client.MyChar))
                        {
                            ChangePage = true;
                            break;
                        }
                    }
                    if (ChangePage)
                        continue;

                    foreach (Reward Reward in Page.Rewards)
                        GetReward(Reward, Client.MyChar);

                    if (Page.Text != null)
                    {
                        Char[] Text = Page.Text.ToCharArray();
                        for (Int16 x = 0; x < Text.Length; x += 250)
                        {
                            Byte Length = 250;
                            if (x + 250 > Text.Length)
                                Length = (byte)(Text.Length - x);

                            Char[] TmpString = new Char[Length];
                            Array.Copy(Text, x, TmpString, 0, Length);
                            Client.SendPacket(CoServer.MyPackets.NPCSay(new String(TmpString)));
                            TmpString = null;
                        }

                        foreach (Option Option in Page.Options)
                            Client.SendPacket(CoServer.MyPackets.NPCLink(Option.Text, Option.UniqId));

                        Client.SendPacket(CoServer.MyPackets.NPCSetFace(m_Header.Face));
                        Client.SendPacket(CoServer.MyPackets.NPCFinish());
                    }
                    return;
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        private bool CheckRequirement(Requirement Requirement, Character User)
        {
            try
            {
                switch ((RequirementType)Requirement.Type)
                {
                    #region CurHP
                    case RequirementType.CurHP:
                        {
                            switch ((Operator)Requirement.Operator)
                            {
                                case Operator.LessThan:
                                    {
                                        if (User.CurHP < Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThan:
                                    {
                                        if (User.CurHP > Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.LessThanOrEqual:
                                    {
                                        if (User.CurHP <= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThanOrEqual:
                                    {
                                        if (User.CurHP >= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Equal:
                                    {
                                        if (User.CurHP == Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Inequal:
                                    {
                                        if (User.CurHP != Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region MaxHP
                    case RequirementType.MaxHP:
                        {
                            switch ((Operator)Requirement.Operator)
                            {
                                case Operator.LessThan:
                                    {
                                        if (User.MaxHP < Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThan:
                                    {
                                        if (User.MaxHP > Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.LessThanOrEqual:
                                    {
                                        if (User.MaxHP <= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThanOrEqual:
                                    {
                                        if (User.MaxHP >= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Equal:
                                    {
                                        if (User.MaxHP == Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Inequal:
                                    {
                                        if (User.MaxHP != Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region CurMP
                    case RequirementType.CurMP:
                        {
                            switch ((Operator)Requirement.Operator)
                            {
                                case Operator.LessThan:
                                    {
                                        if (User.CurMP < Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThan:
                                    {
                                        if (User.CurMP > Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.LessThanOrEqual:
                                    {
                                        if (User.CurMP <= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThanOrEqual:
                                    {
                                        if (User.CurMP >= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Equal:
                                    {
                                        if (User.CurMP == Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Inequal:
                                    {
                                        if (User.CurMP != Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region MaxMP
                    case RequirementType.MaxMP:
                        {
                            switch ((Operator)Requirement.Operator)
                            {
                                case Operator.LessThan:
                                    {
                                        if (User.MaxMP < Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThan:
                                    {
                                        if (User.MaxMP > Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.LessThanOrEqual:
                                    {
                                        if (User.MaxMP <= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThanOrEqual:
                                    {
                                        if (User.MaxMP >= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Equal:
                                    {
                                        if (User.MaxMP == Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Inequal:
                                    {
                                        if (User.MaxMP != Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Money
                    case RequirementType.Money:
                        {
                            switch ((Operator)Requirement.Operator)
                            {
                                case Operator.LessThan:
                                    {
                                        if (User.Silvers < Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThan:
                                    {
                                        if (User.Silvers > Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.LessThanOrEqual:
                                    {
                                        if (User.Silvers <= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThanOrEqual:
                                    {
                                        if (User.Silvers >= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Equal:
                                    {
                                        if (User.Silvers == Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Inequal:
                                    {
                                        if (User.Silvers != Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Exp
                    case RequirementType.Exp:
                        {
                            switch ((Operator)Requirement.Operator)
                            {
                                case Operator.LessThan:
                                    {
                                        if (User.Exp < (ulong)Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThan:
                                    {
                                        if (User.Exp > (ulong)Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.LessThanOrEqual:
                                    {
                                        if (User.Exp <= (ulong)Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThanOrEqual:
                                    {
                                        if (User.Exp >= (ulong)Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Equal:
                                    {
                                        if (User.Exp == (ulong)Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Inequal:
                                    {
                                        if (User.Exp != (ulong)Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region PKPoint
                    case RequirementType.PKPoint:
                        {
                            switch ((Operator)Requirement.Operator)
                            {
                                case Operator.LessThan:
                                    {
                                        if (User.PKPoints < Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThan:
                                    {
                                        if (User.PKPoints > Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.LessThanOrEqual:
                                    {
                                        if (User.PKPoints <= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThanOrEqual:
                                    {
                                        if (User.PKPoints >= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Equal:
                                    {
                                        if (User.PKPoints == Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Inequal:
                                    {
                                        if (User.PKPoints != Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Job
                    case RequirementType.Job:
                        {
                            switch ((Operator)Requirement.Operator)
                            {
                                case Operator.LessThan:
                                    {
                                        if (User.Job < Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThan:
                                    {
                                        if (User.Job > Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.LessThanOrEqual:
                                    {
                                        if (User.Job <= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThanOrEqual:
                                    {
                                        if (User.Job >= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Equal:
                                    {
                                        if (User.Job == Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Inequal:
                                    {
                                        if (User.Job != Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region StatP
                    case RequirementType.StatP:
                        {
                            switch ((Operator)Requirement.Operator)
                            {
                                case Operator.LessThan:
                                    {
                                        if (User.StatP < Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThan:
                                    {
                                        if (User.StatP > Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.LessThanOrEqual:
                                    {
                                        if (User.StatP <= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThanOrEqual:
                                    {
                                        if (User.StatP >= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Equal:
                                    {
                                        if (User.StatP == Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Inequal:
                                    {
                                        if (User.StatP != Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Model
                    case RequirementType.Model:
                        {
                            switch ((Operator)Requirement.Operator)
                            {
                                case Operator.LessThan:
                                    {
                                        if (User.Model < Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThan:
                                    {
                                        if (User.Model > Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.LessThanOrEqual:
                                    {
                                        if (User.Model <= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThanOrEqual:
                                    {
                                        if (User.Model >= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Equal:
                                    {
                                        if (User.Model == Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Inequal:
                                    {
                                        if (User.Model != Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Level
                    case RequirementType.Level:
                        {
                            switch ((Operator)Requirement.Operator)
                            {
                                case Operator.LessThan:
                                    {
                                        if (User.Level < Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThan:
                                    {
                                        if (User.Level > Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.LessThanOrEqual:
                                    {
                                        if (User.Level <= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThanOrEqual:
                                    {
                                        if (User.Level >= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Equal:
                                    {
                                        if (User.Level == Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Inequal:
                                    {
                                        if (User.Level != Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Spirit
                    case RequirementType.Spirit:
                        {
                            switch ((Operator)Requirement.Operator)
                            {
                                case Operator.LessThan:
                                    {
                                        if (User.Spi < Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThan:
                                    {
                                        if (User.Spi > Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.LessThanOrEqual:
                                    {
                                        if (User.Spi <= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThanOrEqual:
                                    {
                                        if (User.Spi >= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Equal:
                                    {
                                        if (User.Spi == Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Inequal:
                                    {
                                        if (User.Spi != Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Vitality
                    case RequirementType.Vitality:
                        {
                            switch ((Operator)Requirement.Operator)
                            {
                                case Operator.LessThan:
                                    {
                                        if (User.Vit < Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThan:
                                    {
                                        if (User.Vit > Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.LessThanOrEqual:
                                    {
                                        if (User.Vit <= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThanOrEqual:
                                    {
                                        if (User.Vit >= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Equal:
                                    {
                                        if (User.Vit == Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Inequal:
                                    {
                                        if (User.Vit != Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Strength
                    case RequirementType.Strength:
                        {
                            switch ((Operator)Requirement.Operator)
                            {
                                case Operator.LessThan:
                                    {
                                        if (User.Str < Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThan:
                                    {
                                        if (User.Str > Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.LessThanOrEqual:
                                    {
                                        if (User.Str <= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThanOrEqual:
                                    {
                                        if (User.Str >= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Equal:
                                    {
                                        if (User.Str == Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Inequal:
                                    {
                                        if (User.Str != Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Agility
                    case RequirementType.Agility:
                        {
                            switch ((Operator)Requirement.Operator)
                            {
                                case Operator.LessThan:
                                    {
                                        if (User.Agi < Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThan:
                                    {
                                        if (User.Agi > Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.LessThanOrEqual:
                                    {
                                        if (User.Agi <= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThanOrEqual:
                                    {
                                        if (User.Agi >= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Equal:
                                    {
                                        if (User.Agi == Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Inequal:
                                    {
                                        if (User.Agi != Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Hair
                    case RequirementType.Hair:
                        {
                            switch ((Operator)Requirement.Operator)
                            {
                                case Operator.LessThan:
                                    {
                                        if (User.Hair < Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThan:
                                    {
                                        if (User.Hair > Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.LessThanOrEqual:
                                    {
                                        if (User.Hair <= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThanOrEqual:
                                    {
                                        if (User.Hair >= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Equal:
                                    {
                                        if (User.Hair == Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Inequal:
                                    {
                                        if (User.Hair != Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region CPs
                    case RequirementType.CPs:
                        {
                            switch ((Operator)Requirement.Operator)
                            {
                                case Operator.LessThan:
                                    {
                                        if (User.CPs < Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThan:
                                    {
                                        if (User.CPs > Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.LessThanOrEqual:
                                    {
                                        if (User.CPs <= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThanOrEqual:
                                    {
                                        if (User.CPs >= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Equal:
                                    {
                                        if (User.CPs == Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Inequal:
                                    {
                                        if (User.CPs != Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region InvContains
                    case RequirementType.InvContains:
                        {
                            switch ((Operator)Requirement.Operator)
                            {
                                case Operator.Equal:
                                    {
                                        string[] Data = Requirement.StrValue.Split(':');
                                        if (User.InventoryContains(UInt32.Parse(Data[0]), UInt32.Parse(Data[1])))
                                            return true;
                                        break;
                                    }
                                case Operator.Inequal:
                                    {
                                        string[] Data = Requirement.StrValue.Split(':');
                                        if (!User.InventoryContains(UInt32.Parse(Data[0]), UInt32.Parse(Data[1])))
                                            return true;
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region InvCount
                    case RequirementType.InvCount:
                        {
                            switch ((Operator)Requirement.Operator)
                            {
                                case Operator.LessThan:
                                    {
                                        if (User.ItemsInInventory < Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThan:
                                    {
                                        if (User.ItemsInInventory > Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.LessThanOrEqual:
                                    {
                                        if (User.ItemsInInventory <= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.GreaterThanOrEqual:
                                    {
                                        if (User.ItemsInInventory >= Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Equal:
                                    {
                                        if (User.ItemsInInventory == Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                                case Operator.Inequal:
                                    {
                                        if (User.ItemsInInventory != Requirement.IntValue)
                                            return true;
                                        break;
                                    }
                            }
                            break;
                        }
                    #endregion
                }
                return false;
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); return false; }
        }

        private void GetReward(Reward Reward, Character User)
        {
            try
            {
                switch ((RewardType)Reward.Type)
                {
                    #region CurHP
                    case RewardType.CurHP:
                        {
                            switch ((Operator)Reward.Operator)
                            {
                                case Operator.Addition:
                                    {
                                        User.CurHP += (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Subtraction:
                                    {
                                        User.CurHP -= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Multiplication:
                                    {
                                        User.CurHP *= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Divison:
                                    {
                                        User.CurHP /= (ushort)Reward.IntValue;
                                        return;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region MaxHP
                    case RewardType.MaxHP:
                        {
                            switch ((Operator)Reward.Operator)
                            {
                                case Operator.Addition:
                                    {
                                        User.MaxHP += (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Subtraction:
                                    {
                                        User.MaxHP -= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Multiplication:
                                    {
                                        User.MaxHP *= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Divison:
                                    {
                                        User.MaxHP /= (ushort)Reward.IntValue;
                                        return;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region CurMP
                    case RewardType.CurMP:
                        {
                            switch ((Operator)Reward.Operator)
                            {
                                case Operator.Addition:
                                    {
                                        User.CurMP += (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Subtraction:
                                    {
                                        User.CurMP -= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Multiplication:
                                    {
                                        User.CurMP *= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Divison:
                                    {
                                        User.CurMP /= (ushort)Reward.IntValue;
                                        return;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region MaxMP
                    case RewardType.MaxMP:
                        {
                            switch ((Operator)Reward.Operator)
                            {
                                case Operator.Addition:
                                    {
                                        User.MaxMP += (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Subtraction:
                                    {
                                        User.MaxMP -= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Multiplication:
                                    {
                                        User.MaxMP *= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Divison:
                                    {
                                        User.MaxMP /= (ushort)Reward.IntValue;
                                        return;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Money
                    case RewardType.Money:
                        {
                            switch ((Operator)Reward.Operator)
                            {
                                case Operator.Addition:
                                    {
                                        User.Silvers += (uint)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Subtraction:
                                    {
                                        User.Silvers -= (uint)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Multiplication:
                                    {
                                        User.Silvers *= (uint)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Divison:
                                    {
                                        User.Silvers /= (uint)Reward.IntValue;
                                        return;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Exp
                    case RewardType.Exp:
                        {
                            switch ((Operator)Reward.Operator)
                            {
                                case Operator.Addition:
                                    {
                                        User.Exp += (ulong)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Subtraction:
                                    {
                                        User.Exp -= (ulong)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Multiplication:
                                    {
                                        User.Exp *= (ulong)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Divison:
                                    {
                                        User.Exp /= (ulong)Reward.IntValue;
                                        return;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region PKPoint
                    case RewardType.PKPoint:
                        {
                            switch ((Operator)Reward.Operator)
                            {
                                case Operator.Addition:
                                    {
                                        User.PKPoints += (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Subtraction:
                                    {
                                        User.PKPoints -= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Multiplication:
                                    {
                                        User.PKPoints *= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Divison:
                                    {
                                        User.PKPoints /= (ushort)Reward.IntValue;
                                        return;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Job
                    case RewardType.Job:
                        {
                            switch ((Operator)Reward.Operator)
                            {
                                case Operator.Addition:
                                    {
                                        User.Job += (byte)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Subtraction:
                                    {
                                        User.Job -= (byte)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Multiplication:
                                    {
                                        User.Job *= (byte)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Divison:
                                    {
                                        User.Job /= (byte)Reward.IntValue;
                                        return;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region StatP
                    case RewardType.StatP:
                        {
                            switch ((Operator)Reward.Operator)
                            {
                                case Operator.Addition:
                                    {
                                        User.StatP += (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Subtraction:
                                    {
                                        User.StatP -= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Multiplication:
                                    {
                                        User.StatP *= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Divison:
                                    {
                                        User.StatP /= (ushort)Reward.IntValue;
                                        return;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Model
                    case RewardType.Model:
                        {
                            switch ((Operator)Reward.Operator)
                            {
                                case Operator.Addition:
                                    {
                                        User.Model += (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Subtraction:
                                    {
                                        User.Model -= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Multiplication:
                                    {
                                        User.Model *= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Divison:
                                    {
                                        User.Model /= (ushort)Reward.IntValue;
                                        return;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Level
                    case RewardType.Level:
                        {
                            switch ((Operator)Reward.Operator)
                            {
                                case Operator.Addition:
                                    {
                                        User.Level += (byte)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Subtraction:
                                    {
                                        User.Level -= (byte)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Multiplication:
                                    {
                                        User.Level *= (byte)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Divison:
                                    {
                                        User.Level /= (byte)Reward.IntValue;
                                        return;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Spirit
                    case RewardType.Spirit:
                        {
                            switch ((Operator)Reward.Operator)
                            {
                                case Operator.Addition:
                                    {
                                        User.Spi += (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Subtraction:
                                    {
                                        User.Spi -= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Multiplication:
                                    {
                                        User.Spi *= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Divison:
                                    {
                                        User.Spi /= (ushort)Reward.IntValue;
                                        return;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Vitality
                    case RewardType.Vitality:
                        {
                            switch ((Operator)Reward.Operator)
                            {
                                case Operator.Addition:
                                    {
                                        User.Vit += (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Subtraction:
                                    {
                                        User.Vit -= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Multiplication:
                                    {
                                        User.Vit *= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Divison:
                                    {
                                        User.Vit /= (ushort)Reward.IntValue;
                                        return;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Strength
                    case RewardType.Strength:
                        {
                            switch ((Operator)Reward.Operator)
                            {
                                case Operator.Addition:
                                    {
                                        User.Str += (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Subtraction:
                                    {
                                        User.Str -= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Multiplication:
                                    {
                                        User.Str *= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Divison:
                                    {
                                        User.Str /= (ushort)Reward.IntValue;
                                        return;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Agility
                    case RewardType.Agility:
                        {
                            switch ((Operator)Reward.Operator)
                            {
                                case Operator.Addition:
                                    {
                                        User.Agi += (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Subtraction:
                                    {
                                        User.Agi -= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Multiplication:
                                    {
                                        User.Agi *= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Divison:
                                    {
                                        User.Agi /= (ushort)Reward.IntValue;
                                        return;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region Hair
                    case RewardType.Hair:
                        {
                            switch ((Operator)Reward.Operator)
                            {
                                case Operator.Addition:
                                    {
                                        User.Hair += (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Subtraction:
                                    {
                                        User.Hair -= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Multiplication:
                                    {
                                        User.Hair *= (ushort)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Divison:
                                    {
                                        User.Hair /= (ushort)Reward.IntValue;
                                        return;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region CPs
                    case RewardType.CPs:
                        {
                            switch ((Operator)Reward.Operator)
                            {
                                case Operator.Addition:
                                    {
                                        User.CPs += (uint)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Subtraction:
                                    {
                                        User.CPs -= (uint)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Multiplication:
                                    {
                                        User.CPs *= (uint)Reward.IntValue;
                                        return;
                                    }
                                case Operator.Divison:
                                    {
                                        User.CPs /= (uint)Reward.IntValue;
                                        return;
                                    }
                            }
                            break;
                        }
                    #endregion
                    #region AddItem
                    case RewardType.AddItem:
                        {
                            string[] Data = Reward.StrValue.Split(':');
                            for (Int32 i = 0; i < Int32.Parse(Data[1]); i++)
                                User.AddItem(Data[0], 0, (uint)Program.Rand.Next(0x7FFFF));
                            break;
                        }
                    #endregion
                    #region DelItem
                    case RewardType.DelItem:
                        {
                            string[] Data = Reward.StrValue.Split(':');
                            for (Int32 i = 0; i < Int32.Parse(Data[1]); i++)
                                User.RemoveItem(User.ItemNext(UInt32.Parse(Data[0])));
                            break;
                        }
                    #endregion
                    #region Teleport
                    case RewardType.Teleport:
                        {
                            string[] Data = Reward.StrValue.Split(':');
                            User.Teleport(ushort.Parse(Data[0]), ushort.Parse(Data[1]), ushort.Parse(Data[2]));
                            break;
                        }
                    #endregion
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }
    }
}