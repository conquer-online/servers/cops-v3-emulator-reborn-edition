﻿using System;
using System.Collections.Generic;
using CoS_Math;

namespace COServer
{
    public class Screen
    {
        private Dictionary<uint, ConquerObject> Entities;
        private ConquerObject[] m_Objects;
        private Character m_Owner;

        public ConquerObject[] Objects { get { return m_Objects; } }
        public Character Owner { get { return m_Owner; } }

        public Screen(Character Owner)
        {
            Entities = new Dictionary<uint, ConquerObject>();
            m_Objects = new ConquerObject[0];
            m_Owner = Owner;
        }

        public bool IsPlayerScreen() { return Owner.IsPlayer(); }

        public bool Contains(uint UnqID)
        {
            return Entities.ContainsKey(UnqID);
        }

        public void Add(object Object, bool SendSpawn)
        {
            lock (Entities)
            {
                if (!Entities.ContainsKey((Object as ConquerObject).UID) && (Object as ConquerObject).UID != Owner.UID)
                {
                    Entities.Add((Object as ConquerObject).UID, (Object as ConquerObject));
                    m_Objects = new ConquerObject[Entities.Count];
                    Entities.Values.CopyTo(m_Objects, 0);

                    if (SendSpawn && IsPlayerScreen())
                    {
                        if ((Object as ConquerObject).IsPlayer())
                        {
                            (Owner as Character).MyClient.SendPacket(CoServer.MyPackets.SpawnEntity((Object as Character)));
                            if ((Object as Character).MyGuild != null)
                                (Owner as Character).MyClient.SendPacket(CoServer.MyPackets.GuildName((Object as Character).GuildID, (Object as Character).MyGuild.Name));
                            if ((Object as Character).MyShop != null)
                                (Owner as Character).MyClient.SendPacket(CoServer.MyPackets.SpawnCarpet((Object as Character), (Object as Character).MyShop.GetId()));
                        }
                        else if ((Object as ConquerObject).IsMonster())
                            (Owner as Character).MyClient.SendPacket(CoServer.MyPackets.SpawnMob((Object as Monster)));
                        else if ((Object as ConquerObject).IsNPC())
                        {
                            if ((Object as ConquerObject).IsDynNPC())
                            {
                                if ((Object as SingleNPC).Sob == 0)
                                    (Owner as Character).MyClient.SendPacket(CoServer.MyPackets.SpawnNPC((Object as SingleNPC)));
                                else if ((Object as SingleNPC).Sob == 1)
                                    (Owner as Character).MyClient.SendPacket(CoServer.MyPackets.SpawnSobNPC((Object as SingleNPC)));
                                else if ((Object as SingleNPC).Sob == 2)
                                    (Owner as Character).MyClient.SendPacket(CoServer.MyPackets.SpawnSobNPCNamed((Object as SingleNPC)));
                                else
                                    (Owner as Character).MyClient.SendPacket(CoServer.MyPackets.SpawnSob((Object as SingleNPC)));
                            }
                            else
                                (Owner as Character).MyClient.SendPacket(CoServer.MyPackets.SpawnNPC((Object as SingleNPC)));
                        }
                    }
                }
            }
        }

        public void Remove(object Object, bool Remove)
        {
            lock (Entities)
            {
                if (Entities.ContainsKey((Object as ConquerObject).UID))
                {
                    Entities.Remove((Object as ConquerObject).UID);
                    m_Objects = new ConquerObject[Entities.Count];
                    Entities.Values.CopyTo(m_Objects, 0);
                }

                if (Remove && IsPlayerScreen())
                {
                    (Owner as Character).MyClient.SendPacket(CoServer.MyPackets.GeneralData((Object as ConquerObject), 0, 132));
                    if ((Object as ConquerObject).IsPlayer() && (Owner as Character).MyShop != null)
                        (Owner as Character).MyClient.SendPacket(CoServer.MyPackets.GeneralData((Object as Character).MyShop.GetId(), 0, 132));
                }
            }
        }

        public void Clear()
        {
            Entities.Clear();
            m_Objects = new ConquerObject[0];
        }

        public void Disconnect()
        {
            foreach (ConquerObject Object in m_Objects)
            {
                if (Object.IsPlayer())
                    (Object as Character).Screen.Remove(Owner, true);
            }
        }

        public void SendScreen(bool RemoveSelf)
        {
            if (RemoveSelf)
            {
                foreach (ConquerObject Object in m_Objects)
                {
                    if (Object.IsPlayer())
                        (Object as Character).Screen.Remove(Owner, true);
                }
            }
            Check();
        }

        public void ChangeMap()
        {
            foreach (ConquerObject Object in m_Objects)
            {
                if (Object.IsPlayer())
                    (Object as Character).Screen.Remove(Owner, true);
            }

            Clear();
            Check();
        }

        public void SendPacket(byte[] Data, bool ToMe)
        {
            lock (Entities)
            {
                foreach (ConquerObject Object in m_Objects)
                {
                    if (Object.IsPlayer())
                        (Object as Character).MyClient.SendPacket(Data);
                }

                if (ToMe && IsPlayerScreen())
                    (Owner as Character).MyClient.SendPacket(Data);
            }
        }

        public void Check()
        {
            lock (Entities)
            {
                foreach (ConquerObject Object in m_Objects)
                {
                    if (Object.LocMap != Owner.LocMap)
                    {
                        Remove(Object, true);

                        if (Object.IsPlayer())
                            (Object as Character).Screen.Remove(Owner, true);
                        continue;
                    }

                    if (!MyMath.CanSee(Object.LocX, Object.LocY, Owner.LocX, Owner.LocY))
                    {
                        Remove(Object, true);

                        if (Object.IsPlayer())
                            (Object as Character).Screen.Remove(Owner, true);
                        continue;
                    }
                }

                foreach (Character User in World.AllChars.Values)
                {
                    if (!Entities.ContainsKey(User.UID) && User.UID != Owner.UID)
                    {
                        if (User.LocMap == Owner.LocMap)
                        {
                            if (MyMath.CanSee(User.LocX, User.LocY, Owner.LocX, Owner.LocY))
                            {
                                Add(User, true);
                                if (!User.Screen.Contains(Owner.UID))
                                    User.Screen.Add(Owner, true);
                            }
                        }
                    }
                }
                foreach (KeyValuePair<uint, Monster> KV in World.AllMobs)
                {
                    Monster Mob = KV.Value;
                    if (!Entities.ContainsKey(Mob.UID))
                    {
                        if (Mob.LocMap == Owner.LocMap)
                            if (Mob.Alive)
                            {
                                if (MyMath.CanSee(Mob.LocX, Mob.LocY, Owner.LocX, Owner.LocY))
                                    Add(Mob, true);
                            }
                    }
                }
                foreach (KeyValuePair<uint, SingleNPC> KV in NPCs.AllNPCs)
                {
                    SingleNPC Npc = KV.Value;
                    if (!Entities.ContainsKey(Npc.UID))
                    {
                        if (Npc.LocMap == Owner.LocMap)
                        {
                            if (MyMath.CanSee(Npc.LocX, Npc.LocY, Owner.LocX, Owner.LocY))
                                Add(Npc, true);
                        }
                    }
                }
            }
        }
    }
}
