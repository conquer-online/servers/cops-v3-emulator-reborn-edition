﻿using System;

namespace COServer
{
    public class ItemHandler
    {
        public static byte ItemQuality(uint ItemId) { return (byte)(ItemId % 10); }
        public static uint RemoveQuality(uint ItemId) { return (ItemId - (ItemId % 10)) + 3; }
        public static uint ChangeQuality(uint ItemId, byte To) { return (ItemId - (ItemId % 10)) + To; }
        public static uint WeaponType(uint ItemId) { return (uint)((ItemId - (ItemId % 1000)) / 1000); }
        public static uint ItemType(uint ItemId) { return (uint)((ItemId - (ItemId % 100000)) / 100000); }
        public static uint ItemType2(uint ItemId) { return (uint)((ItemId - (ItemId % 10000)) / 10000); }

        public static uint GetBonusId(uint ItemId, byte Pos)
        {
            string PItemID = ItemId.ToString();
            if (Pos == 1 || Pos == 3)
            {
                PItemID = PItemID.Remove(3, 1);
                PItemID = PItemID.Insert(3, "0");
                PItemID = PItemID.Remove(5, 1);
                PItemID = PItemID.Insert(5, "0");
            }
            if (Pos == 2 || Pos == 6 || Pos == 8)
            {
                PItemID = PItemID.Remove(5, 1);
                PItemID = PItemID.Insert(5, "0");
            }
            if (Pos == 4 || Pos == 5)
            {
                if (ItemType(ItemId) == 4 && WeaponType(ItemId) != 421)
                {
                    PItemID = PItemID.Remove(5, 1);
                    PItemID = PItemID.Remove(0, 3);
                    PItemID = "444" + PItemID + "0";
                }
                if (ItemType(ItemId) == 5 && WeaponType(ItemId) != 500)
                {
                    PItemID = PItemID.Remove(5, 1);
                    PItemID = PItemID.Remove(0, 3);
                    PItemID = "555" + PItemID + "0";
                }
                if (WeaponType(ItemId) == 500 || WeaponType(ItemId) == 421)
                {
                    PItemID = PItemID.Remove(5, 1);
                    PItemID = PItemID.Insert(5, "0");
                }
                if (WeaponType(ItemId) == 900)
                {
                    PItemID = PItemID.Remove(3, 1);
                    PItemID = PItemID.Insert(3, "0");
                    PItemID = PItemID.Remove(5, 1);
                    PItemID = PItemID.Insert(5, "0");
                }
            }
            return uint.Parse(PItemID);
        }

        public static ushort GetItemHP(uint ItemId)
        {
            if (ItemId == 137310) //GMRobe
                return 30000;

            if (ItemId == 137320 || ItemId == 137420 || ItemId == 137520 || ItemId == 137620 || ItemId == 137720 || ItemId == 137820 || ItemId == 137920) //PhoenixDress
                return 255;

            if (ItemId == 137330 || ItemId == 137430 || ItemId == 137530 || ItemId == 137630 || ItemId == 137730 || ItemId == 137830 || ItemId == 137930) //ElegantDress
                return 255;

            if (ItemId == 137340 || ItemId == 137440 || ItemId == 137540 || ItemId == 137640 || ItemId == 137740 || ItemId == 137840 || ItemId == 137940) //CelestialDress
                return 255;

            if (ItemId == 137350 || ItemId == 137450 || ItemId == 137550 || ItemId == 137650 || ItemId == 137750 || ItemId == 137850 || ItemId == 137950) //WeddingDress
                return 1000;

            if (ItemId == 150000 || ItemId == 150310 || ItemId == 150320) //LoveForever
                return 800;

            if (ItemId == 120319) //LoveLock
                return 200;

            if (ItemId == 2100025) //Gourd HP/MP
                return 800;

            return 0;
        }

        public static ushort GetItemMP(uint ItemId)
        {
            if (ItemId == 2100025) //Gourd HP/MP
                return 800;

            if (ItemId == 2100045) //Gourd MP
                return 400;

            return 0;
        }
    }
}
