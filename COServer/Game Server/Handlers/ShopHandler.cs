﻿using System;
using System.Collections.Generic;
using CoS_Math;

namespace COServer
{
    public class Shop
    {
        private uint Id;
        public uint OwnerId;
        public Dictionary<uint, ShopItem> Items;
        public string Message;

        public uint GetId() { return Id; }

        public static bool Create(Character User)
        {
            try
            {
                uint Id = (uint)MyMath.Generate(102000, 106000);
                while (World.AllShops.ContainsKey(Id))
                    Id = (uint)MyMath.Generate(102000, 106000);

                Shop Shop = new Shop
                {
                    Id = Id,
                    OwnerId = User.UID,
                    Items = new Dictionary<uint, ShopItem>(),
                    Message = ""
                };

                User.MyShop = Shop;
                World.AllShops.Add(Id, Shop);
                User.Screen.SendPacket(CoServer.MyPackets.SpawnCarpet(User, Id), true);
                User.SendSysMsg("Utilisez la commande /close pour fermer votre magasin!");
                return true;
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); return false; }
        }

        public void Destroy()
        {
            Character User = World.AllChars[OwnerId];

            if (World.AllShops.ContainsKey(GetId()))
                World.AllShops.Remove(GetId());

            User.MyShop = null;

            User.MyClient.SendPacket(CoServer.MyPackets.GeneralData(User, User.LocMap, 74));
            User.Screen.Clear();
            User.Screen.SendScreen(false);

            User.Screen.SendPacket(CoServer.MyPackets.GeneralData(GetId(), 0, 132), true);
            foreach (uint ItemUID in Items.Keys)
                User.Screen.SendPacket(CoServer.MyPackets.DelShopItem(ItemUID, GetId()), true);
        }

        public bool AddItem(uint ItemUID, uint Value, bool Money)
        {
            if (ItemUID == 0 || Value == 0)
                return false;

            if (!World.AllChars.ContainsKey(OwnerId))
                return false;

            Character User = World.AllChars[OwnerId];

            if (Items.Count >= 18)
            {
                User.SendSysMsg("STR_BOOTH_FULL");
                return false;
            }

            string Item = User.GetItem(ItemUID);
            if (Item == null || Item == "0")
                return false;

            if (Items.ContainsKey(ItemUID))
                return false;

            Items.Add(ItemUID, new ShopItem() { UniqId = ItemUID, Value = Value, Money = Money });
            User.Screen.SendPacket(CoServer.MyPackets.AddShopItem(ItemUID, Value, (Money ? (byte)22 : (byte)29)), false);
            return true;
        }

        public void DelItem(uint ItemUID)
        {
            if (!World.AllChars.ContainsKey(OwnerId))
                return;

            Character User = World.AllChars[OwnerId];

            if (Items.ContainsKey(ItemUID))
                Items.Remove(ItemUID);

            User.Screen.SendPacket(CoServer.MyPackets.DelShopItem(ItemUID, GetId()), true);
        }

        public void SendItems(Character Viewer)
        {
            if (!World.AllChars.ContainsKey(OwnerId))
                return;

            Character User = World.AllChars[OwnerId];

            if (World.AllChars.ContainsKey(Viewer.UID))
            {
                foreach (ShopItem Item in Items.Values)
                {
                    if (User.GetItem(Item.UniqId) != null && User.GetItem(Item.UniqId) != "0")
                        Viewer.MyClient.SendPacket(CoServer.MyPackets.AddItemToShop(Item.UniqId, GetId(), Item.Value, User.GetItem(Item.UniqId), Item.Money));
                    else
                        DelItem(Item.UniqId);
                }
            }
        }

        public bool BuyItem(Character Buyer, uint ItemUID)
        {
            if (ItemUID == 0)
                return false;

            if (!World.AllChars.ContainsKey(OwnerId))
                return false;

            Character User = World.AllChars[OwnerId];

            if (User.UID == Buyer.UID)
                return false;

            string Item = User.GetItem(ItemUID);
            if (Item == null || Item == "0")
                return false;

            if (!Items.ContainsKey(ItemUID))
                return false;

            ShopItem SItem = Items[ItemUID];

            if (SItem.Value <= 0)
                return false;

            if (SItem.Money)
            {
                if (Buyer.Silvers < SItem.Value)
                {
                    Buyer.SendSysMsg("Vous n'avez pas assez d'argent.");
                    return false;
                }
            }
            else
            {
                if (Buyer.CPs < SItem.Value)
                {
                    Buyer.SendSysMsg("Vous n'avez pas assez d'argent.");
                    return false;
                }
            }

            if (Buyer.ItemsInInventory > 39)
            {
                Buyer.SendSysMsg("Votre inventaire est complet.");
                return false;
            }

            if (SItem.Money)
            {
                Buyer.Silvers -= SItem.Value;
                User.Silvers += SItem.Value;
            }
            else
            {
                Buyer.CPs -= SItem.Value;
                User.CPs += SItem.Value;
            }

            User.RemoveItem(ItemUID);
            Buyer.AddItem(Item, 0, ItemUID);

            User.Save();
            Buyer.Save();

            DelItem(ItemUID);
            User.SendSysMsg("%s vient de vous payer %d!".Replace("%s", Buyer.Name).Replace("%d", SItem.Value.ToString()));
            return true;
        }
    }

    public struct ShopItem
    {
        public uint UniqId;
        public uint Value;
        public bool Money;
    }
}
