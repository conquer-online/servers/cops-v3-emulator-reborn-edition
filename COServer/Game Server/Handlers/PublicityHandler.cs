﻿using System;
using System.Timers;
using System.IO;
using INI_CORE_DLL;

namespace COServer
{
    public class PublicityHandler
    {
        private Publicity[] AllPublicities;
        private Timer Sender;
        private bool IsWorking;
        private int Count;
        private int Interval;
        private int Pos;

        public PublicityHandler()
        {
            GetPublicities();

            Sender = new Timer();
            Sender.Interval = Interval;
            Sender.Elapsed += new ElapsedEventHandler(Send);

            if (Count != 0)
                Start();
        }

        public void GetPublicities()
        {
            if (File.Exists(System.Windows.Forms.Application.StartupPath + "\\Publicity.ini"))
            {
                Ini Reader = new Ini(System.Windows.Forms.Application.StartupPath + "\\Publicity.ini");
                Count = Reader.ReadInt32("Header", "Count");
                Interval = Reader.ReadInt32("Header", "Interval");

                AllPublicities = new Publicity[Count];
                for (int i = 0; i < Count; i++)
                {
                    Publicity Pub = new Publicity();
                    Pub.Message = Reader.ReadValue("Publicity" + i.ToString(), "Message");
                    Pub.Channel = Reader.ReadInt16("Publicity" + i.ToString(), "Channel");
                    AllPublicities[i] = Pub;
                }
            }
        }

        public void Start()
        {
            if (!IsWorking)
            {
                IsWorking = true;
                Sender.Start();
            }
        }

        public void Stop()
        {
            if (IsWorking)
            {
                IsWorking = false;
                Sender.Stop();
            }
        }

        private void Send(object Sender, ElapsedEventArgs Args)
        {
            World.SendMsgToAll(AllPublicities[Pos].Message, "SYSTEM", AllPublicities[Pos].Channel);
            Pos++;

            if (Pos == Count)
                Pos = 0;
        }
    }

    public struct Publicity
    {
        public string Message;
        public short Channel;
    }
}
