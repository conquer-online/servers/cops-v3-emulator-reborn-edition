using System;
using DMapReader;
using CoS_Math;

namespace COServer
{
    public class Other
    {
        public static uint EquipNextLevel(uint ItemId)
        {
            uint NewItem = ItemId;

            if (!Other.ArmorType(ItemId) || ItemHandler.WeaponType(ItemId) == 117)
            {
                if (ItemHandler.ItemType2(ItemId) != 12 && ItemHandler.ItemType2(ItemId) != 15 && ItemHandler.ItemType2(ItemId) != 16 || Other.ItemInfo(ItemId).Level == 45 && ItemHandler.ItemType2(ItemId) == 12 || Other.ItemInfo(ItemId).Level >= 112 && ItemHandler.ItemType2(ItemId) == 12)
                    NewItem += 10;
                else if (ItemHandler.ItemType2(ItemId) == 12 && Other.ItemInfo(ItemId).Level < 45)
                    NewItem += 20;
                else if (ItemHandler.ItemType2(ItemId) == 12 && Other.ItemInfo(ItemId).Level >= 52 && Other.ItemInfo(ItemId).Level < 112)
                    NewItem += 30;
                else if (ItemHandler.ItemType2(ItemId) == 15 && Other.ItemInfo(ItemId).Level == 1 || ItemHandler.ItemType2(ItemId) == 15 && Other.ItemInfo(ItemId).Level >= 110)
                    NewItem += 10;
                else if (ItemHandler.ItemType2(ItemId) == 15)
                    NewItem += 20;
                else if (ItemHandler.ItemType2(ItemId) == 16 && Other.ItemInfo(ItemId).Level < 110)
                    NewItem += 20;
                else if (ItemHandler.ItemType2(ItemId) == 16)
                    NewItem += 10;

                if (ItemHandler.WeaponType(NewItem) == 421)
                {
                    NewItem = ItemId;
                    if (Other.ItemInfo(ItemId).Level == 45 || Other.ItemInfo(ItemId).Level == 55)
                        NewItem += 20;
                    else
                        NewItem += 10;
                }
            }
            else if (ItemHandler.ItemType2(ItemId) != 12 && ItemHandler.ItemType2(ItemId) != 15)
            {
                if (Other.ItemInfo(ItemId).Job == 21)
                    if (ItemHandler.ItemType2(ItemId) == 13)
                    {
                        if (Other.ItemInfo(ItemId).Level < 110)
                            NewItem += 10;
                        else
                            NewItem += 5000;
                    }
                if (Other.ItemInfo(ItemId).Job == 11)
                    if (ItemHandler.ItemType2(ItemId) == 13)
                    {
                        if (Other.ItemInfo(ItemId).Level < 110)
                            NewItem += 10;
                        else
                            NewItem += 5000;
                    }
                if (Other.ItemInfo(ItemId).Job == 40)
                    if (ItemHandler.ItemType2(ItemId) == 13)
                    {
                        if (Other.ItemInfo(ItemId).Level < 112)
                            NewItem += 10;
                        else
                            NewItem += 5000;
                    }
                if (Other.ItemInfo(ItemId).Job == 190)
                    if (ItemHandler.ItemType2(ItemId) == 13)
                    {
                        if (Other.ItemInfo(ItemId).Level < 115)
                            NewItem += 10;
                        else
                            NewItem += 5000;
                    }
                if (Other.ItemInfo(ItemId).Job == 21)
                    if (ItemHandler.ItemType2(ItemId) == 11)
                    {
                        if (Other.ItemInfo(ItemId).Level < 112)
                            NewItem += 10;
                        else
                            NewItem += 920;
                    }
                if (Other.ItemInfo(ItemId).Job == 11)
                    if (ItemHandler.ItemType2(ItemId) == 11)
                    {
                        if (Other.ItemInfo(ItemId).Level < 112)
                            NewItem += 10;
                        else
                            NewItem -= 6010;
                    }
                if (Other.ItemInfo(ItemId).Job == 40)
                    if (ItemHandler.ItemType2(ItemId) == 11)
                    {
                        if (Other.ItemInfo(ItemId).Level < 117)
                            NewItem += 10;
                        else
                            NewItem -= 1060;
                    }
                if (Other.ItemInfo(ItemId).Job == 190)
                    if (ItemHandler.ItemType2(ItemId) == 11)
                    {
                        if (Other.ItemInfo(ItemId).Level < 112)
                            NewItem += 10;
                        else
                            NewItem -= 2050;
                    }

            }

            if (ItemId == 500301)
                NewItem = 500005;

            if (ItemId == 410301)
                NewItem = 410005;

            if (ItemId > 152012 && ItemId < 152020)
            {
                NewItem = ItemId;
                NewItem += 30;
            }

            if (ItemId > 421022 && ItemId < 421030)
            {
                NewItem = ItemId;
                NewItem += 20;
            }

            if (ItemId > 421042 && ItemId < 421050)
            {
                NewItem = ItemId;
                NewItem += 20;
            }

            if (ItemId > 117392 && ItemId < 117400)
            {
                NewItem = ItemId;
                NewItem += 100;
            }

            return NewItem;
        }
        public static bool NoPK(ushort Map)
        {
            bool Rt = false;

            foreach (ushort map in DataBase.NoPKMaps)
            {
                if (map == Map)
                {
                    Rt = true;
                    break;
                }
            }
            return Rt;
        }
        public static bool CanPK(ushort Map)
        {
            bool Rt = false;

            foreach (uint map in DataBase.PKMaps)
            {
                if (map == Map)
                {
                    Rt = true;
                    break;
                }
            }
            return Rt;
        }
        public static bool NoDrop(ushort Map)
        {
            bool Rt = false;

            foreach (ushort map in DataBase.NoDropMaps)
            {
                if (map == Map)
                {
                    Rt = true;
                    break;
                }
            }
            return Rt;
        }
        public static bool NoRez(ushort Map)
        {
            bool Rt = false;

            foreach (ushort map in DataBase.NoRezMaps)
            {
                if (map == Map)
                {
                    Rt = true;
                    break;
                }
            }
            return Rt;
        }

        public static bool CanAttack(Character Attacker, Character Attacked)
        {
            try
            {
                if (NoPK(Attacker.LocMap))
                    return false;

                if (Attacker.PKMode == 0)
                    return true;

                if (Attacker.PKMode == 2)
                    if (Attacker.MyGuild == null || Attacked.MyGuild == null || (Attacked.GuildID != Attacker.GuildID && !Attacker.MyGuild.Allies.Contains(Attacked.GuildID)))
                        if (Attacker.Team == null || Attacked.Team == null || Attacked.Team != Attacker.Team)
                            if (!Attacker.Friends.Contains(Attacked.UID))
                                return true;

                if (Attacker.PKMode == 3)
                    if (Attacked.PKPoints > 99 || Attacked.BlueName)
                        return true;

                return false;
            }
            catch { return false; }
        }

        public static uint CalculateDamage(Character Attacker, Character Attacked, byte AttackType, ushort SkillId, byte SkillLvl)
        {
            if (CanAttack(Attacker, Attacked))
            {
                Attacker.PTarget = Attacked;
                double Damage = 0;
                ushort[] SkillAttributes = DataBase.SkillAttributes[SkillId][SkillLvl];

                if (AttackType == 0 || AttackType == 1)//Melee
                {
                    double reborn = 1;
                    double Shield = 1;
                    double IronShirt = 1;
                    double BonusDef = 0;

                    Damage = MyMath.Generate((int)Attacker.MinAtk, (int)Attacker.MaxAtk);
                    if (AttackType == 0)
                        Damage += SkillAttributes[3];
                    else
                        Damage = (((double)SkillAttributes[3] / 100) * Damage);

                    if (Attacked.MShieldBuff)
                        BonusDef = ((double)Attacked.Defense * (double)((10 + Attacked.MShieldLevel * 5) / 100));

                    if (Attacker.StigBuff)
                        Damage = (Damage * (double)(1 + ((double)(10 + Attacker.StigLevel) / 100)));

                    if (Attacker.SMOn)
                        Damage *= 2;

                    if (Attacked.RBCount == 1)
                        reborn = 0.7;
                    else if (Attacked.RBCount == 2)
                        reborn = 0.5;

                    if (Attacked.Shield)
                        Shield = 3;

                    if (Attacked.IronShirt)
                        IronShirt = 4;

                    Damage -= (int)((Attacked.Defense + BonusDef) * Shield * IronShirt);
                    Damage *= reborn;
                    Damage *= Attacked.Bless;
                    Damage *= Attacked.AddMythique;

                    if (Attacker.LuckTime > 0 && ChanceSuccess(10))
                    {
                        Damage *= 2;
                        World.ShowEffect(Attacker, "LuckyGuy");
                    }

                    if (Attacked.LuckTime > 0 && ChanceSuccess(10))
                    {
                        Damage = 1;
                        World.ShowEffect(Attacked, "LuckyGuy");
                    }

                    if (Damage < 1)
                        Damage = 1;

                    if (Attacked.ReflectOn && ChanceSuccess(15))
                    {
                        if (Damage > 2000)
                            Damage = 2000;

                        Attacker.GetReflect((uint)Damage);
                        World.ShowEffect(Attacker, "WeaponReflect");
                        Damage = 0;
                    }
                    Attacked.BlessEffect();
                }
                else if (AttackType == 2)//Ranged
                {
                    double reborn = 1;
                    double ExtraDodge = 0;

                    Damage = MyMath.Generate((int)Attacker.MinAtk, (int)Attacker.MaxAtk);
                    Damage += SkillAttributes[3];

                    if (Attacker.StigBuff)
                        Damage = (Damage * (double)(1 + ((double)(10 + Attacker.StigLevel) / 100)));

                    if (Attacker.SMOn)
                        Damage *= 2;

                    if (Attacked.RBCount == 1)
                        reborn = 0.7;
                    else if (Attacked.RBCount == 2)
                        reborn = 0.5;

                    if (Attacked.DodgeBuff)
                        ExtraDodge = (20 + Attacked.DodgeLevel * 5);

                    byte Dodge = (byte)(Attacked.Dodge + ExtraDodge);
                    if (Dodge > 100)
                        Dodge = 100;

                    Damage *= (double)((100 - Dodge) / 100);
                    Damage *= 0.12;

                    if (CoServer.ADS)
                        Damage += (Attacker.ADS_Attack * 0.25);

                    Damage *= reborn;
                    Damage *= Attacked.Bless;
                    Damage *= Attacked.AddMythique;

                    if (Attacker.LuckTime > 0 && ChanceSuccess(10))
                    {
                        Damage *= 2;
                        World.ShowEffect(Attacker, "LuckyGuy");
                    }

                    if (Attacked.LuckTime > 0 && ChanceSuccess(10))
                    {
                        Damage = 1;
                        World.ShowEffect(Attacked, "LuckyGuy");
                    }

                    if (Damage < 1)
                        Damage = 1;

                    if (Attacked.ReflectOn && ChanceSuccess(15))
                    {
                        if (Damage > 2000)
                            Damage = 2000;

                        Attacker.GetReflect((uint)Damage);
                        World.ShowEffect(Attacker, "WeaponReflect");
                        Damage = 0;
                    }
                    Attacked.BlessEffect();
                }
                else if (AttackType == 3 || AttackType == 4)//Magic
                {
                    double reborn = 1;

                    Damage = Attacker.MAtk;
                    if (AttackType == 3)
                        Damage += SkillAttributes[3];
                    else
                        Damage = (SkillAttributes[3] * Attacker.MAtk);

                    if (Attacked.RBCount == 1)
                        reborn = 0.7;
                    else if (Attacked.RBCount == 2)
                        reborn = 0.5;

                    Damage *= ((double)(100 - Attacked.MDefense) / 100);
                    Damage -= Attacked.MagicBlock;
                    Damage *= 0.75;
                    Damage *= reborn;
                    Damage *= Attacked.Bless;
                    Damage *= Attacked.AddMythique;

                    if (Attacked.MDefense > 100)
                        Damage = 1;

                    if (Attacker.LuckTime > 0 && ChanceSuccess(10))
                    {
                        Damage *= 2;
                        World.ShowEffect(Attacker, "LuckyGuy");
                    }

                    if (Attacked.LuckTime > 0 && ChanceSuccess(10))
                    {
                        Damage = 1;
                        World.ShowEffect(Attacked, "LuckyGuy");
                    }

                    if (Damage < 1 || Attacked.MDefense >= 100)
                        Damage = 1;

                    if (Attacked.ReflectOn && ChanceSuccess(15))
                    {
                        if (Damage > 2000)
                            Damage = 2000;

                        Attacker.GetReflect((uint)Damage);
                        World.ShowEffect(Attacker, "WeaponReflect");
                        Damage = 0;
                    }
                    Attacked.BlessEffect();
                }
                return (uint)Damage;
            }
            else
            {
                Attacker.PTarget = null;
                return 0;
            }
        }

        public static uint CalculateDamage(Character Attacker, Monster Attacked, byte AttackType, ushort SkillId, byte SkillLvl)
        {
            Attacker.MobTarget = Attacked;
            double Damage = 0;
            ushort[] SkillAttributes = DataBase.SkillAttributes[SkillId][SkillLvl];

            if (AttackType == 0 || AttackType == 1)//Melee
            {
                Damage = MyMath.Generate((int)(Attacker.MinAtk), (int)(Attacker.MaxAtk));
                if (AttackType == 0)
                    Damage += SkillAttributes[3];
                else
                    Damage = (((double)SkillAttributes[3] / 100) * Damage);

                if (Attacker.StigBuff)
                    Damage = (Damage * (double)(1 + ((double)(10 + Attacker.StigLevel) / 100)));

                Damage = BattleSystem.AdjustDamageUser2Monster((uint)Damage, Attacker, Attacked);

                Damage -= Attacked.Defense;

                if (Attacker.SMOn && Attacked.Type != 6)
                    Damage *= 10;
                
                if (Attacker.SMOn && Attacked.Type == 6)
                    Damage *= 2;

                if (Attacker.LuckTime > 0 && ChanceSuccess(10))
                {
                    Damage *= 2;
                    World.ShowEffect(Attacker, "LuckyGuy");
                }

                if (Damage < 1)
                    Damage = 1;

                Damage = BattleSystem.AdjustMinDamageUser2Monster((uint)Damage, Attacker, Attacked);
            }
            else if (AttackType == 2)//Ranged
            {
                Damage = MyMath.Generate((int)(Attacker.MinAtk), (int)(Attacker.MaxAtk));
                Damage = (((double)SkillAttributes[3] / 100) * Damage);

                if (Attacker.StigBuff)
                    Damage = (Damage * (double)(1 + ((double)(10 + Attacker.StigLevel) / 100)));

                Damage = BattleSystem.AdjustDamageUser2Monster((uint)Damage, Attacker, Attacked);

                //Damage *= ((double)(100 - (Attacked.Dodge / 1.5)) / 100);

                if (Attacker.SMOn && Attacked.Type != 6)
                    Damage *= 10;

                if (Attacker.SMOn && Attacked.Type == 6)
                    Damage *= 2;

                if (Attacker.LuckTime > 0 && ChanceSuccess(10))
                {
                    Damage *= 2;
                    World.ShowEffect(Attacker, "LuckyGuy");
                }

                if (Damage < 1)
                    Damage = 1;

                Damage = BattleSystem.AdjustMinDamageUser2Monster((uint)Damage, Attacker, Attacked);
            }
            else if (AttackType == 3 || AttackType == 4)//Magic
            {
                Damage = Attacker.MAtk;
                if (AttackType == 3)
                    Damage += SkillAttributes[3];
                else
                    Damage = (SkillAttributes[3] * Attacker.MAtk);

                Damage = BattleSystem.AdjustDamageUser2Monster((uint)Damage, Attacker, Attacked);

                Damage *= ((100 - (double)Attacked.MDef) / 100);
                Damage *= 0.75;

                if (Attacker.LuckTime > 0 && ChanceSuccess(10))
                {
                    Damage *= 2;
                    World.ShowEffect(Attacker, "LuckyGuy");
                }

                if (Damage < 1)
                    Damage = 1;

                Damage = BattleSystem.AdjustMinDamageUser2Monster((uint)Damage, Attacker, Attacked);
            }

            if (Attacked.Type == 5)
                Damage = 0;

            if (Attacker.Potency < Attacked.BattleLvl)
                Damage /= 100;

            if (Attacked.LocMap == 2021 || Attacked.LocMap == 2022 || Attacked.LocMap == 2023 || Attacked.LocMap == 2024)
                Damage /= 10;

            return (uint)Damage;
        }

        public static uint CalculateDamage(Character Attacker, SingleNPC Attacked, byte AttackType, ushort SkillId, byte SkillLvl)
        {
            double Damage = 0;
            ushort[] SkillAttributes = DataBase.SkillAttributes[SkillId][SkillLvl];

            if (AttackType == 0 || AttackType == 1)//Melee
            {
                Damage = MyMath.Generate((int)(Attacker.MinAtk), (int)(Attacker.MaxAtk));
                if (AttackType == 0)
                    Damage += SkillAttributes[3];
                else
                    Damage = (((double)SkillAttributes[3] / 100) * Damage);

                if (Attacker.StigBuff)
                    Damage = (Damage * (double)(1 + ((double)(10 + Attacker.StigLevel) / 100)));

                if (Attacker.LuckTime > 0 && ChanceSuccess(10))
                {
                    Damage *= 2;
                    World.ShowEffect(Attacker, "LuckyGuy");
                }

                if (Damage < 1)
                    Damage = 1;
            }
            else if (AttackType == 2)//Ranged
            {
                Damage = MyMath.Generate((int)(Attacker.MinAtk), (int)(Attacker.MaxAtk));
                Damage = (((double)SkillAttributes[3] / 100) * Damage);

                if (Attacker.StigBuff)
                    Damage = (Damage * (double)(1 + ((double)(10 + Attacker.StigLevel) / 100)));

                Damage *= ((double)(100 - Attacked.Dodge) / 100);

                if (Attacker.LuckTime > 0 && ChanceSuccess(10))
                {
                    Damage *= 2;
                    World.ShowEffect(Attacker, "LuckyGuy");
                }

                if (Damage < 1)
                    Damage = 1;
            }
            else if (AttackType == 3 || AttackType == 4)//Magic
            {
                Damage = Attacker.MAtk;
                if (AttackType == 3)
                    Damage += SkillAttributes[3];
                else
                    Damage = (SkillAttributes[3] * Attacker.MAtk);

                if (Attacked.Sob == 2)
                    Damage *= 0.05;

                if (Attacker.LuckTime > 0 && ChanceSuccess(10))
                {
                    Damage *= 2;
                    World.ShowEffect(Attacker, "LuckyGuy");
                }

                if (Damage < 1)
                    Damage = 1;
            }

            if (Attacker.GuildID != 0)
            {
                if (Attacked.Sob == 2 && !Attacker.MyGuild.HoldingPole)
                {
                    Attacker.Silvers += (uint)(Damage / 100);
                    Attacker.GuildDonation += (int)(Damage / 100);
                    Attacker.MyGuild.Refresh(Attacker.UID, (int)Attacker.GuildDonation);
                    Attacker.MyClient.SendPacket(CoServer.MyPackets.GuildInfo(Attacker.MyGuild, Attacker));
                }
            }
            return (uint)Damage;
        }

        public static bool ChanceSuccess(double pc)
        {
            return ((double)Program.Rand.Next(1, 1000000)) / 10000 >= 100 - pc;
        }

        public static void LuckyDrop(Character TheChar)
        {
            if (TheChar.ItemsInInventory < 40)
            {
                if (MyMath.Success(0.005))
                {
                    World.ShowEffect(TheChar, "LuckyGuy");
                    World.SendMsgToAll("Parfait, " + TheChar.Name + " a obtenu une potion d'exp triple!", "SYSTEM", 2011);
                    TheChar.AddItem("720393-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                    return;
                }
                if (MyMath.Success(0.0025))
                {
                    World.ShowEffect(TheChar, "LuckyGuy");
                    World.SendMsgToAll("Parfait, " + TheChar.Name + " a obtenu une potion d'exp quintuple!", "SYSTEM", 2011);
                    TheChar.AddItem("720394-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                    return;
                }
                if (MyMath.Success(0.005))
                {
                    World.ShowEffect(TheChar, "LuckyGuy");
                    World.SendMsgToAll("Parfait, " + TheChar.Name + " a obtenu une pierre +5!", "SYSTEM", 2011);
                    TheChar.AddItem("730005-5-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                    return;
                }
                if (MyMath.Success(0.0025))
                {
                    World.ShowEffect(TheChar, "LuckyGuy");
                    World.SendMsgToAll("Parfait, " + TheChar.Name + " a obtenu une pierre +6!", "SYSTEM", 2011);
                    TheChar.AddItem("730006-6-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                    return;
                }
                if (MyMath.Success(0.00075))
                {
                    World.ShowEffect(TheChar, "LuckyGuy");
                    World.SendMsgToAll("Parfait, " + TheChar.Name + " a obtenu une pierre +7!", "SYSTEM", 2011);
                    TheChar.AddItem("730007-7-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                    return;
                }
                if (MyMath.Success(0.0005))
                {
                    World.ShowEffect(TheChar, "LuckyGuy");
                    World.SendMsgToAll("Parfait, " + TheChar.Name + " a obtenu une pierre +8!", "SYSTEM", 2011);
                    TheChar.AddItem("730008-8-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                    return;
                }
                if (MyMath.Success(0.0005))
                {
                    World.ShowEffect(TheChar, "LuckyGuy");
                    World.SendMsgToAll("Parfait, " + TheChar.Name + " a obtenu une foreuse de diamant!", "SYSTEM", 2011);
                    TheChar.AddItem("1200005-0-0-0-0-0", 0, (uint)Program.Rand.Next(345636635));
                    return;
                }
            }
        }

        public static uint GenerateGarment()
        {
            uint Item = 0;
            int Count = 0;
            int Do = Program.Rand.Next(3, 30);
            foreach (uint ItemId in DataBase.Items.Keys)
            {
                if (ItemHandler.ItemType2(ItemId) == 18)
                {
                    Count++;
                    Item = ItemId;
                    if (Count == Do)
                        break;
                }
            }
            return Item;
        }

        public static uint GenerateGem()
        {
            uint ItemId = 0;
            if (ChanceSuccess(5)) //Super Mythique
                ItemId = 700073;
            else //Raffined Gem
                ItemId = (uint)(700002 + (Program.Rand.Next(7) * 10));
            return ItemId;
        }

        public static uint GenerateEquip(byte Level, byte Quality)
        {
            ushort ItemType = 0;
            byte Color = (byte)Program.Rand.Next(3, 10);

            int Type = Program.Rand.Next(0, 7);
            switch (Type)
            {
                case 0: //Head
                    ItemType = 110;
                    break;
                case 1: //Necklace
                    ItemType = 120;
                    break;
                case 2: //Armor
                    ItemType = 130;
                    break;
                case 3: //Weapon
                    ItemType = 400;
                    break;
                case 4: //Shield
                    ItemType = 900;
                    break;
                case 5: //Ring
                    ItemType = 150;
                    break;
                case 6: //Boots
                    ItemType = 160;
                    break;
            }

            //Head
            if (ItemType == 110)
            {
                int WType = Program.Rand.Next(1, 7);
                if (WType == 1)
                    ItemType += 1;
                else if (WType == 2)
                    ItemType += 2;
                else if (WType == 3)
                    ItemType += 3;
                else if (WType == 4)
                    ItemType += 4;
                else if (WType == 5)
                    ItemType += 7;
                else if (WType == 6)
                    ItemType += 8;
            }

            //Necklace
            if (ItemType == 120)
                ItemType += (byte)Program.Rand.Next(0, 2);

            //Armor
            if (ItemType == 130)
            {
                int WType = Program.Rand.Next(1, 10);
                if (WType == 1)
                    ItemType += 1;
                else if (WType == 2)
                    ItemType += 2;
                else if (WType == 3)
                    ItemType += 3;
                else if (WType == 4)
                    ItemType += 4;
                else if (WType == 5)
                    ItemType += 5;
                else if (WType == 6)
                    ItemType += 6;
                else if (WType == 7)
                    ItemType += 8;
                else if (WType == 8)
                    ItemType += 9;
            }

            //Weapon
            if (ItemType == 400)
            {
                int WType = Program.Rand.Next(1, 18);
                if (WType == 1)
                    ItemType = 410;
                else if (WType == 2)
                    ItemType = 420;
                else if (WType == 3)
                    ItemType = 421;
                else if (WType == 4)
                    ItemType = 430;
                else if (WType == 5)
                    ItemType = 440;
                else if (WType == 6)
                    ItemType = 450;
                else if (WType == 7)
                    ItemType = 460;
                else if (WType == 8)
                    ItemType = 480;
                else if (WType == 9)
                    ItemType = 481;
                else if (WType == 10)
                    ItemType = 490;
                else if (WType == 11)
                    ItemType = 500;
                else if (WType == 12)
                    ItemType = 510;
                else if (WType == 13)
                    ItemType = 530;
                else if (WType == 14)
                    ItemType = 540;
                else if (WType == 15)
                    ItemType = 560;
                else if (WType == 16)
                    ItemType = 561;
                else if (WType == 17)
                    ItemType = 580;
            }

            //Ring
            if (ItemType == 150)
                ItemType += (byte)Program.Rand.Next(0, 3);

            uint ItemId = (uint)((ItemType * 1000) + (Level * 10) + Quality);
            if (((int)(ItemType / 10) == 11 && ItemType != 117) || (int)(ItemType / 10) == 13 || (int)(ItemType / 10) == 90)
                ItemId += (uint)(Color * 100);

            if (DataBase.Items.ContainsKey(ItemId))
                return ItemId;

            return 0;
        }

        public static uint GenerateCrap()
        {
            uint ItemId = 0;
            if (ChanceSuccess(25))
            {
                if (ChanceSuccess(30))
                    ItemId = 723711;
                else if (ChanceSuccess(20))
                    ItemId = 723583;
                else if (ChanceSuccess(20))
                    ItemId = 723584;
                else if (ChanceSuccess(15))
                    ItemId = 721259;
                else
                    ItemId = 723017;
                return ItemId;
            }
            else if (ChanceSuccess(20)) //Money Bag
            {
                ItemId = 723716;
                ItemId += (uint)Program.Rand.Next(0, 5);
                if (ChanceSuccess(3))
                    ItemId = 723721;
                if (ChanceSuccess(2))
                    ItemId = 723722;
                if (ChanceSuccess(1))
                    ItemId = 723723;
                return ItemId;
            }
            else if (ChanceSuccess(5)) //Rare
            {
                if (ChanceSuccess(10))
                    ItemId = 730006;
                else if (ChanceSuccess(5))
                    ItemId = 723701;
                else if (ChanceSuccess(2.5))
                    ItemId = 722057;
                else if (ChanceSuccess(2.5))
                    ItemId = 730007;
                else if (ChanceSuccess(1))
                    ItemId = 730008;
                else if (ChanceSuccess(1.75))
                    ItemId = 722058;
                else if (ChanceSuccess(5))
                    ItemId = 2100025;
                else
                    ItemId = 2100045;
                return ItemId;
            }
            else
            {
                ItemId = 730002;
                ItemId += (uint)Program.Rand.Next(0, 4);
                if (ChanceSuccess(20))
                    ItemId = 723712;
                if (ChanceSuccess(15))
                    ItemId = 723728;
                if (ChanceSuccess(10))
                    ItemId = 723729;
                if (ChanceSuccess(5))
                    ItemId = 723730;
                return ItemId;
            }
        }

        public static uint GenerateSpecial()
        {
            uint ItemId = 0;

            if (ChanceSuccess(25)) //ExpBall
                ItemId = 723700;
            else if (ChanceSuccess(20)) //MoneyBag 1
                ItemId = 723713;
            else if (ChanceSuccess(10)) //Grigri
                ItemId = 723727;
            else if (ChanceSuccess(10)) //+1Stone
                ItemId = 730001;
            else if (ChanceSuccess(10)) //DragonBall
                ItemId = 1088000;
            else if (ChanceSuccess(5)) //MoneyBag 2
                ItemId = 723714;
            else if (ChanceSuccess(5)) //MoneyBag 3
                ItemId = 723714;
            else
                ItemId = 1088001;

            return ItemId;
        }

        public static bool CharExist(string Needle, string HayStack)
        {
            int Find = HayStack.IndexOf(Needle);
            int Find2 = HayStack.LastIndexOf(Needle);
            return ((Find >= 0) && (Find2 >= 0));
        }

        public static bool CanEquip(uint ItemId, Character Charr)
        {
            if (ItemId.ToString().Length < 5)
                return false;

            if (!DataBase.Items.ContainsKey(ItemId))
                return false;

            Item TheItem = DataBase.Items[ItemId];

            if (TheItem.Id == ItemId)
            {
                byte TJob = 0;
                byte Model = 0;

                if (Charr.Job > 129 && Charr.Job < 136)
                    TJob = (byte)(Charr.Job + 60);
                else if (Charr.Job > 139 && Charr.Job < 146)
                    TJob = (byte)(Charr.Job + 50);
                else if (Charr.Job > 99 && Charr.Job < 116)
                    TJob = (byte)(Charr.Job + 90);
                else
                    TJob = Charr.Job;

                if (Charr.Model >= 2000)
                    Model = 2;
                else
                    Model = 1;

                if (Charr.Level < TheItem.Level) //LvlReq
                {
                    Charr.SendSysMsg("[Fraude]Vous essayez d'�quiper un objet que vous ne pouvez pas �quiper.");
                    CoServer.WriteCheat(Charr, "STR_EQUIP_CHEAT");
                    return false;
                }

                if (Charr.RBCount < 1 || Charr.RBCount > 0 && TheItem.Level > 70)
                    if (TheItem.Job != 0)
                        if (TJob < TheItem.Job) //JobReq
                        {
                            Charr.SendSysMsg("[Fraude]Vous essayez d'�quiper un objet que vous ne pouvez pas �quiper.");
                            CoServer.WriteCheat(Charr, "STR_EQUIP_CHEAT");
                            return false;
                        }

                if (TheItem.Sex != 0)
                    if (Model != TheItem.Sex) //SexReq
                    {
                        Charr.SendSysMsg("[Fraude]Vous essayez d'�quiper un objet que vous ne pouvez pas �quiper.");
                        CoServer.WriteCheat(Charr, "STR_EQUIP_CHEAT");
                        return false;
                    }

                if (Charr.Str < TheItem.Strength) //StrReq
                {
                    Charr.SendSysMsg("[Fraude]Vous essayez d'�quiper un objet que vous ne pouvez pas �quiper.");
                    CoServer.WriteCheat(Charr, "STR_EQUIP_CHEAT");
                    return false;
                }

                if (Charr.Agi < TheItem.Agility) //AgiReq
                {
                    Charr.SendSysMsg("[Fraude]Vous essayez d'�quiper un objet que vous ne pouvez pas �quiper.");
                    CoServer.WriteCheat(Charr, "STR_EQUIP_CHEAT");
                    return false;
                }

                if (ItemHandler.ItemType(TheItem.Id) == 4 || ItemHandler.ItemType(TheItem.Id) == 5 && ItemHandler.WeaponType(TheItem.Id) != 421)
                {
                    if (ItemHandler.WeaponType(TheItem.Id) == 500)
                        Charr.AtkType = 25;
                    else
                        Charr.AtkType = 2;

                    if (Charr.Profs.Contains(ItemHandler.WeaponType(TheItem.Id)))
                        if ((uint)Charr.Profs[ItemHandler.WeaponType(TheItem.Id)] < TheItem.Prof) //ProfReq
                        {
                            Charr.SendSysMsg("[Fraude]Vous essayez d'�quiper un objet que vous ne pouvez pas �quiper.");
                            CoServer.WriteCheat(Charr, "STR_EQUIP_CHEAT");
                            return false;
                        }
                }
            }

            if (ItemId == 1200006 || ItemId == 722343 || ItemId == 722344 || ItemId == 722345 || ItemId == 722346 || ItemId == 722347 || ItemId == 722348 || ItemId == 722348 || ItemId == 722349 || ItemId == 722350 || ItemId == 722351 || ItemId == 722352)
                return false;
            if (ItemId == 137910 || ItemId == 137810 || ItemId == 137710 || ItemId == 137610 || ItemId == 137510 || ItemId == 137410 || ItemId == 137310 && Charr.MyClient.Status != 8)
                return false;

            if (Charr.MyClient.Status == 7)
                return false;

            if (ItemHandler.WeaponType(ItemId) == 900) //Shield
                if (Charr.Job < 20 || Charr.Job > 25)
                    return false;

            return true;
        }

        public static bool Upgradable(uint item)
        {
            return (ItemHandler.ItemType2(item) == 90 || ItemHandler.ItemType2(item) == 11 || ItemHandler.ItemType2(item) == 12 || ItemHandler.ItemType2(item) == 13 || ItemHandler.ItemType2(item) == 15 || ItemHandler.ItemType2(item) == 16 || ItemHandler.ItemType(item) == 4 || ItemHandler.ItemType(item) == 5);
        }

        public static bool ArmorType(uint item)
        {
            return (ItemHandler.ItemType2(item) == 11 || ItemHandler.ItemType2(item) == 13);
        }

        public static Item ItemInfo(uint ItemId)
        {
            if (DataBase.Items.ContainsKey(ItemId))
                return DataBase.Items[ItemId];

            return new Item();
        }

        public static bool EquipMaxedLvl(uint ItemId)
        {
            uint NextId = EquipNextLevel(ItemId);
            if (!DataBase.Items.ContainsKey(NextId))
                return true;

            return false;
        }

        public static bool HaveQuality(uint ItemId)
        {
            if (ItemHandler.ItemType(ItemId) == 4) //One Hand Weapon
                return true;

            else if (ItemHandler.ItemType(ItemId) == 5) //Two Hand Weapon
                return true;

            else if (ItemHandler.ItemType(ItemId) == 9) //Shield
                return true;

            else if (ItemHandler.ItemType2(ItemId) == 11) //Head
                return true;

            else if (ItemHandler.ItemType2(ItemId) == 13) //Armor
                return true;

            else if (ItemHandler.ItemType2(ItemId) == 15) //Ring
                return true;

            else if (ItemHandler.ItemType2(ItemId) == 12) //Necklace
                return true;

            else if (ItemHandler.WeaponType(ItemId) == 160) //Boots
                return true;

            else
                return false;
        }

        public static short[] GetCoords(ushort PosMap, ushort PosX, ushort PosY)
        {
            short[] Coord = new short[2];
            short TheX = (short)PosX;
            short TheY = (short)PosY;

            while (true)
            {
                TheX = (short)(PosX + Program.Rand.Next(-1, 2));
                TheY = (short)(PosY + Program.Rand.Next(-1, 2));

                if (Other.PlaceFree((short)PosMap, (short)TheX, (short)TheY, false))
                {
                    Coord[0] = TheX;
                    Coord[1] = TheY;
                    return Coord;
                }
            }
        }

        public static bool PlaceFree(short PosMap, short PosX, short PosY, bool IsUniq)
        {
            if (IsUniq)
            {
                foreach (DroppedItem Item in DroppedItems.AllDroppedItems.Values)
                {
                    if (Item.Map == PosMap)
                        if (Item.X == PosX && Item.Y == PosY)
                            return false;
                }
            }

            ushort DMap = 0;
            if (DataBase.AllMaps.ContainsKey((ushort)PosMap))
                DMap = DataBase.AllMaps[(ushort)PosMap].DMap;

            if (DataBase.AllDMaps.ContainsKey(DMap)) //DataBase contains the DMap
            {
                if (!DataBase.AllDMaps[DMap].CanAcess((ushort)PosX, (ushort)PosY)) //The location isn't accessible
                    return false;
            }

            return true;
        }
    }
}
