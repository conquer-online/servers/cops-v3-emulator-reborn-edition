﻿using System;
using System.Collections.Generic;
using System.Timers;
using DMapReader;
using CoS_Math;

namespace COServer.AI
{
    public class MonsterAI
    {
        private Monster m_Owner;
        private Timer m_Brain;
        private ushort m_MoveSpeed;
        private ushort m_AtkSpeed;
        private byte m_ViewRange;
        private byte m_AtkRange;

        private bool IsInBattle;
        private bool m_IsThinking;

        private DateTime LastAttack;
        private DateTime LastMove;

        public MonsterAI(Monster Owner)
        {
            m_Owner = Owner;
            m_MoveSpeed = Owner.MoveSpeed;
            m_AtkSpeed = Owner.AtkSpeed;
            m_ViewRange = Owner.ViewRange;
            m_AtkRange = Owner.AtkRange;
        }

        ~MonsterAI()
        {
            m_Owner = null;
            if (m_Brain != null)
            {
                m_Brain.Stop();
                m_Brain = null;
            }
        }

        public Monster Owner { get { return m_Owner; } }
        public Timer Brain { get { return m_Brain; } }
        public ushort MoveSpeed { get { return m_MoveSpeed; } }
        public ushort AtkSpeed { get { return m_AtkSpeed; } }
        public byte ViewRange { get { return m_ViewRange; } }
        public byte AtkRange { get { return m_AtkRange; } }
        public bool IsThinking { get { return m_IsThinking; } }

        public void Start()
        {
            if (m_Brain == null)
            {
                m_Brain = new Timer();
                m_Brain.Interval += 500;
                m_Brain.Elapsed += new ElapsedEventHandler(Thinking);
                m_Brain.Start();
                m_IsThinking = true;
            }
        }

        public void Stop()
        {
            if (m_Brain != null)
            {
                m_Brain.Stop();
                m_Brain = null;
                m_IsThinking = false;
            }
        }

        private void Thinking(object source, ElapsedEventArgs e)
        {
            if (Owner.Target == null)
                GetTarget(Owner.Type == 4);
            else
            {
                if (IsInBattle && DateTime.Now > LastAttack.AddMilliseconds(AtkSpeed))
                {
                    CheckTarget();
                    Attack();
                }
                if (!IsInBattle && DateTime.Now > LastMove.AddMilliseconds(MoveSpeed))
                {
                    CheckTarget();
                    Move();
                }
            }
        }

        private void Move()
        {
            lock (Brain)
            {
                IsInBattle = false;

                if (Owner.Target == null)
                    return;

                if (CanAttack())
                {
                    Attack();
                    return;
                }

                if (MyMath.GetDistance(Owner.LocX, Owner.LocY, Owner.Target.LocX, Owner.Target.LocY) <= ViewRange)
                {
                    if (Owner.Type == 4)
                    {
                        Owner.PrevX = Owner.LocX;
                        Owner.PrevY = Owner.LocY;

                        Owner.LocX = (ushort)(Owner.Target.LocX + 2);
                        Owner.LocY = (ushort)(Owner.Target.LocY - 1);

                        Owner.Direction = (byte)MyMath.GetDirection(Owner.LocX, Owner.LocY, Owner.PrevX, Owner.PrevY);

                        World.MobJumps(Owner);
                        LastMove = DateTime.Now;
                        return;
                    }

                    if (MyMath.Success(95))
                    {
                        byte ToDir = (byte)(7 - (Math.Floor((MyMath.PointDirection(Owner.LocX, Owner.LocY, Owner.Target.LocX, Owner.Target.LocY) / 45 % 8))) - 1 % 8);

                        if (!PlaceFree(ToDir))
                            return;

                        Owner.PrevX = Owner.LocX;
                        Owner.PrevY = Owner.LocY;
                        if (ToDir > 7)
                            ToDir = 7;
                        Owner.Direction = ToDir;

                        switch (ToDir)
                        {
                            case 0: { Owner.LocY += 1; break; }
                            case 1: { Owner.LocX -= 1; Owner.LocY += 1; break; }
                            case 2: { Owner.LocX -= 1; break; }
                            case 3: { Owner.LocX -= 1; Owner.LocY -= 1; break; }
                            case 4: { Owner.LocY -= 1; break; }
                            case 5: { Owner.LocX += 1; Owner.LocY -= 1; break; }
                            case 6: { Owner.LocX += 1; break; }
                            case 7: { Owner.LocY += 1; Owner.LocX += 1; break; }
                        }

                        World.MobWalks(Owner, ToDir, MyMath.Success(85));
                        LastMove = DateTime.Now;
                    }
                }
            }
        }

        private bool PlaceFree(byte Dir)
        {
            ushort PosX = Owner.LocX;
            ushort PosY = Owner.LocY;

            switch (Dir)
            {
                case 0: { PosY += 1; break; }
                case 1: { PosX -= 1; PosY += 1; break; }
                case 2: { PosX -= 1; break; }
                case 3: { PosX -= 1; PosY -= 1; break; }
                case 4: { PosY -= 1; break; }
                case 5: { PosX += 1; PosY -= 1; break; }
                case 6: { PosX += 1; break; }
                case 7: { PosY += 1; PosX += 1; break; }
            }

            foreach (KeyValuePair<uint, Monster> DE in World.AllMobs)
            {
                Monster Mob = (Monster)DE.Value;
                if (Mob.LocMap == Owner.LocMap)
                    if (Mob.LocX == PosX && Mob.LocY == PosY)
                        return false;
            }

            ushort DMap = 0;
            if (DataBase.AllMaps.ContainsKey(Owner.LocMap))
                DMap = DataBase.AllMaps[Owner.LocMap].DMap;

            if (DataBase.AllDMaps.ContainsKey(DMap))
            {
                if (!DataBase.AllDMaps[DMap].CanAcess(PosX, PosY))
                    return false;
            }

            return true;
        }

        private void Attack()
        {
            lock (Brain)
            {
                IsInBattle = true;

                if (Owner.Target == null)
                {
                    IsInBattle = false;
                    return;
                }

                if (!CanAttack())
                {
                    Move();
                    return;
                }

                if (Owner.Target != null && Owner.Target.Alive)
                {
                    if (MyMath.Success(50))
                    {
                        if (Owner.Type != 2 && Owner.Type != 4) //Melee Attack
                        {
                            double Damage = MyMath.Generate((int)Owner.MinAtk, (int)Owner.MaxAtk);
                            Damage = BattleSystem.AdjustDamageMonster2User((uint)Damage, Owner, Owner.Target);

                            Damage -= Owner.Target.Defense; //Normal Defense

                            if (Owner.Target.Shield) //Warrior Shield (*3)
                                Damage -= Owner.Target.Defense * 2;

                            if (Owner.Target.IronShirt) //Iron Shirt (*4)
                                Damage -= Owner.Target.Defense * 3;

                            if (Owner.Target.MShieldBuff) //Magic Shield (+X%)
                                Damage -= (Owner.Target.Defense * (10 + Owner.Target.MShieldLevel * 5) / 100);

                            //Reborn Bonus
                            if (Owner.Target.RBCount == 1)
                                Damage *= 0.7;
                            else if (Owner.Target.RBCount == 2)
                                Damage *= 0.5;

                            Damage *= Owner.Target.Bless; //Bless Bonus
                            Damage *= Owner.Target.AddMythique; //Gem Bonus

                            if (Damage < 1)
                                Damage = 1;

                            //Lucky Time
                            if (Owner.Target.LuckTime > 0 && MyMath.Success(10))
                            {
                                Damage = 1;
                                World.ShowEffect(Owner.Target, "LuckyGuy");
                            }

                            //Reflect
                            if (Owner.Target.ReflectOn && MyMath.Success(15))
                            {
                                if (Damage > 2000)
                                    Damage = 2000;

                                World.MVM(Owner, Owner, 2, (uint)Damage);
                                World.ShowEffect(Owner, "WeaponReflect");
                                Owner.GetDamage(Owner.Target, (uint)Damage);
                                Damage = 0;
                            }

                            if (Owner.SkillId != 0)
                                World.MobAttacksCharSkill(Owner, Owner.Target, (uint)Damage, Owner.SkillId, 0);
                            else
                                World.MobAttacksChar(Owner, Owner.Target, 2, (uint)Damage);

                            if (Damage > 0)
                                Owner.Target.BlessEffect();

                            if (Owner.Target.GetHitDie((uint)Damage))
                            {
                                World.MobAttacksChar(Owner, Owner.Target, 14, (uint)Damage);
                                Owner.Target = null;
                            }

                            if (MyMath.Success(10))
                                Owner.Target = null;
                        }
                        else //Magic Attack
                        {
                            double Damage = Owner.MaxAtk;
                            Damage = BattleSystem.AdjustDamageMonster2User((uint)Damage, Owner, Owner.Target);

                            //Reborn Bonus
                            if (Owner.Target.RBCount == 1)
                                Damage *= 0.7;
                            else if (Owner.Target.RBCount == 2)
                                Damage *= 0.5;

                            Damage *= ((100 - (double)Owner.Target.MDefense) / 100); //Normal MDefense
                            Damage -= Owner.Target.MagicBlock; //Bonus MDefense
                            Damage *= 0.75; //Adjust Damage

                            Damage *= Owner.Target.Bless; //Bless Bonus
                            Damage *= Owner.Target.AddMythique; //Gem Bonus

                            if (Damage < 1 || Owner.Target.MDefense >= 100)
                                Damage = 1;

                            if (MyMath.Success(25) && Owner.Type != 4)
                                Damage = 0;

                            //Lucky Time
                            if (Owner.Target.LuckTime > 0 && MyMath.Success(10))
                            {
                                Damage = 1;
                                World.ShowEffect(Owner.Target, "LuckyGuy");
                            }

                            //Reflect
                            if (Owner.Target.ReflectOn && MyMath.Success(15))
                            {
                                if (Damage > 2000)
                                    Damage = 2000;

                                World.MVM(Owner, Owner, 2, (uint)Damage);
                                World.ShowEffect(Owner, "WeaponReflect");
                                Owner.GetDamage(Owner.Target, (uint)Damage);
                                Damage = 0;
                            }

                            if (Owner.SkillId != 0)
                                World.MobAttacksCharSkill(Owner, Owner.Target, (uint)Damage, Owner.SkillId, 0);
                            else
                                World.MobAttacksChar(Owner, Owner.Target, 2, (uint)Damage);

                            if (Damage > 0)
                                Owner.Target.BlessEffect();

                            if (Owner.Target.GetHitDie((uint)Damage))
                            {
                                if (Owner.Type == 4)
                                {
                                    if (Owner.Target.PKPoints > 100)
                                    {
                                        Owner.Target.Teleport(6000, 028, 071);
                                        World.SendMsgToAll(Owner.Target.Name + " a été envoyé en prison!", "SYSTEM", 2008);

                                    }
                                }
                                World.MobAttacksChar(Owner, Owner.Target, 14, (uint)Damage);
                                Owner.Target = null;
                            }

                            if (MyMath.Success(10))
                                Owner.Target = null;
                        }
                    }
                    LastAttack = DateTime.Now;
                }
                else
                    Owner.Target = null;
            }
        }

        private void GetTarget(bool TargetIsBlue)
        {
            lock (Brain)
            {
                lock (World.AllChars)
                {
                    byte ShortestDist = ViewRange;
                    foreach (Character Player in World.AllChars.Values)
                    {
                        if (Player.Screen.Contains(Owner.UID))
                        {
                            if (Owner.LocMap == Player.LocMap)
                            {
                                if (MyMath.GetDistance(Owner.LocX, Owner.LocY, Player.LocX, Player.LocY) == ShortestDist)
                                {
                                    if (Owner.Target.Level > Player.Level)
                                        if (TargetIsBlue && Player.BlueName || !TargetIsBlue)
                                        {
                                            Owner.Target = Player;
                                            ShortestDist = (byte)MyMath.GetDistance(Owner.LocX, Owner.LocY, Player.LocX, Player.LocY);
                                        }
                                }
                                else if (MyMath.GetDistance(Owner.LocX, Owner.LocY, Player.LocX, Player.LocY) < ShortestDist)
                                {
                                    if (TargetIsBlue && Player.BlueName || !TargetIsBlue)
                                    {
                                        Owner.Target = Player;
                                        ShortestDist = (byte)MyMath.GetDistance(Owner.LocX, Owner.LocY, Player.LocX, Player.LocY);
                                    }
                                }
                                else
                                    continue;
                            }
                        }
                    }
                }
            }
        }

        private void CheckTarget()
        {
            lock (Brain)
            {
                if (Owner.Target != null)
                    if (!Owner.Alive || Owner.Target.LocMap != Owner.LocMap)
                        Owner.Target = null;

                if (Owner.Target != null)
                    if (MyMath.GetDistance(Owner.LocX, Owner.LocY, Owner.Target.LocX, Owner.Target.LocY) > ViewRange)
                        Owner.Target = null;

                if (Owner.Type == 4)
                    if (Owner.Target != null)
                    {
                        if (!Owner.Target.BlueName)
                            Owner.Target = null;

                        if (Owner.Target.InvisibleBuff)
                            Owner.Target = null;
                    }

                if (Owner.Target != null)
                    if (Owner.Type != 2 && Owner.Type != 4)
                        if (Owner.Target.Flying)
                            Owner.Target = null;

                if (Owner.Type == 5) //Reviver
                    if (Owner.Target != null)
                    {
                        if (!Owner.Target.Alive)
                        {
                            Owner.Target.Revive(false);
                            World.MobAttacksCharSkill(Owner, Owner.Target, Owner.SkillId, 0);
                            Owner.Target = null;
                        }
                        else
                            Owner.Target = null;
                    }

                if (Owner.Target != null)
                    if (!Owner.Target.Alive)
                        Owner.Target = null;
            }
        }

        private bool CanAttack()
        {
            if (Owner.Target != null)
                if (MyMath.GetDistance(Owner.LocX, Owner.LocY, Owner.Target.LocX, Owner.Target.LocY) <= AtkRange)
                    return true;

            return false;
        }
    }
}
