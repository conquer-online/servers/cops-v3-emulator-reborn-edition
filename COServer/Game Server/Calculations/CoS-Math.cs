using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace CoS_Math
{
    public class MyMath
    {
        /// <summary>
        /// Return true if the distance between two coordinates is less than 17. (Normal Screen)
        /// </summary>
        public static bool CanSee(double x1, double y1, double x2, double y2)
        {
            long Value = (long)((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
            return Math.Sqrt(Value) < 17; 
        }

        /// <summary>
        /// Return true if the distance between two coordinates is less than 34. (Big Screen)
        /// </summary>
        public static bool CanSeeBig(double x1, double y1, double x2, double y2)
        {
            long Value = (long)((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
            return Math.Sqrt(Value) < 34; 
        }

        /// <summary>
        /// Return the direction for going to the second coordinate if you start at the first coordinate.
        /// The directions are limited at the eight cardinal points.
        /// </summary>
        [DllImport("CoS-Math")]
        public static extern int GetDirection(double x1, double y1, double x2, double y2);

        /// <summary>
        /// Return the direction for going to the second coordinate if you start at the first coordinate.
        /// </summary>
        public static double PointDirection(double x1, double y1, double x2, double y2)
        {
            double direction = 0;

            double AddX = x2 - x1;
            double AddY = y2 - y1;
            double r = (double)Math.Atan2(AddY, AddX);

            if (r < 0) r += (double)Math.PI * 2;

            direction = 360 - (r * 180 / (double)Math.PI);
            return direction;
        }

        /// <summary>
        /// Return the distance between two coordinates.
        /// </summary>
        public static int GetDistance(double x1, double y1, double x2, double y2)
        {
            long Value = (long)((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
            return (int)Math.Sqrt(Value);
        }

        /// <summary>
        /// Function for calculate a probability of chance in percent. (Precision -> 6)
        /// </summary>
        [DllImport("CoS-Math")]
        public static extern bool Success(double Chance);

        /// <summary>
        /// Generate a number in a specified range. (Max = Max--;)
        /// </summary>
        public static int Generate(int Min, int Max)//;
        {
            return COServer.Program.Rand.Next(Min, Max);
        }
    }
}