﻿using System;

namespace COServer
{
    public class BattleSystem
    {
        /// <summary>
        /// Return the minimal damage when the attacker is a Character and the target a Mob.
        /// Return the damage if the original damage is higther that the minimal.
        /// </summary>
        public static uint AdjustMinDamageUser2Monster(uint Damage, Character Attacker, Monster Target)
        {
            uint MinDmg = 1;
            MinDmg += (uint)(Attacker.Level / 10);

            if (Attacker.Equips[4] != null)
                MinDmg += (uint)ItemHandler.ItemQuality(uint.Parse(Attacker.Equips[4].Split('-')[0]));

            return Math.Max(MinDmg, Damage);
        }

        /// <summary>
        /// Adjust the damage when the attacker is a Character and the target a Mob.
        /// </summary>
        public static uint AdjustDamageUser2Monster(uint Damage, Character Attacker, Monster Target)
        {
            double Dmg = (double)Damage;

            if (!Target.IsGreen(Attacker))
                return Math.Max(0, (uint)Dmg);

            int DeltaLvl = Attacker.Level - Target.Level;
            if (DeltaLvl >= 3 && DeltaLvl <= 5)
                Dmg *= 1.5;
            else if (DeltaLvl > 5 && DeltaLvl <= 10)
                Dmg *= 2;
            else if (DeltaLvl > 10 && DeltaLvl <= 20)
                Dmg *= 2.5;
            else if (DeltaLvl > 20)
                Dmg *= 3;
            else
                Dmg *= 1;

            return Math.Max(0, (uint)Dmg);
        }

        /// <summary>
        /// Adjust the damage when the attacker is a Mob and the target a Character.
        /// </summary>
        public static uint AdjustDamageMonster2User(uint Damage, Monster Attacker, Character Target)
        {
            double Dmg = (double)Damage;

            if (Attacker.IsRed(Target))
                Dmg *= 1.5;
            else if (Attacker.IsBlack(Target))
            {
                int DeltaLvl = Target.Level - Attacker.Level;
                if (DeltaLvl >= -10 && DeltaLvl <= -5)
                    Dmg *= 2;
                else if (DeltaLvl >= -20 && DeltaLvl < -10)
                    Dmg *= 3.5;
                else if (DeltaLvl < -20)
                    Dmg *= 5;
                else
                    Dmg *= 1;
            }
            else
                Dmg *= 1;

            return Math.Max(0, (uint)Dmg);
        }

        /// <summary>
        /// Calculate the gained experience of an attack.
        /// </summary>
        public static uint AdjustExp(uint Damage, Character Attacker, Monster Target)
        {
            double Exp = Damage;

            int DeltaLvl = Attacker.Level - Target.Level;

            if (Target.IsGreen(Attacker))
            {
                if (DeltaLvl >= 3 && DeltaLvl <= 5)
                    Exp = Exp * 70 / 100;
                else if (DeltaLvl > 5 && DeltaLvl <= 10)
                    Exp = Exp * 20 / 100;
                else if (DeltaLvl > 10 && DeltaLvl <= 20)
                    Exp = Exp * 10 / 100;
                else if (DeltaLvl > 20)
                    Exp = Exp * 5 / 100;
            }
            else if (Target.IsRed(Attacker))
            {
                Exp *= 1.3;
            }
            else if (Target.IsBlack(Attacker))
            {
                if (DeltaLvl >= -10 && DeltaLvl < -5)
                    Exp *= 1.5;
                else if (DeltaLvl >= -20 && DeltaLvl < -10)
                    Exp *= 1.8;
                else if (DeltaLvl < -20)
                    Exp *= 2.3;
            }

            return (uint)Math.Max(0, Exp);
        }
    }
}
