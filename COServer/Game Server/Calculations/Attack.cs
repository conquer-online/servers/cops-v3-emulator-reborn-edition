﻿using System;

namespace COServer
{
    public partial class Calculation
    {
        /// <summary>
        /// Set the attack of the specified user.
        /// </summary>
        public static void SetAttack(Character User)
        {
            try
            {
                User.MinAtk = User.Str;
                User.MaxAtk = User.Str;
                User.MAtk = 0;
                User.RealAgi = User.Agi;
                User.ADS_Attack = 0;

                double AddAtkPct = 0;
                double AddMAtkPct = 0;
                double AddDextPct = 0;

                for (byte Pos = 0; Pos < 10; Pos++)
                {
                    if (User.Equips[Pos] != "0" && User.Equips[Pos] != null)
                    {
                        string[] Equip = User.Equips[Pos].Split('-');

                        uint Id = uint.Parse(Equip[0]);
                        byte Craft = byte.Parse(Equip[1]);
                        byte Soc1 = byte.Parse(Equip[4]);
                        byte Soc2 = byte.Parse(Equip[5]);
                        uint BonusId = ItemHandler.GetBonusId(Id, Pos);

                        if (!DataBase.Items.ContainsKey(Id))
                        {
                            Program.WriteLine("Item " + Id + " doesn't exist!");
                            continue;
                        }

                        Item Item = Other.ItemInfo(Id);
                        string[] Bonus = new string[10] { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0" };

                        foreach (string[] PlusInfo in DataBase.DBPlusInfo)
                        {
                            if (BonusId.ToString() == PlusInfo[0])
                                if (Craft == byte.Parse(PlusInfo[1]))
                                {
                                    Bonus = PlusInfo;
                                    break;
                                }
                        }

                        //Add the attack
                        if (Pos != 5)
                        {
                            User.MinAtk += (uint)(Item.MinAtk + uint.Parse(Bonus[3]));
                            User.MaxAtk += (uint)(Item.MaxAtk + uint.Parse(Bonus[4]));
                        }
                        else
                        {
                            User.MinAtk += (uint)(Item.MinAtk / 2 + uint.Parse(Bonus[3]));
                            User.MaxAtk += (uint)(Item.MaxAtk / 2 + uint.Parse(Bonus[4]));
                        }

                        if (CoServer.ADS && ItemHandler.WeaponType(Item.Id) == 500)
                            User.ADS_Attack += (ushort)((uint.Parse(Bonus[3]) + uint.Parse(Bonus[4])) / 2);

                        //Add the magic attack
                        User.MAtk += (uint)(Item.MagicAtk + uint.Parse(Bonus[6]));

                        //Add the dexterity
                        User.RealAgi += (ushort)(Item.Dexterity + ushort.Parse(Bonus[8]));

                        //Get gem effect
                        //DragonGem
                        if (Soc1 == 11)
                            AddAtkPct += 0.05;
                        if (Soc2 == 11)
                            AddAtkPct += 0.05;
                        if (Soc1 == 12)
                            AddAtkPct += 0.10;
                        if (Soc2 == 12)
                            AddAtkPct += 0.10;
                        if (Soc1 == 13)
                            AddAtkPct += 0.15;
                        if (Soc2 == 13)
                            AddAtkPct += 0.15;

                        //PhoenixGem
                        if (Soc1 == 1)
                            AddMAtkPct += 0.05;
                        if (Soc2 == 1)
                            AddMAtkPct += 0.05;
                        if (Soc1 == 2)
                            AddMAtkPct += 0.10;
                        if (Soc2 == 2)
                            AddMAtkPct += 0.10;
                        if (Soc1 == 3)
                            AddMAtkPct += 0.15;
                        if (Soc2 == 3)
                            AddMAtkPct += 0.15;

                        //FuryGem
                        if (Soc1 == 21)
                            AddDextPct += 0.10;
                        if (Soc2 == 21)
                            AddDextPct += 0.10;
                        if (Soc1 == 22)
                            AddDextPct += 0.15;
                        if (Soc2 == 22)
                            AddDextPct += 0.15;
                        if (Soc1 == 23)
                            AddDextPct += 0.25;
                        if (Soc2 == 23)
                            AddDextPct += 0.25;

                        //Get prof attack
                        if (Pos == 4 || Pos == 5)
                        {
                            short ProfId = (short)ItemHandler.WeaponType(Id);
                            if (User.Profs.Contains(ProfId))
                            {
                                byte ProfLvl = (byte)User.Profs[ProfId];
                                if (ProfLvl > 12 && ProfLvl < 21)
                                    AddAtkPct += ((double)(ProfLvl - 12) / 100);
                            }
                        }
                    }
                }

                //Add gem & prof attack
                User.MinAtk += (uint)((double)User.MinAtk * AddAtkPct);
                User.MaxAtk += (uint)((double)User.MaxAtk * AddAtkPct);
                User.MAtk += (uint)((double)User.MAtk * AddMAtkPct);
                User.RealAgi += (ushort)((double)User.RealAgi * AddDextPct);
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }
    }
}
