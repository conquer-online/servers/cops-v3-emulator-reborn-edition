﻿using System;

namespace COServer
{
    public partial class Calculation
    {
        /// <summary>
        /// Set the maximum of life of a specified player.
        /// </summary>
        public static ushort SetMaxHP(Character User, bool First)
        {
            if (!First)
                User.MaxHP -= User.BaseHP;

            double HP = 0;
            double HPFactor = 24;

            switch (User.Job)
            {
                case 11: //Trojan
                    HPFactor = 25.65;
                    break;
                case 12: //VeteranTrojan
                    HPFactor = 26.64;
                    break;
                case 13: //TigerTrojan
                    HPFactor = 27.3;
                    break;
                case 14: //DragonTrojan
                    HPFactor = 27.96;
                    break;
                case 15: //TrojanMaster
                    HPFactor = 28.95;
                    break;
                default:
                    HPFactor = 24;
                    break;
            }

            HP += User.Vit * HPFactor;
            HP += User.Str * 3 + User.Agi * 3 + User.Spi * 3;

            User.BaseHP = (ushort)HP;
            User.MaxHP += User.BaseHP;

            return User.MaxHP;
        }

        /// <summary>
        /// Set the maximum of magic of a specified player.
        /// </summary>
        public static ushort SetMaxMP(Character User, bool First)
        {
            if (!First)
                User.MaxMP -= User.BaseMP;

            double MP = 0;
            double MPFactor = 5;

            if (User.Job == 101) //Taoist
                MPFactor = 10;
            else if (User.Job == 132 || User.Job == 142) //GeneralTaoist
                MPFactor = 15;
            else if (User.Job == 133 || User.Job == 143) //Wizard
                MPFactor = 15;
            else if (User.Job == 134 || User.Job == 144) //Master
                MPFactor = 15;
            else if (User.Job == 135 || User.Job == 145) //Saint
                MPFactor = 30;
            else
                MPFactor = 5;

            MP += User.Spi * MPFactor;

            User.BaseMP = (ushort)MP;
            User.MaxMP += User.BaseMP;

            return User.MaxMP;
        }
    }
}
