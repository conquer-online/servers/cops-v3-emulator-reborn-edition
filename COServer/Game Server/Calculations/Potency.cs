﻿using System;

namespace COServer
{
    public partial class Calculation
    {
        /// <summary>
        /// Set the attack of the specified user.
        /// </summary>
        public static void SetPotency(Character User)
        {
            try
            {
                User.Potency = (ushort)(User.Level + (User.RBCount * 5));

                for (byte Pos = 0; Pos < 10; Pos++)
                {
                    if (User.Equips[Pos] != "0" && User.Equips[Pos] != null)
                    {
                        string[] Equip = User.Equips[Pos].Split('-');

                        uint Id = uint.Parse(Equip[0]);
                        byte Craft = byte.Parse(Equip[1]);
                        byte Soc1 = byte.Parse(Equip[4]);
                        byte Soc2 = byte.Parse(Equip[5]);

                        //Add the quality potency
                        if (ItemHandler.ItemQuality(Id) > 5 && ItemHandler.ItemQuality(Id) < 10)
                            User.Potency += (ushort)(ItemHandler.ItemQuality(Id) - 5);

                        //Add the craft potency
                        User.Potency += Craft;

                        //Add the socket potency
                        if (Soc1 != 0)
                            User.Potency++;
                        if (Soc2 != 0)
                            User.Potency++;

                        //Add the gem potency
                        if (ItemHandler.ItemQuality(Soc1) == 3)
                            User.Potency++;
                        if (ItemHandler.ItemQuality(Soc2) == 3)
                            User.Potency++;
                    }
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }
    }
}
