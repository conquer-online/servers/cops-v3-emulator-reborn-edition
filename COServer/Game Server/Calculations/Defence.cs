﻿using System;

namespace COServer
{
    public partial class Calculation
    {
        /// <summary>
        /// Set the attack of the specified user.
        /// </summary>
        public static void SetDefence(Character User)
        {
            try
            {
                User.Defense = 0;
                User.MDefense = 0;
                User.MagicBlock = 0;
                User.Dodge = 0;

                User.Bless = 1;
                User.AddMythique = 1;

                double AddBlessPct = 0;
                double AddDefPct = 0;

                for (byte Pos = 0; Pos < 10; Pos++)
                {
                    if (User.Equips[Pos] != "0" && User.Equips[Pos] != null)
                    {
                        string[] Equip = User.Equips[Pos].Split('-');

                        uint Id = uint.Parse(Equip[0]);
                        byte Craft = byte.Parse(Equip[1]);
                        byte Bless = byte.Parse(Equip[2]);
                        byte Soc1 = byte.Parse(Equip[4]);
                        byte Soc2 = byte.Parse(Equip[5]);
                        uint BonusId = ItemHandler.GetBonusId(Id, Pos);

                        if (!DataBase.Items.ContainsKey(Id))
                        {
                            Program.WriteLine("Item " + Id + " doesn't exist!");
                            continue;
                        }

                        Item Item = Other.ItemInfo(Id);
                        string[] Bonus = new string[10] { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0" };

                        foreach (string[] PlusInfo in DataBase.DBPlusInfo)
                        {
                            if (BonusId.ToString() == PlusInfo[0])
                                if (Craft == byte.Parse(PlusInfo[1]))
                                {
                                    Bonus = PlusInfo;
                                    break;
                                }
                        }

                        //Add the physical defence
                        User.Defense += (uint)(Item.Defence + uint.Parse(Bonus[5]));

                        //Add the magical defence
                        User.MDefense += (uint)(Item.MagicDef);
                        User.MagicBlock += uint.Parse(Bonus[7]);

                        //Add the dodge defence
                        User.Dodge += (byte)(Item.Dodge + byte.Parse(Bonus[9]));

                        //Add the bless bonus
                        AddBlessPct += (double)Bless / 100;

                        //Get gem effect
                        //TortoiseGem
                        if (Soc1 == 71)
                            AddDefPct += 0.02;
                        if (Soc2 == 71)
                            AddDefPct += 0.02;
                        if (Soc1 == 72)
                            AddDefPct += 0.04;
                        if (Soc2 == 72)
                            AddDefPct += 0.04;
                        if (Soc1 == 73)
                            AddDefPct += 0.06;
                        if (Soc2 == 73)
                            AddDefPct += 0.06;
                    }
                }
                if (User.MDefense > 100)
                    User.MDefense = 100;

                if (AddBlessPct > 1)
                    AddBlessPct = 1;

                if (AddDefPct > 1)
                    AddDefPct = 1;

                User.Bless -= AddBlessPct;
                User.AddMythique -= AddDefPct;
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }
    }
}
