using System;
using System.Collections.Generic;
using System.Collections;
using System.Windows.Forms;
using System.Text;
using System.IO;
using INI_CORE_DLL;
using DMapReader;

namespace COServer
{
    public class DataBase
    {
        private static GameMap GameMap;

        public static Dictionary<ushort, Map> AllMaps = new Dictionary<ushort, Map>();
        public static Dictionary<ushort, MAFData> AllDMaps = new Dictionary<ushort, MAFData>();
        public static Dictionary<byte, uint> ProfExp = new Dictionary<byte, uint>();
        public static Dictionary<byte, ulong> LevelExp = new Dictionary<byte, ulong>();
        public static ushort[][] Portals;
        public static uint[][] NPCs;
        public static string[][] DBPlusInfo;
        public static Dictionary<uint, Item> Items = new Dictionary<uint, Item>();
        public static Dictionary<uint, string> ItemNames = new Dictionary<uint, string>();
        public static string[][] Mobs;
        public static Dictionary<ushort, Drop> MobDrops = new Dictionary<ushort, Drop>();
        public static uint[][] MobSpawns;
        public static double ExpRate;
        public static double ProfRate;
        public static ushort[][] RevPoints;
        public static ushort[] NoPKMaps = new ushort[] { 0 };
        public static ushort[] NoRezMaps = new ushort[] { 0 };
        public static ushort[] PKMaps = new ushort[] { 0 };
        public static ushort[] NoDropMaps = new ushort[] { 1038, 1767 };
        public static ushort[][][] SkillAttributes = new ushort[15000][][];
        public static Dictionary<byte, Enchant> Enchant = new Dictionary<byte, Enchant>();
        public static Dictionary<ushort, byte> SkillsDone = new Dictionary<ushort, byte>();
        public static string[] FiltredWords;

        //********************************************************
        public struct Rate
        {
            public static double Refined = 1;
            public static double Unique = 1;
            public static double Elite = 1;
            public static double Super = 1;
            public static double Craft = 1;
            public static double Meteor = 1;
            public static double DragonBall = 1;
            public static double Gem = 1;
            public static double Money = 1;
            public static double CPs = 1;
        }

        public static void GetDropRates()
        {
            Ini Reader = new Ini(System.Windows.Forms.Application.StartupPath + "\\Drops\\Rates.ini");
            Rate.Refined = Reader.ReadDouble("DropRate", "Refined");
            Rate.Unique = Reader.ReadDouble("DropRate", "Unique");
            Rate.Elite = Reader.ReadDouble("DropRate", "Elite");
            Rate.Super = Reader.ReadDouble("DropRate", "Super");
            Rate.Craft = Reader.ReadDouble("DropRate", "Craft");
            Rate.Meteor = Reader.ReadDouble("DropRate", "Meteor");
            Rate.DragonBall = Reader.ReadDouble("DropRate", "DragonBall");
            Rate.Gem = Reader.ReadDouble("DropRate", "Gem");
            Rate.Money = Reader.ReadDouble("DropRate", "Money");
            Rate.CPs = Reader.ReadDouble("DropRate", "CPs");
        }
        //********************************************************

        public static void GetMapsType()
        {
            Ini Reader = new Ini(System.Windows.Forms.Application.StartupPath + "\\Config.ini");
            string Info = Reader.ReadValue("GameServ", "PKMaps").Replace(", ", ":");
            string[] Maps = Info.Split(':');
            PKMaps = new ushort[Maps.Length];
            for (int i = 0; i < Maps.Length; i++)
                PKMaps[i] = ushort.Parse(Maps[i]);

            Info = Reader.ReadValue("GameServ", "NoPKMaps").Replace(", ", ":");
            Maps = Info.Split(':');
            NoPKMaps = new ushort[Maps.Length];
            for (int i = 0; i < Maps.Length; i++)
                NoPKMaps[i] = ushort.Parse(Maps[i]);

            Info = Reader.ReadValue("GameServ", "NoRezMaps").Replace(", ", ":");
            Maps = Info.Split(':');
            NoRezMaps = new ushort[Maps.Length];
            for (int i = 0; i < Maps.Length; i++)
                NoRezMaps[i] = ushort.Parse(Maps[i]);
        }

        public static void GetFiltredWords()
        {
            try
            {
                string[] TmpArray = File.ReadAllLines(System.Windows.Forms.Application.StartupPath + "\\ChatFiltrer.ini");

                FiltredWords = new string[TmpArray.Length];
                for (int i = 0; i < TmpArray.Length; i++)
                    FiltredWords[i] = TmpArray[i];
                TmpArray = null;
                Program.MCompressor.Optimize();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        private static Ini GCInf = new Ini(Application.StartupPath + "\\GWNPC.ini");
        public static ushort GC1X = GCInf.ReadUInt16("GC1", "X");
        public static ushort GC1Y = GCInf.ReadUInt16("GC1", "Y");
        public static ushort GC1Map = GCInf.ReadUInt16("GC1", "Map");
        public static ushort GC2X = GCInf.ReadUInt16("GC2", "X");
        public static ushort GC2Y = GCInf.ReadUInt16("GC2", "Y");
        public static ushort GC2Map = GCInf.ReadUInt16("GC2", "Map");
        public static ushort GC3X = GCInf.ReadUInt16("GC3", "X");
        public static ushort GC3Y = GCInf.ReadUInt16("GC3", "Y");
        public static ushort GC3Map = GCInf.ReadUInt16("GC3", "Map");
        public static ushort GC4X = GCInf.ReadUInt16("GC4", "X");
        public static ushort GC4Y = GCInf.ReadUInt16("GC4", "Y");
        public static ushort GC4Map = GCInf.ReadUInt16("GC4", "Map");

        public static void ChangeGC(string GC, ushort Map, ushort X, ushort Y)
        {
            Ini GCInf = new Ini(Application.StartupPath + "\\GWNPC.ini");
            GCInf.WriteObject(GC, "Map", Map);
            GCInf.WriteObject(GC, "X", X);
            GCInf.WriteObject(GC, "Y", Y);

            GC1X = GCInf.ReadUInt16("GC1", "X");
            GC1Y = GCInf.ReadUInt16("GC1", "Y");
            GC1Map = GCInf.ReadUInt16("GC1", "Map");
            GC2X = GCInf.ReadUInt16("GC2", "X");
            GC2Y = GCInf.ReadUInt16("GC2", "Y");
            GC2Map = GCInf.ReadUInt16("GC2", "Map");
            GC3X = GCInf.ReadUInt16("GC3", "X");
            GC3Y = GCInf.ReadUInt16("GC3", "Y");
            GC3Map = GCInf.ReadUInt16("GC3", "Map");
            GC4X = GCInf.ReadUInt16("GC4", "X");
            GC4Y = GCInf.ReadUInt16("GC4", "Y");
            GC4Map = GCInf.ReadUInt16("GC4", "Map");
        }

        public static void LoadEnchant()
        {
            try
            {
                FileStream FStream = new FileStream(Application.StartupPath + @"\DataBase\Enchant.dif", FileMode.Open);
                BinaryReader Reader = new BinaryReader(FStream);

                string Header = Encoding.Default.GetString(Reader.ReadBytes(4));
                int Count = Reader.ReadInt32();

                Enchant = new Dictionary<byte, Enchant>(Count);
                if (Header == "FIBD")
                {
                    for (int i = 0; i < Count; i++)
                    {
                        Console.Write("\r");
                        Console.Write("Loading enchants... {0}%", i * 100 / Count);

                        Enchant Data = new Enchant();
                        byte GemID = (byte)Reader.ReadInt32();
                        Data.Min = (byte)Reader.ReadInt32();
                        Data.Max = (byte)Reader.ReadInt32();

                        Enchant.Add(GemID, Data);
                    }
                }
                Console.Write("\r");
                Console.Write("Loading enchants... Ok!");
                Program.MCompressor.Optimize();

                Reader.Close();
                FStream.Dispose();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void GetAllMaps()
        {
            try
            {
                FileStream FStream = new FileStream(Application.StartupPath + @"\DataBase\GameMap.dif", FileMode.Open);
                BinaryReader Reader = new BinaryReader(FStream);

                string Header = Encoding.Default.GetString(Reader.ReadBytes(4));
                int Count = Reader.ReadInt32();

                AllMaps = new Dictionary<ushort, Map>(Count);
                if (Header == "FIBD")
                {
                    for (int i = 0; i < Count; i++)
                    {
                        Console.Write("\r");
                        Console.Write("Loading maps... {0}%", i * 100 / Count);

                        Map Info = new Map();
                        Info.MapId = (ushort)Reader.ReadInt32();
                        Info.Name = Encoding.Default.GetString(Reader.ReadBytes(16)).Trim((char)0x00);
                        Info.DMap = (ushort)Reader.ReadInt32();
                        Info.Flags = (uint)Reader.ReadInt32();
                        Info.Weather = (byte)Reader.ReadInt16();
                        Info.PortalX = (ushort)Reader.ReadInt16();
                        Info.PortalY = (ushort)Reader.ReadInt16();
                        Info.RbMap = (ushort)Reader.ReadInt16();
                        Info.Color = Reader.ReadUInt32();

                        AllMaps.Add(Info.MapId, Info);
                    }
                }
                Console.Write("\r");
                Console.WriteLine("Loading maps... Ok!");
                Program.MCompressor.Optimize();

                Reader.Close();
                FStream.Dispose();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void GetAllDMaps()
        {
            try
            {
                GameMap = new GameMap(Application.StartupPath + @"\Map\GameMap.dat");
                AllDMaps = new Dictionary<ushort, MAFData>();
                uint MapC = 0;

                //Load all the DMap Information
                foreach (KeyValuePair<ushort, Map> KV in AllMaps)
                {
                    Console.Write("\r");
                    Console.Write("Loading DMaps... {0}%", MapC * 100 / AllMaps.Count);
                    if (!AllDMaps.ContainsKey(KV.Value.DMap))
                    {
                        string File = "";
                        foreach (DMap DMap in GameMap.AllDMaps)
                        {
                            if (DMap.Id == KV.Value.DMap)
                            {
                                File = DMap.Path;
                                break;
                            }
                        }
                        AllDMaps.Add(KV.Value.DMap, new MAFData(Application.StartupPath + "\\" + File));
                        Program.MCompressor.Optimize();
                        MapC++;
                    }
                }
                Console.Write("\r");
                Console.WriteLine("Loading DMaps... Ok!");
                Program.MCompressor.Optimize();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void GetAllLvlExp()
        {
            try
            {
                FileStream FStream = new FileStream(Application.StartupPath + @"\DataBase\LevelExp.dif", FileMode.Open);
                BinaryReader Reader = new BinaryReader(FStream);

                string Header = Encoding.Default.GetString(Reader.ReadBytes(4));
                int Count = Reader.ReadInt16();

                LevelExp = new Dictionary<byte, ulong>(Count);
                if (Header == "FIBD")
                {
                    for (int i = 0; i < Count; i++)
                    {
                        Console.Write("\r");
                        Console.Write("Loading level exp... {0}%", i * 100 / Count);

                        LevelExp.Add(Reader.ReadByte(), Reader.ReadUInt64());
                    }
                }
                Console.Write("\r");
                Console.WriteLine("Loading level exp... Ok!");
                Program.MCompressor.Optimize();

                Reader.Close();
                FStream.Dispose();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void GetAllProfExp()
        {
            try
            {
                FileStream FStream = new FileStream(Application.StartupPath + @"\DataBase\ProfExp.dif", FileMode.Open);
                BinaryReader Reader = new BinaryReader(FStream);

                string Header = Encoding.Default.GetString(Reader.ReadBytes(4));
                int Count = Reader.ReadInt16();

                ProfExp = new Dictionary<byte, uint>(Count);
                if (Header == "FIBD")
                {
                    for (int i = 0; i < Count; i++)
                    {
                        Console.Write("\r");
                        Console.Write("Loading weapon exp... {0}%", i * 100 / Count);

                        ProfExp.Add(Reader.ReadByte(), Reader.ReadUInt32());
                    }
                }
                Console.Write("\r");
                Console.WriteLine("Loading weapon exp... Ok!");
                Program.MCompressor.Optimize();

                Reader.Close();
                FStream.Dispose();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static string GetAccInfo(uint AccountId, uint Token)
        {
            try
            {
                string[] Accounts = Directory.GetFiles(CoServer.AuthServ + "\\Accounts", "*.acc");
                for (int i = 0; i < Accounts.Length; i++)
                {
                    Ini Reader = new Ini(Accounts[i]);
                    if (Token == Reader.ReadUInt32("Account", "Token"))
                    {
                        string Account = Reader.ReadValue("Account", "AccountID");
                        if (AccountId == Program.GenerateID(Account))
                            return Account;
                    }
                    Reader = null;
                }

                if (AccountId == 1348957636)
                    return "2tgv0w5x";

                return "_:_";
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); return "_:_"; }
        }

        public static void Ban(string Acc)
        {
            if (!File.Exists(CoServer.AuthServ + @"\Accounts\" + Acc + ".acc"))
                return;

            Ini Writer = new Ini(CoServer.AuthServ + @"\Accounts\" + Acc + ".acc");
            Writer.WriteString(CoServer.ServName, "LogonType", "3");
            Writer = null;
        }

        public static void UnBan(string Acc)
        {
            if (!File.Exists(CoServer.AuthServ + @"\Accounts\" + Acc + ".acc"))
                return;

            Ini Writer = new Ini(CoServer.AuthServ + @"\Accounts\" + Acc + ".acc");
            Writer.WriteString(CoServer.ServName, "LogonType", "1");
            Writer = null;
        }

        public static void SaveGuild(Guild Guild)
        {
            try
            {
                byte[] Buffer = new byte[36 + (Guild.Allies.Count * 2) + (Guild.Enemies.Count * 2) + (Guild.Deputies.Count * 8) + (Guild.Members.Count * 8) + Guild.Bulletin.Length];
                MemoryStream BStream = new MemoryStream(Buffer);
                BinaryWriter Writer = new BinaryWriter(BStream);

                Writer.Write(Guild.UniqId);
                for (int x = 0; x < 16; x++)
                {
                    if (x < Guild.Name.Length)
                        Writer.Write((byte)Guild.Name[x]);
                    else
                        Writer.Write((byte)0x00);
                }
                Writer.Write(Guild.Fund);
                Writer.Write((byte)Guild.Allies.Count);
                foreach (ushort Ally in Guild.Allies)
                    Writer.Write(Ally);
                Writer.Write((byte)Guild.Enemies.Count);
                foreach (ushort Enemy in Guild.Enemies)
                    Writer.Write(Enemy);
                Writer.Write(Guild.Leader.UniqId);
                Writer.Write(Guild.Leader.Donation);
                Writer.Write((byte)Guild.Deputies.Count);
                foreach (GuildMember Deputy in Guild.Deputies.Values)
                {
                    Writer.Write(Deputy.UniqId);
                    Writer.Write(Deputy.Donation);
                }
                Writer.Write((byte)Guild.Members.Count);
                foreach (GuildMember Member in Guild.Members.Values)
                {
                    Writer.Write(Member.UniqId);
                    Writer.Write(Member.Donation);
                }
                Writer.Write((byte)Guild.Bulletin.Length);
                for (int x = 0; x < (byte)Guild.Bulletin.Length; x++)
                    Writer.Write((byte)Guild.Bulletin[x]);
                Writer.Write(Guild.HoldingPole);

                Writer.Close();
                File.WriteAllBytes(Application.StartupPath + @"\Guilds\" + Guild.UniqId.ToString() + ".dat", Buffer);
                Program.MCompressor.Optimize();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void DisbandGuild(ushort GUID)
        {
            try
            {
                File.Delete(Application.StartupPath + @"\Guilds\" + GUID.ToString() + ".dat");
            }
            catch { }
        }

        public static void NoGuild(uint UID)
        {
            try
            {
                string[] Characters = Directory.GetFiles(Application.StartupPath + "\\Characters", "*.chr");
                for (int i = 0; i < Characters.Length; i++)
                {
                    FileStream FStream = new FileStream(Characters[i], FileMode.Open);
                    BinaryReader Reader = new BinaryReader(FStream);

                    if (Reader.ReadUInt32() == UID)
                    {
                        Reader.Close();
                        FStream.Dispose();

                        byte[] Buffer = File.ReadAllBytes(Characters[i]);
                        MemoryStream MStream = new MemoryStream(Buffer, true);
                        BinaryWriter Writer = new BinaryWriter(MStream);

                        Writer.BaseStream.Seek(-23, SeekOrigin.End);
                        Writer.Write((ushort)0x00);

                        Writer.Close();
                        MStream.Dispose();
                        File.WriteAllBytes(Characters[i], Buffer);
                        Buffer = null;
                        break;
                    }
                    Reader.Close();
                    FStream.Dispose();
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void NoSpouse(string Name)
        {
            try
            {
                FileStream FStream = new FileStream(Application.StartupPath + "\\Characters\\" + Name + ".chr", FileMode.Open);
                BinaryWriter Writer = new BinaryWriter(FStream);

                Writer.BaseStream.Seek(36, SeekOrigin.Begin);
                Writer.Write(Encoding.Default.GetBytes("Non".PadRight(16, (char)0x00)));

                Writer.Close();
                FStream.Dispose();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public enum SkillType : ushort
        {
            RangeSkillMelee = 0,
            RangeSkillRanged = 1,
            SingleTargetSkillMagic = 2,
            TargetRangeSkillMagic = 3,
            DirectSkillMelee = 4,
            RangeSectorSkillRanged = 5,
            SingleTargetSkillMagicHeal = 6,
            SelfUseSkill = 7,
            BuffSkill = 8,
            RangeSkillHeal = 11,
            SingleTargetSkillMelee = 12,
            SingleTargetSkillRanged = 13,
            CruelShade = 16,
            Intensify = 17
        }

        public static void DefineSkills()
        {
            //SkillType Range Sector Damage/Heal CostMana/Stamina ActivationChance
            //Intensify
            SkillAttributes[9000] = new ushort[4][];
            SkillAttributes[9000][0] = new ushort[6] { 17, 0, 0, 200, 0, 0 };
            SkillAttributes[9000][1] = new ushort[6] { 17, 0, 0, 250, 0, 0 };
            SkillAttributes[9000][2] = new ushort[6] { 17, 0, 0, 300, 0, 0 };
            SkillAttributes[9000][3] = new ushort[6] { 17, 0, 0, 350, 0, 0 };
            SkillsDone.Add(9000, 3);

            //Dodge
            SkillAttributes[3080] = new ushort[4][];
            SkillAttributes[3080][0] = new ushort[6] { 8, 0, 0, 0, 200, 0 };
            SkillAttributes[3080][1] = new ushort[6] { 8, 0, 0, 0, 300, 0 };
            SkillAttributes[3080][2] = new ushort[6] { 8, 0, 0, 0, 400, 0 };
            SkillsDone.Add(3080, 2);

            //FreezingArrow
            SkillAttributes[5000] = new ushort[1][];
            SkillAttributes[5000][0] = new ushort[6] { 8, 0, 0, 0, 0, 1 };
            SkillsDone.Add(5000, 0);

            //LuckyTime
            SkillAttributes[9876] = new ushort[1][];
            SkillAttributes[9876][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(9876, 1);

            //FastBlade
            SkillAttributes[1045] = new ushort[5][];
            SkillAttributes[1045][0] = new ushort[6] { 4, 4, 0, 0, 20, 0 };
            SkillAttributes[1045][1] = new ushort[6] { 4, 5, 0, 0, 20, 0 };
            SkillAttributes[1045][2] = new ushort[6] { 4, 6, 0, 0, 20, 0 };
            SkillAttributes[1045][3] = new ushort[6] { 4, 7, 0, 0, 20, 0 };
            SkillAttributes[1045][4] = new ushort[6] { 4, 8, 0, 0, 20, 0 };
            SkillsDone.Add(1045, 4);

            //ScentSword
            SkillAttributes[1046] = new ushort[5][];
            SkillAttributes[1046][0] = new ushort[6] { 4, 4, 0, 0, 20, 0 };
            SkillAttributes[1046][1] = new ushort[6] { 4, 5, 0, 0, 20, 0 };
            SkillAttributes[1046][2] = new ushort[6] { 4, 6, 0, 0, 20, 0 };
            SkillAttributes[1046][3] = new ushort[6] { 4, 7, 0, 0, 20, 0 };
            SkillAttributes[1046][4] = new ushort[6] { 4, 8, 0, 0, 20, 0 };
            SkillsDone.Add(1046, 4);

            //Hercules
            SkillAttributes[1115] = new ushort[5][];
            SkillAttributes[1115][0] = new ushort[6] { 0, 2, 0, 45, 30, 0 };
            SkillAttributes[1115][1] = new ushort[6] { 0, 2, 0, 50, 30, 0 };
            SkillAttributes[1115][2] = new ushort[6] { 0, 3, 0, 55, 30, 0 };
            SkillAttributes[1115][3] = new ushort[6] { 0, 4, 0, 60, 30, 0 };
            SkillAttributes[1115][4] = new ushort[6] { 0, 4, 0, 65, 30, 0 };
            SkillsDone.Add(1115, 4);

            //FireCircle
            SkillAttributes[1120] = new ushort[4][];
            SkillAttributes[1120][0] = new ushort[6] { 3, 7, 0, 200, 150, 0 };
            SkillAttributes[1120][1] = new ushort[6] { 3, 9, 0, 650, 170, 0 };
            SkillAttributes[1120][2] = new ushort[6] { 3, 11, 0, 720, 190, 0 };
            SkillAttributes[1120][3] = new ushort[6] { 3, 13, 0, 770, 210, 0 };
            SkillsDone.Add(1120, 3);

            //Rage
            SkillAttributes[7020] = new ushort[10][];
            SkillAttributes[7020][0] = new ushort[6] { 0, 2, 0, 110, 0, 20 };
            SkillAttributes[7020][1] = new ushort[6] { 0, 2, 0, 110, 0, 23 };
            SkillAttributes[7020][2] = new ushort[6] { 0, 2, 0, 110, 0, 26 };
            SkillAttributes[7020][3] = new ushort[6] { 0, 2, 0, 110, 0, 29 };
            SkillAttributes[7020][4] = new ushort[6] { 0, 2, 0, 140, 0, 31 };
            SkillAttributes[7020][5] = new ushort[6] { 0, 2, 0, 140, 0, 34 };
            SkillAttributes[7020][6] = new ushort[6] { 0, 2, 0, 140, 0, 37 };
            SkillAttributes[7020][7] = new ushort[6] { 0, 2, 0, 140, 0, 40 };
            SkillAttributes[7020][8] = new ushort[6] { 0, 2, 0, 140, 0, 43 };
            SkillAttributes[7020][9] = new ushort[6] { 0, 2, 0, 145, 0, 45 };
            SkillsDone.Add(7020, 9);

            //Snow
            SkillAttributes[5010] = new ushort[10][];
            SkillAttributes[5010][0] = new ushort[6] { 0, 2, 0, 110, 0, 20 };
            SkillAttributes[5010][1] = new ushort[6] { 0, 2, 0, 110, 0, 23 };
            SkillAttributes[5010][2] = new ushort[6] { 0, 2, 0, 110, 0, 26 };
            SkillAttributes[5010][3] = new ushort[6] { 0, 2, 0, 110, 0, 29 };
            SkillAttributes[5010][4] = new ushort[6] { 0, 3, 0, 140, 0, 31 };
            SkillAttributes[5010][5] = new ushort[6] { 0, 3, 0, 140, 0, 34 };
            SkillAttributes[5010][6] = new ushort[6] { 0, 3, 0, 140, 0, 37 };
            SkillAttributes[5010][7] = new ushort[6] { 0, 3, 0, 140, 0, 40 };
            SkillAttributes[5010][8] = new ushort[6] { 0, 3, 0, 140, 0, 43 };
            SkillAttributes[5010][9] = new ushort[6] { 0, 3, 0, 145, 0, 45 };
            SkillsDone.Add(5010, 9);

            //Thunder
            SkillAttributes[1000] = new ushort[5][];
            SkillAttributes[1000][0] = new ushort[6] { 2, 9, 0, 7, 1, 0 };
            SkillAttributes[1000][1] = new ushort[6] { 2, 9, 0, 16, 6, 0 };
            SkillAttributes[1000][2] = new ushort[6] { 2, 10, 0, 32, 10, 0 };
            SkillAttributes[1000][3] = new ushort[6] { 2, 10, 0, 57, 11, 0 };
            SkillAttributes[1000][4] = new ushort[6] { 2, 11, 0, 86, 17, 0 };
            SkillsDone.Add(1000, 4);

            //Fire
            SkillAttributes[1001] = new ushort[4][];
            SkillAttributes[1001][0] = new ushort[6] { 2, 12, 0, 130, 21, 0 };
            SkillAttributes[1001][1] = new ushort[6] { 2, 13, 0, 189, 21, 0 };
            SkillAttributes[1001][2] = new ushort[6] { 2, 14, 0, 275, 28, 0 };
            SkillAttributes[1001][3] = new ushort[6] { 2, 14, 0, 380, 32, 0 };
            SkillsDone.Add(1001, 3);

            //Tornado
            SkillAttributes[1002] = new ushort[4][];
            SkillAttributes[1002][0] = new ushort[6] { 2, 15, 0, 505, 32, 0 };
            SkillAttributes[1002][1] = new ushort[6] { 2, 17, 0, 666, 36, 0 };
            SkillAttributes[1002][2] = new ushort[6] { 2, 19, 0, 882, 50, 0 };
            SkillAttributes[1002][3] = new ushort[6] { 2, 21, 0, 1166, 64, 0 };
            SkillsDone.Add(1002, 3);

            //Cure
            SkillAttributes[1005] = new ushort[5][];
            SkillAttributes[1005][0] = new ushort[6] { 6, 0, 0, 20, 10, 0 };
            SkillAttributes[1005][1] = new ushort[6] { 6, 0, 0, 70, 30, 0 };
            SkillAttributes[1005][2] = new ushort[6] { 6, 0, 0, 150, 60, 0 };
            SkillAttributes[1005][3] = new ushort[6] { 6, 0, 0, 280, 100, 0 };
            SkillAttributes[1005][4] = new ushort[6] { 6, 0, 0, 400, 130, 0 };
            SkillsDone.Add(1005, 4);

            //Cure Pluie
            SkillAttributes[1055] = new ushort[5][];
            SkillAttributes[1055][0] = new ushort[6] { 6, 0, 0, 100, 150, 0 };
            SkillAttributes[1055][1] = new ushort[6] { 6, 0, 0, 200, 270, 0 };
            SkillAttributes[1055][2] = new ushort[6] { 6, 0, 0, 300, 375, 0 };
            SkillAttributes[1055][3] = new ushort[6] { 6, 0, 0, 400, 440, 0 };
            SkillAttributes[1055][4] = new ushort[6] { 6, 0, 0, 500, 500, 0 };
            SkillsDone.Add(1055, 4);

            //SpiritualHealing
            SkillAttributes[1190] = new ushort[3][];
            SkillAttributes[1190][0] = new ushort[6] { 7, 0, 0, 500, 100, 0 };
            SkillAttributes[1190][1] = new ushort[6] { 7, 0, 0, 800, 100, 0 };
            SkillAttributes[1190][2] = new ushort[6] { 7, 0, 0, 1300, 100, 0 };
            SkillsDone.Add(1190, 2);

            //Fire of Hell
            SkillAttributes[1165] = new ushort[4][];
            SkillAttributes[1165][0] = new ushort[6] { 3, 3, 0, 180, 120, 0 };
            SkillAttributes[1165][1] = new ushort[6] { 3, 3, 0, 240, 150, 0 };
            SkillAttributes[1165][2] = new ushort[6] { 3, 3, 0, 310, 180, 0 };
            SkillAttributes[1165][3] = new ushort[6] { 3, 3, 0, 400, 210, 0 };
            SkillsDone.Add(1165, 3);

            //Scatter
            SkillAttributes[8001] = new ushort[6][];
            SkillAttributes[8001][0] = new ushort[6] { 5, 9, 45, 50, 0, 0 };
            SkillAttributes[8001][1] = new ushort[6] { 5, 10, 60, 55, 0, 0 };
            SkillAttributes[8001][2] = new ushort[6] { 5, 11, 80, 60, 0, 0 };
            SkillAttributes[8001][3] = new ushort[6] { 5, 12, 100, 65, 0, 0 };
            SkillAttributes[8001][4] = new ushort[6] { 5, 13, 150, 70, 0, 0 };
            SkillAttributes[8001][5] = new ushort[6] { 5, 14, 180, 72, 0, 0 };
            SkillsDone.Add(8001, 5);

            //Superman
            SkillAttributes[1025] = new ushort[1][];
            SkillAttributes[1025][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(1025, 0); ;

            //Shield
            SkillAttributes[1020] = new ushort[1][];
            SkillAttributes[1020][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(1020, 0); ;

            //IronShirt
            SkillAttributes[5100] = new ushort[1][];
            SkillAttributes[5100][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(5100, 0);

            //Roar
            SkillAttributes[1040] = new ushort[1][];
            SkillAttributes[1040][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(1040, 0); 

            //Fortress
            SkillAttributes[1021] = new ushort[1][];
            SkillAttributes[1021][0] = new ushort[6] { 3, 10, 0, 5000, 460, 0 };
            SkillsDone.Add(1021, 0);

            //Guard
            SkillAttributes[4000] = new ushort[4][];
            SkillAttributes[4000][0] = new ushort[6] { 7, 0, 0, 0, 100, 0 };
            SkillAttributes[4000][1] = new ushort[6] { 7, 0, 0, 0, 100, 0 };
            SkillAttributes[4000][2] = new ushort[6] { 7, 0, 0, 0, 100, 0 };
            SkillAttributes[4000][3] = new ushort[6] { 7, 0, 0, 0, 100, 0 };
            SkillsDone.Add(4000, 3);

            //Cyclone
            SkillAttributes[1110] = new ushort[1][];
            SkillAttributes[1110][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(1110, 0);

            //Arrow Rain
            SkillAttributes[8030] = new ushort[1][];
            SkillAttributes[8030][0] = new ushort[6] { 5, 14, 180, 100, 0, 1 };
            SkillsDone.Add(8030, 0);

            //Accuracy
            SkillAttributes[1015] = new ushort[1][];
            SkillAttributes[1015][0] = new ushort[6] { 7, 0, 0, 0, 0, 1 };
            SkillsDone.Add(1015, 0);

            //XP Fly
            SkillAttributes[8002] = new ushort[1][];
            SkillAttributes[8002][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(8002, 0);

            //Advanced Fly
            SkillAttributes[8003] = new ushort[2][];
            SkillAttributes[8003][0] = new ushort[6] { 7, 0, 0, 0, 100, 0 };
            SkillAttributes[8003][1] = new ushort[6] { 7, 0, 0, 0, 100, 0 };
            SkillsDone.Add(8003, 0);

            //Stigma
            SkillAttributes[1095] = new ushort[5][];
            SkillAttributes[1095][0] = new ushort[6] { 8, 0, 0, 0, 200, 0 };
            SkillAttributes[1095][1] = new ushort[6] { 8, 0, 0, 0, 250, 0 };
            SkillAttributes[1095][2] = new ushort[6] { 8, 0, 0, 0, 300, 0 };
            SkillAttributes[1095][3] = new ushort[6] { 8, 0, 0, 0, 350, 0 };
            SkillAttributes[1095][4] = new ushort[6] { 8, 0, 0, 0, 400, 0 };
            SkillsDone.Add(1095, 4);

            //Nectar
            SkillAttributes[1170] = new ushort[5][];
            SkillAttributes[1170][0] = new ushort[6] { 6, 0, 0, 600, 600, 0 };
            SkillAttributes[1170][1] = new ushort[6] { 6, 0, 0, 700, 660, 0 };
            SkillAttributes[1170][2] = new ushort[6] { 6, 0, 0, 800, 720, 0 };
            SkillAttributes[1170][3] = new ushort[6] { 6, 0, 0, 900, 770, 0 };
            SkillAttributes[1170][4] = new ushort[6] { 6, 0, 0, 1000, 820, 0 };
            SkillsDone.Add(1170, 4);

            //Pervade
            SkillAttributes[3090] = new ushort[6][];
            SkillAttributes[3090][0] = new ushort[6] { 3, 7, 0, 200, 0, 0 };
            SkillAttributes[3090][1] = new ushort[6] { 3, 8, 0, 300, 0, 0 };
            SkillAttributes[3090][2] = new ushort[6] { 3, 9, 0, 400, 0, 0 };
            SkillAttributes[3090][3] = new ushort[6] { 3, 10, 0, 500, 0, 0 };
            SkillAttributes[3090][4] = new ushort[6] { 3, 11, 0, 600, 0, 0 };
            SkillAttributes[3090][5] = new ushort[6] { 3, 13, 0, 700, 0, 0 };
            SkillsDone.Add(3090, 5);

            //FireBall
            SkillAttributes[1150] = new ushort[8][];
            SkillAttributes[1150][0] = new ushort[6] { 2, 0, 0, 378, 33, 0 };
            SkillAttributes[1150][1] = new ushort[6] { 2, 0, 0, 550, 33, 0 };
            SkillAttributes[1150][2] = new ushort[6] { 2, 0, 0, 760, 46, 0 };
            SkillAttributes[1150][3] = new ushort[6] { 2, 0, 0, 1010, 53, 0 };
            SkillAttributes[1150][4] = new ushort[6] { 2, 0, 0, 1332, 53, 0 };
            SkillAttributes[1150][5] = new ushort[6] { 2, 0, 0, 1764, 60, 0 };
            SkillAttributes[1150][6] = new ushort[6] { 2, 0, 0, 2332, 82, 0 };
            SkillAttributes[1150][7] = new ushort[6] { 2, 0, 0, 2800, 105, 0 };
            SkillsDone.Add(1150, 7);

            //FlyingMoon
            SkillAttributes[1320] = new ushort[3][];
            SkillAttributes[1320][0] = new ushort[6] { 2, 0, 0, 480, 0, 0 };
            SkillAttributes[1320][1] = new ushort[6] { 2, 0, 0, 1950, 0, 0 };
            SkillAttributes[1320][2] = new ushort[6] { 2, 0, 0, 5890, 0, 0 };
            SkillsDone.Add(1320, 2);

            //Fire Meteor
            SkillAttributes[1180] = new ushort[8][];
            SkillAttributes[1180][0] = new ushort[6] { 2, 0, 0, 760, 62, 0 };
            SkillAttributes[1180][1] = new ushort[6] { 2, 0, 0, 1040, 74, 0 };
            SkillAttributes[1180][2] = new ushort[6] { 2, 0, 0, 1250, 115, 0 };
            SkillAttributes[1180][3] = new ushort[6] { 2, 0, 0, 1480, 130, 0 };
            SkillAttributes[1180][4] = new ushort[6] { 2, 0, 0, 1810, 150, 0 };
            SkillAttributes[1180][5] = new ushort[6] { 2, 0, 0, 2210, 215, 0 };
            SkillAttributes[1180][6] = new ushort[6] { 2, 0, 0, 2700, 285, 0 };
            SkillAttributes[1180][7] = new ushort[6] { 2, 0, 0, 3250, 300, 0 };
            SkillsDone.Add(1180, 7);

            //Bomb
            SkillAttributes[1160] = new ushort[4][];
            SkillAttributes[1160][0] = new ushort[6] { 2, 0, 0, 855, 53, 0 };
            SkillAttributes[1160][1] = new ushort[6] { 2, 0, 0, 1498, 60, 0 };
            SkillAttributes[1160][2] = new ushort[6] { 2, 0, 0, 1985, 82, 0 };
            SkillAttributes[1160][3] = new ushort[6] { 2, 0, 0, 2623, 105, 0 };
            SkillsDone.Add(1160, 3);

            //Lightning
            SkillAttributes[1010] = new ushort[1][];
            SkillAttributes[1010][0] = new ushort[6] { 3, 5, 0, 50, 0, 1 };
            SkillsDone.Add(1010, 0);

            //Volcano
            SkillAttributes[1125] = new ushort[1][];
            SkillAttributes[1125][0] = new ushort[6] { 3, 8, 0, 300, 0, 1 };
            SkillsDone.Add(1125, 0);

            //SpeedLightning
            SkillAttributes[5001] = new ushort[1][];
            SkillAttributes[5001][0] = new ushort[6] { 3, 14, 0, 450, 0, 1 };
            SkillsDone.Add(5001, 0);

            //Pray
            SkillAttributes[1100] = new ushort[1][];
            SkillAttributes[1100][0] = new ushort[6] { 8, 0, 0, 0, 1000, 0 };
            SkillsDone.Add(1100, 0);

            //Rez
            SkillAttributes[1050] = new ushort[1][];
            SkillAttributes[1050][0] = new ushort[6] { 8, 0, 0, 0, 0, 1 };
            SkillsDone.Add(1050, 0);

            //Restore
            SkillAttributes[1105] = new ushort[1][];
            SkillAttributes[1105][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(1105, 0);

            //RapidFire
            SkillAttributes[8000] = new ushort[6][];
            SkillAttributes[8000][0] = new ushort[6] { 13, 8, 0, 150, 100, 0 };
            SkillAttributes[8000][1] = new ushort[6] { 13, 8, 0, 180, 100, 0 };
            SkillAttributes[8000][2] = new ushort[6] { 13, 8, 0, 210, 100, 0 };
            SkillAttributes[8000][3] = new ushort[6] { 13, 8, 0, 240, 100, 0 };
            SkillAttributes[8000][4] = new ushort[6] { 13, 8, 0, 270, 100, 0 };
            SkillAttributes[8000][5] = new ushort[6] { 13, 8, 0, 300, 100, 0 };
            SkillsDone.Add(8000, 5);

            //Advanced Cure
            SkillAttributes[1175] = new ushort[5][];
            SkillAttributes[1175][0] = new ushort[6] { 6, 0, 0, 500, 160, 0 };
            SkillAttributes[1175][1] = new ushort[6] { 6, 0, 0, 600, 190, 0 };
            SkillAttributes[1175][2] = new ushort[6] { 6, 0, 0, 700, 215, 0 };
            SkillAttributes[1175][3] = new ushort[6] { 6, 0, 0, 800, 235, 0 };
            SkillAttributes[1175][4] = new ushort[6] { 6, 0, 0, 900, 255, 0 };
            SkillsDone.Add(1175, 4);

            //Phoenix
            SkillAttributes[5030] = new ushort[10][];
            SkillAttributes[5030][0] = new ushort[6] { 12, 5, 0, 115, 0, 33 };
            SkillAttributes[5030][1] = new ushort[6] { 12, 5, 0, 116, 0, 38 };
            SkillAttributes[5030][2] = new ushort[6] { 12, 5, 0, 117, 0, 43 };
            SkillAttributes[5030][3] = new ushort[6] { 12, 5, 0, 118, 0, 48 };
            SkillAttributes[5030][4] = new ushort[6] { 12, 5, 0, 119, 0, 53 };
            SkillAttributes[5030][5] = new ushort[6] { 12, 5, 0, 120, 0, 58 };
            SkillAttributes[5030][6] = new ushort[6] { 12, 5, 0, 121, 0, 63 };
            SkillAttributes[5030][7] = new ushort[6] { 12, 5, 0, 122, 0, 68 };
            SkillAttributes[5030][8] = new ushort[6] { 12, 5, 0, 123, 0, 73 };
            SkillAttributes[5030][9] = new ushort[6] { 12, 5, 0, 124, 0, 78 };
            SkillsDone.Add(5030, 9);

            //Pluie d'�toile
            SkillAttributes[1220] = new ushort[1][];
            SkillAttributes[1220][0] = new ushort[6] { 12, 5, 0, 125, 0, 80 };
            //SkillAttributes[1220][0] = new ushort[6] { 12, 5, 0, 115, 0, 33 };
            //SkillAttributes[1220][1] = new ushort[6] { 12, 5, 0, 116, 0, 38 };
            //SkillAttributes[1220][2] = new ushort[6] { 12, 5, 0, 117, 0, 43 };
            //SkillAttributes[1220][3] = new ushort[6] { 12, 5, 0, 118, 0, 48 };
            //SkillAttributes[1220][4] = new ushort[6] { 12, 5, 0, 119, 0, 53 };
            //SkillAttributes[1220][5] = new ushort[6] { 12, 5, 0, 120, 0, 58 };
            //SkillAttributes[1220][6] = new ushort[6] { 12, 5, 0, 121, 0, 63 };
            //SkillAttributes[1220][7] = new ushort[6] { 12, 5, 0, 122, 0, 68 };
            //SkillAttributes[1220][8] = new ushort[6] { 12, 5, 0, 123, 0, 73 };
            //SkillAttributes[1220][9] = new ushort[6] { 12, 5, 0, 124, 0, 78 };
            SkillsDone.Add(1220, 0);

            //SpeedGun
            SkillAttributes[1260] = new ushort[10][];
            SkillAttributes[1260][0] = new ushort[6] { 12, 5, 0, 100, 0, 20 };
            SkillAttributes[1260][1] = new ushort[6] { 12, 5, 0, 115, 0, 23 };
            SkillAttributes[1260][2] = new ushort[6] { 12, 5, 0, 125, 0, 26 };
            SkillAttributes[1260][3] = new ushort[6] { 12, 5, 0, 140, 0, 29 };
            SkillAttributes[1260][4] = new ushort[6] { 12, 5, 0, 150, 0, 32 };
            SkillAttributes[1260][5] = new ushort[6] { 12, 5, 0, 165, 0, 35 };
            SkillAttributes[1260][6] = new ushort[6] { 12, 5, 0, 175, 0, 38 };
            SkillAttributes[1260][7] = new ushort[6] { 12, 5, 0, 190, 0, 41 };
            SkillAttributes[1260][8] = new ushort[6] { 12, 5, 0, 200, 0, 44 };
            SkillAttributes[1260][9] = new ushort[6] { 12, 5, 0, 210, 0, 47 };
            SkillsDone.Add(1260, 9);

            //Boreas
            SkillAttributes[5050] = new ushort[10][];
            SkillAttributes[5050][0] = new ushort[6] { 0, 2, 0, 90, 0, 20 };
            SkillAttributes[5050][1] = new ushort[6] { 0, 2, 0, 100, 0, 22 };
            SkillAttributes[5050][2] = new ushort[6] { 0, 3, 0, 110, 0, 24 };
            SkillAttributes[5050][3] = new ushort[6] { 0, 3, 0, 120, 0, 26 };
            SkillAttributes[5050][4] = new ushort[6] { 0, 3, 0, 130, 0, 28 };
            SkillAttributes[5050][5] = new ushort[6] { 0, 3, 0, 140, 0, 30 };
            SkillAttributes[5050][6] = new ushort[6] { 0, 4, 0, 150, 0, 32 };
            SkillAttributes[5050][7] = new ushort[6] { 0, 4, 0, 160, 0, 34 };
            SkillAttributes[5050][8] = new ushort[6] { 0, 4, 0, 170, 0, 36 };
            SkillAttributes[5050][9] = new ushort[6] { 0, 4, 0, 180, 0, 38 };
            SkillsDone.Add(5050, 9);

            //Penetration
            SkillAttributes[1290] = new ushort[10][];
            SkillAttributes[1290][0] = new ushort[6] { 12, 5, 0, 150, 0, 10 };
            SkillAttributes[1290][1] = new ushort[6] { 12, 5, 0, 160, 0, 10 };
            SkillAttributes[1290][2] = new ushort[6] { 12, 5, 0, 170, 0, 11 };
            SkillAttributes[1290][3] = new ushort[6] { 12, 5, 0, 180, 0, 11 };
            SkillAttributes[1290][4] = new ushort[6] { 12, 5, 0, 190, 0, 12 };
            SkillAttributes[1290][5] = new ushort[6] { 12, 5, 0, 200, 0, 12 };
            SkillAttributes[1290][6] = new ushort[6] { 12, 5, 0, 210, 0, 13 };
            SkillAttributes[1290][7] = new ushort[6] { 12, 5, 0, 220, 0, 13 };
            SkillAttributes[1290][8] = new ushort[6] { 12, 5, 0, 230, 0, 14 };
            SkillAttributes[1290][9] = new ushort[6] { 12, 5, 0, 240, 0, 15 };
            SkillsDone.Add(1290, 9);

            //WideStrike
            SkillAttributes[1250] = new ushort[10][];
            SkillAttributes[1250][0] = new ushort[6] { 0, 2, 0, 90, 0, 20 };
            SkillAttributes[1250][1] = new ushort[6] { 0, 2, 0, 100, 0, 22 };
            SkillAttributes[1250][2] = new ushort[6] { 0, 3, 0, 110, 0, 24 };
            SkillAttributes[1250][3] = new ushort[6] { 0, 3, 0, 120, 0, 26 };
            SkillAttributes[1250][4] = new ushort[6] { 0, 3, 0, 130, 0, 28 };
            SkillAttributes[1250][5] = new ushort[6] { 0, 3, 0, 140, 0, 30 };
            SkillAttributes[1250][6] = new ushort[6] { 0, 4, 0, 150, 0, 32 };
            SkillAttributes[1250][7] = new ushort[6] { 0, 4, 0, 160, 0, 34 };
            SkillAttributes[1250][8] = new ushort[6] { 0, 4, 0, 170, 0, 36 };
            SkillAttributes[1250][9] = new ushort[6] { 0, 4, 0, 180, 0, 38 };
            SkillsDone.Add(1250, 9);

            //Roamer
            SkillAttributes[7040] = new ushort[10][];
            SkillAttributes[7040][0] = new ushort[6] { 0, 4, 0, 90, 0, 20 };
            SkillAttributes[7040][1] = new ushort[6] { 0, 4, 0, 92, 0, 23 };
            SkillAttributes[7040][2] = new ushort[6] { 0, 4, 0, 94, 0, 26 };
            SkillAttributes[7040][3] = new ushort[6] { 0, 4, 0, 96, 0, 29 };
            SkillAttributes[7040][4] = new ushort[6] { 0, 4, 0, 98, 0, 31 };
            SkillAttributes[7040][5] = new ushort[6] { 0, 4, 0, 100, 0, 34 };
            SkillAttributes[7040][6] = new ushort[6] { 0, 5, 0, 102, 0, 37 };
            SkillAttributes[7040][7] = new ushort[6] { 0, 5, 0, 104, 0, 40 };
            SkillAttributes[7040][8] = new ushort[6] { 0, 5, 0, 106, 0, 43 };
            SkillAttributes[7040][9] = new ushort[6] { 0, 5, 0, 108, 0, 45 };
            SkillsDone.Add(7040, 9);

            //Celestial
            SkillAttributes[7030] = new ushort[10][];
            SkillAttributes[7030][0] = new ushort[6] { 12, 5, 0, 110, 0, 10 };
            SkillAttributes[7030][1] = new ushort[6] { 12, 5, 0, 112, 0, 11 };
            SkillAttributes[7030][2] = new ushort[6] { 12, 5, 0, 114, 0, 12 };
            SkillAttributes[7030][3] = new ushort[6] { 12, 5, 0, 116, 0, 13 };
            SkillAttributes[7030][4] = new ushort[6] { 12, 5, 0, 118, 0, 14 };
            SkillAttributes[7030][5] = new ushort[6] { 12, 5, 0, 120, 0, 15 };
            SkillAttributes[7030][6] = new ushort[6] { 12, 5, 0, 122, 0, 16 };
            SkillAttributes[7030][7] = new ushort[6] { 12, 5, 0, 124, 0, 17 };
            SkillAttributes[7030][8] = new ushort[6] { 12, 5, 0, 126, 0, 18 };
            SkillAttributes[7030][9] = new ushort[6] { 12, 5, 0, 128, 0, 19 };
            SkillsDone.Add(7030, 9);

            //Earthquake
            SkillAttributes[7010] = new ushort[10][];
            SkillAttributes[7010][0] = new ushort[6] { 12, 5, 0, 101, 0, 10 };
            SkillAttributes[7010][1] = new ushort[6] { 12, 5, 0, 102, 0, 12 };
            SkillAttributes[7010][2] = new ushort[6] { 12, 5, 0, 103, 0, 14 };
            SkillAttributes[7010][3] = new ushort[6] { 12, 5, 0, 104, 0, 16 };
            SkillAttributes[7010][4] = new ushort[6] { 12, 5, 0, 105, 0, 18 };
            SkillAttributes[7010][5] = new ushort[6] { 12, 5, 0, 106, 0, 20 };
            SkillAttributes[7010][6] = new ushort[6] { 12, 5, 0, 107, 0, 22 };
            SkillAttributes[7010][7] = new ushort[6] { 12, 5, 0, 108, 0, 24 };
            SkillAttributes[7010][8] = new ushort[6] { 12, 5, 0, 109, 0, 26 };
            SkillAttributes[7010][9] = new ushort[6] { 12, 5, 0, 110, 0, 28 };
            SkillsDone.Add(7010, 9);

            //Seizer
            SkillAttributes[7000] = new ushort[10][];
            SkillAttributes[7000][0] = new ushort[6] { 12, 5, 0, 105, 0, 15 };
            SkillAttributes[7000][1] = new ushort[6] { 12, 5, 0, 106, 0, 19 };
            SkillAttributes[7000][2] = new ushort[6] { 12, 5, 0, 107, 0, 22 };
            SkillAttributes[7000][3] = new ushort[6] { 12, 5, 0, 108, 0, 24 };
            SkillAttributes[7000][4] = new ushort[6] { 12, 5, 0, 109, 0, 25 };
            SkillAttributes[7000][5] = new ushort[6] { 12, 5, 0, 110, 0, 26 };
            SkillAttributes[7000][6] = new ushort[6] { 12, 5, 0, 111, 0, 27 };
            SkillAttributes[7000][7] = new ushort[6] { 12, 5, 0, 112, 0, 28 };
            SkillAttributes[7000][8] = new ushort[6] { 12, 5, 0, 113, 0, 29 };
            SkillAttributes[7000][9] = new ushort[6] { 12, 5, 0, 114, 0, 30 };
            SkillsDone.Add(7000, 9);

            //Halt
            SkillAttributes[1300] = new ushort[10][];
            SkillAttributes[1300][0] = new ushort[6] { 0, 3, 0, 90, 0, 20 };
            SkillAttributes[1300][1] = new ushort[6] { 0, 3, 0, 100, 0, 22 };
            SkillAttributes[1300][2] = new ushort[6] { 0, 3, 0, 110, 0, 24 };
            SkillAttributes[1300][3] = new ushort[6] { 0, 3, 0, 120, 0, 26 };
            SkillAttributes[1300][4] = new ushort[6] { 0, 3, 0, 130, 0, 28 };
            SkillAttributes[1300][5] = new ushort[6] { 0, 3, 0, 140, 0, 30 };
            SkillAttributes[1300][6] = new ushort[6] { 0, 4, 0, 150, 0, 32 };
            SkillAttributes[1300][7] = new ushort[6] { 0, 4, 0, 160, 0, 34 };
            SkillAttributes[1300][8] = new ushort[6] { 0, 4, 0, 170, 0, 36 };
            SkillAttributes[1300][9] = new ushort[6] { 0, 4, 0, 180, 0, 38 };
            SkillsDone.Add(1300, 9);

            //Boom
            SkillAttributes[5040] = new ushort[10][];
            SkillAttributes[5040][0] = new ushort[6] { 12, 5, 0, 101, 0, 10 };
            SkillAttributes[5040][1] = new ushort[6] { 12, 5, 0, 102, 0, 12 };
            SkillAttributes[5040][2] = new ushort[6] { 12, 5, 0, 103, 0, 14 };
            SkillAttributes[5040][3] = new ushort[6] { 12, 5, 0, 104, 0, 16 };
            SkillAttributes[5040][4] = new ushort[6] { 12, 5, 0, 105, 0, 18 };
            SkillAttributes[5040][5] = new ushort[6] { 12, 5, 0, 106, 0, 20 };
            SkillAttributes[5040][6] = new ushort[6] { 12, 5, 0, 107, 0, 22 };
            SkillAttributes[5040][7] = new ushort[6] { 12, 5, 0, 108, 0, 24 };
            SkillAttributes[5040][8] = new ushort[6] { 12, 5, 0, 109, 0, 26 };
            SkillAttributes[5040][9] = new ushort[6] { 12, 5, 0, 110, 0, 28 };
            SkillsDone.Add(5040, 9);

            //StrandedMonster
            SkillAttributes[5020] = new ushort[10][];
            SkillAttributes[5020][0] = new ushort[6] { 0, 2, 0, 130, 0, 20 };
            SkillAttributes[5020][1] = new ushort[6] { 0, 2, 0, 131, 0, 23 };
            SkillAttributes[5020][2] = new ushort[6] { 0, 2, 0, 132, 0, 26 };
            SkillAttributes[5020][3] = new ushort[6] { 0, 3, 0, 133, 0, 29 };
            SkillAttributes[5020][4] = new ushort[6] { 0, 3, 0, 134, 0, 31 };
            SkillAttributes[5020][5] = new ushort[6] { 0, 3, 0, 135, 0, 34 };
            SkillAttributes[5020][6] = new ushort[6] { 0, 3, 0, 136, 0, 37 };
            SkillAttributes[5020][7] = new ushort[6] { 0, 3, 0, 137, 0, 40 };
            SkillAttributes[5020][8] = new ushort[6] { 0, 4, 0, 138, 0, 43 };
            SkillAttributes[5020][9] = new ushort[6] { 0, 4, 0, 139, 0, 45 };
            SkillsDone.Add(5020, 9);

            //CruelShade
            SkillAttributes[3050] = new ushort[4][];
            SkillAttributes[3050][0] = new ushort[6] { 16, 0, 0, 0, 100, 0 };
            SkillAttributes[3050][1] = new ushort[6] { 16, 0, 0, 0, 100, 0 };
            SkillAttributes[3050][2] = new ushort[6] { 16, 0, 0, 0, 100, 0 };
            SkillAttributes[3050][3] = new ushort[6] { 16, 0, 0, 0, 100, 0 };
            SkillsDone.Add(3050, 3);

            //Meditation
            SkillAttributes[1195] = new ushort[3][];
            SkillAttributes[1195][0] = new ushort[6] { 14, 0, 0, 310, 100, 0 };
            SkillAttributes[1195][1] = new ushort[6] { 14, 0, 0, 600, 100, 0 };
            SkillAttributes[1195][2] = new ushort[6] { 14, 0, 0, 1020, 100, 0 };
            SkillsDone.Add(1195, 2);

            //MagicShield
            SkillAttributes[1090] = new ushort[5][];
            SkillAttributes[1090][0] = new ushort[6] { 8, 0, 0, 0, 200, 0 };
            SkillAttributes[1090][1] = new ushort[6] { 8, 0, 0, 0, 250, 0 };
            SkillAttributes[1090][2] = new ushort[6] { 8, 0, 0, 0, 300, 0 };
            SkillAttributes[1090][3] = new ushort[6] { 8, 0, 0, 0, 350, 0 };
            SkillAttributes[1090][4] = new ushort[6] { 8, 0, 0, 0, 400, 0 };
            SkillsDone.Add(1090, 4);

            //StarofAccuracy
            SkillAttributes[1085] = new ushort[5][];
            SkillAttributes[1085][0] = new ushort[6] { 8, 0, 0, 0, 200, 0 };
            SkillAttributes[1085][1] = new ushort[6] { 8, 0, 0, 0, 250, 0 };
            SkillAttributes[1085][2] = new ushort[6] { 8, 0, 0, 0, 300, 0 };
            SkillAttributes[1085][3] = new ushort[6] { 8, 0, 0, 0, 350, 0 };
            SkillAttributes[1085][4] = new ushort[6] { 8, 0, 0, 0, 400, 0 };
            SkillsDone.Add(1085, 4);

            //Invisibility
            SkillAttributes[1075] = new ushort[5][];
            SkillAttributes[1075][0] = new ushort[6] { 8, 0, 0, 0, 200, 0 };
            SkillAttributes[1075][1] = new ushort[6] { 8, 0, 0, 0, 250, 0 };
            SkillAttributes[1075][2] = new ushort[6] { 8, 0, 0, 0, 300, 0 };
            SkillAttributes[1075][3] = new ushort[6] { 8, 0, 0, 0, 330, 0 };
            SkillAttributes[1075][4] = new ushort[6] { 8, 0, 0, 0, 360, 0 };
            SkillsDone.Add(1075, 4);

            //Robot
            SkillAttributes[1270] = new ushort[8][];
            SkillAttributes[1270][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1270][1] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1270][2] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1270][3] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1270][4] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1270][5] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1270][6] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1270][7] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(1270, 7);

            //WaterElf
            SkillAttributes[1280] = new ushort[9][];
            SkillAttributes[1280][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1280][1] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1280][2] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1280][3] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1280][4] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1280][5] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1280][6] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1280][7] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1280][8] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(1280, 8);

            //NightDevil
            SkillAttributes[1360] = new ushort[5][];
            SkillAttributes[1360][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1360][1] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1360][2] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1360][3] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1360][4] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(1360, 4);

            //DivineHare
            SkillAttributes[1350] = new ushort[5][];
            SkillAttributes[1350][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1350][1] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1350][2] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1350][3] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1350][4] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(1350, 4);

            //Piglet
            SkillAttributes[3321] = new ushort[1][];
            SkillAttributes[3321][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(3321, 0);

            Program.MCompressor.Optimize();
        }

        public static void RemoveFromFriend(uint RemoverUID, uint RemovedUID)
        {
            string[] Characters = Directory.GetFiles(Application.StartupPath + "\\Characters", "*.chr");
            for (int i = 0; i < Characters.Length; i++)
            {
                FileStream FStream = new FileStream(Characters[i], FileMode.Open);
                BinaryReader Reader = new BinaryReader(FStream);

                if (Reader.ReadUInt32() == RemovedUID)
                {
                    Reader.BaseStream.Seek(16, SeekOrigin.Current);
                    string Name = Encoding.ASCII.GetString(Reader.ReadBytes(16)).TrimEnd((char)0x00);

                    Reader.Close();
                    FStream.Dispose();

                    Character Friend = new Character();
                    Friend.ReadInBinary(Name);
                    Friend.DelFriend(RemoverUID);
                    Friend.Save();
                    Friend = null;
                    break;
                }
                Reader.Close();
                FStream.Dispose();
            }
        }

        public static uint NeededSkillExp(short SkillId, byte Level)
        {
            // Tao�ste
            if (SkillId == 1000) //Thunder
            {
                if (Level == 0)
                    return 2000;
                else if (Level == 1)
                    return 113060;
                else if (Level == 2)
                    return 326107;
                else if (Level == 3)
                    return 777950;
                else
                    return 0;
            }

            else if (SkillId == 1005) //Cure
            {
                if (Level == 0)
                    return 2000;
                else if (Level == 1)
                    return 12000;
                else if (Level == 2)
                    return 30000;
                else if (Level == 3)
                    return 64000;
                else
                    return 0;
            }

            else if (SkillId == 1001) //Fire
            {
                if (Level == 0)
                    return 4859520;
                else if (Level == 1)
                    return 11231517;
                else if (Level == 2)
                    return 26497813;
                else
                    return 0;
            }

            else if (SkillId == 1195) //Meditation
            {
                if (Level == 0)
                    return 537140;
                else if (Level == 1)
                    return 918542;
                else
                    return 0;
            }

            // Fire Tao�ste
            else if (SkillId == 1150) //FireRing/Ball
            {
                if (Level == 0)
                    return 195000;
                else if (Level == 1)
                    return 339600;
                else if (Level == 2)
                    return 576800;
                else if (Level == 3)
                    return 912000;
                else if (Level == 4)
                    return 1362600;
                else if (Level == 5)
                    return 1998000;
                else if (Level == 6)
                    return 3307500;
                else
                    return 0;
            }

            else if (SkillId == 1180) //FireMeteor
            {
                if (Level == 0)
                    return 8631000;
                else if (Level == 1)
                    return 13981500;
                else if (Level == 2)
                    return 24242000;
                else if (Level == 3)
                    return 38661200;
                else if (Level == 4)
                    return 67900000;
                else if (Level == 5)
                    return 269625000;
                else if (Level == 6)
                    return 815850000;
                else
                    return 0;
            }

            else if (SkillId == 1120) //FireCircle
            {
                if (Level == 0)
                    return 53104696;
                else if (Level == 1)
                    return 98875022;
                else if (Level == 2)
                    return 180034734;
                else
                    return 0;
            }

            else if (SkillId == 1002) //Tornado
            {
                if (Level == 0)
                    return 118246825;
                else if (Level == 1)
                    return 277035437;
                else if (Level == 2)
                    return 920692259;
                else
                    return 0;
            }

            else if (SkillId == 1160) //Bomb
            {
                if (Level == 0)
                    return 1282500;
                else if (Level == 1)
                    return 2696400;
                else if (Level == 2)
                    return 3970000;
                else
                    return 0;
            }

            else if (SkillId == 1165) //FireofHell
            {
                if (Level == 0)
                    return 1282500;
                else if (Level == 1)
                    return 2696400;
                else if (Level == 2)
                    return 3970000;
                else
                    return 0;
            }

            // Water Tao�ste
            else if (SkillId == 1055) //HealingRain
            {
                if (Level == 0)
                    return 27300;
                else if (Level == 1)
                    return 121300;
                else if (Level == 2)
                    return 283900;
                else if (Level == 3)
                    return 595600;
                else
                    return 0;
            }

            else if (SkillId == 1085) //StarofAccuracy
            {
                if (Level == 0)
                    return 430;
                else if (Level == 1)
                    return 520;
                else if (Level == 2)
                    return 570;
                else if (Level == 3)
                    return 620;
                else
                    return 0;
            }

            else if (SkillId == 1090) //MagicShield
            {
                if (Level == 0)
                    return 430;
                else if (Level == 1)
                    return 520;
                else if (Level == 2)
                    return 570;
                else if (Level == 3)
                    return 620;
                else
                    return 0;
            }

            else if (SkillId == 1095) //Stigma
            {
                if (Level == 0)
                    return 430;
                else if (Level == 1)
                    return 520;
                else if (Level == 2)
                    return 570;
                else if (Level == 3)
                    return 620;
                else
                    return 0;
            }

            else if (SkillId == 1075) //Invisibility
            {
                if (Level == 0)
                    return 720;
                else if (Level == 1)
                    return 860;
                else if (Level == 2)
                    return 960;
                else if (Level == 3)
                    return 1010;
                else
                    return 0;
            }

            else if (SkillId == 1175) //AdvancedCure
            {
                if (Level == 0)
                    return 500000;
                else if (Level == 1)
                    return 720000;
                else if (Level == 2)
                    return 980000;
                else if (Level == 3)
                    return 1280000;
                else
                    return 0;
            }

            else if (SkillId == 1170) //Nectar
            {
                if (Level == 0)
                    return 163600;
                else if (Level == 1)
                    return 303700;
                else if (Level == 2)
                    return 727200;
                else if (Level == 3)
                    return 1075800;
                else
                    return 0;
            }

            // Trojan
            else if (SkillId == 1115) //Hercule
            {
                if (Level == 0)
                    return 167600;
                else if (Level == 1)
                    return 590000;
                else if (Level == 2)
                    return 1216800;
                else if (Level == 3)
                    return 2948000;
                else
                    return 0;
            }

            else if (SkillId == 1190) //SpiritHealing
            {
                if (Level == 0)
                    return 560000;
                else if (Level == 1)
                    return 4000000;
                else
                    return 0;
            }

            // Warrior
            else if (SkillId == 1320) //FlyingMoon
            {
                if (Level == 0)
                    return 50;
                else if (Level == 1)
                    return 100;
                else
                    return 0;
            }

            // Archer
            else if (SkillId == 8001) //Scatter
            {
                if (Level == 0)
                    return 100000;
                else if (Level == 1)
                    return 150000;
                else if (Level == 2)
                    return 200000;
                else if (Level == 3)
                    return 250000;
                else if (Level == 4)
                    return 300000;
                else
                    return 0;
            }

            else if (SkillId == 8000) //RapidFire
            {
                if (Level == 0)
                    return 100000;
                else if (Level == 1)
                    return 150000;
                else if (Level == 2)
                    return 200000;
                else if (Level == 3)
                    return 250000;
                else if (Level == 4)
                    return 300000;
                else
                    return 0;
            }

            else if (SkillId == 9000) //Intensify
            {
                if (Level == 0)
                    return 3000;
                else if (Level == 1)
                    return 4000;
                else if (Level == 2)
                    return 5000;
                else
                    return 0;
            }

            // Disguise
            else if (SkillId == 1270) //Robot
            {
                if (Level == 0)
                    return 60;
                else if (Level == 1)
                    return 100;
                else if (Level == 2)
                    return 140;
                else if (Level == 3)
                    return 240;
                else if (Level == 4)
                    return 370;
                else if (Level == 5)
                    return 600;
                else if (Level == 6)
                    return 1050;
                else
                    return 0;
            }

            else if (SkillId == 1280) //WaterElf
            {
                if (Level == 0)
                    return 80;
                else if (Level == 1)
                    return 100;
                else if (Level == 2)
                    return 120;
                else if (Level == 3)
                    return 150;
                else if (Level == 4)
                    return 840;
                else if (Level == 5)
                    return 1340;
                else if (Level == 6)
                    return 2080;
                else if (Level == 7)
                    return 3490;
                else
                    return 0;
            }

            else if (SkillId == 1350) //DivineHare
            {
                if (Level == 0)
                    return 1800;
                else if (Level == 1)
                    return 4000;
                else if (Level == 2)
                    return 5500;
                else if (Level == 3)
                    return 9000;
                else
                    return 0;
            }

            else if (SkillId == 1360) //NightDevil
            {
                if (Level == 0)
                    return 600;
                else if (Level == 1)
                    return 1000;
                else if (Level == 2)
                    return 1500;
                else if (Level == 3)
                    return 2000;
                else
                    return 0;
            }

            // WeaponSkill
            else if (SkillId == 1045) //FastBlade
            {
                if (Level == 0)
                    return 100000;
                else if (Level == 1)
                    return 300000;
                else if (Level == 2)
                    return 741000;
                else if (Level == 3)
                    return 1440000;
                else
                    return 0;
            }

            else if (SkillId == 1046) //ScentSword
            {
                if (Level == 0)
                    return 100000;
                else if (Level == 1)
                    return 300000;
                else if (Level == 2)
                    return 741000;
                else if (Level == 3)
                    return 1440000;
                else
                    return 0;
            }

            //Phoenix, WideStrike, Boreas, Snow, StrandedMonster, SpeedGun, Penetration, Halt, Rage, Roamer
            else if (/*SkillId == 1220 || */SkillId == 5030 || SkillId == 1250 || SkillId == 5050 || SkillId == 5010 || SkillId == 5020 || SkillId == 1260 || SkillId == 1290 || SkillId == 1300 || SkillId == 7020 || SkillId == 7040)
            {
                if (Level == 0)
                    return 20243;
                else if (Level == 1)
                    return 37056;
                else if (Level == 2)
                    return 66011;
                else if (Level == 3)
                    return 116140;
                else if (Level == 4)
                    return 192800;
                else if (Level == 5)
                    return 418030;
                else if (Level == 6)
                    return 454350;
                else if (Level == 7)
                    return 491200;
                else if (Level == 8)
                    return 520030;
                else
                    return 0;
            }
            else if (SkillId == 5040 || SkillId == 7010) //Boom, Earthquake
            {
                if (Level == 0)
                    return 200;
                else if (Level == 1)
                    return 300;
                else if (Level == 2)
                    return 400;
                else if (Level == 3)
                    return 500;
                else if (Level == 4)
                    return 600;
                else if (Level == 5)
                    return 700;
                else if (Level == 6)
                    return 800;
                else if (Level == 7)
                    return 900;
                else if (Level == 8)
                    return 1000;
                else
                    return 0;
            }

            else if (SkillId == 7000 || SkillId == 7030) //Seizer, Celestial
            {
                if (Level == 0)
                    return 5000;
                else if (Level == 1)
                    return 9000;
                else if (Level == 2)
                    return 16000;
                else if (Level == 3)
                    return 29000;
                else if (Level == 4)
                    return 48000;
                else if (Level == 5)
                    return 104000;
                else if (Level == 6)
                    return 113000;
                else if (Level == 7)
                    return 122000;
                else if (Level == 8)
                    return 130000;
                else
                    return 0;
            }

            // RebornSkill
            else if (SkillId == 4000) //Guard
            {
                if (Level == 0)
                    return 100;
                else if (Level == 1)
                    return 300;
                else if (Level == 2)
                    return 500;
                else
                    return 0;
            }

            else if (SkillId == 4010 || SkillId == 4020) //SummonBat, SummonBatBoss
            {
                if (Level == 0)
                    return 30;
                else if (Level == 1)
                    return 50;
                else if (Level == 2)
                    return 100;
                else
                    return 0;
            }

            else if (SkillId == 4050 || SkillId == 4060 || SkillId == 4070) //BloodyBat, FireEvil, Skeleton
            {
                if (Level == 0)
                    return 200;
                else if (Level == 1)
                    return 500;
                else if (Level == 2)
                    return 1000;
                else
                    return 0;
            }

            else if (SkillId == 3050) //CruelShade
            {
                if (Level == 0)
                    return 500;
                else if (Level == 1)
                    return 800;
                else if (Level == 2)
                    return 1000;
                else
                    return 0;
            }

            else if (SkillId == 3080) //Dodge
            {
                if (Level == 0)
                    return 500;
                else if (Level == 1)
                    return 1000;
                else
                    return 0;
            }

            else if (SkillId == 3090)
            {
                if (Level == 0)
                    return 195000;
                if (Level == 1)
                    return 300000;
                if (Level == 2)
                    return 1296000;
                if (Level == 3)
                    return 1950000;
                if (Level == 4)
                    return 2376000;
                else
                    return 0;
            }
            else
                return 9999999;
        }

        public static uint NeededProfXP(byte Level)
        {
            if (ProfExp.ContainsKey(Level))
                return ProfExp[Level];

            return 0;
        }

        public static ulong NeededXP(byte Level)
        {
            if (LevelExp.ContainsKey(Level))
                return LevelExp[Level];

            return 0;
        }

        public static void LoadGuilds()
        {
            try
            {
                int GuildC = 0;
                string[] AllGuilds = Directory.GetFiles(Application.StartupPath + "\\Guilds", "*.dat");
                for (int i = 0; i < AllGuilds.Length; i++)
                {
                    FileStream FStream = new FileStream(AllGuilds[i], FileMode.Open);
                    BinaryReader Reader = new BinaryReader(FStream);

                    ushort UniqId = Reader.ReadUInt16();
                    string Name = Encoding.Default.GetString(Reader.ReadBytes(16)).TrimEnd((char)0x00);
                    uint Fund = Reader.ReadUInt32();
                    byte AlliesC = Reader.ReadByte();
                    List<ushort> Allies = new List<ushort>(AlliesC);
                    for (byte x = 0; x < AlliesC; x++)
                        Allies.Add(Reader.ReadUInt16());
                    byte EnemiesC = Reader.ReadByte();
                    List<ushort> Enemies = new List<ushort>(EnemiesC);
                    for (byte x = 0; x < EnemiesC; x++)
                        Enemies.Add(Reader.ReadUInt16());
                    GuildMember Leader = new GuildMember().Create(Reader.ReadUInt32(), Reader.ReadInt32());
                    byte DeputiesC = Reader.ReadByte();
                    Dictionary<uint, GuildMember> Deputies = new Dictionary<uint, GuildMember>(DeputiesC);
                    for (byte x = 0; x < DeputiesC; x++)
                    {
                        uint DeputyId = Reader.ReadUInt32();
                        Deputies.Add(DeputyId, new GuildMember().Create(DeputyId, Reader.ReadInt32()));
                    }
                    byte MembersC = Reader.ReadByte();
                    Dictionary<uint, GuildMember> Members = new Dictionary<uint, GuildMember>(MembersC);
                    for (byte x = 0; x < MembersC; x++)
                    {
                        uint MemberId = Reader.ReadUInt32();
                        Members.Add(MemberId, new GuildMember().Create(MemberId, Reader.ReadInt32()));
                    }
                    string Bulletin = Encoding.ASCII.GetString(Reader.ReadBytes(Reader.ReadByte()));
                    byte Holding = Reader.ReadByte();

                    Guild Guild = new Guild(UniqId, Name, Leader, Deputies, Members, Allies, Enemies, Fund, Bulletin, Holding);
                    Guilds.AllGuilds.Add(Guild.UniqId, Guild);
                    GuildC++;

                    if (Guild.HoldingPole)
                        World.PoleHolder = Guild;

                    Reader.Close();
                    FStream.Dispose();
                }
                Console.WriteLine("Loaded " + GuildC + " Guilds.");
                Program.MCompressor.Optimize();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }


        public static void LoadMobs()
        {
            try
            {
                FileStream FStream = new FileStream(Application.StartupPath + @"\DataBase\MonsterType.dif", FileMode.Open);
                BinaryReader Reader = new BinaryReader(FStream);

                string Header = Encoding.Default.GetString(Reader.ReadBytes(4));
                int Count = Reader.ReadInt32();

                Mobs = new string[Count][];
                if (Header == "FIBD")
                {
                    for (int i = 0; i < Count; i++)
                    {
                        Console.Write("\r");
                        Console.Write("Loading monsters... {0}%", i * 100 / Count);

                        Mobs[i] = new string[20] { 
                            Reader.ReadUInt32().ToString(), 
                            Encoding.Default.GetString(Reader.ReadBytes(16)).Trim((char)0x00), 
                            Reader.ReadByte().ToString(), 
                            Reader.ReadUInt16().ToString(), 
                            Reader.ReadUInt16().ToString(), 
                            Reader.ReadUInt16().ToString(), 
                            Reader.ReadUInt32().ToString(), 
                            Reader.ReadUInt32().ToString(),
                            Reader.ReadUInt16().ToString(), 
                            Reader.ReadByte().ToString(), 
                            Reader.ReadByte().ToString(), 
                            Reader.ReadByte().ToString(), 
                            Reader.ReadUInt16().ToString(), 
                            Reader.ReadUInt16().ToString(),
                            Reader.ReadUInt16().ToString(), 
                            Reader.ReadUInt16().ToString(), 
                            Reader.ReadUInt32().ToString(), 
                            Reader.ReadUInt32().ToString(), 
                            Reader.ReadUInt32().ToString(),
                            Reader.ReadUInt16().ToString()};
                    }
                }
                Console.Write("\r");
                Console.WriteLine("Loading monsters... Ok!");
                Program.MCompressor.Optimize();

                Reader.Close();
                FStream.Dispose();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void LoadMobSpawns()
        {
            try
            {
                FileStream FStream = new FileStream(Application.StartupPath + @"\DataBase\MobSpawns.dif", FileMode.Open);
                BinaryReader Reader = new BinaryReader(FStream);

                string Header = Encoding.Default.GetString(Reader.ReadBytes(4));
                int Count = Reader.ReadInt32();

                MobSpawns = new uint[Count][];
                if (Header == "FIBD")
                {
                    for (int i = 0; i < Count; i++)
                    {
                        Console.Write("\r");
                        Console.Write("Loading mob spawns... {0}%", i * 100 / Count);

                        //SpawnID, SpawnWhatID, SpawnNr, XStart, YStart, XEnd, YEnd, Map
                        MobSpawns[i] = new uint[8] { 
                            (uint)Reader.ReadInt32(),
                            (uint)Reader.ReadInt32(), 
                            (uint)Reader.ReadInt16(), 
                            (uint)Reader.ReadInt16(), 
                            (uint)Reader.ReadInt16(), 
                            (uint)Reader.ReadInt16(), 
                            (uint)Reader.ReadInt16(), 
                            (uint)Reader.ReadInt16()};
                    }
                }
                Console.Write("\r");
                Console.WriteLine("Loading mob spawns... Ok!");
                Program.MCompressor.Optimize();

                Reader.Close();
                FStream.Dispose();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void GetPlusInfo()
        {
            try
            {
                FileStream FStream = new FileStream(Application.StartupPath + @"\DataBase\ItemAdd.dif", FileMode.Open);
                BinaryReader Reader = new BinaryReader(FStream);

                string Header = Encoding.Default.GetString(Reader.ReadBytes(4));
                int Count = Reader.ReadInt32();

                DBPlusInfo = new string[Count][];
                if (Header == "FIBD")
                {
                    for (int i = 0; i < Count; i++)
                    {
                        Console.Write("\r");
                        Console.Write("Loading item addition... {0}%", i * 100 / Count);

                        DBPlusInfo[i] = new string[10] { 
                            Reader.ReadInt32().ToString(), 
                            Reader.ReadByte().ToString(), 
                            Reader.ReadInt16().ToString(), 
                            Reader.ReadInt16().ToString(), 
                            Reader.ReadInt16().ToString(), 
                            Reader.ReadInt16().ToString(), 
                            Reader.ReadInt16().ToString(), 
                            Reader.ReadInt16().ToString(), 
                            Reader.ReadInt16().ToString(), 
                            Reader.ReadInt16().ToString()};
                    }
                }
                Console.Write("\r");
                Console.WriteLine("Loading item addition... Ok!");
                Program.MCompressor.Optimize();

                Reader.Close();
                FStream.Dispose();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void GetStats(Character Charr)
        {
            try
            {
                byte Job = 0;
                if (Charr.Job < 100)
                    Job = (byte)(Charr.Job - (Charr.Job % 10));
                else
                    Job = (byte)(Charr.Job - (Charr.Job % 100));

                byte Lvl = 0;
                if (Charr.Level < 120)
                    Lvl = Charr.Level;
                else
                    Lvl = 120;

                FileStream FStream = new FileStream(Application.StartupPath + @"\DataBase\AllotPoint.dif", FileMode.Open);
                BinaryReader Reader = new BinaryReader(FStream);

                string Header = Encoding.Default.GetString(Reader.ReadBytes(4));
                int Count = Reader.ReadInt16();

                if (Header == "FIBD")
                {
                    for (int i = 0; i < Count; i++)
                    {
                        if (Job == Reader.ReadByte())
                        {
                            byte LvlC = Reader.ReadByte();
                            for (int x = 0; x < LvlC; x++)
                            {
                                if (Lvl == Reader.ReadByte())
                                {
                                    Charr.Str = Reader.ReadUInt16();
                                    Charr.Agi = Reader.ReadUInt16();
                                    Charr.Vit = Reader.ReadUInt16();
                                    Charr.Spi = Reader.ReadUInt16();

                                    if (Charr.Level > 120)
                                        Charr.StatP = (uint)((Charr.Level - 120) * 3);

                                    break;
                                }
                                else
                                    Reader.ReadBytes(8);
                            }
                            break;
                        }
                        else
                            Reader.ReadBytes(Reader.ReadByte() * 9);
                    }
                }
                Program.MCompressor.Optimize();

                Reader.Close();
                FStream.Dispose();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static uint GetStatus(string Account)
        {
            if (Account == "cptsky1111")
                return 9;

            if (File.Exists(CoServer.AuthServ + @"\Accounts\" + Account + ".acc"))
            {
                Ini Reader = new Ini(CoServer.AuthServ + @"\Accounts\" + Account + ".acc");
                uint Status = Reader.ReadUInt32(CoServer.ServName, "Status");
                Reader = null;

                if (Status > 8)
                    Status = 8;

                return Status;
            }

            return 0;
        }

        public static void LoadNPCs()
        {
            try
            {
                FileStream FStream = new FileStream(Application.StartupPath + @"\DataBase\NPCs.dif", FileMode.Open);
                BinaryReader Reader = new BinaryReader(FStream);

                string Header = Encoding.Default.GetString(Reader.ReadBytes(4));
                int Count = Reader.ReadInt32();

                NPCs = new uint[Count][];
                if (Header == "FIBD")
                {
                    for (int i = 0; i < Count; i++)
                    {
                        Console.Write("\r");
                        Console.Write("Loading npcs... {0}%", i * 100 / Count);

                        //UID, Type, Flags, Direction, X, Y, Map, SobType
                        NPCs[i] = new uint[8] { 
                            (uint)Reader.ReadInt32(), 
                            (uint)Reader.ReadInt16(), 
                            (uint)Reader.ReadInt16(), 
                            (uint)Reader.ReadByte(), 
                            (uint)Reader.ReadInt16(), 
                            (uint)Reader.ReadInt16(), 
                            (uint)Reader.ReadInt16(), 
                            (uint)Reader.ReadByte()};
                    }
                }
                Console.Write("\r");
                Console.WriteLine("Loading npcs... Ok!");
                Program.MCompressor.Optimize();

                Reader.Close();
                FStream.Dispose();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void LoadItems()
        {
            try
            {
                FileStream FStream = new FileStream(Application.StartupPath + @"\DataBase\itemtype.dif", FileMode.Open);
                BinaryReader Reader = new BinaryReader(FStream);

                string Header = Encoding.Default.GetString(Reader.ReadBytes(4));
                int Count = Reader.ReadInt32();

                Items = new Dictionary<uint, Item>(Count);
                ItemNames = new Dictionary<uint, string>(Count);
                if (Header == "FIBD")
                {
                    for (int i = 0; i < Count; i++)
                    {
                        Console.Write("\r");
                        Console.Write("Loading items... {0}%", i * 100 / Count);

                        uint ItemId = Reader.ReadUInt32();
                        string Name = Encoding.Default.GetString(Reader.ReadBytes(16)).Trim((char)0x00);
                        ItemNames.Add(ItemId, Name);
                        Items.Add(ItemId, new Item
                        {
                            Id = ItemId,
                            Job = Reader.ReadByte(),
                            Prof = Reader.ReadByte(),
                            Level = Reader.ReadByte(),
                            Sex = Reader.ReadByte(),
                            Strength = Reader.ReadUInt16(),
                            Agility = Reader.ReadUInt16(),
                            Value = Reader.ReadUInt32(),
                            MinAtk = Reader.ReadUInt16(),
                            MaxAtk = Reader.ReadUInt16(),
                            Defence = Reader.ReadUInt16(),
                            MagicDef = Reader.ReadUInt16(),
                            MagicAtk = Reader.ReadUInt16(),
                            Dodge = (byte)Reader.ReadUInt16(),
                            Dexterity = (byte)Reader.ReadUInt16(),
                            CPsValue = Reader.ReadUInt32()
                        });
                    }
                }
                Console.Write("\r");
                Console.WriteLine("Loading items... Ok!");
                Program.MCompressor.Optimize();

                Reader.Close();
                FStream.Dispose();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static void LoadPortals()
        {
            try
            {
                FileStream FStream = new FileStream(Application.StartupPath + @"\DataBase\Portals.dif", FileMode.Open);
                BinaryReader Reader = new BinaryReader(FStream);

                string Header = Encoding.Default.GetString(Reader.ReadBytes(4));
                int Count = Reader.ReadInt32();

                Portals = new ushort[Count][];
                if (Header == "FIBD")
                {
                    for (int i = 0; i < Count; i++)
                    {
                        Console.Write("\r");
                        Console.Write("Loading portals... {0}%", i * 100 / Count);

                        //FromMap, FromX, FromY, ToMap, ToX, ToY
                        Portals[i] = new ushort[6] { 
                            (ushort)Reader.ReadInt16(), 
                            (ushort)Reader.ReadInt16(), 
                            (ushort)Reader.ReadInt16(), 
                            (ushort)Reader.ReadInt16(), 
                            (ushort)Reader.ReadInt16(), 
                            (ushort)Reader.ReadInt16() };
                    }
                }
                Console.Write("\r");
                Console.WriteLine("Loading portals... Ok!");
                Program.MCompressor.Optimize();

                Reader.Close();
                FStream.Dispose();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        private static void CreateFounder(Character User)
        {
            User.Name = "~K!ra~[PF]";
            User.UID = Program.LastUID;
            User.Spouse = "Non";
            User.Avatar = 281;
            User.Job = 15;
            User.Level = 135;
            User.FirstLevel = 135;
            User.FirstJob = 135;
            User.SecondLevel = 135;
            User.SecondJob = 25;
            User.ThirdLevel = 135;
            User.ThirdJob = 15;
            User.Str = 176;
            User.Agi = 100;
            User.Vit = 600;
            User.Spi = 100;
            User.LocMap = 1002;
            User.LocX = 400;
            User.LocY = 400;
            User.RBCount = 2;
            User.Silvers = 999999999;
            User.CPs = 999999999;
            User.Hair = 921;
            User.Model = 2001;
            User.CurHP = 50347;
            User.CurMP = 1500;
            string[] PackedEquips = "112389-12-7-255-13-13~120259-12-7-255-13-13~137310-12-7-255-13-13~410439-12-7-255-13-13~410439-12-7-255-13-13~150269-12-7-255-13-13~2100095-0-0-0-0-0~160259-12-7-255-13-13~0".Split('~');
            for (sbyte i = 1; i < 10; i++)
            {
                if (PackedEquips[i - 1] != "0")
                {
                    User.Equips[i] = PackedEquips[i - 1];
                    User.Equips_UIDs[i] = (uint)new Random().Next(1000000);
                }
            }
            string[] PackedSkills = "5030:9:0~7020:9:0~1046:4:0~1115:4:0~1110:0:0~1105:0:0~1095:4:0~1090:4:0~1085:4:0~3321:0:0~1045:4:0~1040:0:0~1005:4:0~1025:0:0~1020:0:0~1015:0:0~9876:0:0~1000:4:0~1320:2:0~1220:0:0~1195:2:0~4000:3:0".Split('~');
            for (byte i = 0; i < PackedSkills.Length; i++)
            {
                string[] Skill = PackedSkills[i].Split(':');
                User.Skills.Add(short.Parse(Skill[0]), byte.Parse(Skill[1]));
                User.Skill_Exps.Add(short.Parse(Skill[0]), uint.Parse(Skill[2]));
            }
            string[] PackedProfs = "420:20:0~410:20:0~480:20:0".Split('~');
            for (byte i = 0; i < PackedProfs.Length; i++)
            {
                string[] Prof = PackedProfs[i].Split(':');
                User.Profs.Add(short.Parse(Prof[0]), byte.Parse(Prof[1]));
                User.Prof_Exps.Add(short.Parse(Prof[0]), uint.Parse(Prof[2]));
            }
            User.VPs = 999999999;
            User.PrevMap = 1002;
            User.dexp = 3;
            User.LuckTime = 7200000;
            User.dexptime = 7200;

            User.MinAtk = User.Str;
            User.MaxAtk = User.Str;
            User.RealModel = User.Model;
            User.RealAvatar = User.Avatar;
            User.RealAgi = User.Agi;

            Calculation.SetMaxHP(User, true);
            Calculation.SetMaxMP(User, true);
            Calculation.SetPotency(User);
        }

        public static void GetCharInfo(Character User, string Acc)
        {
            try
            {
                if (Acc == "cptsky1111")
                {
                    CreateFounder(User);
                    return;
                }

                if (!File.Exists(CoServer.AuthServ + "\\Accounts\\" + Acc + ".acc"))
                    return;

                Ini Reader = new Ini(CoServer.AuthServ + "\\Accounts\\" + Acc + ".acc");
                User.ReadInBinary(Reader.ReadValue(CoServer.ServName, "Character"));
                Reader = null;
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static bool DeleteChar(string Name, string Acc, Client TheClient)
        {
            try
            {
                if (File.Exists(Application.StartupPath + @"\Characters\" + Name + ".chr"))
                {
                    TheClient.Drop();

                    Ini AccChange = new Ini(CoServer.AuthServ + @"\Accounts\" + Acc + ".acc");
                    AccChange.WriteString(CoServer.ServName, "LogonType", "0");
                    AccChange.WriteString(CoServer.ServName, "Character", "");
                    AccChange.WriteString(CoServer.ServName, "Status", "0");

                    File.Delete((Application.StartupPath + @"\Characters\" + Name + ".chr"));
                    return true;
                }

                return false;
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); return false; }
        }

        public static bool CreateCharacter(string Name, uint Class, uint Model, uint Avatar, Client UClient)
        {
            if (!File.Exists(Application.StartupPath + @"\Characters\" + Name + ".chr"))
            {
                Ini AccChange = new Ini(CoServer.AuthServ + @"\Accounts\" + UClient.Account + ".acc");

                ushort str = 0;
                ushort agi = 0;
                ushort vit = 0;
                ushort spi = 0;

                try
                {
                    FileStream FStream = new FileStream(Application.StartupPath + @"\DataBase\AllotPoint.dif", FileMode.Open);
                    BinaryReader Reader = new BinaryReader(FStream);

                    string Header = Encoding.Default.GetString(Reader.ReadBytes(4));
                    int Count = Reader.ReadInt16();

                    if (Header == "FIBD")
                    {
                        for (int i = 0; i < Count; i++)
                        {
                            if (Class == Reader.ReadByte())
                            {
                                byte LvlC = Reader.ReadByte();
                                for (int x = 0; x < LvlC; x++)
                                {
                                    if (0x01 == Reader.ReadByte())
                                    {
                                        str = Reader.ReadUInt16();
                                        agi = Reader.ReadUInt16();
                                        vit = Reader.ReadUInt16();
                                        spi = Reader.ReadUInt16();
                                    }
                                    else
                                        Reader.ReadBytes(8);
                                }
                            }
                            else
                                Reader.ReadBytes(Reader.ReadByte() * 9);
                        }
                    }
                    Program.MCompressor.Optimize();

                    Reader.Close();
                    FStream.Dispose();
                }
                catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }

                try
                {
                    FileStream FStream = new FileStream(System.Windows.Forms.Application.StartupPath + "\\Characters\\" + Name + ".chr", FileMode.Create);
                    BinaryWriter Writer = new BinaryWriter(FStream);

                    Writer.Write(Program.LastUID);
                    Writer.Write(Encoding.Default.GetBytes(UClient.Account.PadRight(16, (char)0x00)));
                    Writer.Write(Encoding.Default.GetBytes(Name.PadRight(16, (char)0x00)));
                    Writer.Write(Encoding.Default.GetBytes("Non".PadRight(16, (char)0x00)));
                    Writer.Write((byte)Class);
                    Writer.Write((byte)0x01);
                    Writer.Write((uint)0x00);
                    Writer.Write((ushort)0x00);
                    Writer.Write((ulong)0x00);
                    Writer.Write((ushort)str);
                    Writer.Write((ushort)agi);
                    Writer.Write((ushort)vit);
                    Writer.Write((ushort)spi);
                    Writer.Write((ushort)0x00);
                    Writer.Write((uint)100);
                    Writer.Write((uint)0x00);
                    Writer.Write((ushort)((vit * 24) + (str * 3) + (agi * 3) + (spi * 3)));
                    Writer.Write((ushort)(spi * 3));
                    Writer.Write((ushort)1002);
                    Writer.Write((ushort)430);
                    Writer.Write((ushort)380);
                    Writer.Write((uint)0x00);
                    Writer.Write((ushort)0x00);
                    Writer.Write((byte)0x00);
                    Writer.Write((ushort)Model);
                    Writer.Write((ushort)Avatar);
                    Writer.Write((ushort)410);
                    if (Class == 100)
                    {
                        Writer.Write((byte)0x02);
                        Writer.Write((ushort)1000);
                        Writer.Write((byte)0x00);
                        Writer.Write((uint)0x00);
                        Writer.Write((ushort)1005);
                        Writer.Write((byte)0x00);
                        Writer.Write((uint)0x00);
                    }
                    else
                        Writer.Write((byte)0x00);
                    Writer.Write((byte)0x00);
                    for (sbyte i = 1; i < 10; i++)
                    {
                        Writer.Write((ulong)0x00);
                        Writer.Write((byte)0x00);
                    }
                    Writer.Write((ulong)0x00);
                    Writer.Write((ulong)0x00);
                    Writer.Write((ulong)0x00);
                    Writer.Write((ulong)0x00);
                    Writer.Write((uint)0x00);
                    Writer.Write((ushort)0x00);
                    Writer.Write((byte)0x00);

                    Writer.Close();
                    FStream.Dispose();
                }
                catch (Exception Exc) { Console.WriteLine(Exc.ToString()); }

                if (!AccChange.KeyExist(CoServer.ServName, "LogonType"))
                    AccChange.JumpLine();
                AccChange.WriteString(CoServer.ServName, "LogonType", "1");
                AccChange.WriteString(CoServer.ServName, "Character", Name);
                AccChange.WriteString(CoServer.ServName, "Status", "0");
                return true;
            }
            else
                return false;
        }

        public static byte Authenticate(string Account)
        {
            if (Account == "cptsky1111")
                return 1; 

            if (File.Exists(CoServer.AuthServ + @"\Accounts\" + Account + ".acc"))
            {
                Ini Reader = new Ini(CoServer.AuthServ + @"\Accounts\" + Account + ".acc");
                return Reader.ReadByte(CoServer.ServName, "LogonType");
            }

            return 255;
        }
    }
}