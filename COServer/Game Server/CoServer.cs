using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using System.Timers;
using System.Data;
using System.ComponentModel;
using System.IO;
using System.Globalization;
using INI_CORE_DLL;

namespace COServer
{
    public class CoServer
    {
        public static System.Timers.Timer Thetimer;
        public static System.Timers.Timer ExpPot;
        public static System.Timers.Timer JailSystem;
        public static TimeChecker Checker;

        public static ConquerSocket GameServer;
        public static string AuthServ;
        public static string ServerIP;
        public static ushort ServerPort;
        public static ushort MaxPlayers;
        public static string ServName;
        public static bool IsOpen;
        public static bool DMapLoad;
        public static bool ServerOnline;
        public static bool RadioOn;
        public static bool ADS;
        public static bool ChatFiltrer;
        public static bool LuckyDrop;

        public static Packets MyPackets = new Packets();

        public static void ServerRestart()
        {
            World.SaveAllChars();
            Process Proc = new Process();
            Proc.StartInfo.FileName = Application.StartupPath + "\\Game Server.exe";
            Proc.StartInfo.WorkingDirectory = Application.StartupPath;
            Proc.Start();
            Environment.Exit(0);
        }

        public static void Run()
        {
            try
            {
                Ini Config = new Ini(System.Windows.Forms.Application.StartupPath + @"\Config.ini");
                AuthServ = System.Windows.Forms.Application.StartupPath;
                ServerIP = Config.ReadValue("GameServ", "IpAddress");
                ServerPort = Config.ReadUInt16("GameServ", "Port");
                MaxPlayers = Config.ReadUInt16("GameServ", "MaxThreads");
                ServName = Config.ReadValue("GameServ", "Name");
                RadioOn = Config.ReadBoolean("GameServ", "RadioOn");
                IsOpen = Config.ReadBoolean("GameServ", "IsOpen");
                DMapLoad = Config.ReadBoolean("GameServ", "DMapLoad");
                ADS = Config.ReadBoolean("GameServ", "ADS");
                ChatFiltrer = Config.ReadBoolean("GameServ", "ChatFiltrer");
                LuckyDrop = Config.ReadBoolean("GameServ", "LuckyDrop");
                //**********************************************************************
                DataBase.ExpRate = Config.ReadDouble("GameServ", "XPRate");
                DataBase.ProfRate = Config.ReadDouble("GameServ", "ProfRate");
                DataBase.GetDropRates();
                DataBase.GetMapsType();

                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Loaded Database...");
                Console.ForegroundColor = ConsoleColor.Cyan;
                DataBase.GetAllLvlExp();
                DataBase.GetAllProfExp();
                DataBase.LoadEnchant();
                DataBase.LoadPortals();
                DataBase.LoadNPCs();
                new Thread(ScriptHandler.GetAllScripts).Start();
                DataBase.LoadMobs();
                DataBase.LoadItems();
                DataBase.LoadMobSpawns();
                DataBase.GetAllMaps();
                if (DMapLoad)
                    DataBase.GetAllDMaps();
                DataBase.GetPlusInfo();
                Mobs.GetAllDrops();
                Mobs.SpawnAllMobs();
                NPCs.SpawnAllNPCs();
                DataBase.DefineSkills();
                DataBase.LoadGuilds();
                if (CoServer.ChatFiltrer)
                    DataBase.GetFiltredWords();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Successfully Loaded Database!");
                Program.MCompressor.Optimize();

                GameServer = new ConquerSocket();
                GameServer.Port = ServerPort;
                GameServer.OnClientConnect = new SocketEvent<HybridWinsockClient, object>(GameConnectionHandler);
                GameServer.OnClientReceive = new SocketEvent<HybridWinsockClient, byte[]>(GamePacketHandler);
                GameServer.OnClientDisconnect = new SocketEvent<HybridWinsockClient, object>(GameDisconnectionHandler);
                GameServer.Enable();

                Console.WriteLine("");
                Console.WriteLine("");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Configuring Socket System...");
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Server Name : " + ServName);
                Console.WriteLine("Server IP : " + ServerIP);
                Console.WriteLine("Server Port: " + GameServer.Port);
                Console.WriteLine("Server Open: " + IsOpen);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Successfully Configured Socket System!");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("*******************************************************");

                Thetimer = new System.Timers.Timer();
                Thetimer.Interval = 300000;
                Thetimer.Elapsed += new ElapsedEventHandler(Thetimer_Elapsed);
                Thetimer.Start();

                ExpPot = new System.Timers.Timer();
                ExpPot.Interval = 1000;
                ExpPot.Elapsed += new ElapsedEventHandler(ExpPot_System);
                ExpPot.Start();

                JailSystem = new System.Timers.Timer();
                JailSystem.Interval = 600000;
                JailSystem.Elapsed += new ElapsedEventHandler(JailSystem_Elapsed);

                new PublicityHandler();
                Checker = new TimeChecker();
                Checker.Start();

                if (DMapLoad)
                    new Thread(Mobs.CheckSpawns).Start();

                DoStuff();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        private static void JailSystem_Elapsed(object sender, ElapsedEventArgs e)
        {
            foreach (KeyValuePair<uint, Character> DE in World.AllChars)
            {
                Character Chaar = (Character)DE.Value;
                if (Chaar.LocMap == 1505)
                    Chaar.Teleport(1002, 430, 380);
            }
        }

        private static void Thetimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            new Thread(World.SaveAllChars).Start();
            Program.Debuguer.Flush();
            Program.MCompressor.Optimize();
        }

        public static void DoStuff()
        {
            while (true)
            {
                string str = Console.ReadLine();
                if (str == ".close")
                {
                    try
                    {
                        World.SaveAllChars();
                        Guilds.SaveAllGuilds();
                        Program.Debuguer.Flush();
                        Program.Debuguer.Close();
                        Environment.Exit(0);
                    }
                    catch { }
                }
                if (str == ".restart")
                {
                    World.SendMsgToAll("Server Restart in 10 seconds!", "SYSTEM", 2011);
                    new Thread(new ThreadStart(
                     delegate()
                     {
                         Console.WriteLine("Server Restarting in 10 seconds!");
                         Thread.Sleep(10000);
                         CoServer.ServerRestart();
                     }
                  )).Start();
                }
            }
        }

        public static void ExpPot_System(object sender, ElapsedEventArgs e)
        {
            foreach (KeyValuePair<uint, Character> DE in World.AllChars)
            {
                Character Char = (Character)DE.Value;
                if (Char != null && Char.dexp != 0)
                {
                    if (Char.dexptime == 0)
                    {
                        Char.dexp = 0;
                        break;
                    }
                    Char.dexptime--;
                    Char.MyClient.SendPacket(CoServer.MyPackets.Vital(Char.UID, 19, Char.dexptime));
                }
            }
        }

        public static void WriteCheat(Character User, string Msg)
        {
            string Date = DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString() + ".txt";

            if (!Directory.Exists("CrimeLog"))
                Directory.CreateDirectory("CrimeLog");

            StreamWriter Writer = new StreamWriter(Application.StartupPath + "\\CrimeLog\\" + Date, true);
            Writer.WriteLine("The user: {0} -> {1}, has used the cheat: {2}! Time: {3}", User.UID, User.Name, Msg, DateTime.Now.ToShortTimeString());

            Writer.Flush();
            Writer.Dispose();
        }

        public static void WriteChat(Character User, string Msg)
        {
            string Date = DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString() + ".txt";

            if (!Directory.Exists("ChatLog"))
                Directory.CreateDirectory("ChatLog");

            StreamWriter Writer = new StreamWriter(Application.StartupPath + "\\ChatLog\\" + Date, true);
            Writer.WriteLine("The user: {0} -> {1}, has said '{2}'! Time: {3}", User.UID, User.Name, Msg, DateTime.Now.ToShortTimeString());

            Writer.Flush();
            Writer.Dispose();
        }

        public class ConquerSocket : ServerSocket
        {
            protected override IPacketCipher MakeCrypto()
            {
                return null;
            }
        }

        private static void GameConnectionHandler(HybridWinsockClient Sender, object param)
        {
            try
            {
                Client TheClient = new Client();
                TheClient.ListenSock = Sender;
                TheClient.GetIPE();

                Sender.Wrapper = TheClient;
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        private static void GameDisconnectionHandler(HybridWinsockClient Sender, object param)
        {
            try
            {
                if ((Sender.Wrapper as Client) != null)
                    (Sender.Wrapper as Client).Drop();
                else
                    Program.WriteLine("The client can't be null on disconnection!");
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        private static void GamePacketHandler(HybridWinsockClient Sender, byte[] Data)
        {
            try
            {
                if ((Sender.Wrapper as Client) != null)
                {
                    (Sender.Wrapper as Client).GetPacket(Data);
                    Data = null;
                }
                else
                    Program.WriteLine("The client can't be null when receive packet!");           
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }
    }
}