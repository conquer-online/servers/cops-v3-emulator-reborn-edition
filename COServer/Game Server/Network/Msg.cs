﻿using System;

namespace COServer.Network
{
    public partial class Msg
    {
        public string MsgError(byte[] Data, Exception pExc)
        {
            string Exception = "Catch error in process msg: $type";
            if (Data.Length > 3)
                Exception = Exception.Replace("$type", ((Data[0x03] << 8) + Data[0x02]).ToString());

            if (pExc != null)
                Exception += "\n" + pExc.ToString();

            return Exception;
        }
    }
}
