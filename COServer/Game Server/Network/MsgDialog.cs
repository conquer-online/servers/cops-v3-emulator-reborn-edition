﻿using System;
using INI_CORE_DLL;

namespace COServer.Network
{
    public class Dialog : Msg
    {
        private byte[] Data;
        private Client Client;
        private Character User;

        public Dialog(byte[] data, Client TheClient)
        {
            Data = data;
            Client = TheClient;
            User = TheClient.MyChar;

            try
            {
                uint NpcId = (uint)((Data[0x07] << 24) + (Data[0x06] << 16) + (Data[0x05] << 8) + Data[0x04]);
                byte Control = Data[0x0a];
                //Control == 0 [First Dialog]
                //Control == 1 to 254 [New Dialog or Action]
                //Control == 255 [Close Dialog]

                //Set NpcId
                if (NpcId == 0)
                    NpcId = (uint)Client.CurrentNPC;
                else
                    Client.CurrentNPC = (int)NpcId;

                if (Control == 255)
                    return;

                if (ScriptHandler.AllScripts.ContainsKey(NpcId))
                    ScriptHandler.AllScripts[NpcId].Execute(Client, Control);
            }
            catch (Exception Exc) { Program.WriteLine(MsgError(Data, Exc)); }
        }

        ~Dialog()
        {
            Data = null;
            Client = null;
            User = null;
        }
    }
}
