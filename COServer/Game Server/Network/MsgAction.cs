﻿using System;
using System.Collections.Generic;
using System.Collections;
using DMapReader;
using CoS_Math;

namespace COServer.Network
{
    public class Action : Msg
    {
        private byte[] Data;
        private Client Client;
        private Character User;

        public Action(byte[] data, Client TheClient)
        {
            Data = data;
            Client = TheClient;
            User = Client.MyChar;

            try
            {
                byte SubType = Data[0x16];
                switch (SubType)
                {
                    case 102:
                        {
                            break;
                        }
                    case 111: //Start Vending
                        {
                            User.Direction = Data[20];

                            if (Shop.Create(User))
                                Client.SendPacket(CoServer.MyPackets.GeneralData(User, User.MyShop.GetId(), 111));
                            break;
                        }
                    case 112: //Destroy Shop
                        {
                            if (User.MyShop != null)
                                User.MyShop.Destroy();
                            break;
                        }
                    case 106: //Search a member
                        {
                            User.Ready = false;
                            uint CharUID = (uint)((Data[0x0b] << 24) + (Data[0x0a] << 16) + (Data[0x09] << 8) + Data[0x08]);
                            Character Member = (Character)World.AllChars[CharUID];
                            if (Member != null)
                                Client.SendPacket(CoServer.MyPackets.GeneralData(Member, Member.LocMap, 106));

                            User.Ready = true;
                            break;
                        }
                    case 118: //Respawn the normal char
                        {
                            User.Ready = false;
                            User.Transform = false;
                            User.GMTransform = false;
                            Client.SendPacket(CoServer.MyPackets.CharacterInfo(User));
                            User.GetEquipStats(1, true);
                            User.GetEquipStats(2, true);
                            User.GetEquipStats(3, true);
                            User.GetEquipStats(4, true);
                            User.GetEquipStats(5, true);
                            User.GetEquipStats(6, true);
                            User.GetEquipStats(7, true);
                            User.GetEquipStats(8, true);
                            User.MinAtk = User.Str;
                            User.MaxAtk = User.Str;
                            Calculation.SetMaxHP(User, false);
                            Calculation.SetMaxMP(User, false);
                            Calculation.SetPotency(User);
                            User.SendEquips(true);
                            World.UpdateSpawn(User);
                            User.Stamina = 100;
                            User.Ready = true;
                            break;
                        }
                    case 95: //Delete Char
                        {
                            uint CharUID = (uint)((Data[0x0b] << 24) + (Data[0x0a] << 16) + (Data[0x09] << 8) + Data[0x08]);
                            uint WHPass = (uint)((Data[0x0f] << 24) + (Data[0x0e] << 16) + (Data[0x0d] << 8) + Data[0x0c]);
                            int LocX = (Data[0x11] << 8) + (Data[0x10]);
                            int LocY = (Data[0x13] << 8) + (Data[0x12]);

                            string WHPW = WHPass.ToString();
                            bool ValidPW = true;

                            if (WHPW.IndexOfAny(new char[27] { ' ', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' }) > -1)
                                ValidPW = false;

                            if (ValidPW)
                                if (User.WHPW == WHPW || User.WHPW == "" && WHPW == "0")
                                    DataBase.DeleteChar(User.Name, Client.Account, Client);

                            break;
                        }
                    case 120:
                        {
                            if (User.Flying)
                            {
                                User.Flying = false;
                                User.Flags.Flying = false;
                            }
                            break;
                        }
                    case 99:
                        {
                            if (User.LocMap >= 1023 && User.LocMap <= 1028)
                                User.Mining = true;
                            break;
                        }
                    case 114:
                        {
                            Client.SendPacket(CoServer.MyPackets.GeneralData(User, User.LocMap, 114));
                            break;
                        }
                    case 54:
                        {
                            uint VUID = (uint)((Data[15] << 24) + (Data[14] << 16) + (Data[13] << 8) + Data[12]);

                            Character ViewedChar = (Character)World.AllChars[VUID];
                            string[] Splitter;
                            Client.SendPacket(CoServer.MyPackets.String(ViewedChar.UID, 16, ViewedChar.Spouse));
                            ViewedChar.MyClient.SendPacket(CoServer.MyPackets.SendMsg(ViewedChar.MyClient.MessageId, "SYSTEM", ViewedChar.Name, User.Name + " est en train de regarder votre équipement.", 2005));

                            if (ViewedChar.Equips[1] != null && ViewedChar.Equips[1] != "0")
                            {
                                Splitter = ViewedChar.Equips[1].Split('-');
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 1, 100, 100));
                            }
                            else
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 1, 0, 0));

                            if (ViewedChar.Equips[2] != null && ViewedChar.Equips[2] != "0")
                            {
                                Splitter = ViewedChar.Equips[2].Split('-');
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 2, 100, 100));
                            }
                            else
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 2, 0, 0));

                            if (ViewedChar.Equips[3] != null && ViewedChar.Equips[3] != "0")
                            {
                                Splitter = ViewedChar.Equips[3].Split('-');
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 3, 100, 100));
                            }
                            else
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 3, 0, 0));

                            if (ViewedChar.Equips[4] != null && ViewedChar.Equips[4] != "0")
                            {
                                Splitter = ViewedChar.Equips[4].Split('-');
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 4, 100, 100));
                            }
                            else
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 4, 0, 0));
                            if (ViewedChar.Equips[5] != null && ViewedChar.Equips[5] != "0")
                            {
                                Splitter = ViewedChar.Equips[5].Split('-');
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 5, 100, 100));
                            }
                            else
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 5, 0, 0));

                            if (ViewedChar.Equips[6] != null && ViewedChar.Equips[6] != "0")
                            {
                                Splitter = ViewedChar.Equips[6].Split('-');
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 6, 100, 100));
                            }
                            else
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 6, 0, 0));

                            if (ViewedChar.Equips[7] != null && ViewedChar.Equips[7] != "0")
                            {
                                Splitter = ViewedChar.Equips[7].Split('-');
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 7, 100, 100));
                            }
                            else
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 7, 0, 0));

                            if (ViewedChar.Equips[8] != null && ViewedChar.Equips[8] != "0")
                            {
                                Splitter = ViewedChar.Equips[8].Split('-');
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 8, 100, 100));
                            }
                            else
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 8, 0, 0));

                            if (ViewedChar.Equips[9] != null && ViewedChar.Equips[9] != "0")
                            {
                                Splitter = ViewedChar.Equips[9].Split('-');
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 9, 100, 100));
                            }
                            else
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 9, 0, 0));
                            break;
                        }
                    case 140:
                        {
                            uint UID = BitConverter.ToUInt32(Data, 12);
                            Character Char = (Character)World.AllChars[UID];
                            if (Char != null)
                                Client.SendPacket(CoServer.MyPackets.FriendEnemyInfoPacket(Char, 0));
                            break;
                        }
                    case 123:
                        {
                            uint UID = BitConverter.ToUInt32(Data, 12);
                            Character Char = (Character)World.AllChars[UID];
                            if (Char != null)
                                Client.SendPacket(CoServer.MyPackets.FriendEnemyInfoPacket(Char, 1));
                            break;
                        }
                    case 93:
                        {
                            User.XpCircle = 0;
                            break;
                        }
                    case 94:
                        {
                            if (DateTime.Now > User.Death.AddSeconds(20))
                                User.Revive(true);
                            break;
                        }
                    case 117:
                        {
                            int Value1 = (Data[7] << 24) + (Data[6] << 16) + (Data[5] << 8) + Data[4];
                            uint CharID = (uint)((Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + Data[8]);
                            uint VUID = (uint)((Data[15] << 24) + (Data[14] << 16) + (Data[13] << 8) + Data[12]);

                            Character ViewedChar = (Character)World.AllChars[VUID];
                            string[] Splitter;
                            Client.SendPacket(CoServer.MyPackets.String(ViewedChar.UID, 16, ViewedChar.Spouse));
                            ViewedChar.MyClient.SendPacket(CoServer.MyPackets.SendMsg(ViewedChar.MyClient.MessageId, "SYSTEM", ViewedChar.Name, User.Name + " est en train de regarder votre équipement.", 2005));

                            if (ViewedChar.Equips[1] != null && ViewedChar.Equips[1] != "0")
                            {
                                Splitter = ViewedChar.Equips[1].Split('-');
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 1, 100, 100));
                            }
                            else
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 1, 0, 0));

                            if (ViewedChar.Equips[2] != null && ViewedChar.Equips[2] != "0")
                            {
                                Splitter = ViewedChar.Equips[2].Split('-');
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 2, 100, 100));
                            }
                            else
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 2, 0, 0));

                            if (ViewedChar.Equips[3] != null && ViewedChar.Equips[3] != "0")
                            {
                                Splitter = ViewedChar.Equips[3].Split('-');
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 3, 100, 100));
                            }
                            else
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 3, 0, 0));

                            if (ViewedChar.Equips[4] != null && ViewedChar.Equips[4] != "0")
                            {
                                Splitter = ViewedChar.Equips[4].Split('-');
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 4, 100, 100));
                            }
                            else
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 4, 0, 0));
                            if (ViewedChar.Equips[5] != null && ViewedChar.Equips[5] != "0")
                            {
                                Splitter = ViewedChar.Equips[5].Split('-');
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 5, 100, 100));
                            }
                            else
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 5, 0, 0));

                            if (ViewedChar.Equips[6] != null && ViewedChar.Equips[6] != "0")
                            {
                                Splitter = ViewedChar.Equips[6].Split('-');
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 6, 100, 100));
                            }
                            else
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 6, 0, 0));

                            if (ViewedChar.Equips[7] != null && ViewedChar.Equips[7] != "0")
                            {
                                Splitter = ViewedChar.Equips[7].Split('-');
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 7, 100, 100));
                            }
                            else
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 7, 0, 0));

                            if (ViewedChar.Equips[8] != null && ViewedChar.Equips[8] != "0")
                            {
                                Splitter = ViewedChar.Equips[8].Split('-');
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 8, 100, 100));
                            }
                            else
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 8, 0, 0));

                            if (ViewedChar.Equips[9] != null && ViewedChar.Equips[9] != "0")
                            {
                                Splitter = ViewedChar.Equips[9].Split('-');
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 9, 100, 100));
                            }
                            else
                                Client.SendPacket(CoServer.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 9, 0, 0));
                            break;
                        }
                    case 142:
                        {
                            ushort Face = (ushort)Data[0x0c];

                            if (User.Model == 2001 || User.Model == 2002)
                                if (Face < 200)
                                    Face += 256;

                            User.Avatar = Face;
                            User.Silvers -= 500;

                            User.Screen.SendPacket(Data, true);
                            break;
                        }
                    case 133:
                        {
                            if (User.Mining)
                                User.Mining = false;

                            if (!User.Alive) //The Char is dead...
                            {
                                Client.SendPacket(CoServer.MyPackets.GeneralData(User, User.LocMap, 108));
                                Client.SendPacket(CoServer.MyPackets.SendMsg(Client.MessageId, "SYSTEM", User.Name, "Vous êtes mort!", 0x7d5));

                                User.Screen.Clear();
                                User.Screen.SendScreen(false);
                                World.SurroundDroppedItems(User, false);
                                User.Attacking = false;
                                return;
                            }

                            ushort PrevX = (ushort)((Data[0x11] << 8) + Data[0x10]); //Set the previous X
                            ushort PrevY = (ushort)((Data[0x13] << 8) + Data[0x12]); //Set the previous Y
                            ushort NewX = (ushort)((Data[0xd] << 8) + Data[0xc]); //Set the new X
                            ushort NewY = (ushort)((Data[0xf] << 8) + Data[0xe]); //Set the new Y
                            byte Dir = (byte)MyMath.GetDirection(PrevX, PrevY, NewX, NewY); //Set the direction

                            if (!MyMath.CanSee(PrevX, PrevY, NewX, NewY))//Big Jump
                            {
                                Client.SendPacket(CoServer.MyPackets.GeneralData(User, User.LocMap, 108));
                                Client.SendPacket(CoServer.MyPackets.SendMsg(Client.MessageId, "SYSTEM", User.Name, "Vous ne pouvez pas sauter si loin!", 0x7d5));

                                User.Screen.Clear();
                                User.Screen.SendScreen(false);
                                World.SurroundDroppedItems(User, false);
                                User.Attacking = false;
                                return;
                            }

                            ushort DMap = 0;
                            if (DataBase.AllMaps.ContainsKey(User.LocMap))
                                DMap = DataBase.AllMaps[User.LocMap].DMap;

                            if (DataBase.AllDMaps.ContainsKey(DMap)) //DataBase contains the DMap
                            {
                                if (DataBase.AllDMaps[DMap].CanAcess(NewX, NewY)) //The location is accessible
                                {
                                    User.Screen.SendPacket(Data, true);

                                    User.Attacking = false;
                                    User.TargetUID = 0;
                                    User.MobTarget = null;
                                    User.TGTarget = null;
                                    User.PTarget = null;
                                    User.SkillLooping = 0;
                                    User.AtkType = 0;
                                    User.PrevX = User.LocX;
                                    User.PrevY = User.LocY;
                                    User.LocX = (ushort)NewX;
                                    User.LocY = (ushort)NewY;
                                    User.Action = 100;
                                    User.Direction = Dir;

                                    User.Screen.Check();
                                    World.SurroundDroppedItems(User, true);
                                }
                                else
                                {
                                    Client.SendPacket(CoServer.MyPackets.GeneralData(User, User.LocMap, 108));
                                    Client.SendPacket(CoServer.MyPackets.SendMsg(Client.MessageId, "SYSTEM", User.Name, "Coordonnée invalide!", 0x7d5));

                                    User.Screen.Clear();
                                    User.Screen.SendScreen(false);
                                    World.SurroundDroppedItems(User, false);
                                    User.Attacking = false;
                                    return;
                                }
                            }
                            else
                            {
                                User.Screen.SendPacket(Data, true);

                                User.Attacking = false;
                                User.TargetUID = 0;
                                User.MobTarget = null;
                                User.TGTarget = null;
                                User.PTarget = null;
                                User.SkillLooping = 0;
                                User.AtkType = 0;
                                User.PrevX = User.LocX;
                                User.PrevY = User.LocY;
                                User.LocX = (ushort)NewX;
                                User.LocY = (ushort)NewY;
                                User.Action = 100;
                                User.Direction = Dir;

                                User.Screen.Check();
                                World.SurroundDroppedItems(User, true);
                            }

                            break;
                        }
                    case 74:
                        {
                            Client.There = true;
                            if (!CoServer.IsOpen && Client.Status < 8)
                            {
                                Client.SendPacket(CoServer.MyPackets.SendMsg(Client.MessageId, "SYSTEM", User.Name, "Le serveur de jeu est fermé au public!", 2101));
                                Client.MySocket.Disconnect();
                                Client.MySocket = null;
                                return;
                            }

                            if (World.AllChars.Count >= CoServer.MaxPlayers)
                            {
                                Client.SendPacket(CoServer.MyPackets.SendMsg(Client.MessageId, "SYSTEM", User.Name, "Le serveur est plein!", 2101));
                                Client.MySocket.Disconnect();
                                Client.MySocket = null;
                                return;
                            }

                            //Request Location
                            User.CheckPortal();
                            if (DataBase.AllMaps.ContainsKey(User.LocMap))
                            {
                                Map ThisMap = DataBase.AllMaps[User.LocMap];
                                Client.SendPacket(CoServer.MyPackets.GeneralData(User, ThisMap.DMap, 74));
                                Client.SendPacket(CoServer.MyPackets.GeneralData(User, ThisMap.Color, 104));
                                if (ThisMap.Flags != 0)
                                    Client.SendPacket(CoServer.MyPackets.MapInfo(ThisMap.MapId, ThisMap.DMap, ThisMap.Flags));
                                if (ThisMap.Weather != 0)
                                    Client.SendPacket(CoServer.MyPackets.Weather(ThisMap.Weather, 150, 75, 0));
                            }
                            else
                            {
                                Client.SendPacket(CoServer.MyPackets.GeneralData(User, User.LocMap, 74));
                                Client.SendPacket(CoServer.MyPackets.GeneralData(User, 0xFFFFFFFF, 104));
                            }

                            //Request Items
                            Client.SendPacket(CoServer.MyPackets.GeneralData(User, 0, 130));
                            User.SendEquips(true);
                            User.SendInventory();
                            Client.SendPacket(CoServer.MyPackets.GeneralData(User, 0, 75));

                            //Request Friends
                            User.SendFriends();
                            User.SendEnemies();
                            Client.SendPacket(CoServer.MyPackets.GeneralData(User, 0, 76));

                            //Request Profs
                            User.SendProfs();
                            Client.SendPacket(CoServer.MyPackets.GeneralData(User, 0, 77));

                            //Request Skills
                            User.SendSkills();
                            Client.SendPacket(CoServer.MyPackets.GeneralData(User, 0, 78));

                            //Request Guilds
                            World.SendAllGuild(User);
                            if (User.MyGuild != null)
                            {
                                Client.SendPacket(CoServer.MyPackets.GuildInfo(User.MyGuild, User));
                                Client.SendPacket(CoServer.MyPackets.SendMsg(Client.MessageId, "SYSTEM", User.Name, User.MyGuild.Bulletin, 2111));
                                Client.SendPacket(CoServer.MyPackets.GuildName(User.GuildID, User.MyGuild.Name));
                            }
                            Client.SendPacket(CoServer.MyPackets.GeneralData(User, 0, 97));

                            //Request Entity
                            User.Screen.Check();
                            User.Screen.SendScreen(false);
                            World.SurroundDroppedItems(User, false);

                            User.PKMode = 3;

                            Client.SendPacket(CoServer.MyPackets.SendMsg(Client.MessageId, "SYSTEM", User.Name, "COPS v3 Emulator - Version " + Program.Version, 2000));
                            Client.SendPacket(CoServer.MyPackets.SendMsg(Client.MessageId, "SYSTEM", User.Name, "Exp Rate: " + DataBase.ExpRate + "x", 2000));
                            Client.SendPacket(CoServer.MyPackets.SendMsg(Client.MessageId, "SYSTEM", User.Name, "Joueurs en ligne: "
                                + World.AllChars.Count, 2005));

                            break;
                        }
                    case 96:
                        {
                            User.PKMode = Data[12];
                            break;
                        }
                    case 79:
                        {
                            User.Direction = Data[20];
                            User.Screen.SendPacket(Data, true);
                            break;
                        }
                    case 81:
                        {
                            User.Action = Data[12];
                            break;
                        }
                    case 85:
                        {
                            uint CharID = (uint)((Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + (Data[8]));

                            if (User.UID == CharID)
                                User.UsePortal();
                            break;
                        }
                    default:
                        {
                            Program.WriteLine("MsgAction::Error -> Type[" + Data[0x16] + "] not implemented!");
                            break;
                        }
                }
            }
            catch (Exception Exc) { Program.WriteLine(MsgError(Data, Exc)); }
        }

        ~Action()
        {
            Data = null;
            Client = null;
            User = null;
        }
    }
}