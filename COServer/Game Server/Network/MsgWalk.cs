﻿using System;
using DMapReader;

namespace COServer.Network
{
    public class Walk : Msg
    {
        private byte[] Data;
        private Client Client;
        private Character User;

        public Walk(byte[] data, Client TheClient)
        {
            Data = data;
            Client = TheClient;
            User = Client.MyChar;

            try
            {
                sbyte NewX = 0;
                sbyte NewY = 0;
                byte Dir = (byte)(Data[8] % 8);
                User.Direction = Dir;

                switch (Dir)
                {
                    case 0: { NewY = 1; break; }
                    case 1: { NewX = -1; NewY = 1; break; }
                    case 2: { NewX = -1; break; }
                    case 3: { NewX = -1; NewY = -1; break; }
                    case 4: { NewY = -1; break; }
                    case 5: { NewX = 1; NewY = -1; break; }
                    case 6: { NewX = 1; break; }
                    case 7: { NewY = 1; NewX = 1; break; }
                }

                if (User.Mining)
                    User.Mining = false;

                ushort DMap = 0;
                if (DataBase.AllMaps.ContainsKey(User.LocMap))
                    DMap = DataBase.AllMaps[User.LocMap].DMap;

                if (DataBase.AllDMaps.ContainsKey(DMap)) //DataBase contains the DMap
                {
                    if (DataBase.AllDMaps[DMap].CanAcess((ushort)(User.LocX + NewX), (ushort)(User.LocY + NewY))) //The location is accessible
                    {
                        User.Screen.SendPacket(Data, true);

                        User.PrevX = User.LocX;
                        User.PrevY = User.LocY;
                        User.LocX = (ushort)(User.LocX + NewX);
                        User.LocY = (ushort)(User.LocY + NewY);

                        User.TargetUID = 0;
                        User.MobTarget = null;
                        User.PTarget = null;
                        User.TGTarget = null;
                        User.AtkType = 0;
                        User.SkillLooping = 0;
                        User.Action = 100;

                        User.Screen.Check();
                        World.SurroundDroppedItems(User, true);
                        User.Attacking = false;
                    }
                    else
                    {
                        Client.SendPacket(CoServer.MyPackets.GeneralData(User, User.LocMap, 74));
                        Client.SendPacket(CoServer.MyPackets.SendMsg(Client.MessageId, "SYSTEM", User.Name, "Coordonnée invalide!", 0x7d5));

                        User.Screen.Clear();
                        User.Screen.SendScreen(false);
                        World.SurroundDroppedItems(User, false);
                        User.Attacking = false;
                        return;
                    }
                }
                else
                {
                    User.Screen.SendPacket(Data, true);

                    User.PrevX = User.LocX;
                    User.PrevY = User.LocY;
                    User.LocX = (ushort)(User.LocX + NewX);
                    User.LocY = (ushort)(User.LocY + NewY);

                    User.TargetUID = 0;
                    User.MobTarget = null;
                    User.PTarget = null;
                    User.TGTarget = null;
                    User.AtkType = 0;
                    User.SkillLooping = 0;
                    User.Action = 100;

                    User.Screen.Check();
                    World.SurroundDroppedItems(User, true);
                    User.Attacking = false;
                }
            }
            catch (Exception Exc) { Program.WriteLine(MsgError(Data, Exc)); }
        }
        
        ~Walk()
        {
            Data = null;
            Client = null;
            User = null;
        }
    }
}