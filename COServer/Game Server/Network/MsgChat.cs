﻿using System;

namespace COServer.Network
{
    public class Chat : Msg
    {
        private byte[] Data;
        private Client Client;
        private Character User;

        public Chat(byte[] data, Client TheClient)
        {
            Data = data;
            Client = TheClient;
            User = Client.MyChar;

            try
            {
                ushort Channel = (ushort)((Data[0x09] << 8) + Data[0x08]);
                string From = "";
                string To = "";
                string Message = "";

                byte Length = Data[0x19];
                for (sbyte i = 0; i < Length; i++)
                    From += (char)Data[i + 0x1a];

                Length = Data[0x1a + From.Length];
                for (sbyte i = 0; i < Length; i++)
                    To += (char)Data[i + 0x1b + From.Length];

                Length = Data[0x1c + From.Length + To.Length];
                for (sbyte i = 0; i < Length; i++)
                    if ((0x1d + From.Length + To.Length) <= Data.Length)
                        Message += (char)Data[i + 0x1d + From.Length + To.Length];

                Commands.Parse(User, Message);
                if (Channel != 0x83f)
                    World.Chat(User, To, Message, Data, Channel);
                else
                    if (User.MyGuild != null)
                    {
                        User.MyGuild.Bulletin = Message;
                        DataBase.SaveGuild(User.MyGuild);
                        Client.SendPacket(CoServer.MyPackets.SendMsg(Client.MessageId, "SYSTEM", From, Message, 0x83f));
                    }

                From = null;
                To = null;
                Message = null;
            }
            catch (Exception Exc) { Program.WriteLine(MsgError(Data, Exc)); }
        }
        
        ~Chat()
        {
            Data = null;
            Client = null;
            User = null;
        }
    }
}