﻿using System;

namespace COServer.Network
{
    public class MsgFriend : Msg
    {
        //Define constant(s) here...
        const byte _MAX_USERFRIENDSIZE = 50;

        const int _FRIEND_APPLY = 10;
        const int _FRIEND_ACCEPT = 11;
        const int _FRIEND_ONLINE = 12;
        const int _FRIEND_OFFLINE = 13;
        const int _FRIEND_BREAK = 14;
        const int _FRIEND_GETINFO = 15;
        const int _ENEMY_ONLINE = 16;			//To client
        const int _ENEMY_OFFLINE = 17;			//To client
        const int _ENEMY_DEL = 18;			//To client & to server
        const int _ENEMY_ADD = 19;			//To client

        enum Status : byte
        {
            OFFLINE = 0,
            ONLINE = 1
        }

        public byte[] Create(uint FriendId, string FriendName, bool Online, byte Action)
        {
            PacketWriter PWriter = new PacketWriter(36);
            PWriter.WriteUInt16(36, 0);
            PWriter.WriteUInt16(1019, 2);
            PWriter.WriteUInt32(FriendId, 4);
            PWriter.WriteByte(Action, 8);
            PWriter.WriteByte(Online ? (byte)Status.ONLINE : (byte)Status.OFFLINE, 9);
            PWriter.WriteString(FriendName, false, 20);

            return PWriter.Flush();
        }

        public void Process(byte[] Data, Client Client)
        {
            Character User = Client.MyChar;

            try
            {
                if (User != null && Client != null)
                {
                    if (!User.IsPlayer())
                        return;

                    switch (Data[0x08])
                    {
                        case _FRIEND_GETINFO:
                            {
                                uint FriendId = (uint)((Data[0x07] << 24) + (Data[0x06] << 16) + (Data[0x05] << 8) + Data[0x04]);
                                Character Target = World.AllChars[FriendId];

                                if (Target != null)
                                    Client.SendPacket(Create(Target.UID, Target.Name, true, _FRIEND_ONLINE));

                                break;
                            }
                        case _ENEMY_ADD:
                            {
                                uint EnemyId = (uint)((Data[0x07] << 24) + (Data[0x06] << 16) + (Data[0x05] << 8) + Data[0x04]);
                                Character Target = World.AllChars[EnemyId];

                                if (Target != null)
                                    Client.SendPacket(Create(Target.UID, Target.Name, true, _ENEMY_ONLINE));

                                break;
                            }
                        case _ENEMY_DEL:
                            {
                                uint EnemyId = (uint)((Data[0x07] << 24) + (Data[0x06] << 16) + (Data[0x05] << 8) + Data[0x04]);

                                if (EnemyId != 0)
                                {
                                    User.DelEnemy(EnemyId);
                                    Client.SendPacket(Create(EnemyId, "", true, _ENEMY_DEL));
                                }

                                break;
                            }
                        case _FRIEND_BREAK:
                            {
                                uint FriendId = (uint)((Data[0x07] << 24) + (Data[0x06] << 16) + (Data[0x05] << 8) + Data[0x04]);
                                Character Target = World.AllChars[FriendId];

                                if (!User.FriendExist(FriendId))
                                {
                                    if (Target != null)
                                        User.SendSysMsg("%s n'est pas votre ami!".Replace("%s", Target.Name));
                                    return;
                                }

                                User.DelFriend(FriendId);
                                Client.SendPacket(Create(FriendId, "", true, _FRIEND_BREAK));

                                if (Target != null)
                                    User.SendSysMsg("%s1 n'aime plus %s2, ils rompent leur amitié.".Replace("%s1", User.Name).Replace("%s2", Target.Name));

                                if (Target != null)
                                {
                                    if (Target.FriendExist(User.UID))
                                    {
                                        Target.DelFriend(User.UID);
                                        Target.MyClient.SendPacket(Create(User.UID, User.Name, true, _FRIEND_BREAK));
                                    }
                                }
                                else
                                    DataBase.RemoveFromFriend(User.UID, Target.UID);

                                break;
                            }
                        case _FRIEND_APPLY:
                            {
                                uint FriendId = (uint)((Data[0x07] << 24) + (Data[0x06] << 16) + (Data[0x05] << 8) + Data[0x04]);
                                Character Target = World.AllChars[FriendId];

                                if (!User.Alive)
                                {
                                    User.SendSysMsg("Vous êtes mort!");
                                    return;
                                }

                                if (Target.LocMap != User.LocMap)
                                    return;

                                if (Target.Friends.Count >= _MAX_USERFRIENDSIZE)
                                {
                                    User.SendSysMsg("%s amis sont dans votre liste, Vous ne pouvez pas ajouter plus d'amis.".Replace("%s", _MAX_USERFRIENDSIZE.ToString()));
                                    return;
                                }

                                if (Target.Friends.Contains(User.UID))
                                {
                                    User.SendSysMsg("%s est déjà votre ami!".Replace("%s", Target.Name));
                                    return;
                                }

                                if (Target.RequestFriendWith != User.UID)
                                {
                                    User.RequestFriendWith = Target.UID;
                                    Target.MyClient.SendPacket(Create(User.UID, User.Name, true, _FRIEND_APPLY));

                                    //Target.SendSysMsg("STR_TO_MAKE_FRIEND");
                                    User.SendSysMsg("La demande d'amitié est parti.");
                                }
                                else
                                {
                                    User.AddFriend(Target);
                                    Target.AddFriend(User);

                                    User.MyClient.SendPacket(Create(Target.UID, Target.Name, true, _FRIEND_ACCEPT));
                                    Target.MyClient.SendPacket(Create(User.UID, User.Name, true, _FRIEND_ACCEPT));

                                    World.SendMsgToAll("%s1, %s2 sont des amis dorénavant!".Replace("%s1", User.Name).Replace("%s2", Target.Name), "SYSTEM", 0x7d5);
                                }

                                break;
                            }
                        default:
                            {
                                Program.WriteLine("MsgFriend::Error -> Type[" + Data[0x08] + "] not implemented!");
                                break;
                            }
                    }
                }
            }
            catch (Exception Exc) { Program.WriteLine(MsgError(Data, Exc)); }

            Data = null;
            Client = null;
            User = null;
        }
    }
}