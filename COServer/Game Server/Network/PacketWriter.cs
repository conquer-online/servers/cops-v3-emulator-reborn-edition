﻿using System;
using System.Text;

namespace COServer
{
    public class PacketWriter
    {
        private byte[] dwData;

        public PacketWriter(short Size)
        {
            dwData = new byte[Size];
        }

        public byte[] Flush()
        {
            return dwData;
        }

        public void WriteByte(byte Value, ushort Offset)
        {
            dwData[Offset + 0] = (byte)(Value);
        }

        public void WriteUInt16(ushort Value, ushort Offset)
        {
            dwData[Offset + 0] = (byte)(Value);
            dwData[Offset + 1] = (byte)(Value >> 8);
        }

        public void WriteUInt32(uint Value, ushort Offset)
        {
            dwData[Offset + 0] = (byte)(Value);
            dwData[Offset + 1] = (byte)(Value >> 8);
            dwData[Offset + 2] = (byte)(Value >> 16);
            dwData[Offset + 3] = (byte)(Value >> 24);
        }

        public void WriteString(string Value, ushort Offset)
        {
            dwData[Offset + 0] = (byte)Value.Length;
            Offset++;

            for (int i = 0; i < Value.Length; i++)
                dwData[Offset + i] = (byte)Value[i];
        }

        public void WriteString(string Value, bool WLength, ushort Offset)
        {
            if (WLength)
            {
                dwData[Offset + 0] = (byte)Value.Length;
                Offset++;
            }
            for (int i = 0; i < Value.Length; i++)
                dwData[Offset + i] = (byte)Value[i];
        }
    }
}
