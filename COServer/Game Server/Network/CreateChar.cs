﻿using System;

namespace COServer.Network
{
    public class CreateChar : Msg
    {
        private byte[] Data;
        private Client Client;

        public CreateChar(byte[] data, Client TheClient)
        {
            Data = data;
            Client = TheClient;

            try
            {
                string Account = "";
                for (sbyte i = 0x04; i < 0x14; i++)
                    if (Data[i] != 0x00)
                        Account += (char)Data[i];

                string Name = "";
                for (sbyte i = 0x14; i < 0x24; i++)
                    if (Data[i] != 0x00)
                        Name += (char)Data[i];

                uint Look = (ushort)((Data[0x35] << 8) + Data[0x34]);
                ushort Job = (ushort)((Data[0x37] << 8) + Data[0x36]);

                ushort Avatar = 67;
                bool ValidName = true;

                if (Look == 2001 || Look == 2002)
                    Avatar = 201;
                else
                    Avatar = 67;

                if (Name.IndexOfAny(new char[12] { ' ', '[', ']', '.', ':', '*', '?', '"', '<', '>', '|', '/' }) > -1)
                    ValidName = false;

                if (ValidName)
                {
                    Client.SendPacket(CoServer.MyPackets.SendMsg(Client.MessageId, "SYSTEM", "ALLUSERS", "ANSWER_OK", 0x834));

                    bool Success = DataBase.CreateCharacter(Name, Job, Look, Avatar, Client);
                    if (!Success)
                    {
                        Client.MySocket.Disconnect();
                        return;
                    }

                    Console.WriteLine("New character: " + Name);
                }
                else
                {
                    Client.SendPacket(CoServer.MyPackets.SendMsg(Client.MessageId, "SYSTEM", "ALLUSERS", "Ce nom de personnage n'est pas valide!", 0x834));
                    Client.MySocket.Disconnect();
                }
            }
            catch (Exception Exc) { Program.WriteLine(MsgError(Data, Exc)); }
        }
    }
}