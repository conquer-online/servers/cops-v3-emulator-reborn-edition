using System;
using System.Collections.Generic;
using System.Collections;

namespace COServer
{
    public unsafe class Packets
    {
        //*****************************************************
        //******************** Shop Packet ********************
        //*****************************************************
        public byte[] AddShopItem(uint ItemUID, uint Price, byte Type)
        {
            ushort PacketType = 1009;
            byte[] Packet = new byte[20];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)ItemUID;
                *((uint*)(p + 8)) = (uint)Price;
                *((uint*)(p + 12)) = (uint)Type;
            }
            return Packet;
        }

        public byte[] DelShopItem(uint ItemUID, uint ShopId)
        {
            ushort PacketType = 1009;
            byte[] Packet = new byte[20];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)ItemUID;
                *((uint*)(p + 8)) = (uint)ShopId;
                *((uint*)(p + 12)) = (uint)23;
            }
            return Packet;
        }

        public byte[] AddItemToShop(uint ItemUID, uint ShopId, uint Price, string Item, bool Money)
        {
            ushort PacketType = 0x454;
            byte[] Packet = new byte[40];

            string[] Splitter = Item.Split('-');
            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)ItemUID;
                *((uint*)(p + 8)) = (uint)ShopId;
                *((uint*)(p + 12)) = (uint)Price;
                *((uint*)(p + 16)) = uint.Parse(Splitter[0]);
                *((ushort*)(p + 20)) = (ushort)100;
                *((ushort*)(p + 22)) = (ushort)100;
                *((byte*)(p + 24)) = Money ? (byte)0x01 : (byte)0x03;
                *((byte*)(p + 32)) = byte.Parse(Splitter[4]);
                *((byte*)(p + 33)) = byte.Parse(Splitter[5]);
                *((byte*)(p + 36)) = byte.Parse(Splitter[1]);
                *((byte*)(p + 37)) = byte.Parse(Splitter[2]);
                *((byte*)(p + 38)) = byte.Parse(Splitter[3]);
            }
            return Packet;
        }

        public byte[] SpawnCarpet(Character Player, uint ShopId)
        {
            ushort PacketType = 1109;
            byte[] Packet = new byte[28 + Player.Name.Length];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)ShopId;
                *((ushort*)(p + 16)) = (ushort)(Player.LocX + 1);
                *((ushort*)(p + 18)) = (ushort)Player.LocY;
                *((ushort*)(p + 20)) = (ushort)406;
                *((ushort*)(p + 22)) = (ushort)14;
                *(p + 26) = 1;
                *(p + 27) = (byte)Player.Name.Length;
                for (int i = 0; i < Player.Name.Length; i++)
                {
                    *(p + 28 + i) = Convert.ToByte(Player.Name[i]);
                }
            }
            return Packet;
        }
        //*****************************************************
        //*****************************************************

        public byte[] MemberInfo(GuildMember Member, byte Position)
        {
            ushort PacketType = 0x458;
            byte[] Packet = new byte[28];
            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)Member.Donation;
                *(p + 8) = Position;
                for (sbyte i = 0; i < Member.Name.Length; i++)
                    *(p + 9 + i) = (byte)Member.Name[i];
            }

            return Packet;
        }

        public byte[] GuildInfo(Guild TheGuild, Character Player)
        {
            ushort PacketType = 0x452;

            byte[] Packet = new byte[40];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((ushort*)(p + 4)) = (ushort)TheGuild.UniqId;
                *((uint*)(p + 8)) = (uint)Player.GuildDonation;
                *((uint*)(p + 12)) = (uint)TheGuild.Fund;
                *((uint*)(p + 16)) = (uint)(TheGuild.Members.Count + TheGuild.Deputies.Count + 1);
                *(p + 20) = Player.GuildPosition;

                for (int i = 0; i < TheGuild.Leader.Name.Length; i++)
                {
                    *(p + 21 + i) = Convert.ToByte(TheGuild.Leader.Name[i]);
                }
            }

            return Packet;
        }

        public byte[] SendGuild(uint GuildID, byte Type)
        {
            ushort PacketType = 0x453;
            byte[] Packet = new byte[12];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *(p + 4) = Type;
                *((uint*)(p + 8)) = (uint)GuildID;
            }
            return Packet;
        }
        public byte[] GuildName(ushort GuildId, string Name)
        {
            ushort PacketType = 0x3f7;
            byte[] Packet = new byte[11 + Name.Length];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)GuildId;
                *(p + 8) = 3;
                *(p + 9) = 1;
                *(p + 10) = (byte)Name.Length;
                for (sbyte i = 0; i < Name.Length; i++)
                    *(p + 11 + i) = (byte)Name[i];
            }
            return Packet;
        }

        public byte[] FriendEnemyInfoPacket(Character Char, byte Enemy)
        {
            ushort PacketType = 0x7f1;
            byte[] Packet = new byte[36];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)Char.UID;
                *((uint*)(p + 8)) = uint.Parse((Char.Avatar.ToString() + Char.Model.ToString()));
                *(p + 12) = Char.Level;
                *(p + 13) = Char.Job;
                *((ushort*)(p + 14)) = (ushort)Char.PKPoints;
                *((ushort*)(p + 16)) = (ushort)Char.GuildID;
                *((ushort*)(p + 19)) = (ushort)Char.GuildPosition;
                for (int i = 0; i < Char.Spouse.Length; i++)
                    *(p + 20 + i) = Convert.ToByte(Char.Spouse[i]);
                //*(p + 35) = Enemy;
            }
            return Packet;
        }
        public byte[] FriendEnemyPacket(uint uid, string name, byte Mode, byte Online)
        {
            ushort PacketType = 0x3fb;
            byte[] Packet = new byte[36];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)uid;
                *(p + 8) = Mode;
                *(p + 9) = Online;
                *(p + 10) = 0;
                *(p + 11) = 0;
                *(p + 12) = 0;
                *(p + 13) = 0;
                *(p + 14) = 0;
                *(p + 15) = 0;
                *(p + 16) = 0;
                *(p + 17) = 0;
                *(p + 18) = 0;
                *(p + 19) = 0;

                for (int i = 0; i < name.Length; i++)
                {
                    *(p + 20 + i) = Convert.ToByte(name[i]);
                }
            }
            return Packet;
        }
        public byte[] TradeItem(uint ItemUID, string Item)
        {
            ushort PacketType = 0x3f0;
            byte[] Packet = new byte[32];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)ItemUID;

                string[] Splitter = Item.Split('-');

                *((uint*)(p + 8)) = uint.Parse(Splitter[0]);
                *(p + 12) = 1 & 0xff;
                *(p + 14) = 1 & 0xff;
                *(p + 16) = 2 & 0xff;
                *(p + 18) = 0xff;
                *(p + 19) = 0;
                *(p + 20) = 0;
                *(p + 21) = 0;
                *(p + 22) = 0;
                *(p + 23) = 0;
                *(p + 24) = byte.Parse(Splitter[4]);
                *(p + 25) = byte.Parse(Splitter[5]);
                *(p + 26) = 1;
                *(p + 27) = 2;
                *(p + 28) = byte.Parse(Splitter[1]);
                *(p + 29) = byte.Parse(Splitter[2]);
                *(p + 30) = byte.Parse(Splitter[3]);
            }
            return Packet;
        }
        public byte[] TradePacket(uint UID, byte Type)
        {
            ushort PacketType = 0x420;
            byte[] Packet = new byte[12];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)UID;
                *((uint*)(p + 8)) = (uint)Type;
            }
            return Packet;
        }
        public byte[] PlayerJoinsTeam(Character Player)
        {
            ushort PacketType = 0x402;
            uint Model = uint.Parse(Player.Avatar.ToString() + Player.Model.ToString());

            byte[] Packet = new byte[36];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *(p + 5) = 1;

                for (int i = 0; i < Player.Name.Length; i++)
                {
                    *(p + 8 + i) = Convert.ToByte(Player.Name[i]);
                }
                *((uint*)(p + 24)) = (uint)Player.UID;
                *((uint*)(p + 28)) = (uint)Model;
                *((ushort*)(p + 32)) = (ushort)Player.MaxHP;
                *((ushort*)(p + 34)) = (ushort)Player.CurHP;
                *((ushort*)(p + 36)) = (ushort)Player.MaxMP;
                *((ushort*)(p + 38)) = (ushort)Player.CurMP;
            }
            return Packet;
        }

        public byte[] TeamPacket(uint CharID, byte Mode)
        {
            ushort PacketType = 0x3ff;
            byte[] Packet = new byte[12];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)Mode;
                *((uint*)(p + 8)) = (uint)CharID;
            }

            return Packet;
        }
        public byte[] WhItems(Character Player, byte WH, ushort NPCID)
        {
            byte Count = 0;
            if (WH == 0)
                Count = Player.TCWHCount;
            if (WH == 1)
                Count = Player.PCWHCount;
            if (WH == 2)
                Count = Player.ACWHCount;
            if (WH == 3)
                Count = Player.DCWHCount;
            if (WH == 4)
                Count = Player.BIWHCount;
            if (WH == 5)
                Count = Player.MAWHCount;
            if (WH == 6)
                Count = Player.AZWHCount;

            string[] WareHouse;
            if (WH != 5)
                WareHouse = new string[] { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null };
            else
                WareHouse = new string[] { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null };
            if (WH == 0)
                WareHouse = Player.TCWH;
            if (WH == 1)
                WareHouse = Player.PCWH;
            if (WH == 2)
                WareHouse = Player.ACWH;
            if (WH == 3)
                WareHouse = Player.DCWH;
            if (WH == 4)
                WareHouse = Player.BIWH;
            if (WH == 5)
                WareHouse = Player.MAWH;
            if (WH == 6)
                WareHouse = Player.AZWH;

            ushort PacketType = 1102;
            byte[] Packet = new byte[16 + (20 * Count)];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)NPCID;
                *((uint*)(p + 12)) = (uint)Count;

                int count = 0;

                foreach (string item in WareHouse)
                {
                    if (item != null)
                    {
                        string[] Splitter = item.Split('-');

                        *((uint*)(p + 16 + count * 20)) = (uint)Player.WHIDs[WH][count];
                        *((uint*)(p + 20 + count * 20)) = uint.Parse(Splitter[0]);
                        *(p + 25 + count * 20) = byte.Parse(Splitter[4]);
                        *(p + 26 + count * 20) = byte.Parse(Splitter[5]);
                        *(p + 29 + count * 20) = byte.Parse(Splitter[1]);
                        *(p + 30 + count * 20) = byte.Parse(Splitter[2]);
                        *(p + 32 + count * 20) = byte.Parse(Splitter[3]);
                    }
                    count++;
                }
            }
            return Packet;
        }

        public byte[] OpenWarehouse(uint NPCID, uint Money)
        {
            ushort PacketType = 1009;
            byte[] Packet = new byte[20];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)NPCID;
                *((uint*)(p + 8)) = (uint)Money;
                *(p + 12) = (byte)(9 & 0xff);
            }

            return Packet;
        }

        public byte[] Disguise(uint CharId, uint Model, uint MaxHp, uint CurHp)
        {
            ushort PacketType = 0x3f9;
            byte[] Packet = new byte[60];
            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)CharId;
                *(p + 8) = 0x03;
                *(p + 12) = 0x0c;
                *((uint*)(p + 16)) = (uint)Model;
                *(p + 24) = 0x01;
                *((uint*)(p + 28)) = (uint)MaxHp;
                *((uint*)(p + 40)) = (uint)CurHp;
            }
            return Packet;
        }

        public byte[] CharSkillUse(Character Attacker, Character Attacked, uint DMG, ushort SkillId, byte SkillLevel)
        {
            ushort PacketType = 1105;
            byte[] Packet = new byte[32];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)Attacker.UID;
                *((ushort*)(p + 8)) = (ushort)Attacked.LocX;
                *((ushort*)(p + 10)) = (ushort)Attacked.LocY;
                *((ushort*)(p + 12)) = (ushort)SkillId;
                *((ushort*)(p + 14)) = (ushort)SkillLevel;
                *(p + 16) = 1;
                *((uint*)(p + 20)) = (uint)Attacked.UID;
                *((uint*)(p + 24)) = (uint)DMG;
            }
            return Packet;
        }

        public byte[] MobSkillUse(Monster Mob, Character Attacked, uint DMG, ushort SkillId, byte SkillLevel)
        {
            ushort PacketType = 1105;
            byte[] Packet = new byte[32];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)Mob.UID;
                *((ushort*)(p + 8)) = (ushort)Attacked.LocX;
                *((ushort*)(p + 10)) = (ushort)Attacked.LocY;
                *((ushort*)(p + 12)) = (ushort)SkillId;
                *((ushort*)(p + 14)) = (ushort)SkillLevel;
                *(p + 16) = 1;
                *((uint*)(p + 20)) = (uint)Attacked.UID;
                *((uint*)(p + 24)) = (uint)DMG;
            }
            return Packet;
        }

        public byte[] SkillUse(Character Charr, Hashtable Targets, Hashtable PTargets, Hashtable NPCTargets, short AimX, short AimY, short SkillId, byte SkillLvl, byte Switch, uint OneTarget, uint TargetDMG)
        {
            ushort PacketType = 1105;
            int Len = 32;
            if (Switch == 0)
                Len = 20 + Targets.Count * 12 + PTargets.Count * 12 + NPCTargets.Count * 12;

            byte[] Packet = new byte[Len];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;

                *((uint*)(p + 4)) = (uint)Charr.UID;

                if (Switch == 0 || Switch == 2)
                {
                    *((ushort*)(p + 8)) = (ushort)AimX;
                    *((ushort*)(p + 10)) = (ushort)AimY;
                }
                else if (Switch == 1)
                    *((uint*)(p + 8)) = (uint)OneTarget;

                *((ushort*)(p + 12)) = (ushort)SkillId;
                *((ushort*)(p + 14)) = (ushort)SkillLvl;

                if (Switch == 0)
                    *((uint*)(p + 16)) = (uint)(Targets.Count + PTargets.Count + NPCTargets.Count);
                else
                    *(p + 16) = 1;


                int Count = 0;

                if (Switch == 0)
                {
                    foreach (DictionaryEntry DE in Targets)
                    {
                        *((uint*)(p + +20 + Count)) = (uint)((Monster)DE.Key).UID;
                        *((uint*)(p + +24 + Count)) = (uint)(uint)DE.Value;

                        Count += 12;
                    }
                    foreach (DictionaryEntry DE in PTargets)
                    {
                        *((uint*)(p + +20 + Count)) = (uint)((Character)DE.Key).UID;
                        *((uint*)(p + +24 + Count)) = (uint)(uint)DE.Value;

                        Count += 12;
                    }
                    foreach (DictionaryEntry DE in NPCTargets)
                    {
                        *((uint*)(p + +20 + Count)) = (uint)((SingleNPC)DE.Key).UID;
                        *((uint*)(p + +24 + Count)) = (uint)(uint)DE.Value;

                        Count += 12;
                    }
                }
                else
                {
                    *((uint*)(p + 20)) = (uint)OneTarget;
                    *((uint*)(p + 24)) = (uint)TargetDMG;
                }
            }

            return Packet;
        }

        public byte[] ViewEquipAdd(uint ViewedCUID, uint ItemId, byte Plus, byte Bless, byte Enchant, byte Soc1, byte Soc2, byte Location, uint MaxDura, uint CurDura)
        {
            ushort PacketType = 1008;
            byte[] Packet = new byte[36];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)ViewedCUID;
                *((uint*)(p + 8)) = (uint)ItemId;
                if (ItemId == 1050002 || ItemId == 1050001 || ItemId == 1050000)
                {
                    *((ushort*)(p + 12)) = (ushort)CurDura;
                    *((ushort*)(p + 14)) = (ushort)MaxDura;
                }
                else
                {
                    *(p + 12) = (byte)((byte)Math.Abs(200 - CurDura) & 0xff);
                    *(p + 13) = (byte)((byte)((CurDura) / 2.56) & 0xff);
                    *(p + 14) = (byte)((byte)Math.Abs(200 - MaxDura) & 0xff);
                    *(p + 15) = (byte)((byte)((MaxDura) / 2.56) & 0xff);
                }
                *(p + 16) = 4;
                *(p + 18) = Location;
                *(p + 19) = Soc1;
                *(p + 24) = Soc1;
                *(p + 25) = Soc2;
                *(p + 28) = Plus;
                *(p + 29) = Bless;
                *(p + 30) = Enchant;
            }
            return Packet;
        }

        public byte[] ItemDrop(uint ItemUID, uint ItemId, uint X, uint Y)
        {
            ushort PacketType = 1101;
            byte[] Packet = new byte[20];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)ItemUID;
                if (Other.HaveQuality(ItemId))
                    ItemId = ItemHandler.RemoveQuality(ItemId);
                *((uint*)(p + 8)) = (uint)ItemId;
                *((ushort*)(p + 12)) = (ushort)X;
                *((ushort*)(p + 14)) = (ushort)Y;
                *(p + 16) = 1;
            }
            return Packet;
        }

        public byte[] ItemDropRemove(uint ItemUID)
        {
            ushort PacketType = 1101;
            byte[] Packet = new byte[20];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)ItemUID;
                *(p + 8) = 0x4d;
                *(p + 9) = 0xa2;
                *((ushort*)(p + 10)) = (ushort)Program.Rand.Next(1, 9);
                *((ushort*)(p + 12)) = (ushort)Program.Rand.Next(99, 153);
                *((ushort*)(p + 14)) = (ushort)Program.Rand.Next(208, 217);
                *(p + 16) = 2;
            }
            return Packet;
        }

        public byte[] Attack(uint UID, uint Target, ushort TargetX, ushort TargetY, byte AttackType, uint Damage)
        {
            ushort PacketType = 1022;
            byte[] Packet = new byte[28];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 8)) = (uint)UID;
                *((uint*)(p + 12)) = (uint)Target;
                *((ushort*)(p + 16)) = (ushort)TargetX;
                *((ushort*)(p + 18)) = (ushort)TargetY;
                *((uint*)(p + 20)) = (uint)AttackType;
                *((uint*)(p + 24)) = (uint)Damage;
            }
            return Packet;
        }

        public byte[] SpawnMob(Monster Mob)
        {
            ushort PacketType = 0x3f6;
            byte[] Packet = new byte[85 + Mob.Name.Length];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)Mob.UID;
                *((uint*)(p + 8)) = (uint)Mob.Look;
                *((ushort*)(p + 48)) = (ushort)Mob.CurHP;
                *((ushort*)(p + 50)) = (ushort)Mob.Level;
                *((ushort*)(p + 52)) = (ushort)Mob.LocX;
                *((ushort*)(p + 54)) = (ushort)Mob.LocY;

                *(p + 58) = Mob.Direction;
                *(p + 59) = 100;
                *(p + 80) = 1;
                *(p + 81) = (byte)Mob.Name.Length;

                for (int i = 0; i < Mob.Name.Length; i++)
                {
                    *(p + 82 + i) = Convert.ToByte(Mob.Name[i]);
                }
            }
            return Packet;
        }

        public byte[] ItemUsage(long ItemUID, int Position, int Packettype)
        {
            ushort PacketType = 1009;
            byte[] Packet = new byte[20];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)ItemUID;
                *((uint*)(p + 8)) = (uint)Position;
                *((uint*)(p + 12)) = (uint)Packettype;
            }
            return Packet;
        }

        public byte[] LearnSkill(short skill_id, byte lvl, uint skill_exp)
        {
            ushort PacketType = 1103;
            byte[] Packet = new byte[12];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)skill_exp;
                *((ushort*)(p + 8)) = (ushort)skill_id;
                *((ushort*)(p + 10)) = (ushort)lvl;
            }

            return Packet;
        }

        public byte[] NPCSay(string Text)
        {
            ushort PacketType = 2032;
            byte[] Packet = new byte[16 + Text.Length];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *(p + 10) = 0xff;
                *(p + 11) = 1;
                *(p + 12) = 1;
                *(p + 13) = (byte)Text.Length;
                for (int i = 0; i < Text.Length; i++)
                {
                    *(p + 14 + i) = Convert.ToByte(Text[i]);
                }
            }
            return Packet;
        }        

        public byte[] NPCLink(string Text, byte DialNr)
        {
            ushort PacketType = 2032;
            byte[] Packet = new byte[16 + Text.Length];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *(p + 10) = DialNr; 
                *(p + 11) = 2;
                *(p + 12) = 1;
                *(p + 13) = (byte)Text.Length;
                for (int i = 0; i < Text.Length; i++)
                {
                    *(p + 14 + i) = Convert.ToByte(Text[i]);
                }
            }
            return Packet;
        }
        public byte[] NPCLink2(string Text, byte DialNr)
        {
            ushort PacketType = 2032;
            byte[] Packet = new byte[16 + Text.Length];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *(p + 10) = DialNr;
                *(p + 11) = 3;
                *(p + 12) = 1;
                *(p + 13) = (byte)Text.Length;
                for (int i = 0; i < Text.Length; i++)
                {
                    *(p + 14 + i) = Convert.ToByte(Text[i]);
                }
            }
            return Packet;
        }
        public byte[] NPCSetFace(short Face)
        {
            ushort PacketType = 2032;
            byte[] Packet = new byte[16];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *(p + 4) = 10;
                *(p + 6) = 10;
                *((ushort*)(p + 8)) = (ushort)Face;
                *(p + 10) = 0xff;
                *(p + 11) = 4;
            }
            return Packet;
        }
        public byte[] NPCFinish()
        {
            ushort PacketType = 2032;
            byte[] Packet = new byte[16];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *(p + 10) = 0xff;
                *(p + 11) = 100;
            }

            return Packet;
        }
        public byte[] Prof(short Type, byte Lvl, uint Exp)
        {
            ushort PacketType = 0x401;
            byte[] Packet = new byte[16];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)Type;
                *((uint*)(p + 8)) = (uint)Lvl;
                *((uint*)(p + 12)) = (uint)Exp;
            }

            return Packet;
        }

        public byte[] SpawnNPC(SingleNPC NPC)
        {
            ushort PacketType = 2030;
            byte[] Packet = new byte[20];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)NPC.UID;
                *((ushort*)(p + 8)) = (ushort)NPC.LocX;
                *((ushort*)(p + 10)) = (ushort)NPC.LocY;
                *((ushort*)(p + 12)) = (ushort)NPC.Type;
                *((ushort*)(p + 14)) = (ushort)NPC.Flags;
                *(p + 16) = NPC.Direction;
            }

            return Packet;
        }

        public byte[] RemoveItem(long UID, byte pos, byte type)
        {
            ushort PacketType = 1009;
            byte[] Packet = new byte[20];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)UID;
                *((uint*)(p + 8)) = (uint)pos;
                *((uint*)(p + 12)) = (uint)type;
            }

            return Packet;
        }

        public byte[] AddItem(long UID, int itemid, byte Plus, byte Bless, byte Enchant, byte soc1, byte soc2, byte Location, int CurArrows, int MaxArrows)
        {
            ushort PacketType = 0x3f0;
            string IDE = itemid.ToString().Remove(2, itemid.ToString().Length - 2);

            byte[] Packet = new byte[36];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;

                *((uint*)(p + 4)) = (uint)UID;
                *((uint*)(p + 8)) = (uint)itemid;

                if (itemid == 1050002 || itemid == 1050001 || itemid == 1050000)
                {
                    *((ushort*)(p + 12)) = (ushort)CurArrows;
                    *((ushort*)(p + 14)) = (ushort)MaxArrows;
                }				
                else if (IDE == "72" || IDE == "10" || IDE == "79" || IDE == "78" || IDE == "72" || IDE == "71" || IDE == "70" && itemid != 1050002 && itemid != 1050001 && itemid != 1050000)
                { }
                else
                {
                    *(p + 12) = (byte)(Math.Abs(200 - CurArrows) & 0xff);
                    *(p + 13) = (byte)((byte)(CurArrows / 2.56) & 0xff);
                    *(p + 14) = (byte)(Math.Abs(200 - MaxArrows) & 0xff);
                    *(p + 15) = (byte)((byte)(MaxArrows / 2.56) & 0xff);
                }
                *(p + 16) = 1;
                *(p + 18) = Location;
                *(p + 19) = soc1;
                *(p + 24) = soc1;
                *(p + 25) = soc2;
                *(p + 28) = Plus;
                *(p + 29) = Bless;
                *(p + 30) = Enchant;
            }
            return Packet;
        }

        public byte[] SpawnSobNPC(SingleNPC NPC)
        {
            ushort PacketType = 1109;
            byte[] Packet = new byte[28];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)NPC.UID;
                *((uint*)(p + 8)) = (uint)NPC.MaxHP;
                *((uint*)(p + 12)) = (uint)NPC.CurHP;
                *((ushort*)(p + 16)) = (ushort)NPC.LocX;
                *((ushort*)(p + 18)) = (ushort)NPC.LocY;
                *((ushort*)(p + 20)) = (ushort)(NPC.Type);
                *((ushort*)(p + 22)) = (ushort)NPC.Flags;
                *(p + 24) = 17;
            }
            return Packet;
        }
        public byte[] SpawnSob(SingleNPC NPC)
        {
            ushort PacketType = 1109;
            byte[] Packet = new byte[28];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)NPC.UID;
                *((uint*)(p + 8)) = (uint)NPC.MaxHP;
                *((uint*)(p + 12)) = (uint)NPC.CurHP;
                *((ushort*)(p + 16)) = (ushort)NPC.LocX;
                *((ushort*)(p + 18)) = (ushort)NPC.LocY;
                *((ushort*)(p + 20)) = (ushort)NPC.Type;
                *((ushort*)(p + 22)) = (ushort)NPC.Flags;
                *(p + 24) = 21;
            }
            return Packet;
        }
        public byte[] SpawnSobNPCNamed(SingleNPC NPC)
        {
            string Pole = "COPS";
            ushort PacketType = 1109;
            byte[] Packet = new byte[32];
            if (World.PoleHolder != null)
                Packet = new byte[28 + World.PoleHolder.Name.Length];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)NPC.UID;
                *((uint*)(p + 8)) = (uint)NPC.MaxHP;
                *((uint*)(p + 12)) = (uint)NPC.CurHP;
                *((ushort*)(p + 16)) = (ushort)NPC.LocX;
                *((ushort*)(p + 18)) = (ushort)NPC.LocY;
                *((ushort*)(p + 20)) = (ushort)NPC.Type;
                *((ushort*)(p + 22)) = (ushort)NPC.Flags;
                *(p + 24) = 17;
                *(p + 26) = 1;
                if (World.PoleHolder != null)
                {
                    *(p + 27) = (byte)World.PoleHolder.Name.Length;
                    for (int i = 0; i < World.PoleHolder.Name.Length; i++)
                    {
                        *(p + 28 + i) = Convert.ToByte(World.PoleHolder.Name[i]);
                    }
                }
                else
                {
                    *(p + 27) = 4;
                    for (int i = 0; i < 4; i++)
                    {
                        *(p + 28 + i) = Convert.ToByte(Pole[i]);
                    }
                }
            }
            return Packet;
        }

        public byte[] StringGuild(long CharId, byte Type, string name, byte Count)
        {
            ushort PacketType = 1015;
            byte[] Packet = new byte[12 + name.Length];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)CharId;

                *(p + 8) = Type;
                *(p + 9) = (byte)Count;

                for (int i = 0; i < name.Length; i++)
                {
                    *(p + 10 + i) = Convert.ToByte(name[i]);
                }
            }

            return Packet;
        }

        public byte[] SpawnEntity(Character Player)
        {
            string[] equip;
            long HeadId = 0;
            long ArmorId = 0;
            long RightHandId = 0;
            long LeftHandId = 0;
            long GarmentId = 0;

            if (Player.Equips[1] != null)
            {
                equip = Player.Equips[1].Split('-');
                HeadId = Convert.ToInt64(equip[0]);
            }

            if (Player.Equips[3] != null)
            {
                equip = Player.Equips[3].Split('-');
                ArmorId = Convert.ToInt64(equip[0]);
            }

            if (Player.Equips[4] != null)
            {
                equip = Player.Equips[4].Split('-');
                RightHandId = Convert.ToInt64(equip[0]);
            }

            if (Player.Equips[5] != null)
            {
                equip = Player.Equips[5].Split('-');
                LeftHandId = Convert.ToInt64(equip[0]);
            }

            if (Player.Equips[9] != null)
            {
                equip = Player.Equips[9].Split('-');
                GarmentId = Convert.ToInt64(equip[0]);
            }

            long ToArmor;

            if (Player.Equips[9] != null)
                ToArmor = GarmentId;
            else
                ToArmor = ArmorId;

            uint Model;
            if (Player.Transform && Player.Alive)
                Model = Player.MobModel;
            else if (Player.Alive)
                Model = uint.Parse(Player.Avatar.ToString() + Player.Model.ToString());
            else
            {
                if (Player.Model == 1003 || Player.Model == 1004)
                    Model = uint.Parse(Player.Avatar.ToString() + "1098");
                else
                    Model = uint.Parse(Player.Avatar.ToString() + "1099");
            }

            ushort PacketType = 0x3f6;
            byte[] Packet = new byte[85 + Player.Name.Length];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)Player.UID;
                *((uint*)(p + 8)) = (uint)Model;
                *((ulong*)(p + 12)) = (ulong)Player.Flags.GetFlags();
                *((ushort*)(p + 20)) = (ushort)Player.GuildID;
                *(p + 23) = Player.GuildPosition;

                if (Player.Alive)
                {
                    *((uint*)(p + 28)) = (uint)HeadId;
                    *((uint*)(p + 32)) = (uint)ToArmor;
                    *((uint*)(p + 36)) = (uint)RightHandId;
                    *((uint*)(p + 40)) = (uint)LeftHandId;
                }
                *((ushort*)(p + 52)) = (ushort)Player.LocX;
                *((ushort*)(p + 54)) = (ushort)Player.LocY;
                *((ushort*)(p + 56)) = (ushort)Player.Hair;

                *(p + 58) = Player.Direction;
                *(p + 59) = Player.Action;
			    p[60] = (byte)Player.RBCount;
                *(p + 62) = (byte)Player.Level;
                *(p + 80) = 1;
                *(p + 81) = (byte)Player.Name.Length;
                for (int i = 0; i < Player.Name.Length; i++)
                {
                    *(p + 82 + i) = Convert.ToByte(Player.Name[i]);
                }
            }
            return Packet;
        }

        //Checked...
        private enum MAP_STATUS_FLAGS : uint
        {
            MAP_NORMAL = 0x0000,
            MAP_PKFIELD = 0x0001,
            MAP_CHGMAP_DISABLE = 0x0002, //magic call team member
            MAP_RECORD_DISABLE = 0x0004,
            MAP_PK_DISABLE = 0x0008,
            MAP_BOOTH_ENABLE = 0x0010,
            MAP_TEAM_DISABLE = 0x0020,
            MAP_TELEPORT_DISABLE = 0x0040, //chgmap by action
            MAP_SYN_MAP = 0x0080,
            MAP_PRISON_MAP = 0x0100,
            MAP_WING_DISABLE = 0x0200, //bowman fly disable
            MAP_FAMILY = 0x0400,
            MAP_MINEFIELD = 0x0800,
            MAP_PKGAME = 0x1000,
            MAP_NEVERWOUND = 0x2000,
            MAP_DEADISLAND = 0x4000,
        };

        public byte[] MapInfo(uint MapId, uint MiniMap, uint Flags)
        {
            ushort PacketType = 0x456;
            byte[] Packet = new byte[16];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)MapId;
                *((uint*)(p + 8)) = (uint)MiniMap;
                *((uint*)(p + 12)) = (uint)Flags;
            }
            return Packet;
        }

        public byte[] Weather(uint Type, uint Intensity, uint Direction, uint Color)
        {
            ushort PacketType = 0x3F8;
            byte[] Packet = new byte[20];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)Type;
                *((uint*)(p + 8)) = (uint)Intensity;
                *((uint*)(p + 12)) = (uint)Direction;
                *((uint*)(p + 16)) = (uint)Color;
            }
            return Packet;
        }

        public byte[] Walk(uint UniqId, byte Dir, bool Run)
        {
            ushort PacketType = 0x3ED;
            byte[] Packet = new byte[12];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)UniqId;
                *(p + 8) = (byte)Dir;
                *(p + 9) = Run ? (byte)0x01 : (byte)0x00;
                *((ushort*)(p + 10)) = (ushort)0x00;
            }
            return Packet;
        }

        public byte[] Vital(uint UniqId, uint Type, ulong Param)
        {
            ushort PacketType = 0x3F9;
            byte[] Packet = new byte[28];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)UniqId;
                *((uint*)(p + 8)) = (uint)0x01;
                *((uint*)(p + 12)) = (uint)Type;
                *((ulong*)(p + 16)) = (ulong)Param;
            }
            return Packet;
        }

        public byte[] Vital(uint UniqId, Dictionary<uint, ulong> Params)
        {
            ushort PacketType = 0x3F9;
            byte[] Packet = new byte[16 + (12 * Params.Count)];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)UniqId;
                *((uint*)(p + 8)) = (uint)Params.Count;

                uint Count = 0;
                foreach (KeyValuePair<uint, ulong> Param in Params)
                {
                    *((uint*)(p + 12 + Count)) = (uint)Param.Key;
                    *((ulong*)(p + 16 + Count)) = (ulong)Param.Value;
                    Count += 12;
                }
            }
            return Packet;
        }

        public byte[] String(uint UniqId, byte Type, string Param)
        {
            ushort PacketType = 0x3F7;
            byte[] Packet = new byte[13 + Param.Length];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)UniqId;
                *(p + 8) = Type;
                *(p + 9) = 1;
                *(p + 10) = (byte)Param.Length;
                for (byte i = 0; i < Param.Length; i++)
                    *(p + 11 + i) = (byte)Param[i];
            }
            return Packet;
        }

        public byte[] GeneralData(uint UniqId, uint Param, ushort Action)
        {
            ushort PacketType = 0x3F2;
            byte[] Packet = new byte[24];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)Environment.TickCount;
                *((uint*)(p + 8)) = (uint)UniqId;
                *((uint*)(p + 12)) = (uint)Param;
                *((ushort*)(p + 16)) = (ushort)0x00;
                *((ushort*)(p + 18)) = (ushort)0x00;
                *((ushort*)(p + 20)) = (ushort)0x00;
                *((ushort*)(p + 22)) = (ushort)Action;
            }
            return Packet;
        }

        public byte[] GeneralData(ConquerObject Entity, uint Param, ushort Action)
        {
            ushort PacketType = 0x3F2;
            byte[] Packet = new byte[24];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)Environment.TickCount;
                *((uint*)(p + 8)) = (uint)Entity.UID;
                *((uint*)(p + 12)) = (uint)Param;
                *((ushort*)(p + 16)) = (ushort)Entity.LocX;
                *((ushort*)(p + 18)) = (ushort)Entity.LocY;
                *((ushort*)(p + 20)) = (ushort)Entity.Direction;
                *((ushort*)(p + 22)) = (ushort)Action;
            }
            return Packet;
        }

        public byte[] CharacterInfo(Character Entity)
        {
            ushort PacketType = 0x3EE;
            byte[] Packet = new byte[70 + Entity.Name.Length + Entity.Spouse.Length];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)Entity.UID;
                *((uint*)(p + 8)) = (uint)(Entity.Avatar * 10000) + Entity.Model;
                *((ushort*)(p + 12)) = (ushort)Entity.Hair;
                *((uint*)(p + 14)) = (uint)Entity.Silvers;
                *((uint*)(p + 18)) = (uint)Entity.CPs;
                *((ulong*)(p + 22)) = (ulong)Entity.Exp;
                *((uint*)(p + 30)) = (uint)0x00;
                *((uint*)(p + 34)) = (uint)0x00;
                *((uint*)(p + 38)) = (uint)0x00;
                *((uint*)(p + 42)) = (uint)Entity.VPs;
                *((ushort*)(p + 46)) = (ushort)Entity.Str;
                *((ushort*)(p + 48)) = (ushort)Entity.Agi;
                *((ushort*)(p + 50)) = (ushort)Entity.Vit;
                *((ushort*)(p + 52)) = (ushort)Entity.Spi;
                *((ushort*)(p + 54)) = (ushort)Entity.StatP;
                *((ushort*)(p + 56)) = (ushort)Entity.CurHP;
                *((ushort*)(p + 58)) = (ushort)Entity.CurMP;
                *((ushort*)(p + 60)) = (ushort)Entity.PKPoints;
                *(p + 62) = (byte)Entity.Level;
                *(p + 63) = (byte)Entity.Job;
                *(p + 64) = (byte)0x00;
                *(p + 65) = (byte)Entity.RBCount;
                *(p + 66) = (byte)0x01;
                *(p + 67) = (byte)0x02;
                *(p + 68) = (byte)Entity.Name.Length;
                for (byte i = 0; i < Entity.Name.Length; i++)
                    *(p + 69 + i) = (byte)Entity.Name[i];
                *(p + 69 + Entity.Name.Length) = (byte)Entity.Spouse.Length;
                for (byte i = 0; i < Entity.Spouse.Length; i++)
                    *(p + 70 + Entity.Name.Length + i) = (byte)Entity.Spouse[i];
            }
            return Packet;
        }

        public byte[] SendMsg(uint MessageId, string Sender, string Receiver, string Message, short Channel)
        {
            ushort PacketType = 0x3EC;
            byte[] Packet = new byte[29 + Sender.Length + Receiver.Length + Message.Length];

            fixed (byte* p = Packet)
            {
                *((ushort*)p) = (ushort)Packet.Length;
                *((ushort*)(p + 2)) = (ushort)PacketType;
                *((uint*)(p + 4)) = (uint)0xFFFFFF;
                *((uint*)(p + 8)) = (uint)Channel;
                *((uint*)(p + 12)) = (uint)MessageId;
                *((uint*)(p + 16)) = (uint)0x00;
                *((uint*)(p + 20)) = (uint)0x00;
                *(p + 24) = (byte)4;
                *(p + 25) = (byte)Sender.Length;
                for (byte i = 0; i < Sender.Length; i++)
                    *(p + 26 + i) = (byte)Sender[i];
                *(p + 26 + Sender.Length) = (byte)Receiver.Length;
                for (byte i = 0; i < Receiver.Length; i++)
                    *(p + 27 + Sender.Length + i) = (byte)Receiver[i];
                *(p + 27 + Sender.Length + Receiver.Length) = (byte)0x00;
                *(p + 28 + Sender.Length + Receiver.Length) = (byte)Message.Length;
                for (byte i = 0; i < Message.Length; i++)
                    *(p + 29 + Sender.Length + Receiver.Length + i) = (byte)Message[i];
            }
            return Packet;
        }
    }
}
