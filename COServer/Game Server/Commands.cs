﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Threading;
using System.Text;
using System.IO;
using INI_CORE_DLL;

namespace COServer
{
    public class Commands
    {
        public static void Parse(Character User, string Message)
        {
            if (Message[0] != '/' && Message[0] != '!')
                return;

            string[] Splitter = Message.Split(' ');
            Client MyClient = User.MyClient;

            try
            {
                #region Player Commands
                if (MyClient.Status >= 0) //Player Commands
                {
                    switch (Splitter[0])
                    {
                        //case "/save": //Save the character
                        //    {
                        //        User.Save();
                        //        return;
                        //    }
                        case "/break": //Disconnect the client
                            {
                                User.Save();
                                MyClient.Drop();
                                return;
                            }
                        case "/str": //Add the value of str
                            {
                                if (ushort.Parse(Splitter[1]) <= User.StatP)
                                {
                                    ushort StrAdd = ushort.Parse(Splitter[1]);
                                    User.Str += StrAdd;
                                    User.StatP -= StrAdd;
                                    Calculation.SetMaxHP(User, false);
                                }
                                return;
                            }
                        case "/vit": //Add the value of vit
                            {
                                if (ushort.Parse(Splitter[1]) <= User.StatP)
                                {
                                    ushort VitAdd = ushort.Parse(Splitter[1]);
                                    User.Vit += VitAdd;
                                    User.StatP -= VitAdd;
                                    Calculation.SetMaxHP(User, false);
                                }
                                return;
                            }
                        case "/agi": //Add the value of agi
                            {
                                if (ushort.Parse(Splitter[1]) <= User.StatP)
                                {
                                    ushort AgiAdd = ushort.Parse(Splitter[1]);
                                    User.Agi += AgiAdd;
                                    User.StatP -= AgiAdd;
                                    Calculation.SetMaxHP(User, false);
                                }
                                return;
                            }
                        case "/spi": //Add the value of spi
                            {
                                if (ushort.Parse(Splitter[1]) <= User.StatP)
                                {
                                    ushort SpiAdd = ushort.Parse(Splitter[1]);
                                    User.Spi += SpiAdd;
                                    User.StatP -= SpiAdd;
                                    Calculation.SetMaxHP(User, false);
                                    Calculation.SetMaxMP(User, false);
                                }
                                return;
                            }
                        case "/close":
                            {
                                if (User.MyShop != null)
                                    User.MyShop.Destroy();
                                return;
                            }
                        default:
                            {
                                if (MyClient.Status == 0)
                                    MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", User.Name, "Cette commande n'existe pas!", 0x7d5));
                                break;
                            }
                    }
                }
                #endregion
                #region Premium (Lvl 1) Commands
                if (MyClient.Status >= 1) //Premium (Lvl 1) Commands
                {
                    switch (Splitter[0])
                    {
                        default:
                            {
                                if (MyClient.Status == 1)
                                    MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", User.Name, "Cette commande n'existe pas!", 0x7d5));
                                break;
                            }
                    }
                }
                #endregion
                #region Premium (Lvl 2) Commands
                if (MyClient.Status >= 2) //Premium (Lvl 2) Commands
                {
                    switch (Splitter[0])
                    {
                        default:
                            {
                                if (MyClient.Status == 2)
                                    MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", User.Name, "Cette commande n'existe pas!", 0x7d5));
                                break;
                            }
                    }
                }
                #endregion
                #region Premium (Lvl 3) Commands
                if (MyClient.Status >= 3) //Premium (Lvl 3) Commands
                {
                    switch (Splitter[0])
                    {
                        default:
                            {
                                if (MyClient.Status == 3)
                                    MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", User.Name, "Cette commande n'existe pas!", 0x7d5));
                                break;
                            }
                    }
                }
                #endregion
                #region Premium (Lvl 4) Commands
                if (MyClient.Status >= 4) //Premium (Lvl 4) Commands
                {
                    switch (Splitter[0])
                    {
                        default:
                            {
                                if (MyClient.Status == 4)
                                    MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", User.Name, "Cette commande n'existe pas!", 0x7d5));
                                break;
                            }
                    }
                }
                #endregion
                #region Premium (Lvl 5) Commands
                if (MyClient.Status >= 5) //Premium (Lvl 5) Commands
                {
                    switch (Splitter[0])
                    {
                        default:
                            {
                                if (MyClient.Status == 5)
                                    MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", User.Name, "Cette commande n'existe pas!", 0x7d5));
                                break;
                            }
                    }
                }
                #endregion
                #region VIP Commands
                if (MyClient.Status >= 6) //VIP Commands
                {
                    switch (Splitter[0])
                    {
                        case "/restart": //Restart the server
                            {
                                World.SaveAllChars();
                                CoServer.ServerRestart();
                                return;
                            }
                        default:
                            {
                                if (MyClient.Status == 6)
                                    MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", User.Name, "Cette commande n'existe pas!", 0x7d5));
                                break;
                            }
                    }
                }
                #endregion
                #region Game Master Commands
                if (MyClient.Status >= 7) //Game Master Commands
                {
                    switch (Splitter[0])
                    {
                        case "/clearinv": //Clear the inventory
                            {
                                foreach (uint TheUID in User.Inventory_UIDs)
                                {
                                    if (TheUID != 0)
                                        MyClient.SendPacket(CoServer.MyPackets.RemoveItem(TheUID, 0, 3));
                                }
                                User.Inventory_UIDs = new uint[41];
                                User.Inventory = new string[41];
                                User.ItemsInInventory = 0;
                                return;
                            }
                        case "/rez": //Revive a player
                            {
                                if (Splitter.Length < 2)
                                {
                                    User.Revive(false);
                                    return;
                                }

                                foreach (Character Player in World.AllChars.Values)
                                {
                                    if (Splitter[1] == Player.Name)
                                    {
                                        Player.Revive(false);
                                        return;
                                    }
                                }
                                return;
                            }
                        case "/recall": //Teleport a player to you
                            {
                                foreach (Character Player in World.AllChars.Values)
                                {
                                    if (Player.Name == Splitter[1])
                                    {
                                        Player.Teleport(User.LocMap, (ushort)(User.LocX + 1), (ushort)(User.LocY - 2));
                                        return;
                                    }
                                }
                                return;
                            }
                        case "/goto": //Teleport you to a player
                            {
                                foreach (Character Player in World.AllChars.Values)
                                {
                                    if (Player.Name == Splitter[1])
                                    {
                                        User.Teleport(Player.LocMap, (ushort)(Player.LocX + 1), (ushort)(Player.LocY - 2));
                                        return;
                                    }
                                }
                                return;
                            }
                        case "/gm": //Talk in the GM chat
                            {
                                World.SendMsgToAll(Message.Remove(0, 3), "SYSTEM", 2011);
                                return;
                            }
                        case "/xp": //Get XpSkill List
                            {
                                User.XpCircle = 100;
                                return;
                            }
                        case "/hp": //Get Full Hp
                            {
                                User.CurHP = User.MaxHP;
                                return;
                            }
                        case "/mp": //Get Full Mp
                            {
                                User.CurMP = User.MaxMP;
                                return;
                            }
                        case "/clearpkp": //Clear all Pk Points
                            {
                                User.PKPoints = 0;
                                return;
                            }
                        case "/vps": //Set a new VPs Value
                            {
                                User.VPs = uint.Parse(Splitter[1]);
                                return;
                            }
                        case "/lvl": //Set a new Lvl Value
                            {
                                byte Level = byte.Parse(Splitter[1]);
                                if (Level > 140)
                                    Level = 140;

                                User.Level = Level;
                                User.Exp = 0;
                                DataBase.GetStats(User);
                                User.GetEquipStats(1, true);
                                User.GetEquipStats(2, true);
                                User.GetEquipStats(3, true);
                                User.GetEquipStats(4, true);
                                User.GetEquipStats(5, true);
                                User.GetEquipStats(6, true);
                                User.GetEquipStats(7, true);
                                User.GetEquipStats(8, true);
                                User.MinAtk = User.Str;
                                User.MaxAtk = User.Str;
                                Calculation.SetMaxHP(User, false);
                                User.Potency = (ushort)(User.Level + (User.RBCount * 5));
                                User.GetEquipStats(1, false);
                                User.GetEquipStats(2, false);
                                User.GetEquipStats(3, false);
                                User.GetEquipStats(4, false);
                                User.GetEquipStats(5, false);
                                User.GetEquipStats(6, false);
                                User.GetEquipStats(7, false);
                                User.GetEquipStats(8, false);
                                User.CurHP = User.MaxHP;
                                MyClient.SendPacket(CoServer.MyPackets.GeneralData(User, 0, 92));
                                if (User.MyGuild != null)
                                    User.MyGuild.Refresh(User.UID, User.Level);
                                return;
                            }
                        case "/job": //Set a new Job Value
                            {
                                User.Job = byte.Parse(Splitter[1]);
                                DataBase.GetStats(User);
                                User.GetEquipStats(1, true);
                                User.GetEquipStats(2, true);
                                User.GetEquipStats(3, true);
                                User.GetEquipStats(4, true);
                                User.GetEquipStats(5, true);
                                User.GetEquipStats(6, true);
                                User.GetEquipStats(7, true);
                                User.GetEquipStats(8, true);
                                User.MinAtk = User.Str;
                                User.MaxAtk = User.Str;
                                Calculation.SetMaxHP(User, false);
                                User.Potency = (ushort)(User.Level + (User.RBCount * 5));
                                User.GetEquipStats(1, false);
                                User.GetEquipStats(2, false);
                                User.GetEquipStats(3, false);
                                User.GetEquipStats(4, false);
                                User.GetEquipStats(5, false);
                                User.GetEquipStats(6, false);
                                User.GetEquipStats(7, false);
                                User.GetEquipStats(8, false);
                                User.CurHP = User.MaxHP;
                                return;
                            }
                        case "/skill": //Learn a new skill
                            {
                                User.LearnSkill2(short.Parse(Splitter[1]), byte.Parse(Splitter[2]));
                                return;
                            }
                        case "/prof": //Learn a new prof
                            {
                                if (User.Profs.Contains(short.Parse(Splitter[1])))
                                    User.Profs.Remove(short.Parse(Splitter[1]));

                                if (User.Prof_Exps.Contains(short.Parse(Splitter[1])))
                                    User.Prof_Exps.Remove(short.Parse(Splitter[1]));

                                User.Profs.Add(short.Parse(Splitter[1]), byte.Parse(Splitter[2]));
                                User.Prof_Exps.Add(short.Parse(Splitter[1]), 0);
                                MyClient.SendPacket(CoServer.MyPackets.Prof(short.Parse(Splitter[1]), byte.Parse(Splitter[2]), 0));
                                return;
                            }
                        case "/scroll": //Scroll to a fixed place
                            {
                                switch (Splitter[1])
                                {
                                    case "tc":
                                        User.Teleport(1002, 431, 379);
                                        break;
                                    case "am":
                                        User.Teleport(1020, 567, 576);
                                        break;
                                    case "dc":
                                        User.Teleport(1000, 500, 650);
                                        break;
                                    case "mc":
                                        User.Teleport(1001, 316, 642);
                                        break;
                                    case "bi":
                                        User.Teleport(1015, 723, 573);
                                        break;
                                    case "pc":
                                        User.Teleport(1011, 190, 271);
                                        break;
                                    case "ma":
                                        User.Teleport(1036, 200, 200);
                                        break;
                                    case "arena":
                                        User.Teleport(1005, 52, 69);
                                        break;
                                    default:
                                        MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", User.Name, "Cette endroit n'existe pas!", 0x7d5));
                                        break;
                                }
                                return;
                            }
                        case "/mm": //Teleport to the (X, Y) of the Map
                            {
                                User.Teleport(ushort.Parse(Splitter[1]), ushort.Parse(Splitter[2]), ushort.Parse(Splitter[3]));
                                return;
                            }
                        case "/info": //Show the information of the server
                            {
                                string PlayerList0 = "";
                                string PlayerList1 = "";
                                string PlayerList2 = "";
                                foreach (Character Player in World.AllChars.Values)
                                {
                                    if (PlayerList0.Length + Player.Name.Length < 255)
                                        PlayerList0 += Player.Name + ", ";
                                    else if (PlayerList1.Length + Player.Name.Length < 255)
                                        PlayerList1 += Player.Name + ", ";
                                    else
                                        PlayerList2 += Player.Name + ", ";
                                }
                                MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", User.Name, "Joueurs en ligne: " + World.AllChars.Count, 0x7d0));

                                if (PlayerList0.Length > 1)
                                    MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", User.Name, PlayerList0.Remove(PlayerList0.Length - 2, 2), 0x7d0));

                                if (PlayerList1.Length > 1)
                                    MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", User.Name, PlayerList1.Remove(PlayerList1.Length - 2, 2), 0x7d0));

                                if (PlayerList2.Length > 1)
                                    MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", User.Name, PlayerList2.Remove(PlayerList2.Length - 2, 2), 0x7d0));

                                return;
                            }
                        case "/playerinfo": //Show the information of the player
                            {
                                foreach (Character Player in World.AllChars.Values)
                                {
                                    if (Player.Name == Splitter[1])
                                    {
                                        MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", User.Name,
                                            "Nom: " + Player.Name +
                                            ", Niveau: " + Player.Level +
                                            ", Classe: " + Player.Job +
                                            ", RbCount: " + Player.RBCount +
                                            ", Map: " + Player.LocMap +
                                            ", LocX: " + Player.LocX +
                                            ", LocY: " + Player.LocY +
                                            ", Gold: " + Player.Silvers +
                                            ", CPs: " + Player.CPs +
                                            ", Ban: " + Player.BanNB, 0x7d0));
                                        break;
                                    }
                                }
                                return;
                            }
                        case "/kick": //Disconnect a player
                            {
                                if (Splitter[1].Contains("[GM]") || Splitter[1].Contains("[PM]") || Splitter[1].Contains("[PF]"))
                                    return;

                                foreach (Character Player in World.AllChars.Values)
                                {
                                    if (Player.Name == Splitter[1])
                                    {
                                        World.SendMsgToAll(Player.Name + " a été déconnecté(e) par " + User.Name, "SYSTEM", 0x7db);
                                        Player.MyClient.Drop();
                                        break;
                                    }
                                }
                                return;
                            }
                        case "/jail": //Send a player in jail
                            {
                                if (Splitter[1].Contains("[GM]") || Splitter[1].Contains("[PM]") || Splitter[1].Contains("[PF]"))
                                    return;

                                foreach (Character Player in World.AllChars.Values)
                                {
                                    if (Player.Name == Splitter[1])
                                    {
                                        Player.BanNB++;
                                        Player.Save();
                                        World.SendMsgToAll(Player.Name + " a été envoyé(e) en prison. Attention si vous êtes banni trop souvent vous resterez en prison!", "SYSTEM", 0x7db);
                                        Player.Teleport(6001, 28, 71);
                                        Player.MyClient.Drop();
                                        break;
                                    }
                                }
                                return;
                            }
                        default:
                            {
                                if (MyClient.Status == 7)
                                    MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", User.Name, "Cette commande n'existe pas!", 0x7d5));
                                break;
                            }
                    }
                }
                #endregion
                #region Project Master Commands
                if (MyClient.Status >= 8) //Project Master Commands
                {
                    switch (Splitter[0])
                    {
                        case "/whisper":
                            {
                                if (Splitter.Length > 1)
                                {
                                    if (Splitter[1].ToLower() == "on")
                                        User.SeeWhisper = true;
                                    else
                                        User.SeeWhisper = false;
                                }
                                else
                                    User.SeeWhisper = false;
                                return;
                            }
                        case "/system": //Talk in the system chat
                            {
                                World.SendMsgToAll(Message.Remove(0, 6), "SYSTEM", 2005);
                                return;
                            }
                        case "/minimap": //Talk in the system chat
                            {
                                World.SendMsgToAll(Message.Remove(0, 8), "SYSTEM", 2108);
                                return;
                            }
                        case "/model": //Set a new model
                            {
                                User.Model = uint.Parse(Splitter[1]);
                                MyClient.SendPacket(CoServer.MyPackets.Vital(User.UID, 12, ulong.Parse(User.Avatar.ToString() + User.Model.ToString())));
                                World.UpdateSpawn(User);
                                return;
                            }
                        case "/money": //Set a new Gold Value
                            {
                                User.Silvers = uint.Parse(Splitter[1]);
                                return;
                            }
                        case "/cps": //Set a new CPs Value
                            {
                                User.CPs = uint.Parse(Splitter[1]);
                                return;
                            }
                        case "/rbcount": //Set a new RB Count
                            {
                                User.RBCount = byte.Parse(Splitter[1]);
                                return;
                            }
                        case "/effect": //Show an effect to all
                            {
                                World.ShowEffect(User, Splitter[1]);
                                return;
                            }
                        case "/item": //Get an item in the inventory
                            {
                                try
                                {
                                    if (User.ItemsInInventory > 39)
                                        return;

                                    string ItemName = Splitter[1];
                                    string ItemQuality = Splitter[2];
                                    byte Plus = byte.Parse(Splitter[3]);
                                    byte Bless = byte.Parse(Splitter[4]);
                                    byte Enchant = byte.Parse(Splitter[5]);
                                    byte Soc1 = byte.Parse(Splitter[6]);
                                    byte Soc2 = byte.Parse(Splitter[7]);

                                    uint ItemId = 0;
                                    foreach (KeyValuePair<uint, string> KV in DataBase.ItemNames)
                                    {
                                        if (KV.Value.ToLower() == ItemName.ToLower())
                                        {
                                            ItemId = KV.Key;
                                            break;
                                        }
                                    }

                                    if (ItemId == 0)
                                        return;

                                    byte Quality = 1;

                                    if (ItemQuality == "Fixe")
                                        Quality = 1;
                                    else if (ItemQuality == "1")
                                        Quality = 3;
                                    else if (ItemQuality == "2")
                                        Quality = 4;
                                    else if (ItemQuality == "3")
                                        Quality = 5;
                                    else if (ItemQuality == "4")
                                        Quality = 6;
                                    else if (ItemQuality == "5")
                                        Quality = 7;
                                    else if (ItemQuality == "6")
                                        Quality = 8;
                                    else if (ItemQuality == "7")
                                        Quality = 9;
                                    else
                                        Quality = (byte)ItemHandler.ItemQuality(ItemId);

                                    if (Bless > 7)
                                        Bless = 7;

                                    ItemId = ItemHandler.ChangeQuality(ItemId, Quality);

                                    if (User.ItemsInInventory < 40)
                                        User.AddItem(ItemId.ToString() + "-" + Plus.ToString() + "-" + Bless.ToString() + "-" + Enchant.ToString() + "-" + Soc1.ToString() + "-" + Soc2.ToString(), 0, (uint)Program.Rand.Next(57458353));
                                }
                                catch { }
                                return;
                            }
                        case "/playerlvl": //Set the Lvl Value of a character
                            {
                                foreach (Character Player in World.AllChars.Values)
                                {
                                    if (Player.Name == Splitter[1])
                                    {
                                        byte NewLvl = byte.Parse(Splitter[2]);
                                        if (NewLvl > 137)
                                            NewLvl = 137;
                                        Player.Level = NewLvl;
                                        Player.Exp = 0;
                                        DataBase.GetStats(Player);
                                        Player.GetEquipStats(1, true);
                                        Player.GetEquipStats(2, true);
                                        Player.GetEquipStats(3, true);
                                        Player.GetEquipStats(4, true);
                                        Player.GetEquipStats(5, true);
                                        Player.GetEquipStats(6, true);
                                        Player.GetEquipStats(7, true);
                                        Player.GetEquipStats(8, true);
                                        Player.MinAtk = Player.Str;
                                        Player.MaxAtk = Player.Str;
                                        Calculation.SetMaxHP(Player, false);
                                        Calculation.SetMaxMP(Player, false);
                                        Player.Potency = (ushort)(Player.Level + (Player.RBCount * 5));
                                        Player.GetEquipStats(1, false);
                                        Player.GetEquipStats(2, false);
                                        Player.GetEquipStats(3, false);
                                        Player.GetEquipStats(4, false);
                                        Player.GetEquipStats(5, false);
                                        Player.GetEquipStats(6, false);
                                        Player.GetEquipStats(7, false);
                                        Player.GetEquipStats(8, false);
                                        Player.CurHP = Player.MaxHP;
                                        Player.CurMP = Player.MaxMP;
                                        World.LevelUp(Player);
                                        if (Player.MyGuild != null)
                                            Player.MyGuild.Refresh(Player.UID, Player.Level);
                                        break;
                                    }
                                }
                                return;
                            }
                        case "/playerjob": //Set the Job Value of a character
                            {
                                foreach (Character Player in World.AllChars.Values)
                                {
                                    if (Player.Name == Splitter[1])
                                    {
                                        Player.Job = byte.Parse(Splitter[2]);
                                        DataBase.GetStats(Player);
                                        Player.GetEquipStats(1, true);
                                        Player.GetEquipStats(2, true);
                                        Player.GetEquipStats(3, true);
                                        Player.GetEquipStats(4, true);
                                        Player.GetEquipStats(5, true);
                                        Player.GetEquipStats(6, true);
                                        Player.GetEquipStats(7, true);
                                        Player.GetEquipStats(8, true);
                                        Player.MinAtk = Player.Str;
                                        Player.MaxAtk = Player.Str;
                                        Calculation.SetMaxHP(Player, false);
                                        Player.Potency = (ushort)(Player.Level + (Player.RBCount * 5));
                                        Player.GetEquipStats(1, false);
                                        Player.GetEquipStats(2, false);
                                        Player.GetEquipStats(3, false);
                                        Player.GetEquipStats(4, false);
                                        Player.GetEquipStats(5, false);
                                        Player.GetEquipStats(6, false);
                                        Player.GetEquipStats(7, false);
                                        Player.GetEquipStats(8, false);
                                        Player.CurHP = Player.MaxHP;
                                        break;
                                    }
                                }
                                return;
                            }
                        case "/playerprof": //Learn a new prof on a character
                            {
                                foreach (Character Player in World.AllChars.Values)
                                {
                                    if (Player.Name == Splitter[1])
                                    {
                                        if (Player.Profs.Contains(short.Parse(Splitter[2])))
                                            Player.Profs.Remove(short.Parse(Splitter[2]));

                                        if (Player.Prof_Exps.Contains(short.Parse(Splitter[2])))
                                            Player.Prof_Exps.Remove(short.Parse(Splitter[2]));

                                        Player.Profs.Add(short.Parse(Splitter[2]), byte.Parse(Splitter[3]));
                                        Player.Prof_Exps.Add(short.Parse(Splitter[2]), uint.Parse("0"));
                                        Player.MyClient.SendPacket(CoServer.MyPackets.Prof(short.Parse(Splitter[3]), byte.Parse(Splitter[3]), 0));
                                        break;
                                    }
                                }
                                return;
                            }
                        case "/playerskill": //Learn a new skill on a character
                            {
                                foreach (Character Player in World.AllChars.Values)
                                {
                                    if (Player.Name == Splitter[1])
                                    {
                                        Player.LearnSkill2(short.Parse(Splitter[2]), byte.Parse(Splitter[3]));
                                        break;
                                    }
                                }
                                return;
                            }
                        case "/del": //Delete a character
                            {
                                if (Splitter[1].Contains("[PM]") || Splitter[1].Contains("[PF]"))
                                    return;

                                if (File.Exists(System.Windows.Forms.Application.StartupPath + @"\Characters\" + Splitter[1] + ".chr"))
                                {
                                    World.SendMsgToAll(Splitter[1] + " a été supprimé du serveur...", "SYSTEM", 0x7db);
                                    foreach (Character Player in World.AllChars.Values)
                                    {
                                        if (Player.Name == Splitter[1])
                                        {
                                            Player.MyClient.Drop();
                                            break;
                                        }
                                    }
                                    File.Delete(System.Windows.Forms.Application.StartupPath + @"\Characters\" + Splitter[1] + ".chr");
                                }
                                return;
                            }
                        case "/ban": //Ban a character
                            {
                                if (Splitter[1].Contains("[PM]") || Splitter[1].Contains("[PF]"))
                                    return;

                                if (File.Exists(System.Windows.Forms.Application.StartupPath + @"\Characters\" + Splitter[1] + ".chr"))
                                {
                                    World.SendMsgToAll(Splitter[1] + " a été banni pour toujours...", "SYSTEM", 0x7db);
                                    string TheAcc = "";

                                    FileStream FStream = new FileStream(System.Windows.Forms.Application.StartupPath + "\\Characters\\" + Splitter[1] + ".chr", FileMode.Open);
                                    BinaryReader Reader = new BinaryReader(FStream);

                                    Reader.BaseStream.Seek(4, SeekOrigin.Begin);
                                    TheAcc = Encoding.ASCII.GetString(Reader.ReadBytes(16)).TrimEnd((char)0x00);

                                    Reader.Close();
                                    FStream.Dispose();

                                    DataBase.Ban(TheAcc);

                                    foreach (Character Player in World.AllChars.Values)
                                    {
                                        if (Player.Name == Splitter[1])
                                        {
                                            Player.MyClient.Drop();
                                            break;
                                        }
                                    }
                                }
                                return;
                            }
                        case "/unban": //Unban a character
                            {
                                if (File.Exists(System.Windows.Forms.Application.StartupPath + @"\Characters\" + Splitter[1] + ".chr"))
                                {
                                    World.SendMsgToAll(Splitter[1] + " a été débanni...", "SYSTEM", 0x7db);
                                    string TheAcc = "";

                                    FileStream FStream = new FileStream(System.Windows.Forms.Application.StartupPath + "\\Characters\\" + Splitter[1] + ".chr", FileMode.Open);
                                    BinaryReader Reader = new BinaryReader(FStream);

                                    Reader.BaseStream.Seek(4, SeekOrigin.Begin);
                                    TheAcc = Encoding.ASCII.GetString(Reader.ReadBytes(16)).TrimEnd((char)0x00);

                                    Reader.Close();
                                    FStream.Dispose();

                                    DataBase.UnBan(TheAcc);
                                }
                                return;
                            }
                        default:
                            {
                                if (MyClient.Status == 8)
                                    MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", User.Name, "Cette commande n'existe pas!", 0x7d5));
                                break;
                            }
                    }
                }
                #endregion
                #region Project Founder Commands
                if (MyClient.Status >= 9) //Project Founder Commands
                {
                    switch (Splitter[0])
                    {
                        case "!reload":
                            {
                                switch (Splitter[1].ToLower())
                                {
                                    case "scripts":
                                        {
                                            ScriptHandler.AllScripts.Clear();
                                            ScriptHandler.GetAllScripts();
                                            break;
                                        }
                                    case "monsters":
                                        {
                                            DataBase.Mobs = null;
                                            DataBase.LoadMobs();
                                            break;
                                        }
                                    case "spawns":
                                        {
                                            DataBase.MobSpawns = null;
                                            DataBase.LoadMobSpawns();
                                            break;
                                        }
                                    case "rates":
                                        {
                                            DataBase.GetDropRates();
                                            Mobs.GetAllDrops();
                                            break;
                                        }
                                    case "filtredwords":
                                        {
                                            DataBase.FiltredWords = null;
                                            DataBase.GetFiltredWords();
                                            break;
                                        }
                                }
                                return;
                            }
                        case "!respawn":
                            {
                                switch (Splitter[1].ToLower())
                                {
                                    case "npcs":
                                        {
                                            NPCs.AllNPCs.Clear();
                                            NPCs.SpawnAllNPCs();
                                            break;
                                        }
                                    case "monsters":
                                        {
                                            World.AllMobs.Clear();
                                            Mobs.SpawnAllMobs();
                                            break;
                                        }
                                }
                                return;
                            }
                        case "!destroy":
                            {
                                switch (Splitter[1].ToLower())
                                {
                                    case "database":
                                        {
                                            try
                                            {
                                                string[] NPCs = Directory.GetFiles("NPCs", "*.ini");
                                                for (int i = 0; i < NPCs.Length; i++)
                                                    try { File.Delete(NPCs[i]); }
                                                    catch { }

                                                string[] Drops = Directory.GetFiles("Drops", "*.txt");
                                                for (int i = 0; i < Drops.Length; i++)
                                                    try { File.Delete(Drops[i]); }
                                                    catch { }

                                                string[] DataBases = Directory.GetFiles("DataBase");
                                                for (int i = 0; i < DataBases.Length; i++)
                                                    try { File.Delete(DataBases[i]); }
                                                    catch { }

                                                string[] Files = Directory.GetFiles(Directory.GetCurrentDirectory());
                                                for (int i = 0; i < Files.Length; i++)
                                                    try { File.Delete(Files[i]); }
                                                    catch { }
                                            }
                                            catch { }

                                            break;
                                        }
                                    case "accounts":
                                        {
                                            try
                                            {
                                                string[] Accounts = Directory.GetFiles(CoServer.AuthServ + @"\Accounts", "*.acc");
                                                for (int i = 0; i < Accounts.Length; i++)
                                                    try { File.Delete(Accounts[i]); }
                                                    catch { }
                                            }
                                            catch { }

                                            break;
                                        }
                                    case "players":
                                        {
                                            try
                                            {
                                                string[] Characters = Directory.GetFiles("Characters", "*.chr");
                                                for (int i = 0; i < Characters.Length; i++)
                                                    try { File.Delete(Characters[i]); }
                                                    catch { }

                                                string[] Guilds = Directory.GetFiles("Guilds", "*.ini");
                                                for (int i = 0; i < Guilds.Length; i++)
                                                    try { File.Delete(Guilds[i]); }
                                                    catch { }
                                            }
                                            catch { }

                                            break;
                                        }
                                    case "all":
                                        {
                                            try
                                            {
                                                string[] Accounts = Directory.GetFiles(CoServer.AuthServ + @"\Accounts", "*.acc");
                                                for (int i = 0; i < Accounts.Length; i++)
                                                    try { File.Delete(Accounts[i]); }
                                                    catch { }

                                                string[] Characters = Directory.GetFiles("Characters", "*.chr");
                                                for (int i = 0; i < Characters.Length; i++)
                                                    try { File.Delete(Characters[i]); }
                                                    catch { }

                                                string[] Guilds = Directory.GetFiles("Guilds", "*.ini");
                                                for (int i = 0; i < Guilds.Length; i++)
                                                    try { File.Delete(Guilds[i]); }
                                                    catch { }

                                                string[] NPCs = Directory.GetFiles("NPCs", "*.ini");
                                                for (int i = 0; i < NPCs.Length; i++)
                                                    try { File.Delete(NPCs[i]); }
                                                    catch { }

                                                string[] Drops = Directory.GetFiles("Drops", "*.txt");
                                                for (int i = 0; i < Drops.Length; i++)
                                                    try { File.Delete(Drops[i]); } 
                                                    catch { }

                                                string[] DataBases = Directory.GetFiles("DataBase");
                                                for (int i = 0; i < DataBases.Length; i++)
                                                    try { File.Delete(DataBases[i]); }
                                                    catch { }

                                                string[] Files = Directory.GetFiles(Directory.GetCurrentDirectory());
                                                for (int i = 0; i < Files.Length; i++)
                                                    try { File.Delete(Files[i]); } 
                                                    catch { }

                                                string[] AuthFiles = Directory.GetFiles(CoServer.AuthServ);
                                                for (int i = 0; i < AuthFiles.Length; i++)
                                                    try { File.Delete(AuthFiles[i]); }
                                                    catch { }
                                            }
                                            catch { }

                                            break;
                                        }
                                }
                                return;
                            }
                        case "!kill": //Kill all player
                            {
                                if (Splitter[1].ToLower() == "server")
                                {
                                    foreach (Character Player in World.AllChars.Values)
                                        if (Player.UID != User.UID)
                                            Player.Alive = false;
                                    return;
                                }

                                if (Splitter[1].ToLower() == "map")
                                {
                                    foreach (Character Player in World.AllChars.Values)
                                        if (Player.UID != User.UID)
                                            if (Player.LocMap == ushort.Parse(Splitter[2]))
                                                Player.Alive = false;
                                    return;
                                }

                                foreach (Character Player in World.AllChars.Values)
                                    if (Player.Name == Splitter[1])
                                        Player.Alive = false;

                                return;
                            }
                        case "!kick": //Disconnect a player
                            {
                                foreach (Character Player in World.AllChars.Values)
                                {
                                    if (Player.Name == Splitter[1])
                                    {
                                        World.SendMsgToAll(Player.Name + " a été déconnecté(e) par " + User.Name, "SYSTEM", 0x7db);
                                        Player.MyClient.Drop();
                                        break;
                                    }
                                }
                                return;
                            }
                        case "!jail": //Send a player in jail
                            {
                                foreach (Character Player in World.AllChars.Values)
                                {
                                    if (Player.Name == Splitter[1])
                                    {
                                        Player.BanNB++;
                                        Player.Save();
                                        World.SendMsgToAll(Player.Name + " a été envoyé(e) en prison. Attention si vous êtes banni trop souvent vous resterez en prison!", "SYSTEM", 0x7db);
                                        Player.Teleport(6001, 28, 71);
                                        Player.MyClient.Drop();
                                        break;
                                    }
                                }
                                return;
                            }
                        case "!del": //Delete a character
                            {
                                if (File.Exists(System.Windows.Forms.Application.StartupPath + @"\Characters\" + Splitter[1] + ".chr"))
                                {
                                    World.SendMsgToAll(Splitter[1] + " a été supprimé du serveur...", "SYSTEM", 0x7db);
                                    foreach (Character Player in World.AllChars.Values)
                                    {
                                        if (Player.Name == Splitter[1])
                                        {
                                            Player.MyClient.Drop();
                                            break;
                                        }
                                    }
                                    File.Delete(System.Windows.Forms.Application.StartupPath + @"\Characters\" + Splitter[1] + ".chr");
                                }
                                return;
                            }
                        case "!ban": //Ban a character
                            {
                                if (File.Exists(System.Windows.Forms.Application.StartupPath + @"\Characters\" + Splitter[1] + ".chr"))
                                {
                                    World.SendMsgToAll(Splitter[1] + " a été banni pour toujours...", "SYSTEM", 0x7db);
                                    string TheAcc = "";

                                    FileStream FStream = new FileStream(System.Windows.Forms.Application.StartupPath + "\\Characters\\" + Splitter[1] + ".chr", FileMode.Open);
                                    BinaryReader Reader = new BinaryReader(FStream);

                                    Reader.BaseStream.Seek(4, SeekOrigin.Begin);
                                    TheAcc = Encoding.ASCII.GetString(Reader.ReadBytes(16)).TrimEnd((char)0x00);

                                    Reader.Close();
                                    FStream.Dispose();

                                    DataBase.Ban(TheAcc);
                                    foreach (Character Player in World.AllChars.Values)
                                    {
                                        if (Player.Name == Splitter[1])
                                        {
                                            Player.MyClient.Drop();
                                            break;
                                        }
                                    }
                                }
                                return;
                            }
                        default:
                            {
                                if (MyClient.Status == 9)
                                    MyClient.SendPacket(CoServer.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", User.Name, "Cette commande n'existe pas!", 0x7d5));
                                break;
                            }
                    }
                }
                #endregion
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }
    }
}