using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Timers;
using System.Text;
using System.IO;
using INI_CORE_DLL;
using KSocket.IPv4;
using KSocket;

namespace COServer
{
    public class CoServer
    {
        //Server
        public static ServerSocket AuthServer;
        public static string AuthIP = "127.0.0.1";
        public static ushort AuthPort = 9958;
        public static bool AuthOpen = false;

        public static System.Timers.Timer BanTimer;

        public static void Run()
        {
            try
            {
                if (File.Exists("Config.ini"))
                {
                    Ini Config = new Ini(System.Windows.Forms.Application.StartupPath + @"\Config.ini");
                    CoServer.AuthIP = Config.ReadValue("AuthServ", "IpAddress");
                    CoServer.AuthPort = Config.ReadUInt16("AuthServ", "Port");
                    CoServer.AuthOpen = Config.ReadBoolean("AuthServ", "IsOpen");

                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine("Configuring Socket System...");
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine("Server IP :  " + CoServer.AuthIP);
                    Console.WriteLine("Server Port : " + CoServer.AuthPort);
                    Console.WriteLine("Server Open :  " + CoServer.AuthOpen);
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Successfully Configured Socket System!");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("*******************************************************");

                    //Load DataBase
                    DataBase.GetBannedIPs();

                    //Start the Server
                    AuthServer = new ServerSocket();
                    AuthServer.Port = AuthPort;
                    AuthServer.OnReceivePacket += new SocketReceiveEvent(AuthPacketHandler);
                    AuthServer.OnClientDisconnect += new SocketDisconnectEvent(AuthDisconnectHandler);
                    new Thread(AuthServer.Listen).Start();

                    //Start Timers
                    BanTimer = new System.Timers.Timer();
                    BanTimer.Interval = 1000 * 60 * 30;
                    BanTimer.Elapsed += new ElapsedEventHandler(GetBannedIPs);
                    BanTimer.Start();

                    DoStuff();
                }
                else
                    Environment.Exit(0);
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        private static void GetBannedIPs(object sender, ElapsedEventArgs e)
        {
            BanTimer.Stop();
            DataBase.GetBannedIPs();
            BanTimer.Start();
        }

        private static void DoStuff()
        {
            while (true)
            {
                string Command = Console.ReadLine();
                switch (Command)
                {
                    case ".close":
                        {
                            Program.Debuguer.Flush();
                            Program.Debuguer.Close();
                            Environment.Exit(0);
                            break;
                        }
                }
            }
        }

        private static void AuthDisconnectHandler(object Sender, ClientSocket Socket, string Reasons)
        {
            try
            {
                if (Socket != null && !Socket.Disconnected)
                    Socket.Disconnect();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        private static void AuthPacketHandler(object Sender, ClientSocket Socket, byte[] Data)
        {
            try
            {
                CoEncryption Crypto = new CoEncryption();

                string Ip = (Socket.WinSock.RemoteEndPoint as IPEndPoint).Address.ToString();
                int Port = (Socket.WinSock.RemoteEndPoint as IPEndPoint).Port;

                //Decrypt the Packet
                Crypto.Decrypt(Data);

                //Packet Length = 52 & Packet Type = 1051
                if (Data[0x00] == 0x34 && Data[0x01] == 0x00 && Data[0x02] == 0x1b && Data[0x03] == 0x04)
                {
                    string TheAcc = "";
                    string ThePass = "";
                    string TheSvr = "";

                    //Account
                    TheAcc = Encoding.ASCII.GetString(Data, 0x04, 0x10).Trim((char)0x00);

                    //Password
                    byte[] Bytes = new byte[16];
                    for (sbyte i = 0; i < Bytes.Length; i++)
                        Bytes[i] = Data[0x14 + i];
                    ThePass = new PwdEncryption().Decrypt(Bytes);

                    System.Security.Cryptography.SHA256 Sha256 = System.Security.Cryptography.SHA256.Create();
                    byte[] Pass = Sha256.ComputeHash(Encoding.ASCII.GetBytes(ThePass));
                    ThePass = "";
                    for (sbyte i = 0; i < Pass.Length; i++)
                    {
                        string Hex = Convert.ToString(Pass[i], 16);
                        if (Hex.Length < 2)
                            Hex = "0" + Hex;
                        ThePass += Hex;
                    }

                    //Server
                    TheSvr = Encoding.ASCII.GetString(Data, 0x24, 0x10).Trim((char)0x00);

                    //Server is closed || The Ip is banned
                    if (!AuthOpen || DataBase.IPBanned(Ip))
                    {
                        Socket.Disconnect();
                        return;
                    }

                    //The account exist and match
                    if (DataBase.Authenticate(TheAcc, ThePass))
                    {
                        if (File.Exists(System.Windows.Forms.Application.StartupPath + @"\Servers\\" + TheSvr + ".svr"))
                        {
                            SvrInfo Svr = new SvrInfo(System.Windows.Forms.Application.StartupPath + @"\Servers\\" + TheSvr + ".svr");

                            string Info = "Connection of %Ip%:%Port% with account: '%Acc%' on the server: '%Svr%'.";
                            Info = Info.Replace("%Ip%", Ip);
                            Info = Info.Replace("%Port%", Port.ToString());
                            Info = Info.Replace("%Acc%", TheAcc);
                            Info = Info.Replace("%Svr%", Svr.Name);
                            Program.WriteLine(Info);

                            //Generate the Keys
                            uint AccountId = Program.GenerateID(TheAcc);
                            uint Token = Program.LastToken;

                            //Set the Account Token
                            DataBase.ChangeAccInfo(TheAcc, Token);

                            //Send the answer packet
                            byte[] Buffer = new _0x41f(AccountId, Token, Svr.IpAddress, Svr.Port).Serialize();
                            Crypto.Encrypt(Buffer);
                            Socket.WinSock.Send(Buffer);
                            Socket.Disconnect();
                        }
                        else
                            Socket.Disconnect();
                    }
                    else
                    {
                        //Send the wrong password packet
                        byte[] Buffer = new _0x41f(0x100000000, "�ʺ���������").Serialize();
                        Crypto.Encrypt(Buffer);
                        Socket.WinSock.Send(Buffer);
                        Socket.Disconnect();
                    }
                }
                else if (Data[0x02] == 0x11 && Data[0x03] == 0x27) //Register
                {
                    new Network.MsgRegister().Process(Data, Socket);
                }
                else if (Data[0x02] == 0x12 && Data[0x03] == 0x27) //Acc Support
                {
                    new Network.MsgInteract().Process(Data, Socket);
                }
                else
                    Socket.Disconnect();
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }
    }
}