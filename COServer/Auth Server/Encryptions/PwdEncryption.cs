﻿using System;
using System.Globalization;

namespace COServer
{
    public class PwdEncryption
    {
        private uint[] m_Key = new uint[30];

        //Not original. From my CORC5 release!
        public unsafe PwdEncryption()
        {
            Byte[] BufKey = new Byte[] { 0x3C, 0xDC, 0xFE, 0xE8, 0xC4, 0x54, 0xD6, 0x7E, 0x16, 0xA6, 0xF8, 0x1A, 0xE8, 0xD0, 0x38, 0xBE };

            fixed (Byte* pBufKey = BufKey)
            {
                for (Int32 z = 0; z < 4; z++)
                    m_Key[z] = ((UInt32*)pBufKey)[z];
            }

            m_Key[4] = 0xB7E15163;
            Int32 i, j, k;
            for (i = 1; i < 26; i++)
                m_Key[i + 4] = m_Key[i - 1 + 4] - 0x9E3779B9;

            UInt32 x, y;
            i = j = 0;
            x = y = 0;
            for (k = 0; k < 3 * Math.Max(4, 26); k++)
            {
                m_Key[i + 4] = rotl((m_Key[i + 4] + x + y), 3);
                x = m_Key[i + 4];
                i = (i + 1) % 26;
                m_Key[j] = rotl((m_Key[j] + x + y), (x + y));
                y = m_Key[j];
                j = (j + 1) % 4;
            }
        }

        private UInt32 rotl(UInt32 Value, UInt32 Count)
        {
            Count %= 32;

            UInt32 High = Value >> (32 - (Int32)Count);
            return (Value << (Int32)Count) | High;
        }
        //End

        ~PwdEncryption()
        {
            m_Key = null;
        }

        private uint RightRotate(uint Value, uint offset)
        {
            uint tmp1, tmp2;
            offset &= 0x1f;
            tmp1 = Value << (int)(32 - offset);
            tmp2 = Value >> (int)offset;
            tmp1 |= tmp2;
            return tmp1;
        }

        public string Decrypt(byte[] Password)
        {
            //Set the 4 uint for the pSeeds
            uint[] pSeeds = new uint[4];

            //Get the uint from the password (each 4 bytes is a uint)
            for (sbyte i = 0; i < pSeeds.Length; i++)
                pSeeds[i] = (uint)((Password[i * 4 + 3] << 24) + (Password[i * 4 + 2] << 16) + (Password[i * 4 + 1] << 8) + Password[i * 4]);

            //Set the 16 bytes for the password
            byte[] plain = new byte[16];

            for (sbyte j = 1; j >= 0; j--)
            {
                uint tmp1 = pSeeds[j * 2 + 1];
                uint tmp2 = pSeeds[j * 2];

                for (sbyte i = 11; i >= 0; i--)
                {
                    tmp1 = RightRotate(tmp1 - m_Key[i * 2 + 7], tmp2) ^ tmp2;
                    tmp2 = RightRotate(tmp2 - m_Key[i * 2 + 6], tmp1) ^ tmp1;
                }

                tmp1 -= m_Key[5];
                tmp2 -= m_Key[4];

                plain[j * 8 + 0] = (byte)(tmp2 & 0xff);
                plain[j * 8 + 1] = (byte)((tmp2 & 0xff00) >> 8);
                plain[j * 8 + 2] = (byte)((tmp2 & 0xff0000) >> 16);
                plain[j * 8 + 3] = (byte)((tmp2 & 0xff000000) >> 24);

                plain[j * 8 + 4] = (byte)(tmp1 & 0xff);
                plain[j * 8 + 5] = (byte)((tmp1 & 0xff00) >> 8);
                plain[j * 8 + 6] = (byte)((tmp1 & 0xff0000) >> 16);
                plain[j * 8 + 7] = (byte)((tmp1 & 0xff000000) >> 24);
            }

            string Decrypted = "";
            for (sbyte i = 0; i < plain.Length; i++)
                Decrypted += (char)plain[i];

            return TrimEnd(Decrypted);
        }

        public string TrimEnd(string ThePass)
        {
            byte Index = 0;
            for (sbyte i = 0; i < ThePass.Length; i++)
                if ((byte)ThePass[i] == 0x00)
                {
                    Index = (byte)i;
                    break;
                }

            return ThePass.Remove(Index);
        }
    }
}
