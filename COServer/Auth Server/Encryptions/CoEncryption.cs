using System;

namespace COServer
{
    //Conquer Online Auth Encryption [5017-]
    public class CoEncryption
    {
        internal class CryptCounter
        {
            ushort m_Counter = 0;

            public byte Key2 { get { return (byte)(m_Counter >> 8); } }
            public byte Key1 { get { return (byte)(m_Counter & 0xFF); } }

            public void Increment() { m_Counter++; }
        }

        private CryptCounter _decryptCounter;
        private CryptCounter _encryptCounter;
        private byte[] _cryptKey1 = new byte[0x100];
        private byte[] _cryptKey2 = new byte[0x100];

        public unsafe CoEncryption()
        {
            _decryptCounter = new CryptCounter();
            _encryptCounter = new CryptCounter();

            //Not original! From my COSAC release!
            Int32 P = 0x13FA0F9D;
            Int32 G = 0x6D5C7962;

            Byte* pBufPKey = (Byte*)&P;
            Byte* pBufGKey = (Byte*)&G;

            for (Int16 i = 0; i < 0x100; i++)
            {
                _cryptKey1[i + 0] = pBufPKey[0];
                _cryptKey2[i] = pBufGKey[0];
                pBufPKey[0] = (Byte)((pBufPKey[1] + (Byte)(pBufPKey[0] * pBufPKey[2])) * pBufPKey[0] + pBufPKey[3]);
                pBufGKey[0] = (Byte)((pBufGKey[1] - (Byte)(pBufGKey[0] * pBufGKey[2])) * pBufGKey[0] + pBufGKey[3]);
            }
            //End
        }

        ~CoEncryption()
        {
            _decryptCounter = null;
            _encryptCounter = null;
            _cryptKey1 = null;
            _cryptKey2 = null;
        }

        public byte[] Encrypt(byte[] buffer)
        {
            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] ^= (byte)0x7D;
                buffer[i] = (byte)(buffer[i] >> 4 | buffer[i] << 4);
                buffer[i] ^= (byte)(_cryptKey1[_encryptCounter.Key1] ^ _cryptKey2[_encryptCounter.Key2]);
                _encryptCounter.Increment();
            }
            return buffer;
        }

        public byte[] Decrypt(byte[] buffer)
        {
            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] ^= (byte)0x7D;
                buffer[i] = (byte)(buffer[i] >> 4 | buffer[i] << 4);
                buffer[i] ^= (byte)(_cryptKey2[_decryptCounter.Key2] ^ _cryptKey1[_decryptCounter.Key1]);
                _decryptCounter.Increment();
            }
            return buffer;
        }
    }
}
