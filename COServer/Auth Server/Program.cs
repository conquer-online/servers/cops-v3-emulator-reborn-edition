using System;
using System.Security.Cryptography;
using System.Windows.Forms;
using System.Threading;
using System.Text;
using System.IO;
using INI_CORE_DLL;
using FNV_HASH_DLL;

namespace COServer
{
    public class Program
    {
        [MTAThread]
        static void Main()
        {
            Console.Title = "COPS v3 - Reborn Edition : Auth Server";
            WriteHead();

            Ini Config = new Ini(System.Windows.Forms.Application.StartupPath + @"\Config.ini");
            CWriter = new Ini(System.Windows.Forms.Application.StartupPath + @"\Config.ini");
            m_LastToken = Config.ReadUInt32("AuthServ", "Token");

            if (!Directory.Exists("Debug"))
                Directory.CreateDirectory("Debug");

            string File = "Auth-" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + ".log";
            Debuguer = new StreamWriter(Application.StartupPath + @"\Debug\" + File, true);
            Debuguer.AutoFlush = true;

            StartDay = DateTime.Now.Day;

            new Thread(CheckTime).Start();
            new Thread(CoServer.Run).Start();
            Console.Read();
        }

        public static StreamWriter Debuguer = null;
        private static Ini CWriter = null;
        private static uint m_LastToken = 0;
        private static int StartDay = 0;

        public static uint LastToken { get { m_LastToken++; CWriter.WriteObject("AuthServ", "Token", m_LastToken); return m_LastToken; } }

        /// <summary>
        /// Generate the UniqId of a specified account.
        /// </summary>
        public static uint GenerateID(string Account) { return BitConverter.ToUInt32(FNV.GetHash(Account, 32), 0); }

        /// <summary>
        /// Write the header of the console. (ASCII Picture)
        /// </summary>
        private static void WriteHead()
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine(@"                  _                 _ _        _____                           ");
            Console.WriteLine(@"                 | |               (_) |      |_   _|                          ");
            Console.WriteLine(@"                 | |     ___   __ _ _| | __     | | _ __   ___                 ");
            Console.WriteLine(@"                 | |    / _ \ / _` | | |/ /     | || '_ \ / __|                ");
            Console.WriteLine(@"                 | |___| (_) | (_| | |   < _   _| || | | | (__                 ");
            Console.WriteLine(@"                 \_____/\___/ \__, |_|_|\_(_)  \___/_| |_|\___|                ");
            Console.WriteLine(@"                               __/ |                                           ");
            Console.WriteLine(@"                              |___/                                            ");
            Console.WriteLine(@"                                                                               ");
            Console.WriteLine(@"                          COPS v3 - Auth Server 3.0.7                          ");
            Console.WriteLine(@"                            Copyright (C) 2008-2010                            ");
            Console.WriteLine(@"                                                                               ");
        }

        private static void CheckTime()
        {
            while (true)
            {
                if (DateTime.Now.Day != StartDay)
                {
                    lock (Debuguer)
                    {
                        string File = "Auth-" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + ".log";
                        Debuguer = new StreamWriter(Application.StartupPath + @"\Debug\" + File, true);
                        Debuguer.AutoFlush = true;

                        StartDay = DateTime.Now.Day;
                    }
                }
                Thread.Sleep(60 * 1000);
            }
        }

        /// <summary>
        /// Write the text in the console and in the debug file.
        /// </summary>
        public static void WriteLine(string Text)
        {
            Debuguer.WriteLine(Text);
            Console.WriteLine(Text);
        }

        /// <summary>
        /// Write the object in the console and in the debug file.
        /// </summary>
        public static void WriteLine(object Object)
        {
            Debuguer.WriteLine(Object.ToString());
            Console.WriteLine(Object);
        }

        /// <summary>
        /// Transform the array of bytes in hexadecimal and convert the value in ANSI.
        /// </summary>
        public static object Dump(byte[] Bytes)
        {
            string Hex = "";
            foreach (byte b in Bytes)
                Hex = Hex + b.ToString("X2") + " ";
            string Out = "";
            while (Hex.Length != 0)
            {
                int SubLength = 0;
                if (Hex.Length >= 48)
                    SubLength = 48;
                else
                    SubLength = Hex.Length;
                string SubString = Hex.Substring(0, SubLength);
                int Remove = SubString.Length;
                SubString = SubString.PadRight(60, ' ') + StrHexToAnsi(SubString);
                Hex = Hex.Remove(0, Remove);
                Out = Out + SubString + "\r\n";
            }
            return Out;
        }

        private static string StrHexToAnsi(string StrHex)
        {
            string[] Data = StrHex.Split(new char[] { ' ' });
            string Ansi = "";
            foreach (string tmpHex in Data)
            {
                if (tmpHex != "")
                {
                    byte ByteData = byte.Parse(tmpHex, System.Globalization.NumberStyles.HexNumber);
                    if ((ByteData >= 32) & (ByteData <= 126))
                        Ansi = Ansi + ((char)(ByteData)).ToString();
                    else
                        Ansi = Ansi + ".";
                }
            }
            return Ansi;
        }
    }
}
