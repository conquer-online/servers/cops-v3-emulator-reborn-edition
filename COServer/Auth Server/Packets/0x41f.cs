﻿using System;

namespace COServer
{
    public class _0x41f
    {
        //private 
        private Byte[] dwData;
        private Boolean IsError;

        //Structure
        private UInt16 dwLength = 0;
        private UInt16 dwType = 0;

        //Forward to GameServer
        private UInt32 dwAccountId = 0;
        private UInt32 dwToken = 0;
        private String dwIpAddress = "";
        private UInt32 dwPort = 0;

        //Error...
        private UInt64 dwErrorId = 0;
        private String dwError = "";
        private UInt32 dwZeroFill = 0;

        public _0x41f(UInt32 AccountId, UInt32 Token, String IpAddress, UInt32 Port)
        {
            dwLength = 0x20;
            dwType = 0x41f;
            dwAccountId = AccountId;
            dwToken = Token;
            dwIpAddress = IpAddress;
            dwPort = Port;
            IsError = false;
        }

        public _0x41f(UInt64 ErrorId, String Error)
        {
            dwLength = 0x20;
            dwType = 0x41f;
            dwErrorId = ErrorId;
            dwError = Error;
            dwZeroFill = 0x00;
            IsError = true;
        }

        public byte[] Serialize()
        {
            dwData = new byte[dwLength];
            PacketWriter Writer = new PacketWriter(dwData);

            Writer.WriteUInt16(dwLength, 0);
            Writer.WriteUInt16(dwType, 2);
            if (!IsError)
            {
                Writer.WriteUInt32(dwAccountId, 4);
                Writer.WriteUInt32(dwToken, 8);
                Writer.WriteString(dwIpAddress, false, 12);
                Writer.WriteUInt32(dwPort, 28);
            }
            else
            {
                Writer.WriteUInt64(dwErrorId, 4);
                Writer.WriteString(dwError, false, 12);
                Writer.WriteUInt32(dwZeroFill, 28);
            }

            return Writer.Flush();
        }
    }
}
