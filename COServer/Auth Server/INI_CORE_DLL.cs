using System;
using System.Runtime.InteropServices;
using System.Text;

namespace INI_CORE_DLL
{
    public class Ini
    {
        [DllImport("kernel32")]
        public static extern int WritePrivateProfileString(string Section, string Key, string String, string File);

        [DllImport("INI_CORE_DLL")]
        private static extern bool GetString(string File, string Section, string Key, StringBuilder BufString, int BufSize);

        [DllImport("INI_CORE_DLL")]
        private static extern sbyte GetSByte(string File, string Section, string Key);

        [DllImport("INI_CORE_DLL")]
        private static extern byte GetByte(string File, string Section, string Key);

        [DllImport("INI_CORE_DLL")]
        private static extern short GetInt16(string File, string Section, string Key);

        [DllImport("INI_CORE_DLL")]
        private static extern ushort GetUInt16(string File, string Section, string Key);

        [DllImport("INI_CORE_DLL")]
        private static extern int GetInt32(string File, string Section, string Key);

        [DllImport("INI_CORE_DLL")]
        private static extern uint GetUInt32(string File, string Section, string Key);

        [DllImport("INI_CORE_DLL")]
        private static extern double GetDouble(string File, string Section, string Key);

        [DllImport("INI_CORE_DLL")]
        private static extern float GetFloat(string File, string Section, string Key);

        private string m_File;

        public Ini(string Path)
        {
            this.m_File = Path;
        }

        //Check if the Key exist
        public bool KeyExist(string Section, string Key)
        {
            try
            {
                const int BufferSize = 32;
                StringBuilder Value = new StringBuilder(BufferSize);
                GetString(m_File, Section, Key, Value, BufferSize);

                if (Value.ToString() != null && Value.ToString() != "")
                    return true;

                return false;
            }
            catch { return false; }
        }

        //Write the key of a section
        public int WriteString(string Section, string Key, string String)
        {
            return WritePrivateProfileString(Section, Key, String, m_File);
        }

        //Write the key of a section
        public void WriteObject(string Section, string Key, object Value)
        {
            WritePrivateProfileString(Section, Key, Value.ToString(), m_File);
        }

        //Jump a line in the file
        public void JumpLine()
        {
            try
            {
                System.IO.StreamWriter SWriter = new System.IO.StreamWriter(m_File, true);
                SWriter.WriteLine("");
                SWriter.Close();
            }
            catch { throw new Exception("INI_CORE_DLL::JumpLine() -> Can't make a jump in the file!"); }
        }

        //Read the sbyte of a key
        public sbyte ReadSByte(string Section, string Key)
        {
            return GetSByte(m_File, Section, Key);
        }

        //Read the byte of a key
        public byte ReadByte(string Section, string Key)
        {
            return GetByte(m_File, Section, Key);
        }

        //Read the short of a key
        public short ReadInt16(string Section, string Key)
        {
            return GetInt16(m_File, Section, Key);
        }

        //Read the ushort of a key
        public ushort ReadUInt16(string Section, string Key)
        {
            return GetUInt16(m_File, Section, Key);
        }

        //Read the int of a key
        public int ReadInt32(string Section, string Key)
        {
            return GetInt32(m_File, Section, Key);
        }

        //Read the uint of a key
        public uint ReadUInt32(string Section, string Key)
        {
            return GetUInt32(m_File, Section, Key);
        }

        //Read the long of a key
        public long ReadInt64(string Section, string Key)
        {
            try
            {
                const int BufferSize = 8192;
                StringBuilder Value = new StringBuilder(BufferSize);
                GetString(m_File, Section, Key, Value, BufferSize);
                return long.Parse(Value.ToString());
            }
            catch { return 0; }
        }

        //Read the ulong of a key
        public ulong ReadUInt64(string Section, string Key)
        {
            try
            {
                const int BufferSize = 8192;
                StringBuilder Value = new StringBuilder(BufferSize);
                GetString(m_File, Section, Key, Value, BufferSize);
                return ulong.Parse(Value.ToString());
            }
            catch { return 0; }
        }

        //Read the double of a key
        public double ReadDouble(string Section, string Key)
        {
            return GetDouble(m_File, Section, Key);
        }

        //Read the float of a key
        public double ReadFloat(string Section, string Key)
        {
            return GetFloat(m_File, Section, Key);
        }

        //Read the boolean of a key
        public bool ReadBoolean(string Section, string Key)
        {
            try
            {
                const int BufferSize = 8192;
                StringBuilder Value = new StringBuilder(BufferSize);
                GetString(m_File, Section, Key, Value, BufferSize);
                return bool.Parse(Value.ToString());
            }
            catch { return false; }
        }

        //Read the value of a key
        public string ReadValue(string Section, string Key)
        {
            const int BufferSize = 8192;
            StringBuilder Value = new StringBuilder(BufferSize);
            GetString(m_File, Section, Key, Value, BufferSize);
            return Value.ToString();
        }
    }
}
