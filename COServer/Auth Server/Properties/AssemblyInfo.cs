using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Auth Server")]
[assembly: AssemblyDescription("COPS v3 - Reborn Edition : Auth Server")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Logik. Inc")]
[assembly: AssemblyProduct("Auth Server.exe")]
[assembly: AssemblyCopyright("Copyright © Logik. Inc 2008-2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("6b026d0e-516f-4d63-8928-55e1925a138c")]

[assembly: AssemblyVersion("3.0.7.446")]
[assembly: AssemblyFileVersion("3.0.7.446")]
