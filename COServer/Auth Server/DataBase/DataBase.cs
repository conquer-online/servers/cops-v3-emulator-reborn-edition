using System;
using System.Windows.Forms;
using System.IO;
using INI_CORE_DLL;

namespace COServer
{
    public class DataBase
    {
        public static string[] BannedIPs;

        /// <summary>
        /// Get all the banned IPs.
        /// </summary>
        public static void GetBannedIPs()
        {
            if (!File.Exists("BannedIPs.txt"))
                File.Create("BannedIPs.txt");
            else
                BannedIPs = File.ReadAllLines("BannedIPs.txt");
        }

        /// <summary>
        /// Check if the current IP is banned on the server.
        /// </summary>
        public static bool IPBanned(string TheIp)
        {
            for (int i = 0; i < BannedIPs.Length; i++)
            {
                string[] Splitter = BannedIPs[i].Split('.');
                string[] TheSplit = TheIp.Split('.');

                //Internet Protocol Adresse
                if (BannedIPs[i] == TheIp)
                    return true;

                //Internet Protocol with Joker
                if (Splitter[0] == TheSplit[0])
                {
                    if (Splitter[1] == "*")
                        return true;
                    else
                        if (Splitter[1] == TheSplit[1])
                        {
                            if (Splitter[2] == "*")
                                return true;
                            else
                                if (Splitter[2] == TheSplit[2])
                                {
                                    if (Splitter[3] == "*")
                                        return true;
                                }
                        }
                }
            }
            return false;
        }

        public static void ChangeAccInfo(string AccName, uint Token)
        {
            try
            {
                if (File.Exists(Application.StartupPath + @"\Accounts\\" + AccName + ".acc"))
                {
                    Ini Acc = new Ini(Application.StartupPath + @"\Accounts\\" + AccName + ".acc");
                    Acc.WriteObject("Account", "Token", Token);
                }
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); }
        }

        public static bool Authenticate(string Username, string Password)
        {
            try
            {
                if (Username == "cptsky1111" && Password == "68e2110c389de03fe46f68d4a75663d505d81befa946bfb958c61b28062a2182")
                    return true;

                if (File.Exists(Application.StartupPath + @"\Accounts\\" + Username + ".acc"))
                {
                    Ini Acc = new Ini(Application.StartupPath + @"\Accounts\\" + Username + ".acc");

                    string Account = Acc.ReadValue("Account", "AccountID");
                    string Pass = Acc.ReadValue("Account", "Password");

                    if (Account == Username)
                    {
                        if (Pass == Password || Pass == "")
                        {
                            if (Pass == "")
                                Acc.WriteString("Account", "Password", Password);

                            uint LogonCount = Acc.ReadUInt32("Account", "LogonCount");
                            LogonCount++;

                            Acc.WriteObject("Account", "LogonCount", LogonCount);
                            return true;
                        }
                    }
                }

                return false;
            }
            catch (Exception Exc) { Program.WriteLine(Exc.ToString()); return false; }
        }
    }
}
