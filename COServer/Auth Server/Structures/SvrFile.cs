﻿using System;
using System.Windows.Forms;
using System.Text;
using System.IO;

namespace COServer
{
    //--- SvrFile Structure ---
    //Header -> Char[3]
    //ZeroFill -> Byte
    //Length -> UInt32
    //Name -> Char[]
    //ZeroFill -> Byte
    //Length -> UInt32
    //IpAddress -> Char[]
    //ZeroFill -> Byte
    //Port -> UInt16
    //-------------------------

    public class SvrInfo
    {
        private string m_Name;
        private string m_Ip;
        private ushort m_Port;

        /// <summary>
        /// Create a new object with the information that the file contains.
        /// </summary>
        public SvrInfo(string File)
        {
            try
            {
                FileStream FStream = new FileStream(File, FileMode.Open);
                BinaryReader Reader = new BinaryReader(FStream);

                string Header = Encoding.ASCII.GetString(Reader.ReadBytes(3));
                uint Length = 0;

                if (Header == "SVR")
                {
                    Reader.ReadByte();
                    Length = Reader.ReadUInt32();
                    m_Name = Encoding.ASCII.GetString(Reader.ReadBytes((int)Length));
                    Reader.ReadByte();
                    Length = Reader.ReadUInt32();
                    m_Ip = Encoding.ASCII.GetString(Reader.ReadBytes((int)Length));
                    Reader.ReadByte();
                    m_Port = Reader.ReadUInt16();
                }
                else
                    Program.WriteLine("SvrInfo::SvrInfo() -> Failed to read the file '" + File + "'");

                Reader.Close();
                FStream.Dispose();
            }
            catch { Program.WriteLine("SvrInfo::SvrInfo() -> Failed to read the file '" + File + "'"); }
        }

        /// <summary>
        /// Create a new object with the specified information.
        /// </summary>
        public SvrInfo(string Name, string IpAddress, ushort Port)
        {
            m_Name = Name;
            m_Ip = IpAddress;
            m_Port = Port;
        }

        public string Name { get { return this.m_Name; } }
        public string IpAddress { get { return this.m_Ip; } }
        public ushort Port { get { return this.m_Port; } }
    }

    public class SvrWriter
    {
        /// <summary>
        /// Write the specified object in a SVR File.
        /// </summary>
        public static void Write(SvrInfo Svr)
        {
            try
            {
                if (Svr != null)
                {
                    byte[] Bytes = new byte[16 + Svr.Name.Length + Svr.IpAddress.Length];
                    MemoryStream BStream = new MemoryStream(Bytes);
                    BinaryWriter BWriter = new BinaryWriter(BStream);
                    string Header = "SVR";

                    //Header
                    for (int x = 0; x < Header.Length; x++)
                        BWriter.Write((byte)Header[x]);

                    //ZeroFill
                    BWriter.Write((byte)0x00);

                    //Length
                    BWriter.Write((uint)Svr.Name.Length);

                    //Name
                    for (int x = 0; x < Svr.Name.Length; x++)
                        BWriter.Write((byte)Svr.Name[x]);

                    //ZeroFill
                    BWriter.Write((byte)0x00);

                    //Length
                    BWriter.Write((uint)Svr.IpAddress.Length);

                    //IpAddress
                    for (int x = 0; x < Svr.IpAddress.Length; x++)
                        BWriter.Write((byte)Svr.IpAddress[x]);

                    //ZeroFill
                    BWriter.Write((byte)0x00);

                    //Port
                    BWriter.Write((ushort)Svr.Port);

                    BWriter.Close();
                    File.WriteAllBytes(Application.StartupPath + @"\Servers\" + Svr.Name + ".svr", Bytes);
                }
            }
            catch (Exception Exc) { Program.WriteLine("SvrWriter::Write() -> Failed to write the file.\n" + Exc); }
        }
    }
}
