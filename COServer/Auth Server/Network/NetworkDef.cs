﻿using System;

namespace COServer.Network
{
    public partial class Msg
    {
        //Msg type define...
        public const ushort _MSG_NONE = 0;
        public const ushort _MSG_GENERAL = 10000;
        public const ushort _MSG_REGISTER = _MSG_GENERAL + 1;
        public const ushort _MSG_INTERACT = _MSG_GENERAL + 2;
        public const ushort _MSG_ANSWER = _MSG_GENERAL + 3;
    }
}