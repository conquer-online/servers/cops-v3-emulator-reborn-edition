﻿using System;
using System.IO;
using System.Text;
using KSocket.IPv4;
using INI_CORE_DLL;

namespace COServer.Network
{
    public class MsgRegister : Msg
    {
        //Define constant(s) here...
        const ushort _MSG_SIZE = 244;

        public byte[] Create(string Account, string Password, string Email, string Name, string Question, string Answer)
        {
            PacketWriter PWriter = new PacketWriter(new byte[_MSG_SIZE]);
            PWriter.WriteUInt16(_MSG_SIZE, 0);
            PWriter.WriteUInt16(_MSG_REGISTER, 2);
            PWriter.WriteString(Account, 4);
            PWriter.WriteString(Password, 20);
            PWriter.WriteString(Email, 84);
            PWriter.WriteString(Name, 124);
            PWriter.WriteString(Question, 164);
            PWriter.WriteString(Answer, 204);

            return PWriter.Flush();
        }

        public void Process(byte[] Data, ClientSocket Client)
        {
            try
            {
                string Account = Encoding.ASCII.GetString(Data, 4, 16).TrimEnd((char)0x00);
                string Password = Encoding.ASCII.GetString(Data, 20, 64).TrimEnd((char)0x00);
                string Email = Encoding.ASCII.GetString(Data, 84, 40).TrimEnd((char)0x00);
                string Name = Encoding.ASCII.GetString(Data, 124, 40).TrimEnd((char)0x00);
                string Question = Encoding.ASCII.GetString(Data, 164, 40).TrimEnd((char)0x00);
                string Answer = Encoding.ASCII.GetString(Data, 204, 40).TrimEnd((char)0x00);

                if (File.Exists(System.Windows.Forms.Application.StartupPath + "\\Accounts\\" + Account + ".acc"))
                {
                    Client.WinSock.Send(new CoEncryption().Encrypt(new MsgAnswer().Create(0x04)));
                    return;
                }

                Ini Writer = new Ini(System.Windows.Forms.Application.StartupPath + "\\Accounts\\" + Account + ".acc");
                Writer.WriteString("Account", "AccountID", Account);
                Writer.WriteString("Account", "Password", Password);
                Writer.WriteString("Account", "RealName", Name);
                Writer.WriteString("Account", "Email", Email);
                Writer.WriteString("Account", "Question", Question);
                Writer.WriteString("Account", "Answer", Answer);
                Writer.WriteString("Account", "LogonCount", "0");
                Writer.WriteString("Account", "Token", "NULL");

                Client.WinSock.Send(new CoEncryption().Encrypt(new MsgAnswer().Create(0x01)));
            }
            catch (Exception Exc) { Program.WriteLine(MsgError(Data, Exc)); }
        }
    }
}