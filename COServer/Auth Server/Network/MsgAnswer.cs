﻿using System;

namespace COServer.Network
{
    public class MsgAnswer : Msg
    {
        //Define constant(s) here...
        const ushort _MSG_SIZE = 8;

        const int _ACCOUNT_CREATED = 1;
        const int _PASSWORD_CHANGED = 2;
        const int _PASSWORD_RESTORED = 3;
        const int _ACCOUNT_EXIST = 4;
        const int _PASSWORD_INVALID = 5;
        const int _INFORMATION_INVALID = 6;

        public byte[] Create(uint Action)
        {
            PacketWriter PWriter = new PacketWriter(new byte[_MSG_SIZE]);
            PWriter.WriteUInt16(_MSG_SIZE, 0);
            PWriter.WriteUInt16(_MSG_ANSWER, 2);
            PWriter.WriteUInt32(Action, 4);

            return PWriter.Flush();
        }
    }
}