﻿using System;
using System.IO;
using System.Text;
using KSocket.IPv4;
using INI_CORE_DLL;

namespace COServer.Network
{
    public class MsgInteract : Msg
    {
        //Define constant(s) here...
        const ushort _MSG_SIZE = 210;

        const int _PASSWORD_CHANGE = 1;
        const int _PASSWORD_RESTORE = 2;

        public byte[] Create(string Account, string OldPassword, string NewPassword)
        {
            PacketWriter PWriter = new PacketWriter(new byte[_MSG_SIZE]);
            PWriter.WriteUInt16(_MSG_SIZE, 0);
            PWriter.WriteUInt16(_MSG_INTERACT, 2);
            PWriter.WriteUInt32(_PASSWORD_CHANGE, 4);
            PWriter.WriteString(Account, 8);
            PWriter.WriteString(OldPassword, 24);
            PWriter.WriteString(NewPassword, 88);

            return PWriter.Flush();
        }

        public byte[] Create(string Account, string NewPassword, string Email, string Question, string Answer)
        {
            PacketWriter PWriter = new PacketWriter(new byte[_MSG_SIZE]);
            PWriter.WriteUInt16(_MSG_SIZE, 0);
            PWriter.WriteUInt16(_MSG_INTERACT, 2);
            PWriter.WriteUInt32(_PASSWORD_RESTORE, 4);
            PWriter.WriteString(Account, 8);
            PWriter.WriteString(NewPassword, 24);
            PWriter.WriteString(Email, 88);
            PWriter.WriteString(Question, 128);
            PWriter.WriteString(Answer, 168);

            return PWriter.Flush();
        }

        public void Process(byte[] Data, ClientSocket Client)
        {
            try
            {
                switch (Data[0x04])
                {
                    case _PASSWORD_CHANGE:
                        {
                            string Account = Encoding.ASCII.GetString(Data, 8, 16).TrimEnd((char)0x00);
                            string OldPassword = Encoding.ASCII.GetString(Data, 24, 64).TrimEnd((char)0x00);
                            string NewPassword = Encoding.ASCII.GetString(Data, 88, 64).TrimEnd((char)0x00);

                            if (!File.Exists(System.Windows.Forms.Application.StartupPath + "\\Accounts\\" + Account + ".acc"))
                            {
                                Client.WinSock.Send(new CoEncryption().Encrypt(new MsgAnswer().Create(0x06)));
                                return;
                            }

                            Ini Reader = new Ini(System.Windows.Forms.Application.StartupPath + "\\Accounts\\" + Account + ".acc");
                            if (Reader.ReadValue("Account", "Password") == OldPassword)
                            {
                                Reader.WriteString("Account", "Password", NewPassword);
                                Client.WinSock.Send(new CoEncryption().Encrypt(new MsgAnswer().Create(0x02)));
                                return;
                            }

                            Client.WinSock.Send(new CoEncryption().Encrypt(new MsgAnswer().Create(0x05)));
                            break;
                        }
                    case _PASSWORD_RESTORE:
                        {
                            string Account = Encoding.ASCII.GetString(Data, 8, 16).TrimEnd((char)0x00);
                            string Password = Encoding.ASCII.GetString(Data, 24, 64).TrimEnd((char)0x00);
                            string Email = Encoding.ASCII.GetString(Data, 88, 40).TrimEnd((char)0x00);
                            string Question = Encoding.ASCII.GetString(Data, 128, 40).TrimEnd((char)0x00);
                            string Answer = Encoding.ASCII.GetString(Data, 168, 40).TrimEnd((char)0x00);

                            if (!File.Exists(System.Windows.Forms.Application.StartupPath + "\\Accounts\\" + Account + ".acc"))
                            {
                                Client.WinSock.Send(new CoEncryption().Encrypt(new MsgAnswer().Create(0x06)));
                                return;
                            }

                            Ini Reader = new Ini(System.Windows.Forms.Application.StartupPath + "\\Accounts\\" + Account + ".acc");
                            if (Reader.ReadValue("Account", "Email") == Email)
                                if (Reader.ReadValue("Account", "Question") == Question)
                                    if (Reader.ReadValue("Account", "Answer") == Answer)
                                    {
                                        Reader.WriteString("Account", "Password", Password);
                                        Client.WinSock.Send(new CoEncryption().Encrypt(new MsgAnswer().Create(0x03)));
                                        return;
                                    }

                            Client.WinSock.Send(new CoEncryption().Encrypt(new MsgAnswer().Create(0x06)));
                            break;
                        }
                    default:
                        {
                            Program.WriteLine("MsgInteract::Error -> Type[" + Data[0x04] + "] not implemented!");
                            break;
                        }
                }
            }
            catch (Exception Exc) { Program.WriteLine(MsgError(Data, Exc)); }
        }
    }
}