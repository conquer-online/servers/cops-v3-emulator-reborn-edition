using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("ScriptCreator")]
[assembly: AssemblyDescription("CptSky Script Creator")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("CptSky")]
[assembly: AssemblyProduct("ScriptCreator.exe")]
[assembly: AssemblyCopyright("Copyright © CptSky 2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("e6a09090-2c27-47d2-bd32-0731d8b1517e")]

[assembly: AssemblyVersion("1.0.0.53")]
[assembly: AssemblyFileVersion("1.0.0.53")]
