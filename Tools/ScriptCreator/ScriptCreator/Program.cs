﻿using System;

namespace ScriptCreator
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "CptSky - Script Creator [KCS]";
            if (args.Length < 1)
            {
                Console.WriteLine("You need to drag the file on the program's file!");
                Console.Read();
            }

            try
            {
                NPCScript Script = new NPCScript(args[0]);
                Script.Write(args[0].Replace(".ini", ".kcs"));
            }
            catch (Exception Exc) { Console.WriteLine(Exc); Console.Read(); }
        }
    }
}
