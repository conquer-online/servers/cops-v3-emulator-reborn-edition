using System;
using System.Runtime.InteropServices;
using System.Text;

namespace INI_CORE_DLL
{
    public class Ini
    {
        internal class Native
        {
            [DllImport("INI_CORE_DLL")]
            public static extern bool GetString(string File, string Section, string Key, StringBuilder BufString, int BufSize);

            [DllImport("INI_CORE_DLL")]
            public static extern sbyte GetSByte(string File, string Section, string Key);

            [DllImport("INI_CORE_DLL")]
            public static extern byte GetByte(string File, string Section, string Key);

            [DllImport("INI_CORE_DLL")]
            public static extern short GetInt16(string File, string Section, string Key);

            [DllImport("INI_CORE_DLL")]
            public static extern ushort GetUInt16(string File, string Section, string Key);

            [DllImport("INI_CORE_DLL")]
            public static extern int GetInt32(string File, string Section, string Key);

            [DllImport("INI_CORE_DLL")]
            public static extern uint GetUInt32(string File, string Section, string Key);

            [DllImport("INI_CORE_DLL")]
            public static extern double GetDouble(string File, string Section, string Key);

            [DllImport("INI_CORE_DLL")]
            public static extern float GetFloat(string File, string Section, string Key);
        }

        private string m_File;

        public Ini(string Path)
        {
            this.m_File = Path;
        }

        ~Ini()
        {
            m_File = null;
        }

        //Check if the Key exist
        public bool KeyExist(string Section, string Key)
        {
            try
            {
                const int BufferSize = 32;
                StringBuilder Value = new StringBuilder(BufferSize);
                Native.GetString(m_File, Section, Key, Value, BufferSize);

                if (Value.ToString() != null && Value.ToString() != "")
                    return true;

                return false;
            }
            catch { return false; }
        }

        //Read the sbyte of a key
        public sbyte ReadSByte(string Section, string Key)
        {
            return Native.GetSByte(m_File, Section, Key);
        }

        //Read the byte of a key
        public byte ReadByte(string Section, string Key)
        {
            return Native.GetByte(m_File, Section, Key);
        }

        //Read the short of a key
        public short ReadInt16(string Section, string Key)
        {
            return Native.GetInt16(m_File, Section, Key);
        }

        //Read the ushort of a key
        public ushort ReadUInt16(string Section, string Key)
        {
            return Native.GetUInt16(m_File, Section, Key);
        }

        //Read the int of a key
        public int ReadInt32(string Section, string Key)
        {
            return Native.GetInt32(m_File, Section, Key);
        }

        //Read the uint of a key
        public uint ReadUInt32(string Section, string Key)
        {
            return Native.GetUInt32(m_File, Section, Key);
        }

        //Read the long of a key
        public long ReadInt64(string Section, string Key)
        {
            try
            {
                const int BufferSize = 8192;
                StringBuilder Value = new StringBuilder(BufferSize);
                Native.GetString(m_File, Section, Key, Value, BufferSize);
                return long.Parse(Value.ToString());
            }
            catch { return 0; }
        }

        //Read the ulong of a key
        public ulong ReadUInt64(string Section, string Key)
        {
            try
            {
                const int BufferSize = 8192;
                StringBuilder Value = new StringBuilder(BufferSize);
                Native.GetString(m_File, Section, Key, Value, BufferSize);
                return ulong.Parse(Value.ToString());
            }
            catch { return 0; }
        }

        //Read the double of a key
        public double ReadDouble(string Section, string Key)
        {
            return Native.GetDouble(m_File, Section, Key);
        }

        //Read the float of a key
        public double ReadFloat(string Section, string Key)
        {
            return Native.GetFloat(m_File, Section, Key);
        }

        //Read the boolean of a key
        public bool ReadBoolean(string Section, string Key)
        {
            try
            {
                const int BufferSize = 8192;
                StringBuilder Value = new StringBuilder(BufferSize);
                Native.GetString(m_File, Section, Key, Value, BufferSize);
                return bool.Parse(Value.ToString());
            }
            catch { return false; }
        }

        //Read the value of a key
        public string ReadValue(string Section, string Key)
        {
            const int BufferSize = 8192;
            StringBuilder Value = new StringBuilder(BufferSize);
            Native.GetString(m_File, Section, Key, Value, BufferSize);
            return Value.ToString();
        }
    }
}
