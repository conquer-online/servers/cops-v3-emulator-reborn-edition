﻿using System;
using System.IO;
using System.Text;
using INI_CORE_DLL;

namespace ScriptCreator
{
    public class NPCScript
    {
        #region Structures
        public struct Header
        {
            public Int32 UniqId;
            public String Name;
            public Int16 Face;
        }

        public struct Binder
        {
            public Page[] Pages;
        }

        public struct Page
        {
            public Requirement[] Requirements;
            public Reward[] Rewards;
            public String Text;
            public Option[] Options;
        }

        public struct Requirement
        {
            public Byte Type;
            public Byte Operator;
            public Int32 IntValue;
            public String StrValue;
        }

        public struct Reward
        {
            public Byte Type;
            public Byte Operator;
            public Int32 IntValue;
            public String StrValue;
        }

        public struct Option
        {
            public Byte UniqId;
            public String Text;
        }
        #endregion
        #region Enumerations
        public enum Operator : byte
        {
            Addition = 0,           // +
            Subtraction = 1,        // -
            Multiplication = 2,     // *
            Divison = 3,            // /
            LessThan = 10,          // <
            GreaterThan = 11,       // >
            LessThanOrEqual = 12,   // <=
            GreaterThanOrEqual = 13,// >=
            Equal = 20,             // ==
            Inequal = 21,           // !=
        }

        public enum RequirementType : byte
        {
            CurHP = 0,
            MaxHP = 1,
            CurMP = 2,
            MaxMP = 3,
            Money = 4,
            Exp = 5,
            PKPoint = 6,
            Job = 7,
            StatP = 11,
            Model = 12,
            Level = 13,
            Spirit = 14,
            Vitality = 15,
            Strength = 16,
            Agility = 17,
            Hair = 27,
            CPs = 30,
            InvContains = 100,
            InvCount = 101
        }

        public enum RewardType : byte
        {
            CurHP = 0,
            MaxHP = 1,
            CurMP = 2,
            MaxMP = 3,
            Money = 4,
            Exp = 5,
            PKPoint = 6,
            Job = 7,
            StatP = 11,
            Model = 12,
            Level = 13,
            Spirit = 14,
            Vitality = 15,
            Strength = 16,
            Agility = 17,
            Hair = 27,
            CPs = 30,
            AddItem = 100,
            DelItem = 101,
            Teleport = 102
        }
        #endregion
        #region Private Members
        private Header m_Header;
        private Binder[] m_Binders;
        #endregion

        public NPCScript(string File)
        {
            try
            {
                m_Header = new Header();
                Console.Write("What is the NPC's UniqID? ");
                m_Header.UniqId = int.Parse(Console.ReadLine());
                Console.Write("What is the NPC's Name? ");
                m_Header.Name = Console.ReadLine();
                Console.Write("What is the NPC's Face? ");
                m_Header.Face = short.Parse(Console.ReadLine());
                Console.WriteLine("Please Wait...");

                Ini Reader = new Ini(File);
                for (byte i = 0; i < 255; i++)
                {
                    if (!Reader.KeyExist("Page" + i.ToString(), "Count"))
                    {
                        m_Binders = new Binder[i];
                        break;
                    }
                }

                for (byte i = 0; i < m_Binders.Length; i++)
                {
                    m_Binders[i].Pages = new Page[Reader.ReadByte("Page" + i.ToString(), "Count")];
                    for (byte x = 0; x < m_Binders[i].Pages.Length; x++)
                    {
                        Page Page = new Page();
                        string[] Options;

                        if (Reader.KeyExist("Page" + i.ToString(), "Req" + x.ToString()))
                        {
                            string ReqLine = Reader.ReadValue("Page" + i.ToString(), "Req" + x.ToString());
                            string[] Reqs = ReqLine.Split('&');
                            Page.Requirements = new Requirement[Reqs.Length];
                            for (int y = 0; y < Reqs.Length; y++)
                                Page.Requirements[y] = ConvertReq(Reqs[y]);
                        }
                        else
                            Page.Requirements = new Requirement[0];

                        if (Reader.KeyExist("Page" + i.ToString(), "Rew" + x.ToString()))
                        {
                            string RewLine = Reader.ReadValue("Page" + i.ToString(), "Rew" + x.ToString());
                            string[] Rews = RewLine.Split('&');
                            Page.Rewards = new Reward[Rews.Length];
                            for (int y = 0; y < Rews.Length; y++)
                                Page.Rewards[y] = ConvertRew(Rews[y]);
                        }
                        else
                            Page.Rewards = new Reward[0];

                        if (Reader.KeyExist("Page" + i.ToString(), "Text" + x.ToString()))
                            Page.Text = Reader.ReadValue("Page" + i.ToString(), "Text" + x.ToString());
                        else
                            Page.Text = null;

                        if (Reader.KeyExist("Page" + i.ToString(), "Opt" + x.ToString()))
                        {
                            Options = GetOptions(Reader.ReadValue("Page" + i.ToString(), "Opt" + x.ToString()));
                            byte OptC = 0;
                            foreach (string Option in Options)
                            {
                                if (Option != null)
                                    OptC++;
                            }
                            Page.Options = new Option[OptC];
                            byte Pos = 0;
                            for (int y = 0; y < Options.Length; y++)
                            {
                                if (Options[y] != null)
                                {
                                    Page.Options[Pos] = new Option();
                                    Page.Options[Pos].UniqId = (byte)y;
                                    Page.Options[Pos].Text = Options[y];
                                    Pos++;
                                }
                            }
                        }
                        else
                            Page.Options = new Option[0];
                        m_Binders[i].Pages[x] = Page;
                    }
                }
            }
            catch (Exception Exc) { Console.WriteLine(Exc.ToString()); Console.Read(); }
        }

        private Requirement ConvertReq(string Req)
        {
            Requirement Requirement = new Requirement();
            string[] ReqPart = Req.TrimStart(' ').Split(' ');
            if (ReqPart.Length >= 3)
            {
                #region Type
                switch (ReqPart[0])
                {
                    case "CurHP":
                        Requirement.Type = (byte)RequirementType.CurHP;
                        break;
                    case "MaxHP":
                        Requirement.Type = (byte)RequirementType.MaxHP;
                        break;
                    case "CurMP":
                        Requirement.Type = (byte)RequirementType.CurMP;
                        break;
                    case "MaxMP":
                        Requirement.Type = (byte)RequirementType.MaxMP;
                        break;
                    case "Money":
                        Requirement.Type = (byte)RequirementType.Money;
                        break;
                    case "Exp":
                        Requirement.Type = (byte)RequirementType.Exp;
                        break;
                    case "PKPoint":
                        Requirement.Type = (byte)RequirementType.PKPoint;
                        break;
                    case "Job":
                        Requirement.Type = (byte)RequirementType.Job;
                        break;
                    case "StatP":
                        Requirement.Type = (byte)RequirementType.StatP;
                        break;
                    case "Model":
                        Requirement.Type = (byte)RequirementType.Model;
                        break;
                    case "Level":
                        Requirement.Type = (byte)RequirementType.Level;
                        break;
                    case "Spirit":
                        Requirement.Type = (byte)RequirementType.Spirit;
                        break;
                    case "Vitality":
                        Requirement.Type = (byte)RequirementType.Vitality;
                        break;
                    case "Strength":
                        Requirement.Type = (byte)RequirementType.Strength;
                        break;
                    case "Agility":
                        Requirement.Type = (byte)RequirementType.Agility;
                        break;
                    case "Hair":
                        Requirement.Type = (byte)RequirementType.Hair;
                        break;
                    case "CPs":
                        Requirement.Type = (byte)RequirementType.CPs;
                        break;
                    case "InvContains":
                        Requirement.Type = (byte)RequirementType.InvContains;
                        break;
                    case "InvCount":
                        Requirement.Type = (byte)RequirementType.InvCount;
                        break;
                }
                #endregion
                #region Operator
                switch (ReqPart[1])
                {
                    case "<":
                        Requirement.Operator = (byte)Operator.LessThan;
                        break;
                    case ">":
                        Requirement.Operator = (byte)Operator.GreaterThan;
                        break;
                    case "<=":
                        Requirement.Operator = (byte)Operator.LessThanOrEqual;
                        break;
                    case ">=":
                        Requirement.Operator = (byte)Operator.GreaterThanOrEqual;
                        break;
                    case "==":
                        Requirement.Operator = (byte)Operator.Equal;
                        break;
                    case "!=":
                        Requirement.Operator = (byte)Operator.Inequal;
                        break;
                }
                #endregion
                #region Value
                if (!Int32.TryParse(ReqPart[2], out Requirement.IntValue))
                    Requirement.StrValue = ReqPart[2];
                #endregion
            }

            return Requirement;
        }

        private Reward ConvertRew(string Rew)
        {
            Reward Reward = new Reward();
            string[] RewPart = Rew.TrimStart(' ').Replace("]", "").Split('[');
            if (RewPart.Length >= 2)
            {
                #region Type
                switch (RewPart[0])
                {
                    case "CurHP":
                        Reward.Type = (byte)RewardType.CurHP;
                        break;
                    case "MaxHP":
                        Reward.Type = (byte)RewardType.MaxHP;
                        break;
                    case "CurMP":
                        Reward.Type = (byte)RewardType.CurMP;
                        break;
                    case "MaxMP":
                        Reward.Type = (byte)RewardType.MaxMP;
                        break;
                    case "Money":
                        Reward.Type = (byte)RewardType.Money;
                        break;
                    case "Exp":
                        Reward.Type = (byte)RewardType.Exp;
                        break;
                    case "PKPoint":
                        Reward.Type = (byte)RewardType.PKPoint;
                        break;
                    case "Job":
                        Reward.Type = (byte)RewardType.Job;
                        break;
                    case "StatP":
                        Reward.Type = (byte)RewardType.StatP;
                        break;
                    case "Model":
                        Reward.Type = (byte)RewardType.Model;
                        break;
                    case "Level":
                        Reward.Type = (byte)RewardType.Level;
                        break;
                    case "Spirit":
                        Reward.Type = (byte)RewardType.Spirit;
                        break;
                    case "Vitality":
                        Reward.Type = (byte)RewardType.Vitality;
                        break;
                    case "Strength":
                        Reward.Type = (byte)RewardType.Strength;
                        break;
                    case "Agility":
                        Reward.Type = (byte)RewardType.Agility;
                        break;
                    case "Hair":
                        Reward.Type = (byte)RewardType.Hair;
                        break;
                    case "CPs":
                        Reward.Type = (byte)RewardType.CPs;
                        break;
                    case "AddItem":
                        Reward.Type = (byte)RewardType.AddItem;
                        break;
                    case "DelItem":
                        Reward.Type = (byte)RewardType.DelItem;
                        break;
                    case "Teleport":
                        Reward.Type = (byte)RewardType.Teleport;
                        break;
                }
                #endregion
                #region Operator
                if (RewPart[1].StartsWith("+"))
                {
                    Reward.Operator = (byte)Operator.Addition;
                    RewPart[1] = RewPart[1].Replace("+", "");
                }
                else if (RewPart[1].StartsWith("-"))
                {
                    Reward.Operator = (byte)Operator.Subtraction;
                    RewPart[1] = RewPart[1].Replace("-", "");
                }
                else if (RewPart[1].StartsWith("*"))
                {
                    Reward.Operator = (byte)Operator.Multiplication;
                    RewPart[1] = RewPart[1].Replace("*", "");
                }
                else if (RewPart[1].StartsWith("/"))
                {
                    Reward.Operator = (byte)Operator.Divison;
                    RewPart[1] = RewPart[1].Replace("/", "");
                }
                else
                    Reward.Operator = (byte)0xFF;
                #endregion
                #region Value
                if (!Int32.TryParse(RewPart[1], out Reward.IntValue))
                    Reward.StrValue = RewPart[1];
                #endregion
            }

            return Reward;
        }

        private string[] GetOptions(string AllOptions)
        {
            try
            {
                string[] Options = new string[0x100];
                string[] OptInfo = AllOptions.Split(':');

                for (byte i = 0; i < OptInfo.Length; i++)
                {
                    string[] Opt = OptInfo[i].Split('-');
                    if (Opt.Length > 2)
                    {
                        for (byte x = 0; x < (Opt.Length - 1); x++)
                            Options[byte.Parse(Opt[Opt.Length - 1])] += Opt[x].TrimStart(' ');
                    }
                    else
                        Options[byte.Parse(Opt[1])] = Opt[0].TrimStart(' ');
                }
                return Options;
            }
            catch { return new string[0x100]; }
        }

        ~NPCScript()
        {
            m_Binders = null;
        }

        public void Write(string File)
        {
            try
            {
                FileStream FStream = new FileStream(File, FileMode.Create, FileAccess.Write);
                BinaryWriter BWriter = new BinaryWriter(FStream);

                //Identifier
                BWriter.Write((Byte)'K'); //Logik.
                BWriter.Write((Byte)'C'); //Conquer
                BWriter.Write((Byte)'S'); //Script

                //Header
                BWriter.Write(m_Header.UniqId);
                for (SByte i = 0; i < 32; i++)
                {
                    if (i < m_Header.Name.Length)
                        BWriter.Write((Byte)m_Header.Name[i]);
                    else
                        BWriter.Write((Byte)0x00);
                }
                BWriter.Write(m_Header.Face);

                //Binders
                BWriter.Write((Byte)m_Binders.Length);
                foreach (Binder Binder in m_Binders)
                {
                    BWriter.Write((Byte)Binder.Pages.Length);
                    foreach (Page Page in Binder.Pages)
                    {
                        BWriter.Write((Byte)Page.Requirements.Length);
                        BWriter.Write((Byte)Page.Rewards.Length);
                        BWriter.Write((Byte)Page.Options.Length);

                        foreach (Requirement Requirement in Page.Requirements)
                        {
                            BWriter.Write(Requirement.Type);
                            BWriter.Write(Requirement.Operator);
                            BWriter.Write(Requirement.StrValue != null ? true : false);
                            if (Requirement.StrValue != null)
                            {
                                for (SByte i = 0; i < 32; i++)
                                {
                                    if (i < Requirement.StrValue.Length)
                                        BWriter.Write((Byte)Requirement.StrValue[i]);
                                    else
                                        BWriter.Write((Byte)0x00);
                                }
                            }
                            else
                                BWriter.Write(Requirement.IntValue);
                        }

                        foreach (Reward Reward in Page.Rewards)
                        {
                            BWriter.Write(Reward.Type);
                            BWriter.Write(Reward.Operator);
                            BWriter.Write(Reward.StrValue != null ? true : false);
                            if (Reward.StrValue != null)
                            {
                                for (SByte i = 0; i < 32; i++)
                                {
                                    if (i < Reward.StrValue.Length)
                                        BWriter.Write((Byte)Reward.StrValue[i]);
                                    else
                                        BWriter.Write((Byte)0x00);
                                }
                            }
                            else
                                BWriter.Write(Reward.IntValue);
                        }

                        BWriter.Write(Page.Text != null ? true : false);
                        if (Page.Text != null)
                        {
                            BWriter.Write((UInt16)Page.Text.Length);
                            for (UInt16 i = 0; i < Page.Text.Length; i++)
                                BWriter.Write((Byte)Page.Text[i]);
                        }

                        foreach (Option Option in Page.Options)
                        {
                            BWriter.Write(Option.UniqId);
                            BWriter.Write((Byte)Option.Text.Length);
                            for (Byte i = 0; i < Option.Text.Length; i++)
                                BWriter.Write((Byte)Option.Text[i]);
                        }
                    }
                }

                BWriter.Close();
                FStream.Close();
            }
            catch (Exception Exc) { Console.WriteLine(Exc.ToString()); Console.Read(); }
        }
    }
}