# COPS v3 Emulator - Reborn Edition

Copyright (C) 2010 CptSky <br />
https://gitlab.com/conquer-online/servers/cops/cops-v3-emulator-reborn-edition

## Overview
COPS v3 Emulator - Reborn Edition is a LOTF-based emulator targeting the version 5016 (1056 in French) of Conquer Online 2.0. The emulator is highly customized and is far from its original LOTF-base.

The emulator was developed between December 2009 and June 13th, 2010 for the COPS v3 - Reborn Edition private server. The emulator was developed following the failure to develop COPS v4 Emulator, a custom emulator targeting the version 4330, in a reasonable amount of time to be used live.

**The emulator shouldn't be used as a base for any new project.**

## Supported systems

The emulator was developed using Visual Studio 2008 and targets the .NET Framework 2.0. The emulator requires Microsoft Windows (32-bit).